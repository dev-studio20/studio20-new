<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelPaymentDetail extends Model

{

    protected $table = 'model_payment_details';

    protected $fillable = [
        'model_id',
        'idn',
        'iban',
        'bank',
        'bank_swift',
        'bank_address',
        'currency'
    ];

}
