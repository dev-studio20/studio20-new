<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelShifts extends Model

{

    protected $table = 'model_shifts';

    protected $guarded = [];

}
