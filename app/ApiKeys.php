<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APIKeys extends Model
{

    protected $table = 'api_keys';

    protected $fillable = [
        'name',
        'api_key',
        'bearer'
    ];
}
