<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsSalesPeriod extends Model
{
    protected $table = 'stats_sales_period';

    protected $fillable = [
        'studio_id',
        'name',
        'earnings',
        'worktime',
        'worktimeneeded',
        'from_date',
        'to_date'
    ];
}
