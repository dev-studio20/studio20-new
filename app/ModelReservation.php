<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelReservation extends Model

{

    protected $table = 'model_reservations';

    protected $guarded = [];


    public function model(){
        return $this->hasOne('App\Models', 'id', 'model_id');
    }

    public function studios(){
        return $this->hasOne('App\Studio', 'id', 'studio_id');
    }

}
