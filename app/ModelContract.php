<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelContract extends Model

{

    protected $table = 'model_contracts';

    protected $fillable = [
        'model_id',
        'pic1',
        'pic1_status',
        'reason_pic1',
        'pic2',
        'pic2_status',
        'reason_pic2',
        'pic3',
        'pic3_status',
        'reason_pic3',
        'reason',
        'contractsigned',
        'signed_at',
    ];

}
