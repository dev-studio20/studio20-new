<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelSale extends Model

{

    protected $table = 'model_sales';

    protected $fillable = ['model_id', 'studio_id', 'year', 'month', 'day', 'sum', 'hours', 'period'];


    public function model(){
        return $this->hasOne('App\Models', 'id', 'model_id');
    }

}
