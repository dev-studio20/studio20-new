<?php

namespace App;

use App\Notifications\AdminResetPasswordNotification;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    protected $guard = 'admin';

    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'studio_id'
    ];

    protected $hidden = [
        //'password',
        'remember_token',
    ];


    public function role(){
        return $this->hasOne('App\AdminRole', 'id', 'role_id');
    }

    public function studio(){
        return $this->hasOne('App\Studio', 'id', 'studio_id');
    }


    public function getRole()
    {
        return Auth::guard('admin')->user()->role_id;
    }

}