<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelAddress extends Model

{

    protected $table = 'model_address';

    protected $guarded = [];

}
