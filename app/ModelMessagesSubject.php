<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelMessagesSubject extends Model

{

    protected $table = 'model_messages_subject';

    protected $guarded = [];


    public function body(){
        return $this->hasMany('App\ModelMessagesBody', 'belongs_to')->orderBy('created_at', 'ASC');
    }

}
