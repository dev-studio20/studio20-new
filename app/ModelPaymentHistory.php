<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelPaymentHistory extends Model

{

    protected $table = 'model_payment_history';

    protected $guarded = [];

    public function model(){
        return $this->hasOne('App\Models', 'id', 'model_id');
    }

    public function studio(){
        return $this->hasOne('App\Studio', 'id','studio_id');
    }


    public function periodName(){
        return $this->hasOne('App\PeriodName', 'id', 'period');
    }

}
