<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class StudioSchedule extends Model

{

    protected $table = 'studio_schedule';

    protected $guarded = [];

}
