<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelSocial extends Model

{

    protected $table = 'model_socials';

    protected $fillable = [
        "artistic_email",
        "artistic_password",
        "twitter_profile",
        "twitter_post",
        "twitter_account",
        "instagram_profile",
        "personal_site"
    ];

}
