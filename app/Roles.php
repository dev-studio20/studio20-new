<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    protected $table = 'admin_roles';

    protected $fillable = [
        'name'
    ];
}
