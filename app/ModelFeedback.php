<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelFeedback extends Model

{

    protected $table = 'model_feedback';

    protected $guarded = [];

}
