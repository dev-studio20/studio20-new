<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelKeys extends Model

{

    protected $table = 'model_api_keys';

    protected $primaryKey = 'model_id';

    protected $fillable = [
        'model_id',
        'key_id'
    ];

    public function apiKeys(){
        return $this->hasOne('App\ApiKeys', 'key_id');
    }

}
