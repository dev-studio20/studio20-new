<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelAddress;
use App\Sync\Oldmodels;
use App\Sync\Syncid;
use App\State;
use App\City;
use App\Country;

class SyncJobAddress implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelAddress::query()->truncate();
        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id){

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelAddress = new ModelAddress;
            $modelAddress->model_id = $my_id;
            $modelAddress->street = $res->Address;
            $address = $this->findCountryStateCityId($res->City, $res->State, $res->Country);
            $modelAddress->city_id = $address['city_id'];
            $modelAddress->state_id = $address['state_id'];
            $modelAddress->country_id = $address['country_id'];

            $modelAddress->save();
        }

    }

    public function findCountryStateCityId($city, $state, $country){

        $country_res = Country::where('name', $country)->first();
        if ($country_res){
            $cid = $country_res->id;
            $state_res = State::where('name', $state)->where('country_id', $cid)->first();
            if ($state_res){
                $sid = $state_res->id;
                $city_res = City::where('name', $city)->where('state_id', $sid)->first();
                if($city_res) {
                    return ["city_id" => $city_res->id, "state_id" => $state_res->id, "country_id" => $country_res->id];
                }

            }

        }

        return null;

    }
}
