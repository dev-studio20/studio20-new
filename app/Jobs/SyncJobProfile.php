<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelProfile;
use App\Sync\Oldmodels;
use App\Sync\Syncid;

class SyncJobProfile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelProfile::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id) {

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelProfile = new ModelProfile;
            $modelProfile->model_id = $my_id;
            $modelProfile->first_name = $res->Name;
            $modelProfile->last_name = $res->Lname;
            $modelProfile->phonenumber = $res->phonenumber;

            $birth = $res->Birthdate;
            $b = explode('.',$birth);
            $new = $b[2]."-".$b[1]."-".$b[0];
            $modelProfile->birthdate = $new;
            //$modelProfile->birthdate = $res->Birthdate;

            $modelProfile->sex = $res->Sex;
            $modelProfile->last_status = $res->last_status;
            $modelProfile->last_update = $res->updated_at;
            $modelProfile->first_login = $res->first_login;

            $modelProfile->save();
        }
    }


}