<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models;

class GetSalesAllModels implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $day = '2019-12-30';

        $models = Models::where('status',1)->limit(50)->get();

        foreach($models as $model){
            //should be with dispatch job
            GetSalesByDay::dispatch($model->modelname, $day, 'model');
            //$this->getSalesDay($modelname, $day);
            //sleep(1);
        }

        //return 'done';
    }

}
