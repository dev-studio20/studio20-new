<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelSocial;
use App\Sync\Oldmodels;
use App\Sync\Syncid;

class SyncJobSocial implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelSocial::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id) {

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelSocial = new ModelSocial;
            $modelSocial->model_id = $my_id;
            $modelSocial->brand = $res->brand;
            $modelSocial->instagram_profile = $res->instagram_profile;
            $modelSocial->instagram_posts = $res->instagram_posts;
            $modelSocial->artistic_email = $res->artistic_email;
            $modelSocial->artistic_password = $res->artistic_password;
            $modelSocial->twitter_account = $res->twitter_account;
            $modelSocial->twitter_campaigne_active = $res->twitter_campaign_active;
            $modelSocial->twitter_profile = $res->twitter_profile;
            $modelSocial->twitter_post = $res->twitter_posts;
            $modelSocial->priority = $res->priority;

            $modelSocial->save();
        }

    }
}
