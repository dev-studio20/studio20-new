<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelReservation;
use App\Sync\Oldreservations;
use App\Sync\Syncid;

class SyncJobReservation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelReservation::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id) {

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldreservations::where('modelid', $old_id)->first();

            if($res){
                $modelReservations = new ModelReservation;
                $modelReservations->model_id = $my_id;

                $month_days = explode('-',$res->month);
                $month = $month_days[0];
                $year = $month_days[1];

                $modelReservations->week = 1;
                $modelReservations->month = $month;
                $modelReservations->year = $year;

                $modelReservations->days = $res->days;

                $hour = explode(' ', $res->hour);
                $modelReservations->hour = $hour[0];

                $modelReservations->status = $res->status;
                $modelReservations->rr = $res->RR;
                $modelReservations->move = $res->MOVE;

                $modelReservations->save();
            }

        }

    }
}
