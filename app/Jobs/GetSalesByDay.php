<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Events\SalesNotifications;
use App\Events\SalesModelNotifications;
use App\Models;
use Carbon\Carbon;
use DB;

class GetSalesByDay implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $modelname = null;

    private $day = null;

    private $switch = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($modelname, $day, $switch)
    {
        $this->modelname = $modelname;
        $this->day = $day;
        $this->switch = $switch;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getSalesDay($this->modelname, $this->day, $this->switch);
    }

    public function getSalesDay($modelname, $day, $switch){

        $fromDate = formatDate($day);
        $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
        $toDate = formatDate($nextday);

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=$fromDate&toDate=$toDate&screenNames[]=$modelname&reports[]=general-overview";

        $model = Models::where('modelname', $modelname)->first();

        $auth = getAuth($model);

        $earnings = 0;
        $workTime = 0;

        if ($auth) {

            $res = curlAuth($auth, $url);

            if (array_key_exists('data', $res)){
                $data = $res['data'][0];

                if(array_key_exists('screenName', $data)){
                    $total =  $data['total'];
                    if(array_key_exists('earnings', $total)){
                        $earn = $total['earnings'];
                        if(array_key_exists('value', $earn)){
                            $earnings = $earn['value'];
                            $earnings = number_format(round($earnings,2), 2, '.', '');
                        }
                    }

                    if(array_key_exists('workTime', $total)){
                        $work = $total['workTime'];
                        if(array_key_exists('value', $work)){
                            $workTime = $work['value'];
                        }
                    }
                }
            }

            $response = [
                "screenName" => $modelname,
                "day" => $day,
                "earnings" => $earnings,
                "workTime" => $workTime,
                "success" => true
            ];


        } else $response = ['success' => false, 'error' => 'api key error..'];

        $resp = response()->json($response);

        DB::table('test_log')->insert(['screenName' => $modelname, 'earnings' => $earnings, 'workTime' => $workTime, 'day' => $day]);

//        switch ($switch) {
//            case 'period':
//                event(new SalesNotifications($resp));
//                break;
//            case 'model':
//                event(new SalesModelNotifications($resp));
//                break;
//            default:
//                event(new SalesNotifications($resp));
//        }

    }
}
