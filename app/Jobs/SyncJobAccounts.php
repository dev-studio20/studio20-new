<?php

namespace App\Jobs;

use App\Sync\Syncid;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelAccount;
use App\Sync\Oldmodels;

class SyncJobAccounts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table accounts
        ModelAccount::query()->truncate();
        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id){

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelAccount = new ModelAccount;

            $modelAccount->model_id = $my_id;
            $modelAccount->type = $res->acctype;
            $modelAccount->company_name = $res->companyname;
            $modelAccount->company_administrator = $res->companyadministrator;
            $modelAccount->company_address = $res->companyaddress;
            $modelAccount->company_vat = $res->companyvat;
            $modelAccount->company_regcode = $res->companyregcode;

            $modelAccount->save();
        }

    }
}
