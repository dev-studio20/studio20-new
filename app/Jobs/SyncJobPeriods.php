<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelPeriod;
use App\Sync\Olderiods;
use App\Sync\Syncid;

class SyncJobPeriods implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelPeriod::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id) {

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Olderiods::where('modelid', $old_id)->get();

            foreach($res as $period){

                $modelPeriod = new ModelPeriod;
                $modelPeriod->model_id = $my_id;
                $modelPeriod->period = convertPeriodNameToValue($period->period);
                $modelPeriod->year = $period->year;
                //$modelPeriod->number = getModelPereeiosd...;
                $modelPeriod->total_amount = $period->totalamount;
                $modelPeriod->total_amount_jasmin = $period->totalamount_jasmine;
                $modelPeriod->hours = convertHoursToSeconds($period->hours);

                $modelPeriod->save();

            }
        }
    }
}
