<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models;
use App\ModelStatus;
use App\ModelReservation;
use App\Mail\MailtrapExample;
use Mail;
use DB;
use App\Sync\Olduser;
use App\Sync\Oldmodels;
use App\Sync\Hmodels;
use App\Sync\Husers;

class ProcessSyncTables implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    //protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->user = $user;

    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(){


        $old_models = Hmodels::all();
        //truncate old_model and insert new data
        Oldmodels::query()->truncate();
        foreach($old_models as $old_model){
            $newmodels = Oldmodels::create($old_model->toArray());
        }

        $old_users = Husers::all();
        //truncate old_users and insert new data
        Olduser::query()->truncate();
        foreach($old_users as $old_user){
            $newusers = Olduser::create($old_user->toArray());
        }


    }

    public function handle2(){
        $data = "XX - ".$this->user->id." - XX";
        DB::table('test_log')->insert(["data" => $data]);
    }

    public function handle3()
    {

        //DB::table('test_log')->insert(["data" => "xx"]);
        //import from hetzner

            //$res = DB::table('old_models')->where('id', $user->id)->select('Modelname', 'id')->first();
            $res = DB::table('old_models')->where('id', $this->user->id)->first();
            //if ($res){
            if (false){
                //insert into my models table
                $model = new Models;
                $model->modelname = $res->Modelname;
                //should check if studio is 0
                $model->studio = $res->studios_id;
                $model->email = $res->Email;
                $model->email_verified_at = $this->user->email_verified_at;
                $model->phone_verified_at = $this->user->email_verified_at;
                $model->password = $this->user->password;
                $model->password_text = $res->Pass;

                //get status id from status table
                $status = ModelStatus::where('name', $res->Status)->first();
                $model->status = $status->id;

                //couple account (0,1)
                $model->couple = $res->coupleacc;

                $model->remember_token = $this->user->remember_token;

                $model->created_at = $this->user->created_at;

                $model->save();

                $model_id = $model->id;

                //model_ accounts, address, contracts, payment_details, profiles, reservations, sales, socials
                // insert


            //reservations
                if(true){
                    //import from hetzner
                    //(should delete dates from 2018)
                    $reservations = DB::table('old_reservations')->where('modelid', $user->id)->get();
                    foreach($reservations as $res){
                        //if exist find the coresp id in our models and get the id
                        //insert in new reservation table data mod

                        $arr = explode('-', $res->month);
                        $m = $arr[0];
                        $y = $arr[1];

                        if( ($res->days) && (strlen($res->days) > 0) ){
                            $arDay = explode(',', $res->days);

                            foreach($arDay as $d){

                                //get days of week.. bla bla bla

                                $record = $this->createArray($d, $m, $y, $model_id, $res->days, $res->hour, $res->status, $res->RR, $res->MOVE);

                                //dd($record);

                                $modelReservation = new ModelReservation($record);

                                //dd($modelReservation);
                                $modelReservation->save();
                            }
                        }
                    }

                    //else echo model id

                }





        }
    }


    public function createArray($d, $m, $y, $id, $days, $hour, $status, $RR, $MOVE){

        $m_id = $id;
        $m_week = $this->findWeek($d, $m, $y);
        $m_month = $m;
        $m_year = $y;
        $m_days = $this->getDays($days);
        $m_hour = $hour;
        $m_status = $status;
        $m_rr = $RR;
        $m_move = $MOVE;

        return [
            "model_id" => $m_id,
            "week" => $m_week,
            "month" => $m_month,
            "year" => $m_year,
            "days" => $m_days,
            "hour" => $m_hour,
            "status" => $m_status,
            "rr" => $m_rr,
            "move" => $m_move,
        ];

    }

    public function getDays($days){

        return $days;
    }

    public function findWeek($d, $m, $y){

        return 23;
    }



}
