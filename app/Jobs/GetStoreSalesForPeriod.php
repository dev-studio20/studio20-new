<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;
use App\ModelSale;
use App\ModelPeriod;

class GetStoreSalesForPeriod implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $timeout = 320;

    private $models;
    private $key;
    private $year;
    private $period;

    public function __construct($models, $key, $year, $period)
    {
        $this->models = $models;
        $this->key = $key;
        $this->year = $year;
        $this->period = $period;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $days = getArrayDaysPeriod($this->year, $this->period);

        foreach($days as $day){
            //should be with dispatch job
            //GetSalesByDay::dispatch($this->modelname, $day, 'period');

            $this->getSalesDay($this->models, $this->key, $day);
            //sleep(1);
        }
    }

    public function getSalesDay($models, $key, $day){

        $fromDate = formatDate($day);
        $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
        $toDate = formatDate($nextday);

        $screenNames = null;
        foreach($models as $model){
            $screenNames .= '&screenNames[]=' . $model;
        }

        $reports = "&reports[]=general-overview&reports[]=earnings-overview&reports[]=working-time";
        $reports = "&reports[]=general-overview&reports[]=working-time";

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$fromDate."&toDate=".$toDate.$screenNames.$reports;
        $res = curlAuth($key, $url);

        if (array_key_exists('data', $res)){
            $dataModels = $res['data'];

            foreach($dataModels as $dataModel){

                $modelSalesAndTime = $this->processData($dataModel);
                $this->storeData($modelSalesAndTime, $day);
            }
        }
    }

    public function processData($data){
        $earnings = 0;
        $workTime = 0;
        $modelname = null;

        if(array_key_exists('screenName', $data)){
            $modelname = $data['screenName'];
            $total =  $data['total'];
            if(array_key_exists('earnings', $total)){
                $earn = $total['earnings'];
                if(array_key_exists('value', $earn)){
                    $earnings = $earn['value'];
                    $earnings = number_format(round($earnings,2), 2, '.', '');
                }
            }

            $workingTime =  $data['workingTime'];
            //$totaltime = $restotaltime['vip_show']['value'] + $restotaltime['pre_vip_show']['value'] + $restotaltime['private']['value'] + $restotaltime['free']['value'];
            if(array_key_exists('vip_show', $workingTime)) $workTime += $workingTime['vip_show']['value'];
            if(array_key_exists('pre_vip_show', $workingTime)) $workTime += $workingTime['pre_vip_show']['value'];
            if(array_key_exists('private', $workingTime)) $workTime += $workingTime['private']['value'];
            if(array_key_exists('free', $workingTime)) $workTime += $workingTime['free']['value'];

        }
        return ["modelname" => $modelname, "earnings" => $earnings, "worktime" => $workTime ];
    }

    public function storeData($data, $day){
        //["modelname" => $modelname, "earnings" => $earnings, "worktime" => $workTime ];
        if ($data) {
            $sum = $data['earnings'];
            $time = $data['worktime'];
            $modelname = $data['modelname'];
            $model_id = modelId($modelname);

            $sale = $this->saveSale($model_id, $day, $sum, $time);

            $period = $this->updatePeriod($sale);

            //dd([$sale, $period]);
            //DB::table('test_log')->insert(['screenName' => $modelname, 'earnings' => $earnings, 'workTime' => $workTime, 'day' => $day]);

        }
    }

    public function updatePeriod(ModelSale $sale):ModelPeriod{

        $periodid = $sale->period;
        $year = $sale->year;
        $modelid = $sale->model_id;

        $periodsum = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('sum');
        $periodhours = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('hours');

        $period = ModelPeriod::updateOrCreate(
            ['model_id' => $modelid, 'period' => $periodid, 'year' => $year],
            ['total_amount' => $periodsum, 'total_amount_jasmin' => $periodsum, 'hours' => $periodhours]
        );

        return $period;

    }

    public function saveSale($modelid, $day, $sum, $hours):ModelSale{

        $arD = explode('-', $day);
        $y = $arD[0];
        $m = $arD[1];
        $d = $arD[2];
        $p = ($d < 16) ? 1 : 0;
        $period = ($m * 2) - $p;

        $sale = ModelSale::updateOrCreate(
            ['model_id' => $modelid, 'year' => $y, 'month' => $m, 'day' => $d, 'period' => $period],
            ['sum' => $sum, 'hours' => $hours]
        );

        return $sale;

    }

}
