<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models;
use Carbon\Carbon;
use App\Events\SalesNotifications;
use DB;

class GetSales implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $studio = null;

    private $year = null;

    private $period = null;

    public function __construct($studio, $year, $period)
    {
        $this->studio = $studio;
        $this->year = $year;
        $this->period = $period;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cond = ($this->studio) ? '=' : '!=';

        //get models from studio, if 0 get all -> approved
        $models = Models::with('key')->where('studio', $cond, $this->studio)->where('status',1)->limit(10)->get()->pluck('modelname');

        foreach($models as $model){
            $this->getSalesPeriod($model);
        }


    }

    public function getSalesPeriod($model){

        $days = getArrayDaysPeriod($this->year, $this->period);

        foreach($days as $day){
            //should be with dispatch job
            //GetSalesByDay::dispatch($this->modelname, $day, 'period');

            $this->getSalesDay($model, $day);
            //sleep(1);
        }

        //return 'done';

    }

    public function getSalesDay($modelname, $day){

        $fromDate = formatDate($day);
        $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
        $toDate = formatDate($nextday);

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=$fromDate&toDate=$toDate&screenNames[]=$modelname&reports[]=general-overview";

        $model = Models::where('modelname', $modelname)->first();

        $auth = getAuth($model);

        $earnings = 0;
        $workTime = 0;

        if ($auth) {

            $res = curlAuth($auth, $url);

            if (array_key_exists('data', $res)){
                $data = $res['data'][0];

                if(array_key_exists('screenName', $data)){
                    $total =  $data['total'];
                    if(array_key_exists('earnings', $total)){
                        $earn = $total['earnings'];
                        if(array_key_exists('value', $earn)){
                            $earnings = $earn['value'];
                            $earnings = number_format(round($earnings,2), 2, '.', '');
                        }
                    }

                    if(array_key_exists('workTime', $total)){
                        $work = $total['workTime'];
                        if(array_key_exists('value', $work)){
                            $workTime = $work['value'];
                        }
                    }
                }
            }

            $response = ['screenName' => $modelname, 'earnings' => $earnings, 'workTime' => $workTime, 'day' => $day];
            DB::table('test_log')->insert(['screenName' => $modelname, 'earnings' => $earnings, 'workTime' => $workTime, 'day' => $day]);

        } else $response = null;

        //$resp = response()->json($response);

    }

}
