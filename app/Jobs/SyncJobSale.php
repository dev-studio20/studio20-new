<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelSale;
use App\Sync\Oldsales;
use App\Sync\Syncid;

class SyncJobSale implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        //ModelSale::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id) {

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $ress = Oldsales::where('modelid', $old_id)->where('pass', 0)->get();
            foreach($ress as $res){
                if ($res){
                    $modelSale = new ModelSale;

                    $modelSale->model_id = $my_id;

                    $modelSale->date = $res->date;

                    $modelSale->sum = $res->sum;

                    $modelSale->hours = convertHoursToSeconds($res->hours);

                    $modelSale->save();

                    Oldsales::where('id', $res->id)->update(['pass' => 1]);
                }
            }
        }
    }

}
