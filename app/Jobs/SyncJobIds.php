<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Sync\Syncid;
use App\Sync\Olduser;
use App\Sync\Oldmodels;
use App\Models;
use App\ModelStatus;

class SyncJobIds implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate sync_id
        Syncid::query()->truncate();
        $old_models = Oldmodels::all();

        //truncate models
        Models::query()->truncate();

        foreach($old_models as $old_model){
            //serach in old users
            $old_id =  $old_model->id;
            $userModel = Olduser::find($old_id);
            //if id found insert into models and into sync_id with my_id, old_id
            if($userModel){

                $model = new Models;
                $model->modelname = $old_model->Modelname;
                //should check if studio is 0
                $model->studio = $old_model->studios_id;
                $model->email = $old_model->Email;
                $model->email_verified_at = $userModel->email_verified_at;
                $model->phone_verified_at = $userModel->email_verified_at;
                $model->password = $userModel->password;
                $model->password_text = $old_model->Pass;

                //get status id from status table
                $status = ModelStatus::where('name', $old_model->Status)->first();
                $model->status = $status->id;

                //couple account (0,1)
                $model->couple = $old_model->coupleacc;

                $model->remember_token = $userModel->remember_token;

                $model->created_at = $userModel->created_at;

                $model->save();

                $sync = new Syncid;
                $sync->my_id = $model->id;
                $sync->old_id = $userModel->id;
                $sync->save();

            }
        }



        //dd($old_users);
    }
}
