<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models;
use Carbon\Carbon;
use App\ModelSignature;
use App\ModelContract;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use File;
use DB;
use App\Events\ContractGenNotifications;
use App\CustomClass\SendNotification;
use App\Events\AccountingNotifications;

class GenerateContract implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 500;

    private $model_id = null;


    public function __construct($model_id)
    {
        $this->model_id = $model_id;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

            //ini_set('max_execution_time', 300); //5 minutes

            $id = $this->model_id;

            $mymodel = Models::with('profile','address','paymentDetails','account')->findOrFail($id);

            $semnaturaManager = DB::table('signature_m')->find(1)->data;

            $id = $mymodel->id;
            $name = $mymodel->profile->first_name;
            $lname = $mymodel->profile->last_name;
            $email = $mymodel->email;
            $country = findCountry($mymodel->address->country_id);
            $state = findState($mymodel->address->state_id);
            $city = findCity($mymodel->address->city_id);
            $address = $mymodel->address->street;
            $idn = $mymodel->paymentDetails->idn;
            $date = now()->format('d-m-Y');
            $phone = $mymodel->profile->phonenumber;
            $iban = $mymodel->paymentDetails->iban;
            $bank = $mymodel->paymentDetails->bank;
            $acctype = $mymodel->account->type;
            $companyname = $mymodel->account->company_name;
            $companyadministrator = $mymodel->account->company_administrator;
            $companyaddress = $mymodel->account->company_address;
            $companyvatcode = $mymodel->account->company_vat;
            $companyregcode = $mymodel->account->company_regcode;
            $modelname = $mymodel->modelname;

            $semnatura = ModelSignature::where('model_id', $id)
                ->firstOrFail()->model_signature;


            $pdf = PDF::loadView('Contract.generate_pdf', compact('id', 'name', 'lname', 'email', 'country', 'state','city', 'address', 'idn', 'date', 'phone', 'iban', 'bank', 'acctype', 'companyname', 'companyadministrator', 'companyaddress', 'companyvatcode', 'companyregcode', 'semnatura', 'semnaturaManager', 'modelname'));

            $fileName = "contract_" . $id . ".pdf";
            $pdfPath = "contracts/" . $fileName;
            Storage::disk('local')->put($pdfPath, $pdf->output());

            $contract = ModelContract::where('model_id', $mymodel->id)->first();
            $contract->contractsigned = 1;
            $contract->signed_at = now()->format('Y-m-d H:i:s');
            $contract->save();

            //return view("Models.contract_sign", compact('id', 'name', 'lname', 'email', 'country', 'state','city', 'address', 'idn', 'date', 'phone', 'iban',
            //    'bank', 'acctype', 'companyname', 'companyadministrator', 'companyaddress', 'companyvatcode', 'companyregcode', 'modelname', 'semnaturaManager'));

            //return ["success" => true, "data" => "filename"];

        event(new AccountingNotifications('Contract Generated: Model '. $modelname));


    }




}
