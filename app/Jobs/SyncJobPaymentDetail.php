<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelPaymentDetail;
use App\Sync\Oldmodels;
use App\Sync\Syncid;

class SyncJobPaymentDetail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelPaymentDetail::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id){

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelPaymentDetail = new ModelPaymentDetail;
            $modelPaymentDetail->model_id = $my_id;
            $modelPaymentDetail->idn = $res->IDN;
            $modelPaymentDetail->iban = $res->IBAN;
            $modelPaymentDetail->bank = $res->Bank;
            $modelPaymentDetail->bank_swift = $res->BankSWIFT;
            $modelPaymentDetail->bank_address = $res->BankAddress;
            $modelPaymentDetail->currency = $res->Currency;

            $modelPaymentDetail->save();
        }

    }
}
