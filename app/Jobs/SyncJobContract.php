<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\ModelContract;
use App\Sync\Syncid;
use App\Sync\Oldmodels;

class SyncJobContract implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //truncate table
        ModelContract::query()->truncate();

        //get all ids fron sync id
        $ids = Syncid::all();
        foreach ($ids as $id){

            $my_id = $id->my_id;
            $old_id = $id->old_id;

            $res = Oldmodels::where('id', $old_id)->first();

            $modelContract = new ModelContract;
            $modelContract->model_id = $my_id;
            $modelContract->pic1 = $res->Pic1;
            $modelContract->pic2 = $res->Pic2;
            $modelContract->pic3 = $res->Pic3;
            $modelContract->signed_at = $res->signupdate;
            $modelContract->reason = $res->reason;
            $modelContract->contractsigned = $res->contractsigned;

            $modelContract->save();
        }

    }
}
