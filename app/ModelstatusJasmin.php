<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelstatusJasmin extends Model

{

    protected $table = 'model_status_jasmin';

    protected $guarded = [];

    public function studios(){
        return $this->hasOne('App\Studio', 'id', 'studio_id');
    }

}
