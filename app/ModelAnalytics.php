<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelAnalytics extends Model

{

    protected $table = 'model_analytics';

    protected $guarded = [];

}
