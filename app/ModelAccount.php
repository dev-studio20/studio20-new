<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelAccount extends Model

{

    protected $table = 'model_accounts';

    protected $fillable = [
        'name'
    ];

}
