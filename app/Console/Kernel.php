<?php

namespace App\Console;

use App\CustomClass\Stories;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Events\StatusLiked;
use Carbon\Carbon;
use App\StatsSalesTodayYday;
use App\ModelSale;
use App\CustomClass\ManageSales;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            //daily bot for sales
            ManageSales::updateTodaySales();

        })->hourlyAt(5);

        $schedule->call(function () {

            //get daily comparision for sales
            ManageSales::statsSalesTodayYesterday();

        })->hourlyAt(10);

        $schedule->call(function () {

            //get sales for month
            ManageSales::statsSalesMonthComparison();

        })->dailyAt('23:35');

        $schedule->call(function () {

            //get period sales comparison
            ManageSales::statsSalesPeriodComparison();

        })->hourlyAt(11);


        //model stories
        $schedule->call(function () {

            //get model stories
            Stories::getStories();

        //})->hourlyAt(55);
        })->everyThirtyMinutes();

    }



    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
