<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PeriodName extends Model

{
    public $timestamps = false;

    protected $table = 'periods_name';

    protected $fillable = [];

}
