<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsSalesTodayYday extends Model
{
    protected $table = 'stats_sales_tday_yday';

    protected $fillable = [
        'studio_id',
        'today_sales',
        'today_worktime',
        'yday_sales',
        'yday_worktime',
        'tday_from',
        'tday_to',
        'yday_from',
        'yday_to'
    ];
}
