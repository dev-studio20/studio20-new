<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Oldsales extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'sales';

    protected $guarded = [];

}
