<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Oldreservations extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'reservations';

    protected $guarded = [];

}
