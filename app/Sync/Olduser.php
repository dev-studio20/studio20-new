<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Olduser extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'users';

    protected $guarded = [];

}
