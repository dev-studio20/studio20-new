<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Syncid extends Model

{

    protected $table = 'sync_id';

    protected $fillable = [
        'my_id',
        'old_id',
    ];

}
