<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;

class Oldperiods extends Model
{
    protected $connection = 'mysql_hetzner';

    protected $table = 'periods';

    protected $guarded = [];

    public function history()
    {
        return $this->hasOne('App\Sync\Oldpaymenthist', 'period', 'id');
    }

}
