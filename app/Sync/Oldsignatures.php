<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Oldsignatures extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'signature';

    protected $guarded = [];

}
