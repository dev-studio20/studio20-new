<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Oldanalytics extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'analytics';

    protected $guarded = [];

}
