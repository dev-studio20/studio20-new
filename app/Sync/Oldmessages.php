<?php

namespace App\Sync;

use Illuminate\Database\Eloquent\Model;


class Oldmessages extends Model

{

    protected $connection = 'mysql_hetzner';

    protected $table = 'messages';

    protected $guarded = [];

}
