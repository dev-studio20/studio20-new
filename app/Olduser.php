<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Olduser extends Model

{

    protected $table = 'old_users';

    protected $fillable = [
        'name'
    ];

}
