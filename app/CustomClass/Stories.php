<?php
namespace App\CustomClass;

use App\Events\SystemNotifications;
use App\ModelStories;
use Carbon\Carbon;
use App\Models;

class Stories
{
    public static function getStories()
    {

        $all  = Models::whereStatus(1)->get();

        $models = $all->pluck('modelname')->toArray();

        $modelsChunk = array_chunk($models, 90);

        $authArr = ["Authorization: Bearer 406ad45b5ed5748abb871cc99ef69bc45d0ad2779f9034f91de5af254f8a9902", "Authorization: Bearer 8c23e7366a3e7096a30914edb145efc95fe241cd5be682a5ec1a366ca01e6c70"];

        $url_base ="https://partner-api.modelcenter.jasmin.com/v1/my-stories?";
        $allStories = [];

        foreach ($authArr as $auth){
            foreach ($modelsChunk as $chunck){

                $screenNames = implode('&screenNames[]=',$chunck);
                $screenNames = '&screenNames[]='.$screenNames;

                $url = $url_base.$screenNames;

                $cSession = curl_init();
                curl_setopt($cSession, CURLOPT_URL, $url);
                curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
                curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
                $result = curl_exec($cSession);
                curl_close($cSession);
                $res = json_decode($result, true);

                if (array_key_exists("data", $res)){
                    $data = $res["data"];
                    foreach($data as $screen){
                        if (array_key_exists("screenName", $screen)){
                            $screenName = $screen["screenName"];
                            //clean db
                            ModelStories::whereModelname($screenName)->delete();
                            if (array_key_exists("stories", $screen)){
                                if (count($screen["stories"]) > 0) {
                                    $stories = $screen["stories"][0];

                                    $allStories[$screenName] = $stories;
                                    $id = (array_key_exists("id", $stories)) ? $stories["id"] : 0;
                                    $status = (array_key_exists("status", $stories)) ? $stories["status"] : '--';
                                    $price = (array_key_exists("price", $stories)) ? $stories["price"] : 0;
                                    $items = (array_key_exists("items", $stories)) ? $stories["items"] : null;
                                    if(count($items)>0) {
                                        foreach($items as $item){
                                            $story_id = (array_key_exists("id", $item)) ? $item["id"] : 0;
                                            $story_status = (array_key_exists("status", $item)) ? $item["status"] : '--';
                                            $story_type = (array_key_exists("type", $item)) ? $item["type"] : '--';
                                            $story_privacy = (array_key_exists("privacy", $item)) ? $item["privacy"] : '--';
                                            $story_expiresAt = (array_key_exists("expiresAt", $item)) ? $item["expiresAt"] : now()->format('Y-m-d H:i:s');
                                            $story_media = (array_key_exists("media", $item)) ? $item["media"] : [];

                                            $dateFormat = Carbon::parse($story_expiresAt)->setTimezone('UTC')->format('Y-m-d H:i:s');

                                            //insert in db..
                                            $ms = ModelStories::updateOrCreate([
                                                "modelname" => $screenName,
                                                "story_id" => $id,
                                                "stories_id" => $story_id,

                                            ],[
                                                "status" => $status,
                                                "price" => $price,
                                                "count" => count($items),
                                                "stories_status" => $story_status,
                                                "stories_type" => $story_type,
                                                "stories_privacy" => $story_privacy,
                                                "stories_media" => json_encode($story_media),
                                                "stories_expire" => $dateFormat
                                            ]);

                                        }
                                    }
                                }
                            }
                        }
                    }
                    sleep(1);
                }

            }
            sleep(1);
        }

        //return $models;
        //event(new StatusLiked('Bot For Sales!'));
        event(new SystemNotifications('BOT: Stories Updated!'));

    }

}