<?php
namespace App\CustomClass;

use Carbon\Carbon;
use App\Models;
use App\ModelSale;
use DB;
use App\Events\SystemNotifications;

class ManageSales
{
    public static function  updateTodaySales()
    {

        //get daily sales (bot)
        $start = Carbon::now()->format('Y-m-d');

        $startDate = Carbon::parse($start)->format('Y-m-d H:i:s');
        $p = getPeriodNumberFromDate($startDate);
        $period = $p["period"];
        $year = $p["year"];
        $month = $p["month"];
        $day = $p["day"];

        $endDate = Carbon::parse($startDate)->format('Y-m-d') . " 23:59:59";

        $monthSales = (getSalesAndTimeInterval($startDate, $endDate));

        //store sales and update
        $from = $monthSales["from"];
        $to = $monthSales["to"];
        $models = $monthSales["dataByModels"];
        $notFound = [];

        foreach ($models as $key => $model) {
            //studio, earnings, worktime, worktimeneeded

            if (array_key_exists('studio', $model) && array_key_exists('earnings', $model) && array_key_exists('worktimeneeded', $model)) {
                $studio = $model["studio"];
                $earnings = $model["earnings"];
                //$workt = $model["worktime"];
                $worktime = $model["worktimeneeded"];

                $modelname = Models::where('modelname', $key)->first();
                if ($modelname) {
                    $studio = $modelname->studio;
                    $model_id = $modelname->id;
                    $modelSale = ModelSale::updateOrCreate(['model_id' => $model_id, 'studio_id' => $studio, 'period' => $period, 'year' => $year, 'month' => $month, 'day' => $day],
                        ["sum" => $earnings, "hours" => $worktime]);
                }
            } else $notFound[$key] = $model;

        }

        //event(new StatusLiked('Bot For Sales!'));
        event(new SystemNotifications('BOT: Sales Updated!'));

    }

    public static function statsSalesTodayYesterday(){

        //for stats

        //get sales from today and yesterday
        $yDayStart = Carbon::yesterday()->format('Y-m-d H:i:s');
        $yDayEnd = Carbon::now()->subHours(24)->format('Y-m-d H:i:s');

        $tDayStart = Carbon::yesterday()->addHours(24)->format('Y-m-d H:i:s');
        $tDayEnd = Carbon::now()->format('Y-m-d H:i:s');

        $yDaySales = (getSalesAndTimeInterval($yDayStart, $yDayEnd));
        sleep(1);
        $tDaySales = (getSalesAndTimeInterval($tDayStart, $tDayEnd));
        insertInDbYdayvVsTday($yDaySales, $tDaySales);

        //event(new StatusLiked('Sales Today Compare!'));
        event(new SystemNotifications('BOT: Stats Today Updated!'));
    }

    public static function statsSalesMonthComparison(){

        //for stats
        //get sales for month comparison
        $month = Carbon::now()->format('Y-m');

        $startDate = Carbon::parse($month)->format('Y-m-d H:i:s');

        $endDate = Carbon::parse($startDate)->addMonth()->format('Y-m-d H:i:s');

        $monthSales = (getSalesAndTimeInterval($startDate, $endDate));

        insertInDbMonthSales($monthSales, $month);

        //event(new StatusLiked('Sales Month Compare!'));
        event(new SystemNotifications('BOT: Stats Month Updated!'));
    }

    public static function statsSalesPeriodComparison(){

        //for stats
        //get period sales comparison

        //detect this period start, now
        $d = now()->format('d');
        $thisMonth = Carbon::now()->format('Y-m').'-';
        $thisMonth .= ($d < 16) ? '01' : '16';
        $startThisPeriod = Carbon::parse($thisMonth);
        $endThisPeriod = Carbon::now();
        $time = Carbon::now()->format('H:i:s');

        //calc day dif
        $dif = $startThisPeriod->diffInDays($endThisPeriod);

        //find prev period start + add dif
        if($d > 15) {
            $prevPeriodDayStart = '01';
            $prevPeriodDayEnd = ((1 + $dif) < 10) ? "0".(1+$dif) : (1+$dif);
            $prevmonth = Carbon::now()->format('Y-m');
        } else {
            $prevPeriodDayStart = 16;
            $prevmonth = Carbon::now()->subMonth()->format('Y-m');
            $daysinmonth = Carbon::parse($prevmonth)->daysInMonth;
            $prevPeriodDayEnd = ((16 + $dif) <= $daysinmonth) ? (16 + $dif) : $daysinmonth;
        }

        $startThisPeriod = $startThisPeriod->format('Y-m-d H:i:s');
        $endThisPeriod = $endThisPeriod->format('Y-m-d H:i:s');

        $startPrevPeriod = $prevmonth.'-'.$prevPeriodDayStart . " 00:00:00" ;
        $endPrevPeriod = $prevmonth.'-'.$prevPeriodDayEnd . " " . $time;

        $prevPeriodSales = (getSalesAndTimeInterval($startPrevPeriod, $endPrevPeriod));
        sleep(1);
        $thisPeriodSales = (getSalesAndTimeInterval($startThisPeriod, $endThisPeriod));

        $sales = ["prevPeriod" => $prevPeriodSales, "thisPeriod" => $thisPeriodSales];

        insertInDbPeriodSales($sales);

        //event(new StatusLiked('Sales Period Compare!'));
        event(new SystemNotifications('BOT: Stats Period Updated!'));

    }

    public static function searchForNewApiKeys(){

        //get api key for model without key
        //"all_models" => $models, "found_found" => $mfound
        $data = getAuthModels();

        DB::table('xxx_to_delete_test_mfound')->insert( ["allmodels" => json_encode($data["all_models"]), "found" => json_encode($data["found_models"])] );
        //event(new StatusLiked('Api Key Search!'));
        event(new SystemNotifications('BOT: Api Key Search!'));

    }

}