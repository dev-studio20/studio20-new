<?php
namespace App\CustomClass;

use App\Events\SystemNotifications;
use App\ModelMessagesBody;
use App\ModelMessagesMass;
use App\ModelMessagesSubject;
use App\ModelProfile;
use App\Studio;
use GuzzleHttp\Client;
use Mail;
use Auth;
use URL;
use App\Models;
use App\Events\AccountingNotifications;
use App\Jobs\GenerateContract;

class SendNotification
{

// 'SYSTEM' event(new SystemNotifications('Test Pusher SYSTEM Event!'));
// 'STUDIO' event(new StudioNotifications('Test Pusher STUDIO Event!'));
// 'ACCOUNTING' event(new AccountingNotifications('Test Pusher ACCOUNTING Event!'));
// 'ADMIN' event(new AdminNotifications('Test Pusher ADMIN Event!'));
// 'DEV' event(new DevNotifications('Test Pusher DEV Event!'));

//    ----------Admin Events------------------------

    public static function registrationEvent(){

        $user = Auth::guard('model')->user();
        $email = $user->email;

        $model = Models::with('profile','studios')->findOrFail($user->id);

        $modelname = $model->profile->first_name . ' ' .$model->profile->last_name;

        $url = URL::signedRoute('autologin', ['model' => $model]);

        $mailFrom = env('MAIL_FROM_ADDRESS','no-reply@studio20.group');
        $mailFromName = env('MAIL_FROM_NAME','Studio20');

        //mail
        Mail::send('auth.verifyautologin', ['modelname' => $modelname, 'url' => $url], function ($message) use ($email, $mailFrom, $mailFromName) {
            $message->to($email)->subject('Email Verification');
            $message->from($mailFrom, $mailFromName);
        });

        //SEND TO OP
        $studio =  $model->studios->name;
        $emailOp = 'op@studio20.com';

        Mail::send('Mail.Admin.registration', ['modelname' => $modelname, 'studio' => $studio], function ($message) use ($emailOp) {
            $message->to($emailOp)->subject('New Model Registration');
            $message->from('no-reply@studio20.group', 'Studio20');
        });

        //sms + check if studio has phone verification
        $smslink = URL::signedRoute('autologinphone', ['model' => $model]);
        $message = 'Phone Verification Link:' . $smslink;
        sendSmsToNotify($model->studio, $model->profile->phonenumber, $message);

        //broadcast event to pusher
        event(new AccountingNotifications('New Registration!'));

    }



    public static function generateContractEvent($id){

        GenerateContract::dispatch($id);

    }

    public static function accountStatusEvent($admin, $modelname, $stat){

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' : Account: ' . $stat . ' for: ' . $modelname));

        $model = Models::with('profile','social','address','studios')->where('modelname', $modelname)->firstOrFail();

        $signedcontractname = storage_path("app/contracts/contract_".$model->id.".pdf");

        //send contract if approved
        if ($stat == 'Approved') {

            $emails = [$model->email];

            $modlename = $model->profile->fierst_name. ' ' .$model->profile->last_name;

            //send mail to model
            Mail::send('Mail.Models.contract', ['modelname' => $modlename,
                'modelNickname' => $model->modelname,
                'promo' => $model->social->brand,
                'brand' => $model->social->brand,
                'artistic_email' => $model->social->artistic_email,
                'artistic_password' => $model->social->artistic_password,
                'twitter_profile' => $model->social->twitter_profile,
                'twitter_posts' => $model->social->twitter_post,
                'instagram_profile' => $model->social->instagram_profile,
                'personal_site' => $model->social->personal_site,
                'country' => findCountry($model->address->country_id),
            ], function ($message) use ($emails, $signedcontractname) {
                $message->to($emails)->subject('Your application with Studio 20 is approved and your contract is signed');
                $message->from('admin@studio20.group','Studio20');
                $message->attach($signedcontractname);
            });

            if($model->studios->active && $model->studios->contact_email){

                $emailsOpAndStudio = ['op@studio20.com', $model->studios->contact_email];
            } else $emailsOpAndStudio = ['op@studio20.com'];


            //send mail to op and studio
            Mail::send('Mail.Admin.contract2', ['modelname' => $modlename,
                'modelNickname' => $model->modelname,
                'promo' => $model->social->brand,
                'brand' => $model->social->brand,
                'artistic_email' => $model->social->artistic_email,
                'artistic_password' => $model->social->artistic_password,
                'twitter_profile' => $model->social->twitter_profile,
                'twitter_posts' => $model->social->twitter_post,
                'instagram_profile' => $model->social->instagram_profile,
                'personal_site' => $model->social->personal_site,
                'country' => findCountry($model->address->country_id),
            ], function ($message) use ($emailsOpAndStudio, $signedcontractname) {
                $message->to($emailsOpAndStudio)->subject('New application for Studio 20, approved and signed');
                $message->from('admin@studio20.group','Studio20');
                $message->attach($signedcontractname);
            });

            //send mail to accountant
            $accountant = 'accounting@globalmaca.net';
            Mail::send('Mail.Admin.accountant', ['modelname' => $modlename,'modelNickname' => $model->modelname],
                function ($message) use ($accountant, $signedcontractname) {
                    $message->to($accountant)->subject('New application for Studio 20, approved and signed');
                    $message->from('admin@studio20.group','Studio20');
                    $message->attach($signedcontractname);
                });

            //notify model via sms
            if ($model->profile->phonenumber && $model->studios->phone_notification){
                $phone = $model->profile->phonenumber;
                $client = new Client();
                $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to='. $phone .'&text=(Studio 20)Your account has been approved!');
            }

        }

    }

    public static function editProfileEvent($admin, $modelname){

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' : Edit Profile Info: ' . $modelname));

    }

    public static function editSocialEvent($admin, $modelname){

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' : Edit Social Info: ' . $modelname));

    }

    public static function editPasswordEvent($admin, $modelname){

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' : Password Change: ' . $modelname));

    }

    public static function addFeedbackEvent($admin, $modelname){

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' : Feedback: ' . $modelname));

    }

    public static function pict1StatusEvent($admin, $modelname, $status){

        //if status Reject send mail to model..

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' ' . $status . ' : ID Front: ' . $modelname));

    }

    public static function pict2StatusEvent($admin, $modelname, $status){

        //if status Reject send mail to model..

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' ' . $status . ' : ID Back: ' . $modelname));

    }

    public static function pict3StatusEvent($admin, $modelname, $status){

        //if status Reject send mail to model..

        //broadcast event to pusher
        event(new AccountingNotifications($admin . ' ' . $status . ' : Face + Id: ' . $modelname));

    }

    public static function closePeriodEvent($msg){

        //broadcast event to pusher
        event(new AccountingNotifications($msg));

    }

    public static function reservationsApprovedEvent($model_id){

        $model = Models::findOrFail($model_id);

        //broadcast event to pusher
        event(new AccountingNotifications('Reservations Approved : ' .$model->modelname ));

    }

    public static function makePaymentEvent($model_id){

        $model = Models::findOrFail($model_id);
        //broadcast event to pusher
        event(new AccountingNotifications('Payment Made : ' . $model->modelname . ' !'));

    }

    public static function updatePeriodEvent($msg){

        //broadcast event to pusher
        event(new SystemNotifications($msg));

    }

    public static function sendMessageNotificationStudio($studioid, $subj, $message, $attachements){

        //broadcast event to pusher
        event(new SystemNotifications('Message Sent to ' . getStudios()[$studioid] .' !'));

        //platform msg
        ModelMessagesMass::insert(['to_studio' => $studioid,
            'subject' => $subj,
            'message' => $message,
            'readtime' => null,
            'attachements' => $attachements]);

        //send sms notification
        //$smslink = 'https://studio20girls.com/modelcenter/messages';
        //$phone = $model->phonenumber;
        //$client = new Client();
        //$res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to='. $phone .'&text=You have a new message at ' . $smslink);

        //send mail
        //$emailModel = $model->Email;
        //$modelname = $model->Modelname;
        //Mail::send('emails.message', ['modelname' => $modelname], function ($message) use ($emailModel) {
        //    $message->to($emailModel)->subject('You have a new message!');
        //    $message->from('admin@studio20girls.com', 'Studio20');
        //});




    }

    public static function resendPhoneVerification(){

        $model = Auth::guard('model')->user();

        //broadcast event to pusher
        event(new SystemNotifications($model->modelname.' : Phone Verification Link Resend!'));

        //check if studio has phone verification

        $phone = $model->profile->phonenumber;
        $smslink = URL::signedRoute('autologinphone', ['model' => $model]);

        $client = new Client();
        $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to='. $phone .'&text=' . $smslink);

    }

    public static function changePhoneEvent($modelname){

        //broadcast event to pusher
        event(new SystemNotifications($modelname. ' : Change Phone!'));

    }

    public static function changeEmailEvent(){

        //broadcast event to pusher
        event(new SystemNotifications('Change Email Event!'));

    }


    public static function sendMessageNotificationModel(Models $model, $subject, $message, $attachements){

        //broadcast event to pusher
        event(new SystemNotifications('Message Sent!'));

        //platform msg
        $to = $model->id;
        $phone = $model->profile->phonenumber;
        $emailModel = $model->email;
        $modlename = $model->modelname;
        $smslink = 'https://studio20.group/models/messages';

        $subj = new ModelMessagesSubject();
        $subj->from_id = 'admin';
        $subj->to_id = $to;
        $subj->subject = $subject;
        $subj->solved = 0;
        $subj->save();

        $body = new ModelMessagesBody();
        $body->from_id = 'admin';
        $body->to_id = $to;
        $body->belongs_to = $subj->id;
        $body->message = $message;
        $body->attachements = $attachements;
        $body->save();

        //send sms notification
        $client = new Client();
        $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to=' . $phone . '&text=You have a new message at ' . $smslink);
        echo $res->getStatusCode();

        //send mail
        Mail::send('Mail.Models.message', ['modelname' => $modlename], function ($message) use ($emailModel) {
            $message->to($emailModel)->subject('You have a new message!');
            $message->from('admin@studio20girls.com', 'Studio20');
        });



    }









//    ----------Models Events------------------------

    public static function newMessage($modelname){

        //broadcast event to pusher
        event(new AccountingNotifications('New Message From: ' . $modelname));

    }

    public static function reuploadPicturesEvent(){

        $model = Auth::guard('model')->user();

        $mprofile = ModelProfile::where('model_id', $model->id)->first();
        if($mprofile) {

            $name = $mprofile->first_name;
            $lname = $mprofile->last_name;

            $email = 'admin@studio20.group';

            Mail::send('Mail.Admin.pictureupload', [], function ($message) use ($name,$lname,$email) {
                $message->to('op@studio20.com')->subject('Picture Upload');
                $message->from($email, $name .' '.$lname);
            });

            //broadcast event to pusher
            event(new AccountingNotifications($model->modelname.' : New Picture Upload!'));

        }



    }


    public static function resendVerificationLinkEvent($model_id){

        $model = Models::with('profile')->findOrFail($model_id);

        $url = URL::signedRoute('autologin', ['model' => $model]);

        //$rowmodel = DB::table('models')->where('id', Auth::user()->id)->first();
        $modlename = $model->profile->first_name . ' ' .$model->profile->last_name;

        $email = $model->email;
        Mail::send('auth.verifyautologin', ['modelname' => $modlename, 'url' => $url], function ($message) use ($email) {
            $message->to($email)->subject('Email Verification');
            $message->from('no-reply@studio20.group', 'Studio20');
        });

    }

    public static function resendPhoneVerificationLinkEvent($model_id){

        $model = Models::with('profile')->findOrFail($model_id);

        $smslink = URL::signedRoute('autologinphone', ['model' => $model]);
        $message = 'Phone Verification Link:' . $smslink;
        sendSmsToNotify($model->studio, $model->profile->phonenumber, $message);

    }

    public static function requestPaymentEvent($modelname, $period){

        $periodname = getAllPeridos()[$period];
        $email = 'op@studio20.com';

        Mail::send('Mail.Admin.paymentrequest', ['modelname' => $modelname, 'periodname' => $periodname], function ($message) use ($email) {
            $message->to($email)->subject('Model has requested payment!');
            $message->from('admin@studio20.group', 'Studio20');
        });

        //broadcast event to pusher
        event(new AccountingNotifications($modelname . ' Requested Payment!'));

    }

    public static function reservationMadeEvent($reservation_id, $modelname, $days, $month){

        //link
        $link = "https://studio20.group/admin/reservations/".$reservation_id;

        $email = 'op@studio20.com';

        $month = get_month($month-1);

        Mail::send('Mail.Admin.reservation', ['modelname' => $modelname, 'link' => $link, 'days' => $days, 'month' => $month], function ($message) use ($email, $modelname, $month) {
            $message->to($email)->subject('New reservations from '.$modelname.'('.$month.')');
            $message->from('admin@studio20.group', 'Studio20');
        });

        //broadcast event to pusher
        event(new AccountingNotifications($modelname. ' : Reservation Made!'));

    }





















    public static function  test_send_sms($phone, $message)
    {
        $client = new Client();
        $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to=' . $phone . '&text=' . $message);

        $smsresponse = '';
        $resCode = $res->getStatusCode();
        if ($res->getStatusCode() == 200){
            $smsresponse = json_decode($res->getBody())->details;
        } else {
            $smsresponse = json_decode($res->getBody());
        }

        return ["code" => $resCode, "response" => $smsresponse];


    }

    public static function test_send_email($email){

        //$email = 'gabriel.basca@studio20.com';

        Mail::send('Mail.Models.test', [], function ($message) use ($email) {
            $message->to($email)->subject('New Mail Test');
            $message->from('no-reply@studio20.group', 'Studio20');
        });


    }


}