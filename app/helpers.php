<?php

use App\APIKeys;
use App\Currency;
use App\ModelKeys;
use App\Models;
use App\Country;
use App\City;
use App\State;
use App\Studio;
use Illuminate\Support\Carbon;
use App\StatsSalesTodayYday;
use App\StatsSalesMonth;
use App\StatsSalesPeriod;
use App\Sync\Oldmodels;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

if (!function_exists('mydd')) {

    function mydd($var)
    {
        if (get_client_ip() == '188.214.255.240') {
            dd($var);
        }

    }
}

if (!function_exists('modelId')) {

    function modelId($modelname)
    {

        return Models::where('modelname', $modelname)->first()->id ?? 0;

    }
}

if (!function_exists('findCountry')) {

    function findCountry($id)
    {
        return Country::find($id)->name ?? '*Romania';
    }
}

if (!function_exists('findState')) {

    function findState($id)
    {
        return State::find($id)->name ?? '*Bucharest';
    }
}

if (!function_exists('findCity')) {

    function findCity($id)
    {
        return City::find($id)->name ?? '*Bucharest';
    }
}

if (!function_exists('findStudio')) {

    function findStudio($id)
    {
        return Studio::find($id)->name ?? '*Home';
    }
}

if (!function_exists('getCurrentPeriod')) {

    function getCurrentPeriod()
    {

        $d = intval(date('d'));
        $m = intval(date('m'));
        $y = date('Y');

        $p = ($d < 16) ? 1 : 0;
        $p = ($m * 2) - $p;

        return $p;

    }
}

if (!function_exists('getCurrentPeriodAndYear')) {

    function getCurrentPeriodAndYear()
    {

        $d = intval(date('d'));
        $m = intval(date('m'));
        $y = date('Y');

        $p = ($d < 16) ? 1 : 0;
        $p = ($m * 2) - $p;

        return [$p, $y];

    }
}

if (!function_exists('getPeriodFromDate')) {

    function getPeriodFromDate($date)
    {

        $myDate = Carbon::parse($date)->format('Y-m-d');
        $myDate = explode('-', $myDate);

        $d = $myDate[2];
        $m = $myDate[1];
        $y = $myDate[0];

        $p = ($d < 16) ? 1 : 0;
        $p = ($m * 2) - $p;

        return getAllPeridos()[$p];

    }
}


if (!function_exists('getPeriodNumberFromDate')) {

    function getPeriodNumberFromDate($date)
    {

        $myDate = Carbon::parse($date)->format('Y-m-d');
        $myDate = explode('-', $myDate);

        $d = $myDate[2];
        $m = $myDate[1];
        $y = $myDate[0];

        $p = ($d < 16) ? 1 : 0;
        $p = ($m * 2) - $p;

        return ["period" => $p, "year" => $y, "month" => $m, "day" => $d];

    }
}

if (!function_exists('getCurrentPeriodAndYearMinus2')) {

    function getCurrentPeriodAndYearMinus2()
    {

        $d = intval(date('d'));
        $m = intval(date('m'));
        $y = intval(date('Y'));

        $p = ($d < 16) ? 1 : 0;
        $p = ($m * 2) - $p;

        if ($p <= 2) {
            $p = 24 - (2 - $p);
            $y = $y - 1;
        } else $p = $p - 2;

        return [$p, $y];

    }
}


if (!function_exists('getCurrency')) {

    function getCurrency()
    {
        $currency = Currency::whereSelected(1)->first()->symbol ?? '?';

        return $currency;

    }
}

if (!function_exists('getDaysInMonth')) {
    function getDaysInMonth($year, $month){
        $date = $year.'-'.$month;
        return Carbon::parse($date)->daysInMonth;
    }
}

if (!function_exists('getArrayDaysPeriod')) {

    function getArrayDaysPeriod($year, $period)
    {

        $arrMonth = get_month_period($period);
        $month = $arrMonth[0];
        $p = $arrMonth[1];


        $nrdays = getDaysInMonth($year, $month);

        $start = 1;
        $end = 15;

        if ($p == 2) {
            $start = 16;
            $end = $nrdays;
        }

        $arrDays = [];
        for($i = $start; $i <= $end; $i++){
            $arrDays[] = $year.'-'.decimal($month).'-'.decimal($i);
        }

        return $arrDays;

    }
}

if (!function_exists('decimal')) {

    function decimal($val)
    {
        $val = ($val < 10) ? '0'.$val : $val;

        return $val;

    }
}

if (!function_exists('convertHoursToSeconds')) {

    function convertHoursToSeconds($hours)
    {
        $h = explode(':', $hours);
        if (sizeof($h) == 3) {

            $h[0] = intval($h[0]);
            $h[1] = intval($h[1]);
            $h[2] = intval($h[2]);

            //if (!is_numeric($h[0])) dd($hours);
            //if (!is_numeric($h[1])) dd($hours);
            //if (!is_numeric($h[2])) dd($hours);

            $h0 = $h[0] * 3600;
            $h1 = $h[1] * 60;
            $h2 = $h[2];
            return $h0 + $h1 + $h2;
        } else return 0;

    }
}

if (!function_exists('formatDate')) {

    function formatDate($date)
    {
        return $date."T00%3A00%3A00+%2B01%3A00";

    }
}

if (!function_exists('formatDateWithHour')) {

    function formatDateWithHour($date)
    {
        //2020-01-15 09:38:34
        $d = explode(' ', $date);
        $t = explode(':', $d[1]);

        return $d[0]."T".$t[0]."%3A".$t[1]."%3A".$t[2]."+%2B01%3A00";

    }
}

if (!function_exists('curlAuth')) {

    function curlAuth($auth, $url){

        $cSession = curl_init();

        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($cSession);
        curl_close($cSession);
        $res = json_decode($result, true);

        return $res;

    }
}

if (!function_exists('getAuthModels')) {

    function getAuthModels(){

        $models = Models::whereNull('key_id')->where('status',1)->get()->pluck('modelname')->toArray();

        //$models[] = 'MellAnyass'; //2
        //$models[] = 'IAmKallisa'; //1

        $mfound = [];

        $chuncks = array_chunk($models, 30);

        foreach($chuncks as $chunck){

            $mfound[] = findKey($chunck);
            sleep(1);
        }



        return ["all_models" => $models, "found_models" => $mfound];

    }
}

if (!function_exists('findKey')) {

    function findKey($models){

        $date = Carbon::now()->format('Y-m-d');

        $screenNames = null;
        foreach($models as $model){
            $screenNames .= '&screenNames[]=' . $model;
        }

        $reports = "&reports[]=general-overview";

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$date."&toDate=".$date.$screenNames.$reports;

        $keys = APIKeys::all();

        $mfound = [];

        foreach($keys as $key){
            $auth = $key->bearer;
            $res = curlAuth($auth, $url);

            if (array_key_exists('data', $res)){
                foreach($res['data'] as $data){
                    if (array_key_exists('screenName', $data)){
                        $model = Models::where('modelname', $data['screenName'])->update(['key_id' => $key->id]);
                        $mfound[] = ["modelname" => $data['screenName'], "id" => $key->id, "bearer" => $key->bearer];
                    }

                }

            }

            sleep(0.5);
        }

        return $mfound;

    }
}


if (!function_exists('convertPeriodNameToValue')) {

    function convertPeriodNameToValue($periodName)
    {

        $period = explode(' ', $periodName);
        if (count($period) == 2) {
            $k = get_month_index($period[0]);
            $p = ($period[1] == "I") ? 1 : 2;
            return ($k * 2) + $p;

        } else return 0;

    }
}

if (!function_exists('get_month')) {

    function get_month($m)
    {
        $month = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];

        return $month[$m];

    }
}

if (!function_exists('get_period')) {

    function get_period($m)
    {
        $month = getAllPeridos();

        return $month[$m];

    }
}

if (!function_exists('getStudioName')) {

    function getStudioName($id)
    {

        $m = Models::with('studios')->where('id',$id)->first();

        $studio = ($m && $m->studios) ? $m->studios->name : '--';

        return $studio;

    }
}


if (!function_exists('getAllPeridos')) {
    function getAllPeridos()
    {

        $month = [
            1 => "January I",
            2 => "January II",
            3 => "February I",
            4 => "February II",
            5 => "March I",
            6 => "March II",
            7 => "April I",
            8 => "April II",
            9 => "May I",
            10 => "May II",
            11 => "June I",
            12 => "June II",
            13 => "July I",
            14 => "July II",
            15 => "August I",
            16 => "August II",
            17 => "September I",
            18 => "September II",
            19 => "October I",
            20 => "October II",
            21 => "November I",
            22 => "November II",
            23 => "December I",
            24 => "December II"
        ];

        return $month;
    }

}

if (!function_exists('get_month_period')) {

    function get_month_period($period)
    {
        $month = [
            1 => [1, 1],
            2 => [1, 2],
            3 => [2, 1],
            4 => [2, 2],
            5 => [3, 1],
            6 => [3, 2],
            7 => [4, 1],
            8 => [4, 2],
            9 => [5, 1],
            10 => [5, 2],
            11 => [6, 1],
            12 => [6, 2],
            13 => [7, 1],
            14 => [7, 2],
            15 => [8, 1],
            16 => [8, 2],
            17 => [9, 1],
            18 => [9, 2],
            19 => [10, 1],
            20 => [10, 2],
            21 => [11, 1],
            22 => [11, 2],
            23 => [12, 1],
            24 => [12, 2]
        ];

        return $month[$period];

    }
}

if (!function_exists('get_month_index')) {

    function get_month_index($m)
    {
        $months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];

        $key = array_search($m, $months);

        return $key;

    }
}


if (!function_exists('get_client_ip')) {
    function get_client_ip()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}

if (!function_exists('convertSecondsToHours')) {
    function convertSecondsToHours($seconds)
    {

        $hours = floor($seconds / 3600) . gmdate(":i:s", $seconds % 3600);
        return $hours;
    }

}

if (!function_exists('getModelsStatsColor')) {
    function getModelsStatsColor($key)
    {

        $colors = ["Approved" => 'info', "Pending" => 'warning', "Suspended" => 'primary', "Rejected" => 'danger', "Inactive" => 'rose', "Total" => 'success'];

        return $colors[$key];
    }

}

if (!function_exists('getModelsStatusColor')) {
    function getModelsStatusColor($key)
    {

        $colors = [1 => 'success', 2 => 'warning', 3 => 'primary', 4 => 'danger', 5 => 'rose'];

        return $colors[$key];
    }

}

if (!function_exists('getPictureStatusColor')) {
    function getPictureStatusColor($key)
    {

        $colors = [0 => 'warning' ,1 => 'success', 2 => 'danger'];

        return $colors[$key];
    }

}

if (!function_exists('getModelsStatsIcon')) {
    function getModelsStatsIcon($key)
    {

        $icons = ["Approved" => 'perm_identity', "Pending" => 'schedule', "Suspended" => 'pause_circle_outline', "Rejected" => 'highlight_off', "Inactive" => 'hourglass_empty', "Total" => 'supervisor_account'];

        return $icons[$key];
    }

}

if (!function_exists('getLastDaysLabel')) {
    function getLastDaysLabel()
    {
        $weekMap = [
            0 => 'S',
            1 => 'M',
            2 => 'T',
            3 => 'W',
            4 => 'T',
            5 => 'F',
            6 => 'S',
        ];

        $dayOfTheWeek = Carbon::now()->dayOfWeek;

        $mydays = [];


        for ($i = 0; $i < 7; $i++) {

            $mydays[] = $weekMap[$dayOfTheWeek];
            $dayOfTheWeek--;
            if ($dayOfTheWeek == -1) $dayOfTheWeek = 6;

        }

        $mydays = array_reverse($mydays, false);

        return $mydays;

    }

    if (!function_exists('getAllStudios')) {
        function getAllStudios()
        {

            $studios = Studio::all()->pluck('name', 'id')->toArray();
            $studios[0] = 'All';

            return $studios;
        }

    }


    if (!function_exists('getStudios')) {
        function getStudios()
        {

            $studios = Studio::whereIn('id', getAdminStudios())->get()->pluck('name', 'id')->toArray();
            if (canViewStudio()) $studios[0] = 'All';

            return $studios;
        }

    }

    if (!function_exists('getStudiosIds')) {
        function getStudiosIds()
        {

            $studios = Studio::whereIn('id', getAdminStudios())->get()->pluck('id')->toArray();

            return $studios;
        }

    }

    if (!function_exists('getRole')) {
        function getRole()
        {
            //Admin Roles
            //1 Accounting
            //2 Admin
            //3 Super Admin
            //4 Developer
            //5 Studio Manager
            //6 Support

            return Auth::guard('admin')->user()->getRole();
        }
    }

    if (!function_exists('getChannelForRole')) {
        function getChannelForRole()
        {
            //Admin Roles
            //1 Accounting
            //2 Admin
            //3 Super Admin
            //4 Developer
            //5 Studio Manager

            $role = getRole();
            $arr = [];

            if ($role == 1) $arr =  ["ACCOUNTING"];
            if ($role == 2) $arr =  ["STUDIO"];
            if ($role == 3) $arr =  ["ADMIN", "ACCOUNTING", "STUDIO"];
            if ($role == 4) $arr =  ["ADMIN", "ACCOUNTING", "STUDIO", "SYSTEM", "DEV"];
            if ($role == 5) $arr =  ["STUDIO"];

            return implode(',',$arr);
        }
    }



    if (!function_exists('adminStudio')) {
        function adminStudio()
        {

            $studioSessionOrAdmins = session('studio', Auth::guard('admin')->user()->studio_id);

            return $studioSessionOrAdmins;
        }
    }

    if (!function_exists('canViewStudio')) {
        function canViewStudio()
        {
            // [1,2,3,4] = can view all studios
            // [5] = can view only his studio

            return (in_array(getRole(), [1, 2, 3, 4]));

        }
    }

    if (!function_exists('getAdminStudios')) {
        function getAdminStudios()
        {
            if (canViewStudio()) {
                $studioId = Studio::whereActive(1)->pluck('id')->toArray();
            } else {
                $studioId = [Auth::guard('admin')->user()->studio_id];
            }

            return $studioId;

        }
    }

    if (!function_exists('findStudioName')) {
        function findStudioName($id)
        {
            $studioName = Studio::find($id)->name;

            return $studioName;

        }
    }

    if (!function_exists('getModelSex')) {

        function getModelSex($modelid)
        {

            $sex = \App\ModelProfile::where('model_id', $modelid)->first();
            $sex = ($sex) ? $sex->sex : 'F';
            $sex = ($sex == 'C') ? 'F' : $sex;

            return $sex;

        }
    }

//    if (!function_exists('getAllModels')) {
//
//        function getAllModels()
//        {
//            //approved status = 1
//
//            $models = Models::with('key')->where('status', 1)->get();
//            $all = [];
//
//            foreach ($models as $model) {
//                $key = getAuth($model);
//                $modelname = $model->modelname;
//
//                $all[$key][] = $modelname;
//            }
//
//            return $all;
//
//        }
//    }

    if (!function_exists('getAllModels')) {

        function getAllModels()
        {
            //Approved = 1, Pending = 2, Suspended = 3, Rejected = 4, Inactive = 5
            $status = [1, 3, 5];
            $studios = Studio::whereActive(1)->get()->pluck('id');

            $models = Models::with('key')->whereIn('status', $status)->whereIn('studio', $studios)->whereNotNull('key_id')->get();
            $all = [];

            foreach ($models as $model) {
                $key = $model->key->bearer;

                if (!$key) {
                    $key = "key_missing";
                }
                $modelname = $model->modelname;
                $studio = $model->studio;

                $all[$key][] = ["modelname" => $modelname, "studio" => $studio];
            }

            return $all;

        }
    }


    //------------ get sales and work time from jasmin -----------------------
    if (!function_exists('getSalesAndTimeInterval')) {
        function getSalesAndTimeInterval($startDate, $endDate)
        {

            $all = getAllModels();

            $modelStudio = [];
            $studioModel = [];

            foreach ($all as $mod) {
                foreach ($mod as $mod_arr) {
                    $modelStudio[$mod_arr["modelname"]] = ["studio" => $mod_arr["studio"]];
                }
            }

            $data = getSalesForInterval($all, $startDate, $endDate, $modelStudio);
            //"earnings", "worktime", "worktimeneeded", "count", "totalcount", "modelstudio",  "from", "to"

            $dataByModels = $data["modelstudio"];


            $dataByStudios = calcTotal($data["modelstudio"], $studioModel);

            return [
                "dataByModels" => $dataByModels,
                "dataByStudios" => $dataByStudios,
                "count" => $data["count"],
                "totalcount" => $data["totalcount"],
                "from" => $data["from"],
                "to" => $data["to"],
            ];

            //insertInDb($todaySales, $today_from, $today_to, $yesdaySales, $yesday_from, $yesday_to);

        }
    }

    //        $msg = ModelMessagesSubject::with('body')->where('to_id','admin')->orderby('created_at','DESC')->where('solved', 0)->get()->toArray();
    if (!function_exists('msgNotReadAdmin')) {
        function msgNotReadAdmin(){

            $msg = \App\ModelMessagesSubject::with('body')->where('to_id','admin')->orderby('created_at','DESC')->where('solved', 0)->get()->toArray();

//            foreach ($msg as $subj){
//                mydd($subj->body->id);
//            }

            if (count($msg) > 0) return "color:red!important";
            else return "color:#a9afbb";
        }
    }


    if (!function_exists('msgReadAdmin')) {
        function msgReadAdmin($belongs_to){

            $msg = \App\ModelMessagesBody::where('belongs_to', $belongs_to)->where('to_id','admin')->whereNull('readtime')->get();
            if (count($msg) > 0) return 'bold';
            else return 'normal';
        }
    }

    if (!function_exists('updateReadAdmin')) {
        function updateReadAdmin($belongs_to){

            $msg = \App\ModelMessagesBody::where('belongs_to', $belongs_to)->where('to_id','admin')->whereNull('readtime')->update(['readtime' => now()]);

        }
    }

    if (!function_exists('getSalesForInterval')) {
        function getSalesForInterval($all, $startDate, $endDate, $modelStudio)
        {

            $allChuncks = [];

            $allModelsData = [];

            $totalEarnings = 0;
            $totalWorkTime = 0;
            $totalWorkNeeded = 0;
            $countTotal = 0;
            $from = Carbon::parse($startDate)->format('Y-m-d H:i:s');
            $to = Carbon::parse($endDate)->format('Y-m-d H:i:s');

            foreach ($all as $k => $v) {

                $chuncks = array_chunk($all[$k], 90);

                $countTotal += count($all[$k]);
                if ($k != "key_missing") $allChuncks[$k] = $chuncks;

            }

            foreach ($allChuncks as $key => $allmodels) {
                sleep(0.5);

                foreach ($allmodels as $models) {
                    $val = getSalesInterval($models, $key, $startDate, $endDate);

                    foreach ($val as $mod => $dat) {
                        $allModelsData[$mod] = $dat;

                        if ( array_key_exists($mod,$modelStudio) && array_key_exists('studio',$modelStudio[$mod])){

                            $modelStudio[$mod] = [
                                "studio" => $modelStudio[$mod]["studio"],
                                "earnings" => (double) $dat["earnings"],
                                "worktime" => $dat["worktime"],
                                "worktimeneeded" => $dat["worktimeneeded"],
                            ];
                            $totalEarnings += (double) $dat["earnings"];
                            $totalWorkTime += $dat["worktime"];
                            $totalWorkNeeded += $dat["worktimeneeded"];
                        }


                    }
                }
            }

            $countFound = count($allModelsData);

            $total = [
                "earnings" => (double) $totalEarnings,
                "worktime" => $totalWorkTime,
                "worktimeneeded" => $totalWorkNeeded,
                "count" => $countFound,
                "totalcount" => $countTotal,
                "modelstudio" => $modelStudio,
                "from" => $from,
                "to" => $to
            ];

            return $total;

        }
    }

    if (!function_exists('getSalesInterval')) {
        function getSalesInterval($models, $key, $startDate, $endDate)
    {

        $modelsWithData = [];

        $startDate = formatDateWithHour($startDate);
        $endDate = formatDateWithHour($endDate);

        $screenNames = null;
        foreach ($models as $model) {
            $screenNames .= '&screenNames[]=' . $model["modelname"];
        }

        //$reports = "&reports[]=general-overview&reports[]=earnings-overview&reports[]=working-time";
        $reports = "&reports[]=general-overview&reports[]=working-time";
        //$reports = "&reports[]=general-overview";

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=" . $startDate . "&toDate=" . $endDate . $screenNames . $reports;
        $res = curlAuth($key, $url);

        if (array_key_exists('data', $res)) {
            $dataModels = $res['data'];

            foreach ($dataModels as $dataModel) {

                $modelSalesAndTime = processData($dataModel);
                $modelsWithData[$modelSalesAndTime["modelname"]] = [
                    'earnings' => $modelSalesAndTime["earnings"],
                    'worktime' => $modelSalesAndTime["worktime"],
                    'worktimeneeded' => $modelSalesAndTime["worktimeneeded"],
                    'from' => $modelSalesAndTime["from"],
                    'to' => $modelSalesAndTime["to"]
                ];

            }
        }

        return $modelsWithData;
    }
    }

    if (!function_exists('processData')) {

        function processData($data)
        {
            $earnings = 0;
            $workTime = 0;
            $workTimeNeeded = 0;
            $modelname = null;
            $from = '2000-01-01T00:00:00+01:00';
            $to = '2000-01-01T00:00:00+01:00';

            if (array_key_exists('date', $data)) {
                $date = $data['date'];
                if (array_key_exists('from', $date) && array_key_exists('to', $date)) {
                    $from = $date["from"];
                    $to = $date["to"];
                }
            }

            if (array_key_exists('screenName', $data)) {
                $modelname = $data['screenName'];
                $total = $data['total'];

                if (array_key_exists('earnings', $total)) {
                    $earn = $total['earnings'];
                    if (array_key_exists('value', $earn)) {
                        $earnings = $earn['value'];
                        $earnings = number_format(round($earnings, 2), 2, '.', '');
                    }
                }

                if (array_key_exists('workTime', $total)) {
                    $workingTime = $total['workTime'];
                    if (array_key_exists('value', $workingTime)) $workTime += $workingTime['value'];
                }

                if (array_key_exists('workingTime', $data)) {
                    $wTime = $data['workingTime'];

                    //vip_show, pre_vip_show, video_call, private, free, member
                    if(array_key_exists('vip_show', $wTime)) $workTimeNeeded += $wTime['vip_show']['value'];
                    if(array_key_exists('pre_vip_show', $wTime)) $workTimeNeeded += $wTime['pre_vip_show']['value'];
                    if(array_key_exists('private', $wTime)) $workTimeNeeded += $wTime['private']['value'];
                    if(array_key_exists('free', $wTime)) $workTimeNeeded += $wTime['free']['value'];
                }


            }
            return ["modelname" => $modelname, "earnings" => $earnings, "worktime" => $workTime, "worktimeneeded" => $workTimeNeeded, "from" => $from, "to" => $to];
        }
    }

    if (!function_exists('calcTotal')) {

        function calcTotal($modelstd, $studioModel)
        {

            $totalEarnings = 0;
            $totalWorktime = 0;
            $totalWorktimeNeeded = 0;

            foreach ($modelstd as $ms) {

                if (array_key_exists('studio', $ms) && array_key_exists('earnings', $ms) && array_key_exists('worktime', $ms)) {
                    if (array_key_exists($ms['studio'], $studioModel)) {
                        $studioModel[$ms['studio']]["earnings"] += $ms['earnings'];
                        $studioModel[$ms['studio']]["worktime"] += $ms['worktime'];
                        $studioModel[$ms['studio']]["worktimeneeded"] += $ms['worktimeneeded'];
                        $totalEarnings += $ms['earnings'];
                        $totalWorktime += $ms['worktime'];
                        $totalWorktimeNeeded += $ms['worktimeneeded'];
                    } else {
                        $studioModel[$ms['studio']]["earnings"] = $ms['earnings'];
                        $studioModel[$ms['studio']]["worktime"] = $ms['worktime'];
                        $studioModel[$ms['studio']]["worktimeneeded"] = $ms['worktimeneeded'];
                        $totalEarnings += $ms['earnings'];
                        $totalWorktime += $ms['worktime'];
                        $totalWorktimeNeeded += $ms['worktimeneeded'];
                    }
                }
            }

            $studioModel[0]["earnings"] = $totalEarnings;
            $studioModel[0]["worktime"] = $totalWorktime;
            $studioModel[0]["worktimeneeded"] = $totalWorktimeNeeded;


            return $studioModel;
        }
    }

    if (!function_exists('insertInDbYdayvVsTday')) {

        function insertInDbYdayvVsTday($yesterdayData, $todayData)
        {

            //"dataByModels" => array:334 [▶]
            //    "MellAnyass" => array:4 [▼
            //      "studio" => 15
            //      "earnings" => 1.92
            //      "worktime" => 173
            //      "worktimeneeded" => 0
            //    ]
            //"dataByStudios" => array:23 [▶]
            //  21 => array:3 [ …3]
            //  4 => array:3 [ …3]
            //  24 => array:3 [ …3]
            //  0 => array:3 [ …3]
            //"count" => 334
            //"totalcount" => 335
            //"from" => "2020-01-15 00:00:00"
            //"to" => "2020-01-15 12:10:46"

            $yesdaySales = $yesterdayData['dataByStudios'];
            $todaySales = $todayData['dataByStudios'];

            $yesday_from = $yesterdayData['from'];
            $yesday_to = $yesterdayData['to'];
            $today_from = $todayData['from'];
            $today_to = $todayData['to'];

            foreach ($todaySales as $studio => $data) {

                $sales = StatsSalesTodayYday::updateOrCreate(
                    ['studio_id' => $studio],
                    [
                        'today_sales' => $data["earnings"],
                        'today_worktime' => $data["worktime"],
                        'tday_from' => $today_from,
                        'tday_to' => $today_to,
                        'yday_sales' => $yesdaySales[$studio]["earnings"],
                        'yday_worktime' => $yesdaySales[$studio]["worktime"],
                        'yday_from' => $yesday_from,
                        'yday_to' => $yesday_to
                    ]
                );

            }

        }
    }

    if (!function_exists('insertInDbMonthSales')) {

        function insertInDbMonthSales($sales, $month)
        {

            //"dataByModels" => array:334 [▶]
            //    "MellAnyass" => array:4 [▼
            //      "studio" => 15
            //      "earnings" => 1.92
            //      "worktime" => 173
            //      "worktimeneeded" => 0
            //    ]
            //"dataByStudios" => array:23 [▶]
            //  21 => array:3 [ …3]
            //  4 => array:3 [ …3]
            //  24 => array:3 [ …3]
            //  0 => array:3 [ …3]
            //"count" => 334
            //"totalcount" => 335
            //"from" => "2020-01-15 00:00:00"
            //"to" => "2020-01-15 12:10:46"

            $yesdaySales = $sales['dataByStudios'];


            foreach ($yesdaySales as $studio => $data) {

                $sales = StatsSalesMonth::updateOrCreate(
                    ['studio_id' => $studio, 'month' => $month],
                    [
                        'earnings' => $data["earnings"],
                        'worktime' => $data["worktime"],
                        'worktimeneeded' => $data["worktimeneeded"],
                    ]
                );

            }

        }
    }


    if (!function_exists('insertInDbPeriodSales')) {

        function insertInDbPeriodSales($sales)
        {
            //"prevPeriod" => array:6 [▶]
            //"thisPeriod" => array:6 [▶]
                //"dataByModels" => array:334 [▶]
                //    "MellAnyass" => array:4 [▼
                //      "studio" => 15
                //      "earnings" => 1.92
                //      "worktime" => 173
                //      "worktimeneeded" => 0
                //    ]
                //"dataByStudios" => array:23 [▶]
                //  21 => array:3 [ …3]
                //  4 => array:3 [ …3]
                //  24 => array:3 [ …3]
                //  0 => array:3 [ …3]
                //"count" => 334
                //"totalcount" => 335
                //"from" => "2020-01-15 00:00:00"
                //"to" => "2020-01-15 12:10:46"

            //$prevPeriod = $sales['prevPeriod'];
            //$thisPeriod = $sales['thisPeriod'];

            foreach($sales as $period_name => $period_sales){
                $p_sales = $period_sales["dataByStudios"];
                foreach ($p_sales as $studio => $data) {

                    $sales = StatsSalesPeriod::updateOrCreate(
                        ['studio_id' => $studio, 'name' => $period_name],
                        [
                            'earnings' => $data["earnings"],
                            'worktime' => $data["worktime"],
                            'worktimeneeded' => $data["worktimeneeded"],
                            'from_date' => $period_sales["from"],
                            'to_date' => $period_sales["to"],
                        ]
                    );

                }

            }

        }
    }


    if (!function_exists('getCurrentModel')) {

        function getCurrentModel(){

            $model = Auth::guard('model')->user()->id;
            return $model;
        }
    }



    //------------ helpers for sms -----------------------

    if (!function_exists('sendSmsToNotify')) {
         function sendSmsToNotify($studio, $phone, $message){

            $studioPhoneVerification = Studio::where('id',$studio)->first()->phone_verification;

            if($studioPhoneVerification){

                $client = new Client();
                $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to='. $phone .'&text=' . $message);
                //echo $res->getStatusCode();
            }
        }
    }



    //------------ helpers for sync tables -----------------------

    //find old id -> new id models
    if (!function_exists('finModelId')) {

        function finModelId($old_id){

            $new_id = null;

            $oldModel = Oldmodels::find($old_id);
            $modelname = ($oldModel) ? $oldModel->Modelname : null;

            if ($modelname){

                $newModel = Models::where('modelname', $modelname)->first();
                if ($newModel) $new_id = $newModel->id;
            }

            return $new_id;

        }
    }


    if (!function_exists('newFunc')) {

        function newFunc(){

            $token = '406ad45b5ed5748abb871cc99ef69bc45d0ad2779f9034f91de5af254f8a9902';
            //$token = '8c23e7366a3e7096a30914edb145efc95fe241cd5be682a5ec1a366ca01e6c70';

            $fromDate = '2020-01-01';
            $toDate = '2020-01-01';

            $models = Models::limit(1)->get()->pluck('modelname')->toArray();

            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept'        => 'application/json',
                'debug' => true
            ];

            $names = array('screenNames[]' => array("MellAnyass","AdaniaBelle"));
            $queryNames = http_build_query($names, null, '&');
            $stringNames = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $queryNames);

            $reports = array('reports[]' => array("general-overview", "working-time"));
            $queryReports = http_build_query($reports, null, '&');
            $stringReports = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $queryReports);


            $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=$fromDate&toDate=$toDate&".$stringNames."&".$stringReports;



            $client = new Client(['base_uri' => $url]);

            try {

                $response = $client->request('GET', '', [
                    'headers' => $headers
                ]);

                // Here the code for successful request
                $res = json_decode($response->getBody(), true);

            } catch (RequestException $e) {

                // Catch all 4XX errors

                // To catch exactly error 400 use
                if ($e->getResponse()->getStatusCode() == '400') {
                    //echo "Got response 400";
                }

                // You can check for whatever error status code you need

            } catch (\Exception $e) {

                // There was another exception.

            }

        }
    }


}



?>