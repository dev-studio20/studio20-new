<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelNewPeriod extends Model

{

    //protected $connection = 'mysql_hetzner';

    protected $table = 'model_periods';

    protected $guarded = [];

}
