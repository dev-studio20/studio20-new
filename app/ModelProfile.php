<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelProfile extends Model

{

    protected $table = 'model_profiles';

    protected $fillable = [
        'model_id',
        'first_name',
        'last_name',
        'phonenumber',
        'birthdate',
        'sex'
    ];

}
