<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelLog extends Model

{

    protected $table = 'log';

    protected $fillable = [
        'username',
        'ip',
        'rank',
        'action',
        'time'
    ];

}
