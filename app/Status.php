<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Status extends Model

{

    protected $table = 'model_status';

    protected $fillable = [
        'name'
    ];

}
