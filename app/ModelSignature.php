<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelSignature extends Model

{

    protected $table = 'model_signature';

    protected $fillable = [
        'model_id',
        'model_signature',
    ];

}
