<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelStories extends Model

{

    protected $table = 'model_stories';

    protected $guarded = [];

}
