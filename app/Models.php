<?php

namespace App;

use App\Notifications\ModelResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Models extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'models';

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ModelResetPasswordNotification($token));
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\ModelProfile', 'model_id');
    }

    public function studios(){
        return $this->hasOne('App\Studio', 'id', 'studio');
    }

    public function statusName(){
        return $this->hasOne('App\ModelStatus', 'id', 'status');
    }

    public function address(){
        return $this->hasOne('App\ModelAddress', 'model_id');
    }

    public function paymentDetails(){
        return $this->hasOne('App\ModelPaymentDetail', 'model_id');
    }

    public function social(){
        return $this->hasOne('App\ModelSocial', 'model_id');
    }

    public function contract(){
        return $this->hasOne('App\ModelContract', 'model_id');
    }

    public function key(){
        return $this->hasOne('App\ApiKeys', 'id', 'key_id');
    }

    public function account(){
        return $this->hasOne('App\ModelAccount', 'model_id');
    }

    public function signature(){
        return $this->hasOne('App\ModelSignature', 'model_id');
    }

    public function feedback(){
        return $this->hasMany('App\ModelFeedback', 'model_id');
    }

    public function periods(){
        return $this->hasMany('App\ModelPeriod', 'model_id')->orderBy('year','ASC')->orderBy('period','ASC');
    }

    public function paymentHistory(){
        return $this->hasMany('App\ModelPaymentHistory', 'model_id');
    }


}
