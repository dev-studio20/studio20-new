<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageCreateRequest extends FormRequest
{

    protected $rules = [
        'subject' => 'bail|required|max:200|min:2',
        'message' => 'bail|required|max:1000|min:2',
        //'uploadFile.*' => 'nullable|max:62500000',
    ];


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'subject.required' => 'A subject is required',
            'subject.min'  => 'Subject requires minimum 2 characters',
            'subject.max'  => 'Subject requires maximum 200 characters ',
            'message.required' => 'A message is required',
            'message.min'  => 'Message requires 2 characters',
            'message.max'  => 'Message requires maximum 1000 characters',
            'uploadFile.*.required' => 'Please upload an image',
            'uploadFile.*.max' => 'File must not be larger than 50 MB',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return $this->rules;

    }
}
