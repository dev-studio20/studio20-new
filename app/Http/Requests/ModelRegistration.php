<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModelRegistration extends FormRequest
{

    protected $rules = [
        'name' => 'bail|required|string|regex:/^[a-zA-Z \-]+$/u|max:222|min:2',
        'lname' => 'bail|required|string|regex:/^[a-zA-Z \-]+$/u|max:222|min:2',
        'email' => 'bail|required|unique:models|email|max:222',
        'password' => ['required',
            'min:8',
            'regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+-=]).*$/',
            'confirmed'],
        'registerModelName1' => 'bail|required|max:200|min:3',
        'registerModelName2' => 'nullable|max:200|min:1',
        'registerModelName3' => 'nullable|max:200|min:1',
        'realregisterphoneprefix' => 'bail|required',
        'registerphone' => 'bail|required|max:25|min:7',

        'registeridn' => 'required|max:77|min:1',
        'registerIban' => 'required|max:100|min:1',
        'registerBank' => 'required|max:77|min:1',
//        'registerBankAddress' => 'required|max:200|min:1',
        'registerAddress' => 'max:200|min:1',
    ];


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

//        $before = \Carbon\Carbon::now()->subYears(18)->format('m/d/Y');
//
//        return [
//            'model_first_name' => 'bail|required|string|regex:/^[a-zA-Z]+$/u|max:222|min:2',
//            'model_last_name' => 'bail|required|string|regex:/^[a-zA-Z]+$/u|max:222|min:2',
//            'email' => 'bail|required|unique:models|email|max:222',
//            'password' => ['required',
//                'min:8',
//                'regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+-=]).*$/',
//                'confirmed'],
//            'birthdate' => 'bail|required|date_format:Y-m-d|before:'.$before,
//            'sex' => 'required',
//        ];

        $before = \Carbon\Carbon::now()->subYears(18)->format('m/d/Y');

        $file_field1 = ($this->hasFile('front1')) ? 'front1' : 'front2';
        $file_field2 = ($this->hasFile('faceId1')) ? 'faceId1' : 'faceId2';

        $this->rules['birthdate'] = 'bail|required|date_format:Y-m-d|before:'.$before;

        $this->rules[$file_field1] = 'required';
        $this->rules[$file_field2] = 'required';

        return $this->rules;

    }
}
