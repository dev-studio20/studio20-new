<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{

    protected $rules = [
        'message' => 'bail|required|max:500|min:2',
        //'uploadFile[]' => 'file',
    ];


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'message.required' => 'A message is required',
            'message.min'  => 'Minimum 2 characters are required',
            'message.max'  => 'Maximum 500 characters are required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return $this->rules;

    }
}
