<?php

namespace App\Http\Controllers;

use App\APIKeys;
use App\Models;
use App\ModelSale;
use App\ModelPeriod;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;


class JasminApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return '';

    }

    public function getSalesPeriod($modelname, $year, $period){

        $days = getArrayDaysPeriod($year, $period);

        foreach($days as $day){
            //should be with dispatch job
            $this->getSalesDay($modelname, $day);
            sleep(1);
        }

        return 'done';

    }

    public function getSalesDay(Request $request){

        if ($request->has('modelname') && $request->has('date')){
            $modelname = $request->get('modelname');
            $day = $request->get('date');

            $fromDate = formatDate($day);
            $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
            $toDate = formatDate($nextday);

            $reports = "&reports[]=general-overview&reports[]=working-time";

            $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$fromDate."&toDate=".$toDate."&screenNames[]=".$modelname.$reports;
            //$url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=" . $startDate . "&toDate=" . $endDate . $screenNames . $reports;

            $model = Models::where('modelname', $modelname)->first();

            $auth = APIKeys::where('id', $model->key_id)->first()->bearer;

            $earnings = 0;
            $workTime = 0;

            if ($auth) {

                $res = $this->curlAuth($auth, $url);

                if (array_key_exists('data', $res)){
                    $data = $res['data'][0];

                    if(array_key_exists('screenName', $data)){
                        $total =  $data['total'];
                        if(array_key_exists('earnings', $total)){
                            $earn = $total['earnings'];
                            if(array_key_exists('value', $earn)){
                                $earnings = $earn['value'];
                                $earnings = number_format(round($earnings,2), 2, '.', '');
                            }
                        }

                        if(array_key_exists('workingTime', $data)){
                            $work = $data['workingTime'];

                            //vip_show, pre_vip_show, video_call, private, free, member
                            if(array_key_exists('vip_show', $work)) $workTime += $work['vip_show']['value'];
                            if(array_key_exists('pre_vip_show', $work)) $workTime += $work['pre_vip_show']['value'];
                            if(array_key_exists('private', $work)) $workTime += $work['private']['value'];
                            if(array_key_exists('free', $work)) $workTime += $work['free']['value'];

                        }
                    }
                }

                $response = [
                    "screenName" => $modelname,
                    "day" => $day,
                    "earnings" => $earnings,
                    "workTime" => $workTime,
                    "res" => $res,
                    "success" => true
                ];


            } else $response = ['success' => false, 'error' => 'api key error..'];



            return response()->json($response);

        }



    }

    function curlAuth($auth, $url){

        $authorization = $auth;
        $cSession = curl_init();

        //step2
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($cSession);
        curl_close($cSession);
        $res = json_decode($result, true);

        return $res;

    }


}
