<?php
namespace App\Http\Controllers\Api;
use App\APIKeys;
use App\CustomClass\ManageSales;
use App\CustomClass\Stories;
use App\Models;
use App\ModelShifts;
use App\ModelstatusJasmin;
use App\Http\Controllers\Controller;
use App\ModelStories;
use App\Studio;
use Carbon\Carbon;

class TestController extends Controller
{

    public function login(){

        $apidomain = 'https://api.studio20.group/';
        $url_login = $apidomain . 'login';

        $auth = [
            'email' => "api@studio20.com",
            'password' => "Studio20!",
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_login,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($auth),
            CURLOPT_HTTPHEADER => array(
                // Set here requred headers
                "accept: */*",
                "accept-language: en-US,en;q=0.8",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = json_decode($response);

            $token = $res->success->token;
            return $token;
        }


    }

    public function testapidetails(){

        $apidomain = 'https://api.studio20.group/';

        $url_allmodels = $apidomain . 'allmodels';
        $url_modelsapproved = $apidomain . 'modelsapproved';
        $url_stories = $apidomain . 'stories';

        $token = $this->login();

        $cSession = curl_init();
        $url = $url_allmodels;

        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: Bearer ".$token));
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
        $resCurl = curl_exec($cSession);
        curl_close($cSession);
        $res = json_decode($resCurl, true);

        return $res;


    }

    public function showmodels(){

        $studios = Studio::get()->pluck('name', 'id')->toArray();
        $all = [];
        $output ="<root>";

        foreach($studios as $id => $name){
            $models = Models::whereStatus(1)->whereStudio($id)->pluck('modelname')->toArray();
            $all[$name] = $models;
            $output .="<studio>".$name;
            foreach($models as $model){
                $output .= "<name>".$model."</name>";
            }
            $output .= "</studio>";
        }
        $output .= "</root>";

        header('Content-Type: application/xml');
        print ($output);

    }

    public function salesget(){

        ManageSales::statsSalesPeriodComparison();


    }

    public function sales(){

        $month = Carbon::now()->format('Y-m');

        $startDate = Carbon::parse($month)->format('Y-m-d H:i:s');

        $endDate = Carbon::parse($startDate)->addMonth()->format('Y-m-d H:i:s');


        $monthSales = (getSalesAndTimeInterval($startDate, $endDate));

        //dd($month);
        //dd($monthSales);
        insertInDbMonthSales($monthSales, $month);

    }

    public function story(){

        $all  = Models::whereStatus(1)->get();

        $models = $all->pluck('modelname')->toArray();

        $modelsChunk = array_chunk($models, 90);

        $authArr = ["Authorization: Bearer 406ad45b5ed5748abb871cc99ef69bc45d0ad2779f9034f91de5af254f8a9902", "Authorization: Bearer 8c23e7366a3e7096a30914edb145efc95fe241cd5be682a5ec1a366ca01e6c70"];

        $url_base ="https://partner-api.modelcenter.jasmin.com/v1/my-stories?";
        $allStories = [];

        foreach ($authArr as $auth){
            foreach ($modelsChunk as $chunck){

                $screenNames = implode('&screenNames[]=',$chunck);
                $url = $url_base.$screenNames;

                $cSession = curl_init();
                curl_setopt($cSession, CURLOPT_URL, $url);
                curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
                curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
                $result = curl_exec($cSession);
                curl_close($cSession);
                $res = json_decode($result, true);

                if (array_key_exists("data", $res)){
                    $data = $res["data"];
                    foreach($data as $screen){
                        if (array_key_exists("screenName", $screen)){
                            $screenName = $screen["screenName"];
                            //clean db
                            ModelStories::whereModelname($screenName)->delete();

                            if (array_key_exists("stories", $screen)){
                                if (count($screen["stories"]) > 0) {
                                    $stories = $screen["stories"][0];

                                    $allStories[$screenName] = $stories;
                                    $id = (array_key_exists("id", $stories)) ? $stories["id"] : 0;
                                    $status = (array_key_exists("status", $stories)) ? $stories["status"] : '--';
                                    $price = (array_key_exists("price", $stories)) ? $stories["price"] : 0;
                                    $items = (array_key_exists("items", $stories)) ? $stories["items"] : null;
                                    if(count($items)>0) {
                                        foreach($items as $item){
                                            $story_id = (array_key_exists("id", $item)) ? $item["id"] : 0;
                                            $story_status = (array_key_exists("status", $item)) ? $item["status"] : '--';
                                            $story_type = (array_key_exists("type", $item)) ? $item["type"] : '--';
                                            $story_privacy = (array_key_exists("privacy", $item)) ? $item["privacy"] : '--';
                                            $story_expiresAt = (array_key_exists("expiresAt", $item)) ? $item["expiresAt"] : now()->format('Y-m-d H:i:s');
                                            $story_media = (array_key_exists("media", $item)) ? $item["media"] : [];

                                            $dateFormat = Carbon::parse($story_expiresAt)->setTimezone('UTC')->format('Y-m-d H:i:s');

                                            //insert in db..
                                            $ms = ModelStories::updateOrCreate([
                                                "modelname" => $screenName,
                                                "story_id" => $id,
                                                "stories_id" => $story_id,

                                            ],[
                                                "status" => $status,
                                                "price" => $price,
                                                "count" => count($items),
                                                "stories_status" => $story_status,
                                                "stories_type" => $story_type,
                                                "stories_privacy" => $story_privacy,
                                                "stories_media" => json_encode($story_media),
                                                "stories_expire" => $dateFormat
                                            ]);

                                        }
                                    }
                                }
                            }
                        }
                    }
                    sleep(1);
                }

            }
            sleep(1);
        }

        //return $models;
    }

    public function index(){





        $al = ModelstatusJasmin::all();
        foreach($al as $a){

            $findst = Models::where('modelname', $a->modelname)->first();
            //$a->studio_id = $findst->studio;
            $a->model_id = $findst->id;
            $a->save();
        }

        dd('here');

//        $mm = [];
//        $allmodels = [];
//
//        $models = Models::whereStatus(1)->select('modelname', 'key_id')->get();
//
//        $s_models = $models->groupBy('key_id')->toArray();
//
//        foreach($s_models as $key => $val){
//            $key = ($key !== '') ? $key : 0;
//            foreach($val as $mod){
//                $mm[$key][] = $mod["modelname"];
//            }
//        }
//
//        foreach($mm as $key => $models){
//            $cmodels = $this->chunk($key, $models);
//            $allmodels = array_merge($allmodels, $cmodels);
//        }
//
//        $this->insertShift($allmodels);

//        foreach ($allmodels as $mod => $status){
//
//            ModelstatusJasmin::insert(["modelname" => $mod, "status" => $status]);
//        }



//        return response()->json([
//            'success' => true,
//            'models' => $allmodels,
//        ]);

    }

    function insertShift($models){

        $stop = 4 * 3600; //4h mark as shift end

        foreach ($models as $model => $status){

            $res = ModelstatusJasmin::whereModelname($model)->orderBy('start', 'DESC')->first();
            if ($res) {
                //if status other ->insert new status row
                if($res->status !== $status) ModelstatusJasmin::insert(['modelname' => $model, "shift_start" => $res->shift_start, 'start' => time(), 'end' => time(), 'status' => $status]);
                else {
                    //if status offline -> check total offline
                    if ( ($res->status == 'offline') && ((time() - $res->start) > $stop) ){
                        // move to new table , remove all records of model from this table
                        ModelShifts::insert(["modelname" => $res->modelname, "start_time" => $res->shift_start, "end_time" => time()]);
                        ModelstatusJasmin::whereModelname($model)->delete();

                    } else $res->update(['end' => time(), 'status_total' => (time() - $res->start)]);
                }



            } elseif($status !== 'offline') ModelstatusJasmin::insert(['modelname' => $model, 'start' => time(), 'end' => time(), 'status' => $status]);





        }

    }

    function chunk($key, $arrayModels){

        $modelsAll = [];

        //split into chunks and get status from jasmin
        $modelsChunk = array_chunk($arrayModels, 60);

        foreach($modelsChunk as $models){
            if(!$key) $models = $this->getModelsStatusWithoutKey($models);
            else $models = $this->getModelsStatusWithKey($key, $models, 1);
            sleep(1);

            $modelsAll = array_merge($modelsAll, $models);
        }

        return $modelsAll;

    }

    public function getModelsStatusWithKey($key_id, $models, $hasKey){

        $modelStatus = [];

        $key = APIKeys::find($key_id)->bearer;

        $screenNames = "screenNames[]=" . implode('&screenNames[]=', $models);
        $url = "https://partner-api.modelcenter.jasmin.com/v1/performer-states?".$screenNames;

        $response = $this->curlAuth($key, $url);
        $res = $response["res"];
        $xrate = $response["X-RateLimit-Remaining"];

        if (array_key_exists('data', $res)){
            foreach($res['data'] as $model){
                $modelStatus[$model["screenName"]] = $model["state"];
                //update model api_key
                if(!$hasKey){
                    Models::where('modelname', $model["screenName"])->update(['key_id' => $key_id]);
                }

            }
        }

        return $modelStatus;

    }

    public function getModelsStatusWithoutKey($models){

        $models1 = $this->getModelsStatusWithKey(1, $models, 0);

        sleep(1);

        $models2 = $this->getModelsStatusWithKey(2, $models, 0);

        sleep(1);

        return array_merge($models1, $models2);
    }

    public function curlAuth($auth, $url){

        sleep(1);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        curl_close($ch);

        //check the header
        $array_header = $this->decodeHeader($header);

        //X-Ratelimit-Remaining
        $ratelimit = ( isset($array_header["X-RateLimit-Remaining"]) ) ? ($array_header["X-RateLimit-Remaining"] * (-1)) : 0;

        $res = json_decode($body, true);

        return ["X-RateLimit-Remaining" => $ratelimit, "res" => $res, "response" => $response];

    }

    public function decodeHeader($header){
        //process in one line units, and treat ":” as a delimiter
        $result = array();
        foreach (explode("\n", $header) as $i=>$line) {
            $temp = explode(":",$line);
            $temp = array_map('trim',$temp);  //trim each element
            if ( isset($temp[0]) and isset($temp[1]) ){
                // process only the data separated by ”:”
                $result[$temp[0]] = $temp[1];
            }
        }
        return $result;
    }



}