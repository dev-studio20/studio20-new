<?php
namespace App\Http\Controllers\Api;
use App\APIKeys;
use App\Models;
use App\ModelShifts;
use App\ModelstatusJasmin;
use App\Http\Controllers\Controller;
use App\ModelStories;
use Carbon\Carbon;

class StatusController extends Controller
{

   public function index(){

        $mm = [];
        $allmodels = [];

        $models = Models::whereStatus(1)->select('id','studio','modelname', 'key_id')->get();

        $s_models = $models->groupBy('key_id')->toArray();
        $my_models = $models->groupBy('modelname')->toArray();

        foreach($s_models as $key => $val){
            $key = ($key !== '') ? $key : 0;
            foreach($val as $mod){
                $mm[$key][] = $mod["modelname"];
            }
        }

        foreach($mm as $key => $models){
            $cmodels = $this->chunk($key, $models);
            $allmodels = array_merge($allmodels, $cmodels);
        }

        foreach ($allmodels as $model => $value){
            if(array_key_exists($model, $my_models)){
                $my_models[$model] = ["status" => $value, "id" => $my_models[$model][0]["id"], "studio" => $my_models[$model][0]["studio"]];
            }
        }

        $this->insertShift($my_models);

        $last_update =  ModelstatusJasmin::orderBy('updated_at', 'DESC')->first()->updated_at;

        $allstatus = ModelstatusJasmin::with('studios')->where('updated_at', $last_update)->get()->toArray();

        return response()->json([
            'success' => true,
            'models' => $allstatus,
        ]);

    }

    function insertShift($models){

        ini_set('max_execution_time', 3000);

        $stop = 4 * 3600; //4h mark as shift end

        foreach ($models as $model => $value){

            if(array_key_exists("status", $value)){
                $res = ModelstatusJasmin::whereModelname($model)->orderBy('start', 'DESC')->first();
                if ($res) {
                    //if status other ->insert new status row
                    if($res->status !== $value["status"]) {
                        ModelstatusJasmin::insert(['modelname' => $model, 'model_id' => $value["id"], 'studio_id' => $value["studio"], "shift_start" => $res->shift_start, 'start' => time(), 'end' => time(), 'status' => $value["status"]]);
                    }
                    else {
                        //if status offline -> check total offline
                        if ( ($res->status == 'offline') && ((time() - $res->start) > $stop) ){
                            //get sales
                            $model = $res->modelname;
                            $startTime = $res->shift_start;
                            $endTime = Carbon::now()->format('Y-m-d H:i:s');

                            $format_start_date = formatDateWithHour($startTime);
                            $format_end_date = formatDateWithHour($endTime);

                            $earnings = $this->getModelShiftSales($model, $format_start_date, $format_end_date);

                            // move to new table , remove all records of model from this table
                            ModelShifts::insert(["modelname" => $res->modelname, "start_time" => $res->shift_start, "end_time" => time(), "earnings" => $earnings]);
                            ModelstatusJasmin::whereModelname($model)->delete();

                        } else $res->update(['end' => time(), 'status_total' => (time() - $res->start)]);
                    }



                } elseif($value["status"] !== 'offline') {
                    ModelstatusJasmin::insert(['modelname' => $model, 'model_id' => $value["id"], 'studio_id' => $value["studio"], 'start' => time(), 'end' => time(), 'status' => $value["status"]]);
                }
            }


        }

    }

    public function getModelShiftSales($model, $start, $end){

        $keys = ["","Authorization: Bearer 8c23e7366a3e7096a30914edb145efc95fe241cd5be682a5ec1a366ca01e6c70", "Authorization: Bearer 406ad45b5ed5748abb871cc99ef69bc45d0ad2779f9034f91de5af254f8a9902"];
        $key = Models::whereModelname($model)->first();

        if ($key && $key->key_id){
            $key = $keys[$key->key_id];

            $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$start."&toDate=".$end."&screenNames[]=".$model."&reports[]=general-overview";

            $res = curlAuth($key, $url);
            if (array_key_exists('data', $res)){

                $data = $res['data'][0];
                if (array_key_exists('screenName', $data)){
                    $total = $data["total"];
                    $earnings = $total["earnings"];
                    $value = $earnings["value"];
                    return $value;
                }

            }
        }


        return 0.00;

    }

    function chunk($key, $arrayModels){

        $modelsAll = [];

        //split into chunks and get status from jasmin
        $modelsChunk = array_chunk($arrayModels, 60);

        foreach($modelsChunk as $models){

            if(!$key) $models = $this->getModelsStatusWithoutKey($models);
            else $models = $this->getModelsStatusWithKey($key, $models, 1);
            sleep(1);

            $modelsAll = array_merge($modelsAll, $models);
        }

        return $modelsAll;

    }

    public function getModelsStatusWithKey($key_id, $models, $hasKey){

        $modelStatus = [];

        $key = APIKeys::find($key_id)->bearer;

        $screenNames = "screenNames[]=" . implode('&screenNames[]=', $models);
        $url = "https://partner-api.modelcenter.jasmin.com/v1/performer-states?".$screenNames;

        $response = $this->curlAuth($key, $url);
        $res = $response["res"];
        $xrate = $response["X-RateLimit-Remaining"];

        if (array_key_exists('data', $res)){
            foreach($res['data'] as $model){
                $modelStatus[$model["screenName"]] = $model["state"];
                //update model api_key
                if(!$hasKey){
                    Models::where('modelname', $model["screenName"])->update(['key_id' => $key_id]);
                }

            }
        }

        return $modelStatus;

    }

    public function getModelsStatusWithoutKey($models){

        $models1 = $this->getModelsStatusWithKey(1, $models, 0);

        sleep(1);

        $models2 = $this->getModelsStatusWithKey(2, $models, 0);

        sleep(1);

        return array_merge($models1, $models2);
    }

    public function curlAuth($auth, $url){

        sleep(1);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        curl_close($ch);

        //check the header
        $array_header = $this->decodeHeader($header);

        //X-Ratelimit-Remaining
        $ratelimit = ( isset($array_header["X-RateLimit-Remaining"]) ) ? ($array_header["X-RateLimit-Remaining"] * (-1)) : 0;

        $res = json_decode($body, true);

        return ["X-RateLimit-Remaining" => $ratelimit, "res" => $res, "response" => $response];

    }

    public function decodeHeader($header){
        //process in one line units, and treat ":” as a delimiter
        $result = array();
        foreach (explode("\n", $header) as $i=>$line) {
            $temp = explode(":",$line);
            $temp = array_map('trim',$temp);  //trim each element
            if ( isset($temp[0]) and isset($temp[1]) ){
                // process only the data separated by ”:”
                $result[$temp[0]] = $temp[1];
            }
        }
        return $result;
    }


    public function modelstatus(){


        $new = [];
        $models = Models::whereStatus(1)->pluck('modelname')->toArray();

        //get data from awe
        $res = $this->getData($models);

        //get avg from model_shifts
        foreach($res["models"] as $model){
            $modelname = $model["performerId"];

            $earn = ModelShifts::whereModelname($modelname)->pluck('earnings')->toArray();
            if ($earn) {
                $sum = round(array_sum($earn),2);
                $count = count($earn);
                $avg = round($sum/$count);
                $dataAvg = ["count" => $count, "earnings" => $sum, "avg" => $avg];
            } else $dataAvg = ["count" => 0, "earnings" => 0, "avg" => 0];
            $model["avg"] = $dataAvg;

            //models stories
            $ms = ModelStories::whereModelname($modelname)->get()->toArray();
            $studio = Models::with('studios')->whereModelname($modelname)->first();
            if ($studio) {
                $s_id = $studio->studios->id;
                $s_name = $studio->studios->name;
            }else {
                $s_id = 0;
                $s_name = 'error..';
            }

            $model["stories"] = $ms;
            $model["studios"] = ["id" => $s_id, "name" => $s_name];
            $new[] = $model;
        }

        $data["models"] = $new;
        $data["total"] = $res["total"];
        $data["found"] = $res["found"];

        return response()->json($data);

    }

    public function getData($models){

        $modelStatus2 = array();

        $countInit = count($models);

        $arrchuck = array_chunk($models, 70);

        foreach($arrchuck as $arr){
            $modelstring = '';
            foreach($arr as $ar){
                $modelstring .= $ar.',';
            }

            $cSession = curl_init();
            $url = "http://pt.ptawe.com/api/model/feed?siteId=gjasmin&psId=14noiembrie&psTool=213_1&psProgram=revs&campaignId=&category=girl&limit=10&imageSizes=320x180&imageType=glamour&showOffline=1&extendedDetails=1&responseFormat=json&performerId=".$modelstring."&subAffId={SUBAFFID}&accessKey=ea0f7bf083974543186e2abb1f8ac09c&legacyRedirect=1";

            curl_setopt($cSession, CURLOPT_URL, $url);
            curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
            $resCurl = curl_exec($cSession);
            curl_close($cSession);
            $res = json_decode($resCurl, true);

            if ( (isset($res['status'])) && ($res['status'] == 'OK')  && ($res['errorCode'] == 0)) {

                $modelStatus = $res['data']['models'];
                $modelStatus2 = array_merge($modelStatus,$modelStatus2);
            }
        }

        $countRes = count($modelStatus2);

        return ["models" => $modelStatus2, "total" => $countInit, "found" => $countRes];

    }

}