<?php
namespace App\Http\Controllers\Api;
use App\APIKeys;
use App\CustomClass\Stories;
use App\Models;
use App\ModelShifts;
use App\ModelstatusJasmin;
use App\Http\Controllers\Controller;
use App\ModelStories;
use Carbon\Carbon;

class ApiController extends Controller
{

    public function allModels(){

        $all = Models::with('studios','profile')->get();

        return response()->json(['data' => $all], 200);


    }

    public function modelsApproved(){

        $models_approved = Models::whereStatus(1)->get()->pluck('modelname');

        return response()->json(['data' => $models_approved], 200);


    }

    public function stories(){

        $stories = ModelStories::all();

        return response()->json(['data' => $stories], 200);


    }



}