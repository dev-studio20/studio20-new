<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\MailtrapExample;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendEmail;
use Carbon;
use DB;
use App\ModelPeriod;
use App\Sync\Olderiods;
use App\Sync\Syncid;

class TestmailController extends Controller
{

    public function index2(Request $request)
    {

        $emails = DB::table('test_mail_models')->get()->pluck('email')->toArray();

        foreach ($emails as $email){
            Mail::to($email)->send(new MailtrapExample());
        }




        //var_dump( Mail:: failures());

        return 'messages sent to Mailtrap!';
    }

    public function index(Request $request)
    {
        $emails = DB::table('test_mail_models')->limit(15)->get()->pluck('email')->toArray();
        foreach ($emails as $email){
            $details = ['email' => $email];
            //SendEmail::dispatch($details);
            $emailJob = (new      SendEmail($details));
            dispatch($emailJob);
            sleep(5);
        }


    }

    public function testhelper(){

            //truncate table
            ModelPeriod::query()->truncate();

            //get all ids fron sync id
            $ids = Syncid::all();
            foreach ($ids as $id) {

                $my_id = $id->my_id;
                $old_id = $id->old_id;

                $res = Olderiods::where('modelid', $old_id)->get();

                foreach($res as $period){

                    $modelPeriod = new ModelPeriod;
                    $modelPeriod->model_id = $my_id;
                    $modelPeriod->period = convertPeriodNameToValue($period->period);
                    $modelPeriod->year = $period->year;
                    $modelPeriod->total_amount = $period->totalamount;
                    $modelPeriod->total_amount_jasmin = $period->totalamount_jasmine;
                    $modelPeriod->hours = convertHoursToSeconds($period->hours);

                    $modelPeriod->save();
                }
            }

    }

}