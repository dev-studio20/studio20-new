<?php

namespace App\Http\Controllers\Admin;

use App\Events\SystemNotifications;
use App\Events\StudioNotifications;
use App\Events\AccountingNotifications;
use App\Events\AdminNotifications;
use App\Events\DevNotifications;
use App\Http\Controllers\Controller;
use App\ModelMessagesBody;
use App\ModelMessagesSubject;
use App\ModelSignature;
use App\Sync\Oldsales;
use App\Sync\Oldsignatures;
use Illuminate\Http\Request;
use App\Models;
use App\ModelSale;
use App\ModelPeriod;
use App\ModelStatus;
use App\ModelAccount;
use App\ModelContract;
use App\ModelPaymentDetail;
use App\ModelProfile;
use App\ModelReservation;
use App\ModelSocial;
use App\ModelAddress;
use App\ModelAnalytics;
use App\ModelPaymentHistory;
use App\State;
use App\City;
use App\Country;
use App\Sync\Oldmodels;
use App\Sync\Olduser;
use App\Sync\Oldstudios;
use App\Sync\Oldreservations;
use App\Sync\Oldperiods;
use App\Sync\Oldmessages;
use App\Sync\Oldanalytics;
use App\ModelMessages;
use App\Studio;

class SyncTaskController extends Controller
{

    public function index()
    {
        //$this->syncMessages();

        //dd(getRole());


        return view('Admin.sync.home');
    }



    public function syncModels(){


        $oldModels = Oldmodels::all();
        //truncate table models
        Models::query()->truncate();

        foreach($oldModels as $omodel){
            $oldUser = Olduser::find($omodel->id);

            $model = new Models();
            $model->id = $omodel->id;
            $model->modelname = $omodel->Modelname;
            $model->studio = ($omodel->studios_id == 0) ? 100 : $omodel->studios_id;
            $model->email = $omodel->Email;
            $model->password = $oldUser->password ?? '';
            $model->password_text = $omodel->Pass;
            $model->status = ModelStatus::where('name',$omodel->Status)->first()->id;
            $model->couple = $omodel->coupleacc;
            //$model->key_id = 0;
            $model->email_verified_at = $oldUser->email_verified_at ?? now(0);
            $model->phone_verified_at = $oldUser->phone_verified_at ?? now(0);
            $model->updated_at = $oldUser->updated_at ?? now(0);
            $model->created_at = $oldUser->created_at ?? now(0);
            $model->remember_token = $oldUser->remember_token ?? '';
            $model->save();

        }

        return ["success" => true];

    }

    public function syncAccounts(){

        //truncate table accounts
        ModelAccount::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $modelAccount = new ModelAccount;

            $modelAccount->model_id = $omodel->id;
            $modelAccount->type = $omodel->acctype;
            $modelAccount->company_name = $omodel->companyname;
            $modelAccount->company_administrator = $omodel->companyadministrator;
            $modelAccount->company_address = $omodel->companyaddress;
            $modelAccount->company_vat = $omodel->companyvat;
            $modelAccount->company_regcode = $omodel->companyregcode;

            $modelAccount->save();
        }

        return ["success" => true];

    }

    public function syncAddress(){

        //truncate table
        ModelAddress::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $modelAddress = new ModelAddress;
            $modelAddress->model_id = $omodel->id;
            $modelAddress->street = $omodel->Address;
            $address = $this->findCountryStateCityId($omodel->id, $omodel->City, $omodel->State, $omodel->Country);
            $modelAddress->city_id = $address['city_id'];
            $modelAddress->state_id = $address['state_id'];
            $modelAddress->country_id = $address['country_id'];

            $modelAddress->save();
        }

        return ["success" => true];
    }

    public function syncContract(){

        //truncate table
        ModelContract::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $oldUser = Olduser::find($omodel->id);

            $modelContract = new ModelContract;
            $modelContract->model_id = $omodel->id;

            $modelContract->pic1 = $omodel->Pic1;
            $modelContract->pic1_status = $oldUser->pic1_status ?? 0;
            $modelContract->reason_pic1 = $oldUser->reason_pic1_status;

            $modelContract->pic2 = $omodel->Pic2;
            $modelContract->pic2_status = $oldUser->pic2_status ?? 0;
            $modelContract->reason_pic2 = $oldUser->reason_pic2_status;

            $modelContract->pic3 = $omodel->Pic3;
            $modelContract->pic3_status = $oldUser->pic3_status ?? 0;
            $modelContract->reason_pic3 = $oldUser->reason_pic3_status;

            $modelContract->signed_at = $omodel->signupdate;
            $modelContract->reason = $omodel->reason;
            $modelContract->contractsigned = $omodel->contractsigned;

            $modelContract->save();
        }

        return ["success" => true];

    }

    public function syncPaymentDetails(){

        //truncate table
        ModelPaymentDetail::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $modelPaymentDetail = new ModelPaymentDetail;
            $modelPaymentDetail->model_id = $omodel->id;
            $modelPaymentDetail->idn = $omodel->IDN;
            $modelPaymentDetail->iban = $omodel->IBAN;
            $modelPaymentDetail->bank = $omodel->Bank;
            $modelPaymentDetail->bank_swift = $omodel->BankSWIFT;
            $modelPaymentDetail->bank_address = $omodel->BankAddress;
            $modelPaymentDetail->currency = $omodel->Currency;

            $modelPaymentDetail->save();
        }

        return ["success" => true];
    }

    public function syncProfile(){

        //truncate table
        ModelProfile::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $modelProfile = new ModelProfile;
            $modelProfile->model_id = $omodel->id;
            $modelProfile->first_name = $omodel->Name;
            $modelProfile->last_name = $omodel->Lname;
            $modelProfile->phonenumber = $omodel->phonenumber;

            $birth = $omodel->Birthdate;
            $b = explode('.',$birth);
            $new = $b[2]."-".$b[1]."-".$b[0];
            $modelProfile->birthdate = $new;

            $modelProfile->sex = $omodel->Sex;
            $modelProfile->last_status = $omodel->last_status;
            $modelProfile->last_update = $omodel->updated_at;
            $modelProfile->first_login = $omodel->first_login;

            $modelProfile->save();
        }

        return ["success" => true];

    }

    public function syncSocial()
    {
        //truncate table
        ModelSocial::query()->truncate();

        $oldModels = Oldmodels::all();

        foreach($oldModels as $omodel){

            $modelSocial = new ModelSocial;
            $modelSocial->model_id = $omodel->id;
            $modelSocial->brand = $omodel->brand;
            $modelSocial->instagram_profile = $omodel->instagram_profile;
            $modelSocial->instagram_posts = $omodel->instagram_posts;
            $modelSocial->artistic_email = $omodel->artistic_email;
            $modelSocial->artistic_password = $omodel->artistic_password;
            $modelSocial->twitter_account = $omodel->twitter_account;
            $modelSocial->twitter_campaigne_active = $omodel->twitter_campaign_active;
            $modelSocial->twitter_profile = $omodel->twitter_profile;
            $modelSocial->twitter_post = $omodel->twitter_posts;
            //$modelSocial->priority = $omodel->priority;

            $modelSocial->save();
        }

        return ["success" => true];

    }

    public function syncStudios(){

        //truncate table
        Studio::query()->truncate();

        $studios = Oldstudios::all();

        foreach($studios as $studio){

            $newStudio = new Studio;
            $newStudio->id = $studio->id;
            $newStudio->name = $studio->name;
            $newStudio->phone_notification = $studio->phone_notification;
            $newStudio->email_notification = 1;
            $newStudio->reservations_required = 1;
            $newStudio->payment_request = 1;
            $newStudio->active = $studio->state;
            $newStudio->address = $studio->address;
            $newStudio->contact_name = $studio->contact_name;
            $newStudio->contact_phone = $studio->contact_phone;
            $newStudio->contact_email = $studio->contact_email;
            $newStudio->updated_at = $studio->updated_at;
            $newStudio->created_at = $studio->created_at;
            $newStudio->save();
        }

        //insert home
        $newStudio = new Studio;
        $newStudio->id = 100;
        $newStudio->name = 'Home';
        $newStudio->phone_notification = 0;
        $newStudio->email_notification = 0;
        $newStudio->reservations_required = 0;
        $newStudio->payment_request = 0;
        $newStudio->active = 1;
        $newStudio->address = 'Home';
        $newStudio->contact_name = 'Home';
        $newStudio->contact_phone = '';
        $newStudio->contact_email = 'Home';
        $newStudio->updated_at = now();
        $newStudio->created_at = now();
        $newStudio->save();

        return ["success" => true];
    }

    public function syncSales()
    {
        ini_set('max_execution_time', 3000);

        //truncate table
        ModelSale::query()->truncate();

        $ress = Oldsales::all();

        foreach($ress as $res){

            $date = explode('-', $res->date);

            $modelSale = new ModelSale;
            $modelSale->model_id = $res->modelid;
            //$modelSale->studio_id = Models::where('id',$res->modelid)->first()->studio ?? 'xx';
            $modelSale->studio_id = 0;
            $modelSale->period = getPeriodNumberFromDate($res->date)["period"];
            $modelSale->year = $date[0];
            $modelSale->month = $date[1];
            $modelSale->day = $date[2];
            $modelSale->sum = $res->sum;
            $modelSale->hours = convertHoursToSeconds($res->hours);
            $modelSale->save();

        }

        $models = Models::all();
        foreach($models as $model){

            ModelSale::where('model_id', $model->id)->update(['studio_id'=> $model->studio]);

        }



        return ["success" => true];
    }


    public function syncReservation()
    {
        //truncate table
        ModelReservation::query()->truncate();

        $modelnotfound = [337,234,228,214,144,47,46,41];
        $ress = Oldreservations::whereNotIn('modelid',$modelnotfound)->get();

        foreach($ress as $res){
            $modelReservations = new ModelReservation();
            $modelReservations->model_id = $res->modelid;
            $modelReservations->studio_id = (Models::where('id',$res->modelid)->first()) ? Models::where('id',$res->modelid)->first()->studio : 0;

            $month_days = explode('-',$res->month);
            $month = $month_days[0];
            $year = $month_days[1];

            $modelReservations->month = $month;
            $modelReservations->year = $year;

            $modelReservations->days = $res->days;

            $hour = explode(' ', $res->hour);
            $modelReservations->hour = $hour[0];

            $modelReservations->status = $res->status;
            $modelReservations->rr = $res->RR;
            $modelReservations->move = $res->MOVE;
            $modelReservations->updated_at = $res->created_at;
            $modelReservations->created_at = $res->created_at;

            $modelReservations->save();
        }


        return ["success" => true];

    }

    public function syncPeriods()
    {
        ini_set('max_execution_time', 3000);


        //truncate table
        ModelPeriod::query()->truncate();
        ModelPaymentHistory::query()->truncate();

        $periods = Oldperiods::with('history')->get();

        //dd($periods);

        foreach($periods as $period){

            $modelPeriod = new ModelPeriod();

            $modelPeriod->model_id = $period->modelid;
            $sid= Models::where('id', $period->modelid)->first();
            $sid = ($sid) ? ($sid->studio) : 0;
            $modelPeriod->studio_id = $sid;
            $modelPeriod->period = convertPeriodNameToValue($period->period);
            $modelPeriod->year = $period->year;
            $modelPeriod->total_amount = $period->totalamount;
            $modelPeriod->total_amount_jasmin = ($period->totalamount_jasmine) ? $period->totalamount_jasmine : 0;
            $modelPeriod->hours = convertHoursToSeconds($period->hours);
            $modelPeriod->closed = (is_null($period->history))  ? 0 : 1;
            $modelPeriod->created_at = $period->buttondate;
            $modelPeriod->save();

            try {

                if(!is_null($period->history)){
                    $modelPayHist = new ModelPaymentHistory();
                    $modelPayHist->model_id = $period->modelid;
                    $modelPayHist->studio_id = $sid;
                    $modelPayHist->period = convertPeriodNameToValue($period->period);
                    $modelPayHist->period_id = $modelPeriod->id;
                    $modelPayHist->year = $period->year;
                    $modelPayHist->jasmin_amount = $period->history->jasminamount;
                    $modelPayHist->hours = convertHoursToSeconds($period->hours);
                    $modelPayHist->total_amount_usd = $period->history->totalamount;
                    $modelPayHist->total_amount_eur = $period->history->totalamounteur;
                    $modelPayHist->exchange = $period->history->exchangerate;
                    $modelPayHist->commission_base = $period->history->basecomision;
                    $modelPayHist->commission_total = $period->history->totalcomision;
                    $modelPayHist->commission_90h = $period->history->bonusv;
                    $modelPayHist->commission_100h = $period->history->bonusw;
                    $modelPayHist->commission_10k = $period->history->bonusx;
                    $modelPayHist->commission_location = $period->history->bonusz;
                    $modelPayHist->affliate_rev = $period->history->affrevenue;
                    $modelPayHist->personal_rev = $period->history->personalsiterevenue;
                    $modelPayHist->floria_rev = $period->history->floriarevenue;
                    $modelPayHist->others_rev = $period->history->otherrevenue;
                    $modelPayHist->deductions = ($period->history->deductions != '') ? $period->history->deductions : 0 ;
                    $modelPayHist->pay_request = ($period->paymentrequest == 'YES') ? 1 : 0;
                    $modelPayHist->pay_request_date = $period->buttondate;
                    $modelPayHist->pay_done = ($period->history->status == 'Paid' ) ? 1 : 0;
                    $modelPayHist->pay_amount = ($period->history->amountpaid != '') ? $period->history->amountpaid : 0.00 ;
                    $modelPayHist->pay_date = ($period->history->paydate == '1970-01-01') ? $period->buttondate : $period->history->paydate;
                    $modelPayHist->save();
                }

            } catch (\Exception $e) {
                dd("Ecept ".$e);
            }



        }


        return ["success" => true];

    }

    public function syncSignatures(){

        //truncate table
        ModelSignature::query()->truncate();

        $sigs = Oldsignatures::all();

        foreach ($sigs as $sig){

            $newsig = new ModelSignature();
            $newsig->model_id = $sig->model_id;
            $newsig->model_signature = $sig->model_signature;
            $newsig->save();
        }


        return ["success" => true];

    }

    public function syncMessages(){

        ini_set('max_execution_time', 3000);

        //truncate table
        ModelMessagesSubject::query()->truncate();
        ModelMessagesBody::query()->truncate();

        $msgs = Oldmessages::orderBy('id','ASC')->where('replyto', 'NO')->get();

        foreach ($msgs as $msg){

            $messsage_subj = new ModelMessagesSubject();
            $messsage_subj->id = $msg->id;
            $messsage_subj->from_id = $msg->fromid;
            $messsage_subj->to_id = $msg->toid;
            $messsage_subj->subject = $msg->subject;
            //$messsage_subj->message = $msg->message;
            //$messsage_subj->readtime = $msg->readtime;
            $messsage_subj->solved = $msg->solved;
            $messsage_subj->updated_at = $msg->sendtime1;
            $messsage_subj->created_at = $msg->sendtime1;
            //$messsage_subj->attachements = $msg->path;
            $messsage_subj->save();
        }




        $msgsss = Oldmessages::orderBy('id','ASC')->where('replyto','!=' ,'NO')->get();

        foreach ($msgsss as $msg1){

                //check if message is not duplicate
                $mb = ModelMessagesBody::updateOrCreate(
                    ["from_id" => $msg1->fromid, "to_id" => $msg1->toid, "belongs_to" => $msg1->replyto, "message" => $msg1->message, "attachements" => $msg1->path],
                    ["readtime" => $msg1->readtime, "updated_at" => $msg1->sendtime1, "created_at" => $msg1->sendtime1]
                );

//                $messsage_body = new ModelMessagesBody();
//                $messsage_body->from_id = $msg1->fromid;
//                $messsage_body->to_id = $msg1->toid;
//                $messsage_body->belongs_to = $sf->id;
//                $messsage_body->message = $msg1->message;
//                $messsage_body->readtime = $msg1->readtime;
//                $messsage_body->updated_at = $msg1->sendtime1;
//                $messsage_body->created_at = $msg1->sendtime1;
//                $messsage_body->attachements = $msg1->path;
//                $messsage_body->save();


        }


        return ["success" => true];

    }

    public function fixMessagesGroup(){

        return 'here';

    }

    public function syncAnalytics(){

        //truncate table
        ModelAnalytics::query()->truncate();

        $analytics = Oldanalytics::all();

        foreach ($analytics as $analytic){

            $ana = new ModelAnalytics();

            $ana->fill($analytic->toArray());

            $ana->save();
        }


        return ["success" => true];

    }


    public function pushertest(Request $request){

        //return getRole();

        if ($request->has('channel')){
            $channel = $request->get('channel');

            if ($channel === 'SYSTEM') event(new SystemNotifications('Test Pusher SYSTEM Event!'));
            if ($channel === 'STUDIO') event(new StudioNotifications('Test Pusher STUDIO Event!'));
            if ($channel === 'ACCOUNTING') event(new AccountingNotifications('Test Pusher ACCOUNTING Event!'));
            if ($channel === 'ADMIN') event(new AdminNotifications('Test Pusher ADMIN Event!'));
            if ($channel === 'DEV') event(new DevNotifications('Test Pusher DEV Event!'));

            return $channel;
        }


    }











    public function findCountryStateCityId($id, $city, $state, $country){



        $country_res = Country::where('name', $country)->first();
        if ($country_res){
            $cid = $country_res->id;
            $state_res = State::where('name', $state)->where('country_id', $cid)->first();
            if ($state_res){
                $sid = $state_res->id;
                $city_res = City::where('name', $city)->where('state_id', $sid)->first();
                if($city_res) {
                    return ["city_id" => $city_res->id, "state_id" => $state_res->id, "country_id" => $country_res->id];
                } //else dd($id, $city, $state, $country, ["city_id" => null, "state_id" => $state_res->id, "country_id" => $country_res->id]);

            } //else dd($id, $city, $state, $country, [ "state_id" => null, "country_id" => $country_res->id]);

        } //else dd($id, $city, $state, $country, [ "country_id" => null]);

        return null;

    }


}
