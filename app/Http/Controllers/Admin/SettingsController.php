<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Currency;
use App\ApiKeys;

class SettingsController extends Controller
{

    public function index()
    {

        $currency = Currency::all();

        $api = ApiKeys::all();

        return view('Admin.settings.home', compact('currency', 'api'));
    }



}
