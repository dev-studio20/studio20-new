<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\ModelReservation;
use App\Studio;
use App\StudioSchedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ScheduleController extends Controller
{

    public function show(Request $request)
    {

        $months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ];

        $role = Auth::guard('admin')->user()->getRole();

        if ($request->ajax() && $request->has('studio') && $request->has('month') && $request->has('year')){

            $s_id = $request->get('studio');
            if (!$s_id) return response()->json(["success" => false]);
            else {

                //get selected studio
                $studio = Studio::findOrFail($s_id);

                $month = $request->get('month');
                $year = $request->get('year');
                $schedule = StudioSchedule::where('studio_id',$s_id)->where('month', $month)->where('year',$year)->first();
                $schedule = ($schedule) ? json_decode($schedule->data) : [];

                $daysMonth = getDaysInMonth($year, $month);
                $daysMonth = $this->arrayDays($year, $month, $daysMonth);

                $mainManager = Admin::where('studio_id',$s_id)->where('role_id',5)->where('main',1)->first();

                $supports = Admin::where('role_id',6)->where('studio_id',$s_id)->get()->pluck('name','id')->toArray();
                $supports[0] = "Select";
                ksort($supports);

                $reserv = ModelReservation::where('studio_id',$s_id)->where('month', $month)->where('year',$year)->where('status',1)->select('days','hour')->get();

                $res = $this->countReservations($reserv);

                $data = ["studio_id" => $s_id, "studio" => $studio, "mainManager" => $mainManager,"months" => $months, "reservations" => $res,
                    "supports" => $supports, "daysMonth" => [$daysMonth["p1"], $daysMonth["p2"]], "schedule" => $schedule];

                return response()->json(["success" => true, "data" => $data]);
            }

        }


        return view('Admin.schedule.show', compact('months','role'));
    }

    public function insert(Request $request){

        // data:{studio:studio, schedule:jData, month:month, year: year},

        if ( $request->has('studio') && $request->has('schedule') && $request->has('month') && $request->has('year')){

            $studio_id = $request->get('studio');
            $json = json_encode($request->get('schedule'));
            $month = $request->get('month');
            $year = $request->get('year');

            StudioSchedule::updateOrCreate([
                "studio_id" => $studio_id,
                "month" => $month,
                "year" => $year
            ],[
                "data" => $json
            ]);


            return response()->json(["studio" => $studio_id, "data" => $json,"month" => $month, "year" => $year]);

        }

    }

    function countReservations($res){

        $data["T1"] = '';
        $data["T2"] = '';

        foreach ($res as $r){

            $h = $r["hour"];
            $t = explode(':',$h)[0];

            if (($t >= 7) && ($t < 19)) $data["T1"] .= $r["days"].',';
            else $data["T2"] .= $r["days"].',';

        }

        $vals["T1"] = array_count_values(explode(',',$data["T1"]));
        $vals["T2"] = array_count_values(explode(',',$data["T2"]));

        return $vals;

    }

    function arrayDays($year, $month, $max){
        $days = [];
        for ($i=1;$i<=$max;$i++){
            if($i<16) $days["p1"][] = decimal($i)."-".decimal($month)."-".$year;
            else $days["p2"][] = decimal($i)."-".decimal($month)."-".$year;
        }
        return $days;
    }


}
