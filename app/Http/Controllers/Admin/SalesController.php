<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ModelPeriod;
use DataTables;
use App\Models;
use Illuminate\Http\Request;
use App\ModelSale;
use Response;


class SalesController extends Controller
{

    public function viewsales($modelid, $year, $period)
    {

        $p = $period;
        $darr = get_month_period($period);
        $periodnNr = $period;
        $period = $darr[1];

        $daysInPeriod = getArrayDaysPeriod($year, $p);

        $sales = ModelSale::where('model_id', $modelid)->where('year', $year)->where('period', $p)->orderBy('day', 'ASC')->get();

        $modelname = Models::find($modelid)->modelname ?? '--';

        $mperiod = ModelPeriod::where('period', $p)->where('model_id', $modelid)->where('year',$year)->first();

        $key = Models::with('key')->find($modelid)->key_id ?? '--';

        return view('Admin.sales.index', compact('sales', 'modelname', 'period', 'year', 'periodnNr', 'daysInPeriod', 'p', 'key', 'modelid', 'mperiod'));
    }

    public function viewsale($id){

        $sale = ModelSale::find($id);
        return Response::json($sale);

    }

    public function updatesale(Request $request){
//
        $sum = $request->input('sum');
        $time = $request->input('time');
        $day = $request->input('day');
        $modelname = $request->input('modelname');
        $model_id = modelId($modelname);

        $sale = $this->saveSale($model_id, $day, $sum, $time);

        $period = $this->updatePeriod($sale);

        return $this->prepareResponse($sale, $period);

    }

    public function addsales(Request $request){

        //find period
        //"model_id":model_id, "studio_id":studio_id, "year":year, "month":month, "day":day, "sum":sum, "hours":seconds
        if ( $request->has('model_id') && $request->has('studio_id') && $request->has('year') &&
            $request->has('month') && $request->has('day') && $request->has('sum') && $request->has('hours') ){

            $model_id = $request->get('model_id');
            $studio_id = $request->get('studio_id');
            $year = $request->get('year');
            $month = $request->get('month');
            $day = $request->get('day');
            $sum = $request->get('sum');
            $hours = $request->get('hours');

            $period = getPeriodNumberFromDate($year.'-'.$month.'-'.$day)["period"];

            $sale = new ModelSale();
            $sale->model_id = $model_id;
            $sale->studio_id = $studio_id;
            $sale->period = $period;
            $sale->year = $year;
            $sale->month = $month;
            $sale->day = $day;
            $sale->sum = $sum;
            $sale->hours = $hours;
            $sale->save();

            return $sale;
        }

    }

    public function deletesale($id){

        $sale = ModelSale::find($id);
        $sale->sum = '0.00';
        $sale->hours = 0;
        $sale->save();

        $period = $this->updatePeriod($sale);

        return $this->prepareResponse($sale, $period);

    }

    public function prepareResponse(ModelSale $sale, ModelPeriod $period){

        return Response::json([
            'sale_id' => $sale->id,
            'sum' => $sale->sum .' '. getCurrency(),
            'hours' => convertSecondsToHours($sale->hours),
            'model_id' => $sale->model_id,
            'period' => $sale->period,
            'year' => $sale->year,
            'month' => $sale->month,
            'day' => $sale->day,
            'totalSum' => $period->total_amount .' '. getCurrency(),
            'totalSumJasmin' => $period->total_amount_jasmin,
            'totalHours' => convertSecondsToHours($period->hours),
            'success' => true
        ]);

    }

    public function updatePeriod(ModelSale $sale):ModelPeriod{

        $periodid = $sale->period;
        $year = $sale->year;
        $modelid = $sale->model_id;

        $periodsum = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('sum');
        $periodhours = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('hours');

        $period = ModelPeriod::where('model_id', $modelid)->where('period', $periodid)->where('year',$year)->first();

        return $period;

    }

    public function saveSale($modelid, $day, $sum, $hours):ModelSale{

        $arD = explode('-', $day);
        $y = $arD[0];
        $m = $arD[1];
        $d = $arD[2];
        $p = ($d < 16) ? 1 : 0;
        $period = ($m * 2) - $p;

        $studio = Models::where('id',$modelid)->first()->studio;

        $sale = ModelSale::updateOrCreate(
            ['model_id' => $modelid, 'year' => $y, 'month' => $m, 'day' => $d, 'period' => $period, 'studio_id' => $studio],
            ['sum' => $sum, 'hours' => $hours]
        );

        return $sale;

    }

    public function fixSalePeriod(){

        set_time_limit(0);
        ini_set('memory_limit', '1024M');


        $sales = ModelSale::where('period', 0)->get();
        foreach ($sales as $sale){

            $p = ($sale->day < 16) ? 1 : 0;
            $period = ($sale->month * 2) - $p;
            $sale->period = $period;
            $sale->save();
        }

    }

    public function fixsales(){

        ini_set('max_execution_time', 180);

        $sales = ModelSale::whereNull('year')->get();

        foreach ($sales as $sale) {

            $date = $sale->date;

            $edate = explode('-', $date);
            $d = $edate[2];
            $m = $edate[1];
            $y = $edate[0];

            ModelSale::where('id', $sale->id)
                ->update(['day' => $d, 'month' => $m, 'year' => $y]);




        }
    }

    public function closeperiod($periodid){

        //period->closed != period->closed;
        $period = ModelPeriod::where('id', $periodid)->first();
        $period->closed = !$period->closed;
        $period->save();

        //return 'period open/close';
        return Response::json([
            'status' => ['open','closed'][$period->closed],
            'success' => true
        ]);
    }


}