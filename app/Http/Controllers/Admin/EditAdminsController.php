<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\AdminRole;
use App\Studio;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class EditAdminsController extends Controller
{

    public function index(Request $request)
    {
        if (in_array(getRole(),[6])) return redirect('/admin');

        if ($request->ajax() && $request->has('studio')) {
            $studio = $request->get('studio');
            $op = ($studio == 0) ? '!=' : '=';

            if (getRole() == 5) $data = Admin::with('role','studio')->whereIn('role_id',[5,6])->where('studio_id', $op, $studio)->get();
            else $data = Admin::with('role','studio')->where('studio_id', $op, $studio)->get();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser">Edit</a>';

                    //$btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteUser">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('Admin.users.index');
    }

    public function roles(){

        $role = getRole();

        //if role is studio manager(5) can add only support
        //else can add all

        if($role == 5) $roles = AdminRole::where('id',6)->get()->pluck('name','id');
        else $roles = AdminRole::get()->pluck('name','id');

        return $roles;
    }

    public function studios(){

        $role = getRole();
        $studio_id = \Auth::guard('admin')->user()->studio_id;
        //if role is studio manager(5) can add only support
        //else can add all

        if($role == 5) $studios = Studio::where('id',$studio_id)->get()->pluck('name','id');
        else $studios = Studio::get()->pluck('name','id');

        return $studios;
    }

    public function viewUser($id){

        $user = Admin::with('role', 'studio')->find($id);

        $studios = getStudios();

        $role = getRole();
        if($role == 5) $roles = AdminRole::where('id',6)->get()->pluck('name','id');
        else $roles = AdminRole::get()->pluck('name','id');

        //$roles = AdminRole::get()->pluck('name', 'id')->toArray();

        return ["user" => $user, "studios" => $studios, "roles" => $roles];
    }

    public function editUser(Request $request){
        $user = $request->all();

        if($request->has('name') && $request->has('email') && $request->has('role') && $request->has('studio') && $request->has('password')){
            $hashed = Hash::make($user["password"]);

            //insert or update
            $user_exist = Admin::where('id', $user["user_id"])->first();
            if($user_exist){
                //update
                $data = Admin::updateOrCreate(
                    ['id' => $user["user_id"]],
                    ['name' => $user["name"], 'email' => $user["email"], 'role_id' => $user["role"], 'studio_id' => $user["studio"], 'password' => $hashed]);

                return $data;

            }else {
                //create
                //check email
                $email_exist = Admin::where('email',$user["email"])->first();
                if($email_exist) return 'email error..';
                else{
                    $data = Admin::updateOrCreate(
                        ['id' => $user["user_id"]],
                        ['name' => $user["name"], 'email' => $user["email"], 'role_id' => $user["role"], 'studio_id' => $user["studio"], 'password' => $hashed]);

                    return $data;
                }
            }




        } else return 'error';


        }

    public function deleteUser($id){

        $data = Admin::where('id', $id)->delete();

        return $data;
    }



}
