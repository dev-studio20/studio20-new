<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\GetSales;
use App\Jobs\GetSalesAllModels;
use App\Jobs\GetStoreSalesForPeriod;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Models;
use App\ModelSale;
use App\ModelPeriod;
use App\CustomClass\ManageSales;

class BackgroundController extends Controller
{

    public function index()
    {
        $models = Models::with('key')->where('status',1)->limit(100)->get();
        $all = [];
        $year = 2020;
        $period = 1;

//        foreach($models as $model){
//            $key = getAuth($model);
//            $modelname = $model->modelname;
//
//            $all[$key][] = $modelname;
//        }
//
//        foreach($all as $k => $v){
//
//            //call api method with all models => key (split)
//            $this->chunckGetSales($all[$k], $k, $year, $period);
//
//            sleep(1);
//
//        }
        //dd($all);

        $years = [ now()->year - 1, now()->year, now()->year + 1 ];

        $py = getCurrentPeriodAndYearMinus2();
        $p = $py[0];
        $y = $py[1];

        //ManageSales::statsSalesMonthComparison();

        return view('Admin.background.home', compact('p','y', 'years'));
    }

    public function chunckGetSales($models, $key, $year, $period){

        $chuncks = array_chunk($models, 30);

        foreach($chuncks as $chunck){

            //call api and store data
            //$this->getStoreSalesForPeriod($chunck, $key, $year, $period);
            GetStoreSalesForPeriod::dispatch($chunck, $key, $year, $period);
        }

    }

    public function getStoreSalesForPeriod($models, $key, $year, $period){

        $days = getArrayDaysPeriod($year, $period);

        foreach($days as $day){
            //should be with dispatch job
            //GetSalesByDay::dispatch($this->modelname, $day, 'period');

            $this->getSalesDay($models, $key, $day);
            //sleep(1);
        }

    }

    public function getSalesDay($models, $key, $day){

        $fromDate = formatDate($day);
        $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
        $toDate = formatDate($nextday);

        $screenNames = null;
        foreach($models as $model){
            $screenNames .= '&screenNames[]=' . $model;
        }

        $reports = "&reports[]=general-overview&reports[]=earnings-overview&reports[]=working-time";
        $reports = "&reports[]=general-overview&reports[]=working-time";

        $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$fromDate."&toDate=".$toDate.$screenNames.$reports;
        $res = curlAuth($key, $url);

        if (array_key_exists('data', $res)){
            $dataModels = $res['data'];

            foreach($dataModels as $dataModel){

                $modelSalesAndTime = $this->processData($dataModel);
                $this->storeData($modelSalesAndTime, $day);
            }
        }
    }

    public function storeData($data, $day){
        //["modelname" => $modelname, "earnings" => $earnings, "worktime" => $workTime ];
        if ($data) {
            $sum = $data['earnings'];
            $time = $data['worktime'];
            $modelname = $data['modelname'];
            $model_id = modelId($modelname);

            $sale = $this->saveSale($model_id, $day, $sum, $time);

            $period = $this->updatePeriod($sale);

            //dd([$sale, $period]);
            //DB::table('test_log')->insert(['screenName' => $modelname, 'earnings' => $earnings, 'workTime' => $workTime, 'day' => $day]);

        }
    }

    public function updatePeriod(ModelSale $sale):ModelPeriod{

        $periodid = $sale->period;
        $year = $sale->year;
        $modelid = $sale->model_id;

        $periodsum = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('sum');
        $periodhours = ModelSale::where('period', $periodid)->where('model_id', $modelid)->where('year', $year)->sum('hours');

        $period = ModelPeriod::updateOrCreate(
            ['model_id' => $modelid, 'period' => $periodid, 'year' => $year],
            ['total_amount' => $periodsum, 'total_amount_jasmin' => $periodsum, 'hours' => $periodhours]
        );

        return $period;

    }

    public function saveSale($modelid, $day, $sum, $hours):ModelSale{

        $arD = explode('-', $day);
        $y = $arD[0];
        $m = $arD[1];
        $d = $arD[2];
        $p = ($d < 16) ? 1 : 0;
        $period = ($m * 2) - $p;

        $sale = ModelSale::updateOrCreate(
            ['model_id' => $modelid, 'year' => $y, 'month' => $m, 'day' => $d, 'period' => $period],
            ['sum' => $sum, 'hours' => $hours]
        );

        return $sale;

    }

    public function processData($data){
        $earnings = 0;
        $workTime = 0;
        $modelname = null;

            if(array_key_exists('screenName', $data)){
                $modelname = $data['screenName'];
                $total =  $data['total'];
                if(array_key_exists('earnings', $total)){
                    $earn = $total['earnings'];
                    if(array_key_exists('value', $earn)){
                        $earnings = $earn['value'];
                        $earnings = number_format(round($earnings,2), 2, '.', '');
                    }
                }

                $workingTime =  $data['workingTime'];
                //$totaltime = $restotaltime['vip_show']['value'] + $restotaltime['pre_vip_show']['value'] + $restotaltime['private']['value'] + $restotaltime['free']['value'];
                if(array_key_exists('vip_show', $workingTime)) $workTime += $workingTime['vip_show']['value'];
                if(array_key_exists('pre_vip_show', $workingTime)) $workTime += $workingTime['pre_vip_show']['value'];
                if(array_key_exists('private', $workingTime)) $workTime += $workingTime['private']['value'];
                if(array_key_exists('free', $workingTime)) $workTime += $workingTime['free']['value'];

            }
            return ["modelname" => $modelname, "earnings" => $earnings, "worktime" => $workTime ];



    }


    public function getsales()
    {

        GetSales::dispatch();

        //return 'done';

        //return view('Admin.background.home');
    }

    public function getsalesallmodels()
    {

        GetSalesAllModels::dispatch();

    }

    public function getsalesallperiod(Request $request){

        if ( ($request->has('studio')) && ($request->has('period')) && ($request->has('year')) ){
            $s = $request->input('studio');
            $p = $request->input('period');
            $y = $request->input('year');


            GetSales::dispatch($s, $y, $p);

//            $arrStartEndDates = $this->getDates($p, $y);
//            $start = $arrStartEndDates[0];
//            $end =$arrStartEndDates[1];
//
//            $days = [];
//
//            $daysInPeriods = CarbonPeriod::between($start, $end);
//            foreach($daysInPeriods as $day){
//                $days[] = $dt = Carbon::parse($day)->setTimezone('Europe/Bucharest')->format('Y-m-d');
//            }

            //return response()->json(["STUDIO" => $s, "PERIOD" => $p, "YEAR" => $y, "DATES" => $arrStartEndDates, "ARR_DATES" => $days]);
            return 'done';
        } else return 'error';
    }


    function getDates($period, $year){

        $month = get_month_period($period);

        $m = decimal($month[0]);
        $dt = $year.'-'.$m;
        $dt = Carbon::parse($dt)->format('Y-m');

        $start = ($month[1] == 1) ? '01' : '16';
        $end = ($month[1] == 1) ? '15' : Carbon::parse($dt)->daysInMonth;

        $sdate = $year.'-'.$m.'-'.$start;
        $edate = $year.'-'.$m.'-'.$end;

        return [$sdate, $edate];
    }


}
