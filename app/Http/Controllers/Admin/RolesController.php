<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DataTables;
use App\AdminRole;
use Illuminate\Http\Request;


class RolesController extends Controller
{

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = AdminRole::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('Admin.roles.index',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        AdminRole::updateOrCreate(['id' => $request->product_id],
            ['name' => $request->name, 'detail' => $request->detail]);

        return response()->json(['success'=>'Product saved successfully.']);
    }

    public function edit($id)
    {
        $product = AdminRole::find($id);
        return response()->json($product);
    }

    public function destroy($id)
    {
        AdminRole::find($id)->delete();

        return response()->json(['success'=>'Role deleted successfully.']);
    }
}