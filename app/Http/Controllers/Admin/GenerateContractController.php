<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\CustomClass\SendNotification;


class GenerateContractController extends Controller
{

    public function store(Request $request)
    {



        if($request->has('model_id')){

            $id = $request->get('model_id');

            SendNotification::generateContractEvent($id);

            return ["success" => true, "data" => "job started .."];

        } else return ["success" => false];

    }


    public function showContract($slug)
    {


        if ( (Auth::guard('admin')->user()) || (Auth::guard('model')->user()->id == $slug) ){
            $contract = Storage::get('contracts/contract_'.$slug.'.pdf');
            return response()->make($contract, 200, ['content-type' => 'application/pdf']);
        } else return "Contract Not Found!";

    }


}
