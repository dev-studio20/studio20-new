<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ModelPaymentHistory;
use App\ModelPeriod;
use App\ModelReservation;
use App\Models;
use App\ModelStatus;
use App\ModelstatusJasmin;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\StatsSalesTodayYday;
use App\StatsSalesMonth;
use App\StatsSalesPeriod;
use Yajra\Datatables\Datatables;
use App\CustomClass\SendNotification;

class ReservationsController extends Controller
{

    public function index(Request $request)
    {
        $role = getRole();

        if ($request->ajax()) {

            $s = $request->get('studio');

            $op = ($s) ? '=' : '!=';

            $reservations = ModelReservation::with('model','studios')
                ->where('studio_id', $op, $s)
                ->orderBy('year','ASC')
                ->orderBy('month','ASC')
                ->where('status',0)->get();


            return Datatables::of($reservations)->make(true);

        }

        return view('Admin.reservations.index', compact('reservations','role'));
    }

    public function show(Request $request, $id){

        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if($request->has('dates')){
            $dates = $request->get('dates');
            $ar = [];
            foreach($dates as $date){
                $ar[] = explode('/',$date)[1];
            }

            $ar = implode(',',$ar);

            $reservation = ModelReservation::findOrFail($id);
            $reservation->days = $ar;
            $reservation->status = 1;
            $reservation->save();

            SendNotification::reservationsApprovedEvent($reservation->model_id);


            return ["success" => true, "dates" => $ar];
        }

        $res = ModelReservation::findOrFail($id);
        $days = $res->days;
        $month = $res->month;
        $year = $res->year;
        $hour = $res->hour;

        $model = Models::with('studios')->findOrFail($res->model_id);

        $status = ($res->status) ? true : false;

        $d = $year.'-'.$month.'-01';
        $minDate = Carbon::parse($d)->format('m/d/Y');
        $maxDay = Carbon::parse($d)->daysInMonth;

        $maxDate = Carbon::parse($year.'-'.$month.'-'.$maxDay)->format('m/d/Y');

        $color = ($res->status) ? "red!important" : "yellow!important";
        $font = ($res->status) ? "white!important" : "black!important";

        return view('Admin.reservations.show', compact('res','days', 'month','year','maxDate','minDate','status','color','font','id','model','hour'));
    }




}
