<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models;
use App\ModelShifts;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ModelStatsController extends Controller
{

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $new = [];
            $models = Models::whereStatus(1)->pluck('modelname')->toArray();

            //get data from awe
            $res = $this->getData($models);

            //get avg from model_shifts
            foreach($res["models"] as $model){
                $modelname = $model["performerId"];

                $earn = ModelShifts::whereModelname($modelname)->pluck('earnings')->toArray();
                if ($earn) {
                    $sum = round(array_sum($earn),2);
                    $count = count($earn);
                    $avg = round($sum/$count);
                    $dataAvg = ["count" => $count, "earnings" => $sum, "avg" => $avg];
                } else $dataAvg = ["count" => 0, "earnings" => 0, "avg" => 0];
                $model["avg"] = $dataAvg;
                $new[] = $model;
            }

            $data["models"] = $new;
            $data["total"] = $res["total"];
            $data["found"] = $res["found"];

            return response()->json($data);

        }


        return view('Admin.Modelstats.home');
    }

    public function updateshiftsales(){

        $shifts = ModelShifts::whereNull('earnings')->first();

        $model = $shifts->modelname;
        $startTime = $shifts->start_time;
        $endTime = $shifts->created_at;

        $format_start_date = formatDateWithHour($startTime);
        $format_end_date = formatDateWithHour($endTime);

        $shifts->earnings = $this->getModelShiftSales($model, $format_start_date, $format_end_date);

        $shifts->save();

        sleep(1);

        return $shifts->id;

    }

    public function getModelShiftSales($model, $start, $end){

        $keys = ["","Authorization: Bearer 8c23e7366a3e7096a30914edb145efc95fe241cd5be682a5ec1a366ca01e6c70", "Authorization: Bearer 406ad45b5ed5748abb871cc99ef69bc45d0ad2779f9034f91de5af254f8a9902"];
        $key = Models::whereModelname($model)->first();

        if ($key && $key->key_id){
            $key = $keys[$key->key_id];

            $url = "https://partner-api.modelcenter.jasmin.com/v1/reports/performers?fromDate=".$start."&toDate=".$end."&screenNames[]=".$model."&reports[]=general-overview";

            $res = curlAuth($key, $url);
            if (array_key_exists('data', $res)){

                $data = $res['data'][0];
                if (array_key_exists('screenName', $data)){
                    $total = $data["total"];
                    $earnings = $total["earnings"];
                    $value = $earnings["value"];
                    return $value;
                }

            }
        }


        return 0.00;

    }


    public function getData($models){

        $modelStatus2 = array();

        $countInit = count($models);

        $arrchuck = array_chunk($models, 70);

        foreach($arrchuck as $arr){
            $modelstring = '';
            foreach($arr as $ar){
                $modelstring .= $ar.',';
            }

            $cSession = curl_init();
            $url = "http://pt.ptawe.com/api/model/feed?siteId=gjasmin&psId=14noiembrie&psTool=213_1&psProgram=revs&campaignId=&category=girl&limit=10&showOffline=0&extendedDetails=0&responseFormat=json&performerId=".$modelstring."&subAffId={SUBAFFID}&accessKey=ea0f7bf083974543186e2abb1f8ac09c&legacyRedirect=1";
            $url = "http://pt.ptawe.com/api/model/feed?siteId=gjasmin&psId=14noiembrie&psTool=213_1&psProgram=revs&campaignId=&category=girl&limit=10&imageSizes=320x180&imageType=glamour&showOffline=1&extendedDetails=1&responseFormat=json&performerId=".$modelstring."&subAffId={SUBAFFID}&accessKey=ea0f7bf083974543186e2abb1f8ac09c&legacyRedirect=1";

            curl_setopt($cSession, CURLOPT_URL, $url);
            curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
            $resCurl = curl_exec($cSession);
            curl_close($cSession);
            $res = json_decode($resCurl, true);

            if ( (isset($res['status'])) && ($res['status'] == 'OK')  && ($res['errorCode'] == 0)) {

                $modelStatus = $res['data']['models'];
                $modelStatus2 = array_merge($modelStatus,$modelStatus2);
            }
        }

        $countRes = count($modelStatus2);

        return ["models" => $modelStatus2, "total" => $countInit, "found" => $countRes];

    }




}
