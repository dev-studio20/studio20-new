<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DataTables;
use App\Studio;
use Illuminate\Http\Request;

class StudioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax()) {
            $data = Studio::all();
            return Datatables::of($data)
                ->addIndexColumn()

                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editProduct">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteProduct">Delete</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('Admin.studios.index', compact('studio'));
    }

    public function store(Request $request)
    {
        Studio::updateOrCreate(['id' => $request->product_id],
            ['name' => $request->name, 'detail' => $request->detail]);

        return response()->json(['success'=>'Product saved successfully.']);
    }

    public function edit($id)
    {
        $product = Studio::find($id);
        return response()->json($product);
    }

    public function destroy($id)
    {
        Studio::find($id)->delete();

        return response()->json(['success'=>'Role deleted successfully.']);
    }

}
