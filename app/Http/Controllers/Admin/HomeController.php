<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ModelPaymentHistory;
use App\ModelPeriod;
use App\ModelReservation;
use App\Models;
use App\ModelStatus;
use App\ModelstatusJasmin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\StatsSalesTodayYday;
use App\StatsSalesMonth;
use App\StatsSalesPeriod;
use Yajra\Datatables\Datatables;
use App\CustomClass\SendNotification;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        //Admin Roles
        //1 Accounting
        //2 Admin
        //3 SUPER ADMIN
        //4 DEVELOPER
        //5 STUDIO MANAGER
        //6 SUPPORT

        $role = getRole();

        $studioId = Auth::guard('admin')->user()->studio_id;

        $mStatsCount = $this->getModelStatsCount($studioId);

        if ($request->ajax() && $request->has('widget') && $request->has('studio')){
            $data = $this->ajaxDatatables($request);
            return Datatables::of($data)->make(true);
        }



        if ($request->ajax() && $request->has('studio')) {

            $studio_id = $request->input('studio');

            $mStatsCount = $this->getModelStatsCount($studio_id);

            $lastDays = $this->getTodayVsYesterday($studio_id);

            $lastYear = $this->getLastYearSales($studio_id);

            $periodSales = $this->getLastPeriodsSales($studio_id);

            $modelsOnline = $this->getModelsOnlineAndFree($studio_id);

            return [
                "success" => true,
                "lastDays" => $lastDays,
                "lastYear" => $lastYear,
                "periodSales" => $periodSales,
                'studioName' => getStudios()[$studio_id],
                "mStatsCount" => $mStatsCount,
                "modelsOnline" => $modelsOnline
                ];

        }

        return view('Admin.admin', compact("mStatsCount", "last10Req", "last10Pay", "role"));
    }


    public function ajaxDatatables(Request $request){

        if ($request->ajax() && $request->has('widget') && $request->has('studio')) {
            //approvedModels

            $widget = $request->get('widget');
            $studio = $request->get('studio');
            $op = ($studio) ? '=' : '!=';

            if ($widget == 'reservationsApproval'){
                $modelInApproval = ModelReservation::with('model','studios')->where('status', 0)->where('studio_id', $op, $studio)->get();

                return $modelInApproval;
            }

            if ($widget == 'pendingModels'){
                $modelInPending = Models::with('studios')->where('status', 2)->where('studio', $op, $studio)->get();

                return $modelInPending;
            }

            if ($widget == 'approvedModels'){
                $lastModelsApproved = Models::with('studios')->orderBy('updated_at', 'DESC')->whereStatus(1)->where('studio', $op, $studio)->limit(30)->get();

                return $lastModelsApproved;
            }

            if ($widget == 'openPeriods'){

                $last10ModWithout = ModelPeriod::with('model','periodName','studio')->orderBy('created_at', 'ASC')->whereHas('model')
                    ->where('closed', 0)->where('studio_id', $op, $studio)->limit(30)->get();

                return $last10ModWithout;
            }

            if ($widget == 'paymentRequest'){

                $last10Req = ModelPaymentHistory::with('model','periodName','studio')->whereHas('model')
                    ->orderBy('pay_request_date', 'ASC')
                    ->where('pay_request', 1)->where('pay_done',0)
                    ->where('studio_id', $op, $studio)->limit(30)->get();

                return $last10Req;
            }

            if ($widget == 'paymentMade'){

                $last10Pay = ModelPaymentHistory::with('model','periodName','studio')->whereHas('model')
                    ->orderBy('pay_request_date', 'DESC')->where('studio_id', $op, $studio)
                    ->where('pay_done', 1)->limit(30)->get();

                return $last10Pay;
            }


        }

    }


    public function getModelsOnlineAndFree($studio_id){

        $op = ($studio_id) ? '=' : '!=';
        $modelsFromThisStudio = Models::where('studio',$op,$studio_id)->whereStatus(1)->pluck('modelname')->all();

        $modelsOnline = $this->getModelsInFree30minAndALL();
        $modelsOnlineFromThisStudio = [];

        if ($modelsOnline){
            foreach($modelsOnline as $mo){

                if (in_array($mo["modelname"], $modelsFromThisStudio)){
                    $modelsOnlineFromThisStudio[] = $mo;
                }
            }
        }



        return ["models_from_this_studio" => $modelsFromThisStudio, "models_online_from_this_studio" => $modelsOnlineFromThisStudio, "studio" => $studio_id];

    }

    public function getModelsInFree30minAndALL(){

        $last_update =  ModelstatusJasmin::orderBy('updated_at', 'DESC')->first()->updated_at;

        $allstatus = ModelstatusJasmin::with('studios')->where('updated_at', $last_update)->where('status','!=','offline')->get()->toArray();

        return $allstatus;
    }

//    public function apiCallStatus1(){
//
//        $cSession = curl_init();
//        $url = 'https://api.studio20.group/api/getmodelsstatuswithfree';
//
//        curl_setopt($cSession, CURLOPT_URL, $url);
//        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIzZmE4NWJhODJmZDhjNzBiOWZlMDhiMmI5MDRkNDJjZDBjYjM4Zjg3OTlhYzZkNjM3OGM4YTcyY2I0YjQ5N2RhYThmMTllZjUxYjVlYjhkIn0.eyJhdWQiOiIzIiwianRpIjoiMjNmYTg1YmE4MmZkOGM3MGI5ZmUwOGIyYjkwNGQ0MmNkMGNiMzhmODc5OWFjNmQ2Mzc4YzhhNzJjYjRiNDk3ZGFhOGYxOWVmNTFiNWViOGQiLCJpYXQiOjE1NDkzNzI1OTIsIm5iZiI6MTU0OTM3MjU5MiwiZXhwIjoxNTgwOTA4NTkyLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.EPY6bLbYwoPrtwKjChlsZqcEQPcarrcttaGziTaOvt5didcsWE25tQOH4k_7DKqVUgzyRLFDjHqQqrVstAX2fTusKhjZ-_N2-1SPh3rhVLEHS1WEBAF59FVdar-RGFPPsrmXm7cZDwpUjJWmXcwbr58HGnuIKVEuxpch4HhLACTuSwfTOXByrvqmywykhsUlRWM_-rGCo7zd5B0Y-GkmpPAg5eMplAdc78dmVEtaDoKktS6QjyphuZDkqmoxWVAPFJgi5bl5CiheQqYzXwLhSXRUWLx2g-aiDmuu5bU5NYxNdaqzF_E9iIm_DrAc_ey098CeE4nqwJdIHD8ygkeSlsde1U8PrHXf9598zVckch5ZyGNXKVUMUcpTz8Ic5PXuo9fXEvLO5qdU5CkxhCEzLVLew8iTkDq4SB-iqjLoj3bDz6k74EX_9-N0w9IIDdVcwQnw5mbAMGEWQnudJp5ytuKCeL5BTrqJd1gcDcP5URsku7lUUY6AyNfGIRc81dWRg7AMROCULQZM46crhcYGVer_Lce6wtVYQRL4SwXxDvl_Otm5QN42LdECzXYLjC36b5wZHm9EM8Xc4xKZef14mgtrb4qIs1Lanvdg21RBgUQ0ArPLflBQx4DzUCkCTMS7JbEA09miWXKs3pyAfXtRfMnXs2JEIKN2bAxixbc1AW4"));
//        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
//        $resCurl = curl_exec($cSession);
//        curl_close($cSession);
//
//        $res = json_decode($resCurl, true);
//
//
//
//        if($res['success'] == true){
//
//            return [
//                'success' => true,
//                'models_online' => $res['models_online']
//            ];
//
//        } else return [
//            'success' => false,
//            'models_online' => null
//        ];
//
//    }

    public function stats(Request $request){

        $studio_id = ($request->has('studio')) ? $request->input('studio') : 0;

        $lastDays = $this->getTodayVsYesterday($studio_id);

        return ["success" => true, "lastDays" => $lastDays];
    }

    public function getTodayVsYesterday($studio_id){

        $sales = StatsSalesTodayYday::where('studio_id', $studio_id)->first();

        if($sales) $sales->time_update = explode(' ', $sales->tday_to)[1];

        return $sales;
    }

    public function updatestudio(Request $request){

        if($request->has('studio')){

            $studio = $request->input('studio');
            session(['studio' => $studio]);

            return ["success" => true];
        } else return ["success" => false];
    }

    public function models()
    {
        return view('Admin.models');
    }

    function getModelStatsCount($studio_id = null){

        $path["Total"] = '/admin/models';
        $path["Approved"] = '/admin/approvedmodels';
        $path["Pending"] = '/admin/pendingmodels';
        $path["Suspended"] = '/admin/suspendedmodels';
        $path["Rejected"] = '/admin/rejectedmodels';
        $path["Inactive"] = '/admin/inactivemodels';

        $studioId = ($studio_id) ? [$studio_id] : getAdminStudios();
        $mStatsCount = [];

        $status = ModelStatus::orderBy('id', 'ASC')->get();
        $mStatsCount["Total"] = Models::whereIn('studio', $studioId)->count();
        //$mStatsCount["Total"] = ["path" => $path["Total"], "count" => $mStatsCount["Total"]];
        foreach ($status as $stat){
            $mStatsCount[$stat->name] =  Models::where('status', $stat->id)->whereIn('studio', $studioId)->count();
            //$mStatsCount[$stat->name] = ["path" => $path[$stat->name], "count" => $mStatsCount[$stat->name]];
        }

        return $mStatsCount;
    }


    function getLastPeriodsSales($studio_id){

        if (StatsSalesPeriod::whereStudio_id($studio_id)->where('name', 'prevPeriod')->get()->first()) {
            $salesPrevPeriod = StatsSalesPeriod::whereStudio_id($studio_id)->where('name', 'prevPeriod')->get()->first()->toArray();
        } else {
            $salesPrevPeriod = ["from_date" => now()->format('Y-m-d'), 'to_date' => now()->format('Y-m-d')];
            $salesPrevPeriod["earnings"] = 0;
            $salesPrevPeriod["updated_at"] = now()->format('Y-m-d');
        }


        if(StatsSalesPeriod::whereStudio_id($studio_id)->where('name', 'thisPeriod')->get()->first()){
            $salesThisPeriod = StatsSalesPeriod::whereStudio_id($studio_id)->where('name', 'thisPeriod')->get()->first()->toArray();
        } else {
            $salesThisPeriod = ["from_date" => now()->format('Y-m-d'), 'to_date' => now()->format('Y-m-d')];
            $salesThisPeriod["earnings"] = 0;
        }

        $l1 = getPeriodFromDate($salesPrevPeriod["from_date"]);
        $l2 = getPeriodFromDate($salesThisPeriod["from_date"]);

        $labels = [$l1, $l2];

        $periodSales = [$salesPrevPeriod["earnings"], $salesThisPeriod["earnings"]];

        $updated = $salesPrevPeriod["updated_at"];

        $max = max($periodSales);

        return [ 'periodSales' => $periodSales, 'max' => $max, 'labels' => $labels, 'updated' => $updated];
    }

    function getLastYearSales($studio_id){
        //last 12 months
        $lastYearSales = [];
        $monthLabels = [];
        $date = null;

        $sales = StatsSalesMonth::whereStudio_id($studio_id)->orderBy('month', 'DESC')->limit(12)->get()->toArray();
        $sales = array_reverse($sales, false);
        foreach ($sales as $sale){
            $month = explode('-',$sale["month"])[1];
            $month = $sale["month"];
            $monthLabels[] = Carbon::parse($month)->format('M');
            $lastYearSales[] = $sale["earnings"];
            $worktime = $sale["worktime"];
            $worktimeneeded = $sale["worktimeneeded"];
            $date = $sale["updated_at"];
        }

        $max = ($lastYearSales) ? max($lastYearSales) : 0;

        return([ 'yearSales' => $lastYearSales, 'max' => $max, "updated" => $date, "labels" => $monthLabels]);
    }

}
