<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\ModelReservation;
use App\Models;
use App\Studio;
use App\StudioSchedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ScheduleModelsController extends Controller
{

    public function show(Request $request)
    {

        $years = [2020,2021];

        $month = ($request->has('months')) ? $request->get('months') + 1 : 3;

        $year = ($request->has('years')) ? $years[$request->get('years')]  : 2020;

        $studio_id = ($request->has('studio')) ? $request->get('studio')  : Auth::guard('admin')->user()->studio;

        $months = ["January","February","March","April","May","June","July","August","September","October","November","December"];


        $daysMonth = getDaysInMonth($year, $month);
        $period = $this->arrayDaysFull($year, $month, $daysMonth);

        $reserv = ModelReservation::with('model')->where('studio_id',$studio_id)->where('month', $month)
            ->where('year',$year)->where('status',1)->orderBy('hour','ASC')->get();
        $period = $this->addReservations($period, $reserv);

        return view('Admin.schedule.showmodels', compact('period', "months" ,"years"));
    }

    public function ajaxreservations(Request $request){

        if ( $request->has('month')  && $request->has('modelid') ){

            $month = explode('-',$request->get('month'))[0];
            $year = explode('-',$request->get('month'))[1];

            $modelid = $request->get('modelid');
            $res = ModelReservation::where('month', $month)->where('year', $year)->where('model_id', $modelid)->first();

            if (!$res) {
                $res = new class{};
                $res->id = null;
                $res->modelid = $modelid;
                $res->month = $month;
                $res->days = null;
                $res->hour = null;
                $res->status = null;
                $res->RR = [];
                $res->MOVE = [];
                $res->created_at = null;
            } else {
                $res->days = ($res->days) ? explode(',',$res->days) : [];
                $res->RR = ($res->RR) ? explode(',',$res->RR) : [];
            }

            return json_encode($res);
        }

        if ( $request->has('ajxMoveDays') && $request->has('olddays') && $request->has('newdays') && $request->has('modelid') && $request->has('period')){

            $old = $request->get('olddays');
            $new = $request->get('newdays');
            $modelid = $request->get('modelid');
            $period = $request->get('period');

            $month = explode('-',$period)[0];
            $year = explode('-',$period)[1];

            $res = ModelReservation::where('year', $year)->where('month', $month)->where('model_id', $modelid)->first();
            if($res){
                if ($res->days && ($res->days != '' )){
                    $days = explode(',', $res->days);
                    $result = array_diff($days,$old);
                    $days = implode(',',$days);
                    $result = array_merge($new, $result);

                    sort($result);

                    $result = implode(',',$result);

                    //DB::connection('mysql3')->table('reservations')->where('month', $period)->where('modelid', $modelid)->update(['days' => $result]);
                    ModelReservation::where('year', $year)->where('month', $month)->where('model_id', $modelid)->update(['days' => $result]);

                    return $result;
                }

            }else return 'error';

        }


        if ( $request->has('ajaxFindMonthRr') && $request->has('modelid') ){

            $modelid = $request->get('modelid');

            $res = $this->searchInReservations($modelid);

            return $res;

        }

        if ( $request->has('dayfoundrr') && $request->has('modelid') && $request->has('selectedday')){

            $selectedday = $request->get('selectedday');
            $modelid = $request->get('modelid');
            $day = $request->get('dayfoundrr');

            //split day and period
            //selected day 16-10-2019
            //rr day 2019-10-21

            $selected = explode('-',$selectedday);
            $selected_day = $selected[0];
            $selected_month = $selected[1];
            $selected_year = $selected[2];

            $rr = explode('-',$day);
            $rr_day = $rr[2];
            $rr_month = $rr[1];
            $rr_year = $rr[0];


            $s_res = ModelReservation::where('month', $selected_month)->where('year', $selected_year)->where('model_id', $modelid)->first();
            if ($s_res) {
                $newdays = array();
                $days = explode(',', $s_res->days);
                foreach ($days as $day){
                    if ($selected_day !== $day ) $newdays[] = $day;
                }

                $days = (count($newdays) == 1) ? $newdays[0] : implode(',', $newdays);

                //update in db with days
                ModelReservation::where('id', $s_res->id)->update(['days' => $days]);

                //insertnew day in period + RR(create period if not exist)
                $update_res = ModelReservation::where('month', $rr_month)->where('year', $rr_year)->where('model_id', $modelid)->first();

                //update with rr if exist
                if ($update_res){
                    $u_days = $update_res->days;
                    $u_days = explode(',',$u_days);
                    $u_days[] = $rr_day;
                    $u_days = implode(',',$u_days);


                    $u_rr = $update_res->RR;
                    if ($u_rr) {
                        $u_rr = explode(',',$u_rr);
                        $u_rr[] = $rr_day;
                        $u_rr = implode(',',$u_rr);
                    } else $u_rr = $rr_day;


                    ModelReservation::where('id', $update_res->id)->update(['days' => $u_days, 'RR' => $u_rr]);
                } else {
                    //insert with rr
                    ModelReservation::insert(['model_id' => $modelid, 'month' => $rr_month, 'year' => $rr_year, 'hour' => '06:00 AM', 'days' => $rr_day, 'RR' => $rr_day]);
                }


            } else return 'error';

            $tst = [
                "sm" => $selected_month,
                "sd" => $selected_day,
                "rm" => $rr_month,
                "rd" => $rr_day,
            ];

            return $tst;

        }

        }

    public function searchInReservations($modelid){

        $now = Carbon::now();

        $y = $now->year;
        $m = $now->month;
        $d = $now->day;
        $w = $now->weekOfYear;

        $dayFound = null;

        // for each week until found condition free day
        while (!$dayFound){
            $dayFound = $this->checkThisWeek($w, $y,$modelid);
            $w++;
        }


        return $dayFound;

    }

    function checkThisWeek($week, $year, $modelid){

        $startOfWeek = Carbon::now()->setISODate($year,$week)->startOfWeek();
        $endOfWeek = Carbon::now()->setISODate($year,$week)->endOfWeek();

        $p1_year = $startOfWeek->year;
        $p1_month = ($startOfWeek->month < 10) ? "0".$startOfWeek->month : $startOfWeek->month;
        $p1_day = ($startOfWeek->day < 10) ? "0".$startOfWeek->day : $startOfWeek->day;
        $p1_day = $p1_year.'-'.$p1_month.'-'.$p1_day;
        $p1_arr = explode('-',$p1_day);
        $p1 = $p1_month.'-'.$p1_year;

        $p2_year = $endOfWeek->year;
        $p2_month = ($endOfWeek->month < 10) ? "0".$endOfWeek->month : $endOfWeek->month;
        $p2_day = ($endOfWeek->day < 10) ? "0".$endOfWeek->day : $endOfWeek->day;
        $p2_day = $p2_year.'-'.$p2_month.'-'.$p2_day;
        $p2_arr = explode('-',$p2_day);
        $p2 = $p2_month.'-'.$p2_year;

        //$p = [$p1, $p2];
        $months = [$p1_month, $p2_month];
        $years = [$p1_year, $p2_year];

        $first = Carbon::create($p1_arr[0], $p1_arr[1], $p1_arr[2]);
        $second = Carbon::create($p2_arr[0], $p2_arr[1], $p2_arr[2]);

        $period = $this->generateDateRange($first, $second);

        $reservationsDays = [];
        $daysWithRR = [];

        //add default day value

        $rez = ModelReservation::whereIn('month', $months)->whereIn('year', $years)->where('model_id', $modelid)->get();

        foreach($rez as $res){

            //if day between week start and end .. add
            $arr = explode(',',$res->days);
            //$mdd = explode('-',$res->month);
            $mdd = [$res->month, $res->year];

            if ($res->days && ($res->days != '') ){
                foreach($arr as $d){
                    //dd($mdd[1].' : '.$mdd[0].' : '. $d);
                    $mdt = Carbon::create($mdd[1], $mdd[0], $d);
                    //dd($first.' -> ' . $second . ' -> ' . $mdd);
                    if ($mdt->between($first, $second)) $reservationsDays[] = $mdd[1].'-'.$mdd[0].'-'.$d;
                }
            }

            $ar = explode(',',$res->RR);
            if ($res->RR && ($res->RR != '') ) {
                foreach ($ar as $r) {
                    if ($r) {
                        $mdz = Carbon::create($mdd[1], $mdd[0], $r);
                        if ($mdz->between($first, $second)) $daysWithRR[] = $mdd[1] . '-' . $mdd[0] . '-' . $r;
                    }
                }
            }
        }

        if (count($daysWithRR)) return null;
        else if (count($reservationsDays) > 6) return null;
        else {
            foreach($period as $p){
                if ( (!in_array($p, $reservationsDays)) && ( $p >= Carbon::now())) return $p;
            }
        }

    }

    function generateDateRange(Carbon $start_date, Carbon $end_date)    {
        $dates = [];
        for($date = $start_date->copy(); $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    public function reservations($rid, Request $request)
    {

        $reservations = ModelReservation::findOrFail($rid);
        $model = Models::findOrFail($reservations->model_id);
        $modelname = $model->modelname;
        $model_email = $model->email;
        $model_id = $model->id;

        $days = ($reservations && $reservations->days) ? explode(',', $reservations->days) : [];
        $status = ($reservations) ? $reservations->status : 1;
        $hour = ($reservations) ? $reservations->hour : '00:00';
        $period = $reservations->month.'-'.$reservations->year;

        $manager_id = 12;


        return view('Admin.schedule.reservationsid', compact('period','model_id','modelname','model_email','manager_id','status','hour','days'));
    }


    function addReservations($period, $res){

        $arr = [];

        foreach($period as $day){

            $cond = (Carbon::parse($day) >= Carbon::today()) ? '' : 'cond';

            $arr[$day] = [
                "cond" => $cond,
                "day_name" => Carbon::parse($day)->format('l'),
                "T1" => [],
                "T2" => [],
                ];
            $d = explode('-',$day)[0];

            foreach ($res as $r){

                $h = $r->hour;
                $t = explode(':',$h)[0];
                if (strpos($r->days, $d) !== false) {
                    if (($t >= 7) && ($t < 19)) $arr[$day]["T1"][] = ["modelname" => $r->model->modelname, "model_id" => $r->model_id, "hour" => $r->hour];
                    else $arr[$day]["T2"][] = ["modelname" => $r->model->modelname, "model_id" => $r->model_id, "hour" => $r->hour];
                }



            }

        }

        return $arr;

    }


    function arrayDays($year, $month, $max){
        $days = [];
        for ($i=1;$i<=$max;$i++){
            if($i<16) $days["p1"][] = decimal($i)."-".decimal($month)."-".$year;
            else $days["p2"][] = decimal($i)."-".decimal($month)."-".$year;
        }
        return $days;
    }

    function arrayDaysFull($year, $month, $max){
        $days = [];
        for ($i=1;$i<=$max;$i++){
            $days[] = decimal($i)."-".decimal($month)."-".$year;
        }
        return $days;
    }



}
