<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AdminRole;

class NewsController extends Controller
{

    public function index()
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');


        return view('Admin.news.home');
    }



}
