<?php

namespace App\Http\Controllers\Admin;

use App\CustomClass\SendNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageCreateRequest;
use App\ModelMessages;
use App\ModelMessagesBody;
use App\ModelMessagesSubject;
use App\Models;
use App\Studio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use Mail;


class MessageController extends Controller
{

    public function index()
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        $allModels = Models::all()->pluck('modelname', 'id')->toArray();

        $mb = ModelMessagesBody::where('to_id','admin')->orWhere('from_id','admin')->pluck('belongs_to')->toArray();
        $mb = array_unique($mb);

        $hasReply = [];
        foreach($mb as  $m){
            $count = ModelMessagesBody::where('belongs_to', $m)->where('to_id', 'admin')->count();
            //mydd($count);
            if ($count > 0) $hasReply[] =  $m;
        }

        $mb = $hasReply;
        //$msg = ModelMessagesSubject::with('body')->whereIn('id', $mb)->limit(30)->orderBy('updated_at','DESC')->get();


        $msg = ModelMessagesSubject::with('body')->whereIn('id', $mb)->orderby('updated_at','DESC')->where('solved', 0)->get()->toArray();
        //$msg = ModelMessagesSubject::with('body')->where('to_id','admin')->orderby('created_at','DESC')->where('solved', 0)->get()->toArray();

        return view('Admin.messages.inbox', compact('msg', 'allModels'));
    }

    public function insert(Request $request, $id){

        if (!$request->has('modelid')) return redirect()->back();

        $message = '';

        if ($request->has('reply')) {
            $request->validate([
                'message' => 'bail|required|min:1|max:1000',
            ]);
            $message = $request->get('message');
        }
        $images = [];
        $fileNameToStore = null;
        $model_id = $request->get('modelid');

        if($files = $request->file('uploadFile')){
            foreach($files as $key => $file){

                $name = 'admin_'.$key;
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = 'msg_' . $name.'_'.time().'.'.$extension;
                $path = $file->storeAs('public/models/msgpictures', $fileNameToStore);
                $images[] = $fileNameToStore;
            }
            $fileNameToStore = implode("|",$images);

        }

        $msgSubj = ModelMessagesSubject::where('id', $id)->first();

        if ($request->has('solve')) {
            $msgSubj->solved = 1;
            $msgSubj->save();
            return redirect('/admin/inbox');

        } else {
            $msgBody = new ModelMessagesBody();
            $msgBody->from_id = 'admin';
            $msgBody->to_id = $model_id;
            $msgBody->belongs_to = $id;
            $msgBody->message = $message;
            $msgBody->attachements = $fileNameToStore;

            $msgBody->save();
            return redirect()->back();
        }

    }

    public function show($id)
    {

        $subj = ModelMessagesSubject::with('body')->where("id", $id)->first();

        $messages = $subj->body;

        $model_id = ($subj->from_id != 'admin') ? $subj->from_id : $subj->to_id;

        $model = Models::with('profile')->where('id',$model_id)->first();

        $sex = ($model->profile) ? $model->profile->sex : 'F';

        $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/bmp','image/svg+xml'];


        $msg = [];
        updateReadAdmin($id);

        foreach ($messages as $message){
            $msg_attachments = [];

            if($message->attachements && ($message->attachements != '')) {
                $msg_attach = explode('|', $message->attachements);

                foreach ($msg_attach as $att){
                    $path_exist = Storage::disk('local')->exists('/public/models/msgpictures/' . $att);

                    if ($path_exist){

                        $mimetype = Storage::mimeType('/public/models/msgpictures/' . $att);

                        $path = asset('storage/models/msgpictures/' . $att);

                        $src = (in_array($mimetype, $allowedMimeTypes) ) ? asset('storage/models/msgpictures/' . $att) : '/img/models/doc.png';

                        $msg_attachments[] = ["filename" => $att, "path" => $path, "src" => $src];

                    }

                }

            }

            $msg[] = ["id" => $message->id, "from_id" => $message->from_id, "to_id" => $message->to_id,
                "belongs_to" => $message->belongs_to, "message" => $message->message, "readtime" => $message->readtime,
                "attachements" => $msg_attachments, "updated_at" => $message->updated_at, "created_at" => $message->created_at];

        }


        return view('Admin.messages.message', compact('subj','messages', 'sex', 'msg','model'));

    }

    public function sendtoone(Request $request){

        if ( $request->has('id') && $request->has('modelname') && $request->has('email') && $request->has('subject') && $request->has('message')){

            $id = $request->get('id');
            $subject = $request->get('subject');
            $msg = $request->get('message');

            //to do insert in db
            $subj = new ModelMessagesSubject();
            $subj->from_id = 'admin';
            $subj->to_id = $id;
            $subj->subject = $subject;
            $subj->solved = 0;
            $subj->save();

            $body = new ModelMessagesBody();
            $body->from_id = 'admin';
            $body->to_id = $id;
            $body->belongs_to = $subj->id;
            $body->message = $msg;
            $body->attachements = null;
            $body->save();


            $modelname = $request->get('modelname');
            $email = $request->get('email');

            //send mail
            Mail::send('Mail.Models.message', ['modelname' => $modelname], function ($message) use ($email) {
                $message->to($email)->subject('You have a new message!');
                $message->from('admin@studio20.group', 'Studio20');
            });

            sleep(1);
            return response()->json(["id" => $id, "status" => 'done']);

        }

    }

    public function onebyone(Request $request){

        if($request->has('sendmailall') ){

            $to = $request->get('sendmailall');
            if($to == 'All') {
                //approved and suspended 1 + 3
                $all = Models::where('status', 1)->select('id', 'modelname', 'email')->get();
                return response()->json($all);

                //return "Mail to ALL!!!";
            }
            else{
                //approved and suspended 1 + 3
                $studio = Studio::where('name', $to)->first();
                if ($studio) {
                    $studio_id = $studio->id;
                    $all = Models::where('studio', $studio_id)->where('status', 1)->select('id', 'modelname', 'email')->get();

                    return response()->json($all);
                } else {
                    return "Studio not found!!";
                }

            }


            //return $to;

        }


    }

    public function composemessage(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->input('send')) {

            $to = $request->input('to');
            $subj = $request->input('subject');
            $message = $request->input('message');
            $fileNameToStore = null;

            if($files=$request->file('uploadFile')){
                $images = [];
                foreach($files as $key => $file){

                    $name='pic'.$key;
                    $extension = $file->getClientOriginalExtension();
                    $fileNameToStore = 'msg_' . $name.'_'.time().'.'.$extension;
                    $path = $file->storeAs('public/models/msgpictures', $fileNameToStore);
                    $images[] = $fileNameToStore;

                }
                $fileNameToStore = implode("|",$images);

            }

            $toGroup = $request->input('inputGroup');
            //1 - modelname 2 - studio 3 - all

            if($toGroup == '2'){

                $studio = Studio::whereName($to)->firstOrFail();

                SendNotification::sendMessageNotificationStudio($studio->id, $subj, $message, $fileNameToStore);

                return redirect()->back();

            }

            if($toGroup == '3'){

                SendNotification::sendMessageNotificationStudio(0, $subj, $message, $fileNameToStore);

                return redirect()->back();
            }

            //send to model
            if($toGroup == '1') {

                $model = Models::with('profile')->whereModelname($to)->firstOrFail();

                SendNotification::sendMessageNotificationModel($model, $subj, $message, $fileNameToStore);

                return redirect()->back();
            }

        }

        return view('Admin.messages.composemessage');

    }

    public function messagessent()
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        $allModels = Models::all()->pluck('modelname', 'id')->toArray();

        $messages = ModelMessagesSubject::with('body')->where('from_id', 'admin')->orderBy('updated_at', 'DESC')->get();

        return view('Admin.messages.messagessent', compact('messages', 'allModels'));

    }

    public function solved()
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        $allModels = Models::all()->pluck('modelname', 'id')->toArray();

        $messages = ModelMessagesSubject::where('solved', 1)->orderBy('updated_at', 'DESC')->get();

        return view('Admin.messages.solved', compact('messages', 'allModels'));

    }

    public function autotype(Request $request)
    {

        if ($request->input('query')) {

            $jsonz = Models::where('status',1)->where('modelname', 'like', '%' . $request->input('query') . '%')->limit(10)->get()->pluck('modelname');

            return json_encode($jsonz);
        }

        if ($request->input('queryStudio')) {

            $jsonz = Studio::whereActive(1)->where('name', 'like', '%' . $request->input('queryStudio') . '%')->limit(10)->pluck('name')->toArray();

            return json_encode($jsonz);

        }

    }

}
