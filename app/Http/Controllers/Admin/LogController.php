<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AdminRole;
use Carbon\Carbon;
use App\ModelLog;

class LogController extends Controller
{

    public function index()
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        $result = ModelLog::orderBy('id', 'DESC')->limit(100)->get();

        foreach($result as $res){
            $res->rank = AdminRole::find($res->rank)->name;
            $res->color = (strpos($res->action, 'Delete') !== false) ? 'bg-danger text-warning' : 'bg-info text-white';
            $res->Date = Carbon::parse($res->time)->format('Y-m-d');
            $res->Hour = Carbon::parse($res->time)->format('H:i:s');
        }

        return view('Admin.log.home', compact('result'));
    }



}
