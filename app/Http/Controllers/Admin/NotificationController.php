<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;


class NotificationController extends Controller
{

    public function index(Request $request)
    {

        if ($request->has('message')){
            $message = $request->input('message');

            $notification = new Notification();
            $notification->message = $message;
            $notification->save();

            $query = Notification::whereNull('read_at')->get();

            return response()->json(['success' => true, 'data' => $query]);

        }
        else return response()->json(['success' => false]);

    }

    public function addReadTime(Request $request){

        if ($request->has('data')){
            $notifArr = $request->input('data');
            $notifArr=json_decode($notifArr);

            foreach ($notifArr as $notif){
                $not = Notification::find($notif);
                if ($not) {
                    $date = date('Y-m-d H:i:s');
                    $not->read_at = $date;
                    $not->save();
                }
            }

        }

    }




}
