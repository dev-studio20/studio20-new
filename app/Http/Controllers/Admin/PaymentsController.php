<?php

namespace App\Http\Controllers\Admin;

use App\APIKeys;
use App\Http\Controllers\Controller;
use App\ModelPaymentHistory;
use App\ModelPeriod;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\CustomClass\SendNotification;
use App\Models;
use App\ModelSale;
use Carbon\Carbon;


class PaymentsController extends Controller
{

    public function index()
    {


        return view('Admin.payments.insertsale');
    }

    //closeperiods
    public function closeperiods(Request $request){
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        $years = ModelPeriod::whereClosed(0)->orderBy('year', 'ASC')->select('year')->distinct('year')->get()->pluck('year')->toArray();


        $p = ($request->has('periods')) ? $request->input('periods') : 1;
        $y = ($request->has('year')) ? $request->input('year') : $years[0];
        //$studios = ($request->has('studio')) ? [$request->input('studio')] : getStudiosIds();

        //$periods = ModelPeriod::with('model', 'studio', 'periodName')->whereHas('model')->where('period', $p)->where('year', $y)->get();

        if ($request->has('period') && $request->has('year') && $request->has('studio')) {
            $p = $request->get('period');
            $y = $request->get('year');
            $s = $request->get('studio');

            $op = ($s) ? '=' : '!=';

            $periods = ModelPeriod::with('model', 'studio', 'periodName','profile')->where('period', $p)
                ->where('year', $y)
                ->where('studio_id', $op, $s)
                ->get();

            return Datatables::of($periods)
                ->addColumn('full_name', function($row){

                    $fullname = $row->profile->last_name .' '. $row->profile->first_name;

                    return $fullname;
                })
                ->rawColumns(['full_name'])
                ->make(true);
        }


        return view('Admin.payments.closeperiods', compact('years','y', 'p'));

    }

    public function openperiod(Request $request){

        if ($request->has('period_id')){
            $p_id = $request->get('period_id');

            $p = ModelPeriod::findOrFail($p_id);
            $p->closed = 0;
            $p->save();

            return ["success" => true];
        }
    }

    public function closeperiod($periodid){

        $period = ModelPeriod::with('model')->where( 'id', $periodid)->first();

        $model = Models::where('id',$period->model_id)->first();

        $sales = ModelSale::where('model_id', $period->model_id)->where('year', $period->year)->where('period', $period->period)->orderBy('day', 'ASC')->get();

        $daysInPeriod = getArrayDaysPeriod($period->year, $period->period);

        return view('Admin.payments.closeperiodid', compact('period', 'periodid', 'model', 'sales','daysInPeriod'));

    }

    public function closeperiodid(Request $request, $periodid){

        $base = ($request->input('base')) ?? 0;
        $commission_90h = ( $request->input('commission_90h') == 'on') ? 2 : 0;
        $commission_100h = ( $request->input('commission_100h') == 'on') ? 5 : 0;
        $commission_10k = ( $request->input('commission_10k') == 'on') ? 5 : 0;
        $commission_loc = ( $request->input('commission_loc') == 'on') ? 30 : 0;
        $affrev = ($request->input('affrev')) ?? 0.00;
        $siterev = ($request->input('siterev')) ?? 0.00;
        $floriarev = ($request->input('floriarev')) ?? 0.00;
        $otherrev = ($request->input('otherrev')) ?? 0.00;
        $deductions = ($request->input('deductions')) ?? 0.00;
        $cursvalutar = ($request->input('cursvalutar')) ?? 0.00;
        $paydue = ($request->input('paydue')) ?? 0.00;
        $paydueeur = ($request->input('paydueeur')) ?? 0.00;

        $period = ModelPeriod::where('id', $periodid)->first();
        $period->closed = 1;
        $period->save();



        $payment_history = ModelPaymentHistory::updateOrCreate(
            [
                'period_id' => $periodid,
                'model_id' => $period->model_id
            ],
            [
                'jasmin_amount' => $period->total_amount,
                'hours' => $period->hours,
                'studio_id' => $period->studio_id,
                'period' => $period->period,
                'year' => $period->year,
                'total_amount_usd' => $paydue,
                'total_amount_eur' => $paydueeur,
                'pay_amount' => 0.00,
                'exchange' => $cursvalutar,
                'commission_base' => $base,
                'commission_total' => $base  + $commission_90h + $commission_100h + $commission_10k + $commission_loc,
                'commission_90h' => $commission_90h,
                'commission_100h' => $commission_100h,
                'commission_10k' => $commission_10k,
                'commission_location' => $commission_loc,
                'affliate_rev' => $affrev,
                'personal_rev' => $siterev,
                'floria_rev' => $floriarev,
                'others_rev' => $otherrev,
                'deductions' => $deductions,
            ]
        );

        $model = Models::where('id',$period->model_id)->first();

        $modelname = ($model) ? $model->modelname : '';

        $msg = 'Period Closed: '.$modelname . ' (' .$period->period.')';

        SendNotification::closePeriodEvent($msg);

        return redirect('/admin/closeperiod/');
    }


    public function updateSalesPeriod(Request $request){

        if( $request->has('period') && $request->has('year')  && $request->has('index')){

            $p = $request->get('period');
            $y = $request->get('year');
            $index = $request->get('index');

            $arr = getArrayDaysPeriod($y, $p);
            $mid = [];

            $ad = explode('-',$arr[$index]);

            $y = (int)$ad[0];
            $mz = (int)$ad[1];
            $d = (int)$ad[2];

            $mid = [];

            $mp = ModelPeriod::where('period', $p)->where('year', $y)->where('closed',0)->get();

            foreach($mp as $m){
                $mid[] = $m->model_id;
            }

            $models1 = Models::whereIn('id',$mid)->where('key_id',1)->select('modelname')->get()->toArray();
            $models2 = Models::whereIn('id',$mid)->where('key_id',2)->select('modelname')->get()->toArray();

            $all = $this->getSalesDay($arr[$index], $models1, $models2);

            foreach($all as $k => $v){

                $model = Models::where('modelname',$k)->first();
                if($model){
                    //$sale = ModelSale::where('model_id',$model->id)->where('period', $p)->where('year',$y)->where('month',$mz)->where('day',$d)->first();
                    $sale = ModelSale::updateOrCreate([
                        'model_id' => $model->id,
                        'period' => $p,
                        'year' => $y,
                        'month' => $mz,
                        'day' => $d
                    ],[
                        'sum' => $v["earnings"],
                        'studio_id' => $model->studio,
                        'hours' => $v["worktimeneeded"]
                    ]);
                }

            }

            return ["day" => $arr[$index],"count_models" => count($all), "count_days" => count($arr)];

        }

    }


    public function getSalesDay($day, $models1, $models2){

        $fromDate = $day . " 00:00:00";
        $nextday = Carbon::createFromFormat('Y-m-d', $day)->add(1, 'day')->format('Y-m-d');
        $toDate = $nextday . " 00:00:00";

        $key1 = APIKeys::find(1)->bearer;
        $key2 = APIKeys::find(2)->bearer;

        $all = [];
        $all1 = [];
        $all2 = [];

        $chuncks1 = array_chunk($models1, 60);
        foreach($chuncks1 as $models){
            sleep(1);
            $sales = getSalesInterval($models, $key1, $fromDate, $toDate);
            $all1=array_merge($all1, $sales);
            sleep(1);
        }

        $chuncks2 = array_chunk($models2, 60);
        foreach($chuncks2 as $models2){
            sleep(1);
            $sales2 = getSalesInterval($models2, $key2, $fromDate, $toDate);
            $all2=array_merge($all2, $sales2);
            sleep(1);
        }

        $all = array_merge($all1,$all2);

        return $all;

    }

    function curlAuth($auth, $url){

        $authorization = $auth;
        $cSession = curl_init();

        //step2
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($cSession);
        curl_close($cSession);
        $res = json_decode($result, true);

        return $res;

    }

    public function  checkPeriodSales(Request $request)
    {

        if ($request->has('period') && $request->has('year')){
            $m_p = $request->get('period');
            $m_y = $request->get('year');

            $arr = getArrayDaysPeriod($m_y, $m_p);
            $start = $arr[0];
            $end = $arr[count($arr)-1];

            $startDate = Carbon::parse($start)->format('Y-m-d H:i:s');
            $p = getPeriodNumberFromDate($startDate);
            $period = $p["period"];
            $year = $p["year"];

            $endDate = Carbon::parse($end)->format('Y-m-d') . " 23:59:59";

            $monthSales = getSalesAndTimeInterval($startDate, $endDate);


            $from = $monthSales["from"];
            $to = $monthSales["to"];
            $models = $monthSales["dataByModels"];
            $notFound = [];

            foreach ($models as $key => $model) {
                //studio, earnings, worktime, worktimeneeded

                if (array_key_exists('studio', $model) && array_key_exists('earnings', $model) && array_key_exists('worktimeneeded', $model)) {
                    $studio = $model["studio"];
                    $earnings = $model["earnings"];
                    //$workt = $model["worktime"];
                    $worktime = $model["worktimeneeded"];

                    $modelname = Models::where('modelname', $key)->first();


                    if ($modelname) {
                        $studio = $modelname->studio;
                        $model_id = $modelname->id;

                        //check if period exist and is not closed
                        $model_p = ModelPeriod::where('model_id', $model_id)->where('period',$period)->where('year', $year)->where('closed',0)->update(['total_amount_jasmin' => $earnings]);

                    }
                } //else $notFound[$key] = $model;

            }

            return 'done';

        }






        //event(new StatusLiked('Bot For Sales!'));
        //event(new SystemNotifications('BOT: Sales Updated!'));

    }

    public function makepaymentid(Request $request){

        if ($request->has('pay_hystory_id')){
            $id = $request->input('pay_hystory_id');

            $date = $request->input('date');

            $mph = ModelPaymentHistory::findOrFail($id);
            $mph->pay_done = 1;
            $mph->pay_amount = $mph->total_amount_eur;
            //$mph->pay_date = date('Y-m-d H:i:s');
            $mph->pay_date = $date;
            $mph->save();

            SendNotification::makePaymentEvent($mph->model_id);

            return ["success" => true];

        } else return ["success" => false];


    }

    //makepayments
    public function makepayment(Request $request){
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax() && $request->has('studio')) {

            $studios = $request->input('studio');

            $op = ($studios) ? '=' : '!=';

            $data = ModelPaymentHistory::with('model','studio','periodName')->where('pay_done',0)->where('pay_request', 1)->where('studio_id', $op, $studios)->get();

            return  Datatables::of($data)->make(true);

        }

        return view('Admin.payments.makepayments');

    }

    //paymenthistory
    public function paymenthistory(Request $request){
        if (in_array(getRole(),[5,6])) return redirect('/admin');
        //$years = [ now()->year - 1, now()->year, now()->year + 1 ];

        $years = ModelPaymentHistory::where('pay_done',1)->orderBy('year', 'ASC')->select('year')->distinct('year')->get()->pluck('year')->toArray();

        //$arr_py = getCurrentPeriodAndYearMinus2();

        $p = ($request->has('periods')) ? $request->input('periods') : 1;
        $y = ($request->has('year')) ? $request->input('year') : $years[0];

        if ($request->ajax() && $request->has('period') && $request->has('year') && $request->has('studio')) {

            $p = ($request->has('period')) ? $request->input('period') : $p;
            $y = ($request->has('year')) ? $request->input('year') : $y;

            $studios = $request->input('studio');

            $op = ($studios) ? '=' : '!=';

            $data = ModelPaymentHistory::with('model','studio','periodName')->where('pay_done','=',1)->where('period', $p)->where('year', $y)->where('studio_id', $op, $studios)->get();


            return  Datatables::of($data)->make(true);

        }

        return view('Admin.payments.paymenthistory', compact('years','y', 'p'));

    }

}
