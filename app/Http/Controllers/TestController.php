<?php

namespace App\Http\Controllers;

use App\APIKeys;
use App\ModelKeys;
use App\ModelPeriod;
use App\Models;
use Illuminate\Http\Request;
use Carbon\Carbon;


class TestController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {

        $login_url = 'https://www.chat.fan/en/auth/login';

        //captcha_type: google
        //form_token: 5db254afef5977629a4ce6c46a150aa79c825f78
        //username: oneaccountforall
        //password: 05081984
        //keepmeloggedin: on
        //captcha_response: 03AOLTBLRYeHR1z2FNJcYGMFK-978aEjNVmeug82CHFp10aFgVKJI5O9r_1paaJxq6vh32IpftWBueTfSPX-AzVmOqnEAzX8Sdk6obw5SfuOZnylK3jrXRzVhjTrLPsbvVWcvB66cLfr0gIekN22AlQ3sRZp_GaXyo8L4QuEbPhc2yv-DDFRQgXiYMnhnrhgidZC7K9DgtALVXiHNmEmJi_TRXy5fOnake7zRIeTqhZQehjq4OzJJThlTFToCVcnA7M4zc7SIT4GukFlGK54iz7XVN48L9LVDdhDRxaTPfbFkRjAGXmeRU-LtQdE_e-64_KlEpKj7gr7ultKJbY43y0XEJDQHHlejgCcUr7QrCUw4e8QS7M1pRMMp8hMVs75m0TV1U4Vehnq362g-tS9-Xh11Xbr6qEdSFVGN5hjRdLff2BwIqENMYOE3TH1NXiWhYnaithvcmiYY-qhP-sWQ1gWump_nTCsVzv2MfuMezKr_3n7m1NUJ5MYrQr9nv2WGEpXXu8NrXx4Yk

        $username_filed = 'username';
        $username = 'oneaccountforall';
        $password_filed = 'password';
        $password = '05081984';

        //These are the post data username and password
        $post_data = 'username=oneaccountforall&amp;password=05081984';

        //Create a curl object
        $ch = curl_init();

        //Set the useragent
        $agent = $_SERVER['HTTP_USER_AGENT'];
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);

        //Set the URL
        curl_setopt($ch, CURLOPT_URL, $login_url );

        //This is a POST query
        curl_setopt($ch, CURLOPT_POST, 1 );

        //Set the post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        //We want the content after the query
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //Follow Location redirects
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        /*
        Set the cookie storing files
        Cookie files are necessary since we are logging and session data needs to be saved
        */

        $cookie_file = storage_path('app/cookies.txt');

        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);

        //Execute the action to login
        $postResult = curl_exec($ch);
        echo  ($postResult);

//        $url = 'https://www.chat.fan/en/free/chat/NataliaSynn#!/';
//        curl_setopt($ch, CURLOPT_URL, $url);
//        $output = curl_exec ($ch);
//        curl_close ($ch);
//        dd  ($output);

        //return 'here';

    }


    public function getApiKeys(){

        //get all models
        $models = Models::all();


        //foreach call function
        foreach($models as $model){
            if (!( ($model->key) && ($model->key->key_id) )) {
                //dd($model);
                findKey($model);
            }

            sleep(0.2);
        }

        return 'done';

    }

}
