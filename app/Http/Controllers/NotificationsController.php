<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Storage;
use App\CustomClass\SendNotification;


class NotificationsController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function test(Request $request)
    {
        if($request->has('type') && $request->has('val')){
            $type = $request->get('type');
            $val = $request->get('val');

            $message = "You have a new message..";
            $response = '';

            if ($type == "phone") $response = SendNotification::test_send_sms($val, $message);

            if ($type == "email") SendNotification::test_send_email($val);

            return ["type" => $type, "val" => $val, "response" => $response];
        }

    }



}
