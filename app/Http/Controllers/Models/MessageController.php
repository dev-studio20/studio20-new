<?php

namespace App\Http\Controllers\Models;

use App\CustomClass\SendNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageCreateRequest;
use App\ModelMessagesBody;
use App\ModelMessagesMass;
use App\ModelMessagesSubject;
use App\ModelProfile;
use Auth;
use Storage;

class MessageController extends Controller
{

//    public function messages_old(){
//
//        $model = Auth::guard('model')->user();
//        $studio_id = $model->studio;
//
//        $messages = ModelMessagesSubject::with('body')->where('to_id', $model->id)->limit(30)->orderBy('updated_at','DESC')->get();
//
//        $msgout = ModelMessagesSubject::with('body')->where('from_id', $model->id)->limit(30)->orderBy('updated_at','DESC')->get();
//
//        $msgSystem = ModelMessagesMass::whereIn('to_studio', [$studio_id, 0])->orderBy('created_at','DESC')->get();
//
//        return view('Models.messages.messages', compact('messages','msgout','msgSystem'));
//
//    }

    public function messages(){

        $model = Auth::guard('model')->user();
        $studio_id = $model->studio;

        $mb = ModelMessagesBody::where('to_id',$model->id)->orWhere('from_id',$model->id)->pluck('belongs_to')->toArray();
        $mb = array_unique($mb);

        $hasReply = [];
        foreach($mb as  $m){
            $count = ModelMessagesBody::where('belongs_to', $m)->where('from_id', 'admin')->count();
            //mydd($count);
            if ($count > 0) $hasReply[] =  $m;
        }

        $mb = $hasReply;
        $messages = ModelMessagesSubject::with('body')->whereIn('id', $mb)->limit(30)->orderBy('updated_at','DESC')->get();
        //$messages = ModelMessagesSubject::with('body')->where('to_id', $model->id)->limit(30)->orderBy('updated_at','DESC')->get();

        $msgout = ModelMessagesSubject::with('body')->where('from_id', $model->id)->limit(30)->orderBy('updated_at','DESC')->get();

        $msgSystem = ModelMessagesMass::whereIn('to_studio', [$studio_id, 0])->orderBy('created_at','DESC')->get();

        return view('Models.messages.messages_test', compact('messages','msgout','msgSystem'));

    }

    public function createmsg(){

        return view('Models.messages.messages_compose');
    }

    public function createmsgpost(MessageCreateRequest $request){

        ini_set('max_execution_time', 3000);

        $id = Auth::guard('model')->user()->id;
        $modelname = Auth::guard('model')->user()->modelname;
        $images = [];
        $fileNameToStore = null;

        $subject = $request->get('subject');
        $message = $request->get('message');

        if($files=$request->file('uploadFile')){
            foreach($files as $key => $file){

                $name='pic'.$key;
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = 'msg_' . $name.'_'.time().'.'.$extension;
                $path = $file->storeAs('public/models/msgpictures', $fileNameToStore);
                $images[] = $fileNameToStore;

            }
            $fileNameToStore = implode("|",$images);

        }


        $subj = new ModelMessagesSubject();
        $subj->from_id = $id;
        $subj->to_id = 'admin';
        $subj->subject = $subject;
        $subj->solved = 0;
        $subj->save();

        $body = new ModelMessagesBody();
        $body->from_id = $id;
        $body->to_id = 'admin';
        $body->belongs_to = $subj->id;
        $body->message = $message;
        $body->attachements = $fileNameToStore;
        $body->save();

        SendNotification::newMessage($modelname);

        return redirect('/models/messages');


    }

    public function showmass($id){

        $model = Auth::guard('model')->user();
        $model_id= $model->id;
        $model_studio = $model->studio;
        $subj_attachments = [];

        $message = ModelMessagesMass::where('id',$id)->orderBy('created_at','DESC')->firstOrFail();
        if (($message->to_studio == $model_studio) || ($message->to_studio == 0)){

            if($message->attachements && ($message->attachements != '')) {
                $msg_attach = explode('|', $message->attachements);
                $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/bmp','image/svg+xml'];

                foreach ($msg_attach as $att){
                    $path_exist = Storage::disk('local')->exists('/public/models/msgpictures/' . $att);

                    if ($path_exist){
                        $mimetype = Storage::mimeType('/public/models/msgpictures/' . $att);

                        $path = asset('storage/models/msgpictures/' . $att);

                        $src = (in_array($mimetype, $allowedMimeTypes) ) ? asset('storage/models/msgpictures/' . $att) : '/img/models/doc.png';

                        $subj_attachments[] = ["filename" => $att, "path" => $path, "src" => $src];
                    }

                }

            }

            return view('Models.messages.messagemassid', compact('message', 'subj_attachments'));

        } else return redirect('/models');
    }

    public function show($id){

        //check if id belongs to this model
        $model = Auth::guard('model')->user();

        $profile = ModelProfile::where('model_id',$model->id)->first();

        $sex = ($profile) ? $profile->sex : 'F';

        $subj = ModelMessagesSubject::with('body')->where('id',$id)->first();

        $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/bmp','image/svg+xml'];

        $messages = $subj->body;
        $msg = [];


        foreach ($messages as $message){
            $msg_attachments = [];

            if($message->to_id == $model->id){
                $m = ModelMessagesBody::where('id', $message->id)->update(["readtime" => time()]);
            }

            if($message->attachements && ($message->attachements != '')) {
                $msg_attach = explode('|', $message->attachements);

                foreach ($msg_attach as $att){
                    $path_exist = Storage::disk('local')->exists('/public/models/msgpictures/' . $att);

                    if ($path_exist){
                        $mimetype = Storage::mimeType('/public/models/msgpictures/' . $att);

                        $path = asset('storage/models/msgpictures/' . $att);

                        $src = (in_array($mimetype, $allowedMimeTypes) ) ? asset('storage/models/msgpictures/' . $att) : '/img/models/doc.png';

                        $msg_attachments[] = ["filename" => $att, "path" => $path, "src" => $src];
                    }

                }

            }

            $msg[] = ["id" => $message->id, "from_id" => $message->from_id, "to_id" => $message->to_id,
                "belongs_to" => $message->belongs_to, "message" => $message->message, "readtime" => $message->readtime,
                "attachements" => $msg_attachments, "updated_at" => $message->updated_at, "created_at" => $message->created_at];

        }

        if ( ($subj->from_id != $model->id) && ($subj->to_id != $model->id) ) return redirect('/models');

        return view('Models.messages.messageid', compact('subj', 'sex', 'msg'));

    }

    public function insert(MessageCreateRequest $request, $id){

        $model = Auth::guard('model')->user();

        if (!$model || !$request->has('message')) return redirect()->back();

        $message = ($request->has('message')) ? $request->get('message') : 'error..';
        $images = [];
        $fileNameToStore = null;

        if($files = $request->file('uploadFile')){
            foreach($files as $key => $file){

                $name = $model->modelname.'_'.$key;
                $extension = $file->getClientOriginalExtension();
                $fileNameToStore = 'msg_' . $name.'_'.time().'.'.$extension;
                $path = $file->storeAs('public/models/msgpictures', $fileNameToStore);
                $images[] = $fileNameToStore;
            }
            $fileNameToStore = implode("|",$images);

        }

        $msgBody = new ModelMessagesBody();
        $msgBody->from_id = $model->id;
        $msgBody->to_id = 'admin';
        $msgBody->belongs_to = $id;
        $msgBody->message = $message;
        $msgBody->attachements = $fileNameToStore;

        $msgBody->save();

        $msgSubj = ModelMessagesSubject::where('id', $id)->first();
        $msgSubj->solved = 0;
        $msgSubj->save();

        return redirect('/models/messages');

    }

}
