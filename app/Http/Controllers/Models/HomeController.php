<?php

namespace App\Http\Controllers\Models;

use App\Http\Controllers\Controller;
use App\AdminRole;
use App\Http\Requests\MessageCreateRequest;
use App\Http\Requests\MessageRequest;
use App\ModelMessages;
use App\ModelMessagesBody;
use App\ModelMessagesSubject;
use App\ModelPaymentHistory;
use App\ModelPeriod;
use App\ModelProfile;
use App\ModelReservation;
use App\ModelSale;
use App\ModelSignature;
use App\Studio;
use Auth;
use App\Models;
use DB;
use Illuminate\Http\Request;
use URL;
use Storage;
use App\CustomClass\SendNotification;
use Carbon\Carbon;

class HomeController extends Controller
{

    public function index()
    {
        if (Auth::guard('model')->user()->status != 1) return redirect()->route('models.approval');

        $modelid = Auth::guard('model')->user()->id;

        $period = $this->getHours($modelid);

        $onlineHoursThisPeriod = $period["hours"];

        $earningsThisPeriod = $period["sum"];

        $arr = $this->getLastPeriods($modelid);

        $periodName = getAllPeridos()[getCurrentPeriod()];

        $lsTotal = $arr["total"];
        $lsMonths = $arr["months"];
        $lsSums = $arr["sums"];
        $lsMax = max($arr["sums"]);

        return view('Models.home', compact('onlineHoursThisPeriod', 'earningsThisPeriod', 'lsTotal', 'lsMonths', 'lsSums', 'lsMax', 'periodName'));
    }

    public function approval(){

        if (Auth::guard('model')->user() && (Auth::guard('model')->user()->status == 1) ){
            return redirect('/login');
        }

        $model = Models::with('contract','profile','signature')->findOrFail(Auth::guard('model')->user()->id);

        $emailApproved = $model->email_verified_at;
        $phoneApproved = $model->phone_verified_at;

        $pic1reason = $model->contract->reason_pic1;
        $pic2reason = $model->contract->reason_pic2;
        $pic3reason = $model->contract->reason_pic3;

        $stats = [0 => 'Pendig approval', 1 => 'Approved', 2 => 'Rejected'];

        $photo1Status = $model->contract->pic1_status;
        $photo2Status = $model->contract->pic2_status;
        $photo3Status = $model->contract->pic3_status;

        $id = $model->id;

        $pic1 = ($model->contract->pic1) ? "storage/models/images/" . $model->contract->pic1 : "img/models/id1.jpg";
        $pic2 = ($model->contract->pic2) ? "storage/models/images/" . $model->contract->pic2 : "img/models/id2.jpg";
        $pic3 = ($model->contract->pic3) ? "storage/models/images/" . $model->contract->pic3 : "img/models/id3.jpg";
        $email = $model->email;
        $phone = $model->profile->phonenumber;

        $status = $model->status;
        $reason = '';

        if ($status == 3){
            $reason = $model->contract->reason;
        }

        //sms verification
        //$user = User::find($id);
        $smslink = URL::signedRoute('autologinphone', ['model' => $model]);

        //contract re-sign
        $showContractSign = '';
        $findSignature = ($model->signature) ? $model->signature->model_signature : null;
        if (($findSignature !== null) && ($findSignature !== '')) $showContractSign = 'invisible';


        return view('Models.approval', compact('emailApproved', 'phoneApproved', 'pic1', 'pic2', 'pic3', 'email', 'phone', 'photo1Status', 'photo2Status', 'photo3Status', 'stats', 'code', 'pic1reason', 'pic2reason', 'pic3reason', 'smslink', 'reason', 'status', 'showContractSign'));


    }

    public function profile(){

        if (Auth::guard('model')->user()->status != 1) return redirect()->route('models.approval');

        $modelid = Auth::guard('model')->user()->id;

        $model = Models::with('profile', 'paymentDetails', 'address')->where('id', $modelid)->first();

        //dd($model);

        return view('Models.profile', compact('model'));
    }

    public function sales(){

        if (Auth::guard('model')->user()->status != 1) return redirect()->route('models.approval');

        $modelid = Auth::guard('model')->user()->id;

        $year = now()->year;

        $period = getCurrentPeriod();

        $periodName = getAllPeridos()[$period];

        $sales = ModelSale::where('model_id', $modelid)->where('period', $period)->where('year', $year)->orderBy('day', 'ASC')->get();

        return view('Models.sales', compact('sales', 'periodName', 'year'));

    }

    public function payment(){

        $model = Auth::guard('model')->user();

        if($model) $modelid = $model->id;
        else return redirect()->back();


        $periods = ModelPeriod::with('payHistory')->whereHas('payHistory', function ($q) {
            $q->whereNotNull('id');
        })->where('model_id', $modelid)->orderBy('year', 'DESC')->orderBy('period','DESC')->get();


        $studio = Studio::findOrFail($model->studio);

        $d = now()->format('d');

        $next = Carbon::now()->addMonths(1)->format('Y-m');
        $next = explode('-',$next);
        $y = $next[0];
        $m = $next[1];

        $res = ModelReservation::where('model_id', $model->id)->where('year', $y)->where('month',$m)->first();

        $condition = ($res && $res->status);

        if ($d > 15 ){
            $nextplus = Carbon::now()->addMonths(2)->format('Y-m');
            $nextplus = explode('-',$nextplus);
            $yplus = $nextplus[0];
            $mplus = $nextplus[1];

            $resplus = ModelReservation::where('model_id', $model->id)->where('year', $yplus)->where('month',$mplus)->first();

            $conditionplus = ($resplus && $resplus->status);
            $condition = ($condition && $conditionplus);
        }



        return view('Models.payment', compact('periods','studio','condition'));

    }

    public function payment2(){

        $model = Auth::guard('model')->user();

        if($model) $modelid = $model->id;
        else return redirect()->back();

        $condition = [];

        $periods = ModelPeriod::with('payHistory')->whereHas('payHistory', function ($q) {
            $q->whereNotNull('id');
        })->where('model_id', $modelid)->orderBy('year', 'DESC')->orderBy('period','DESC')->get();


        $studio = Studio::findOrFail($model->studio);

        foreach ($periods as $period){

            $mp = get_month_period($period->period);

            $m = $mp[0];
            $p = $mp[1];

            $next = Carbon::parse($period->year.'-'.$m)->addMonths(1)->format('Y-m');
            $next = explode('-',$next);
            $y = $next[0];
            $mo = $next[1];

            $res = ModelReservation::where('model_id', $model->id)->where('year', $y)->where('month',$mo)->first();

            $cond = ($res && $res->status);

            if ($p == 2){
                $nextplus = Carbon::parse($period->year.'-'.$m)->addMonths(2)->format('Y-m');
                $nextplus = explode('-',$nextplus);
                $yplus = $nextplus[0];
                $mplus = $nextplus[1];

                $resplus = ModelReservation::where('model_id', $model->id)->where('year', $yplus)->where('month',$mplus)->first();

                $condplus = ($resplus && $resplus->status);
                $cond = ($cond && $condplus);
            }

            $condition[] = $cond;

        }

        return view('Models.payment2', compact('periods','studio','condition'));

    }


    public function requestpayment(Request $request){

        if($request->has('periodhistoryid')) {
            $id = $request->input('periodhistoryid');

            $period = ModelPaymentHistory::findOrFail($id);
            $period->pay_request = 1;
            $period->pay_request_date = now()->format('Y-m-d H:i:s');
            $period->save();

            $model = Auth::guard('model')->user()->modelname;

            SendNotification::requestPaymentEvent($model, $period->period);

            return ["success" => true, "data" => $id];
        } else return ["success" => false, "data" => 'error..'];


    }

    public function paymentdetails($id){

        //check if id belongs to current user
        $modelid = Auth::guard('model')->user()->id;

        $details = ModelPaymentHistory::findOrFail($id);

        if ($details->model_id == $modelid) {

            $period = ModelPeriod::findOrFail($details->period_id);
            $periodname = getAllPeridos()[$period->period];

            return view('Models.paymentdetails', compact('details', 'periodname'));
        }
        else return redirect('/models/payment');



    }

    public function reservations(){

        if (Auth::guard('model')->user()->status != 1) return redirect()->route('models.approval');

        $id =  Auth::guard('model')->user()->id;

        $monthnames = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        );

        $d = date("d");
        $j = date("j");
        $n = date("n");
        $m = date("m");
        $y = date("Y");
        $nmm = $m + 1;


        if ($nmm > 12) $nmm = $nmm -12;

        if ($nmm < 10) {
            $nmm = "0$nmm";
        }

        $nmy = date("Y", strtotime("+1 month"));
        $tmcd = cal_days_in_month(CAL_GREGORIAN, $m, $y);
        $nmcd = cal_days_in_month(CAL_GREGORIAN, $nmm, $nmy);
        $thismonthlastday = "$m-$tmcd-$y";

        $thismonth = date("m-Y");
        $thism = date("m");
        $thisy = date("Y");

        $nextmonth = "$nmm-$nmy";
        $nextm = (int) (explode('-',$nextmonth)[0]);
        $nexty = explode('-',$nextmonth)[1];

        $thismonthname = date("F");
        $key = array_search($thismonthname, $monthnames); // $key = 2;
        $key = $key + 1;
        if ($key > 11) $key = $key - 12;
        $nextmonthname = $monthnames[$key];
        $nextmonthfirstday = "$nmm-01-$nmy";
        $nextmonthlastday = "$nmm-$nmcd-$nmy";

        $thismonthresult = ModelReservation::where('month', $thism)->where('year', $thisy)->where('model_id', $id)->first();

        $nextmonthresult = ModelReservation::where('month', $nextm)->where('year', $nexty)->where('model_id', $id)->first();

        if ($thismonthresult && $thismonthresult->days) {
            $thismonthcount = count(explode(',',$thismonthresult->days));
            $thismonthhour = $thismonthresult->hour;
            $thismonthdays = $thismonthresult->days;
            $thismonthcolor = $thismonthresult->status;

        } else {
            $thismonthcount = 0;
            $thismonthhour = '06:00 AM';
            $thismonthdays = '';
            $thismonthcolor = 1;
        }



        if ($nextmonthresult && $nextmonthresult->days) {
            $nextmonthcount = count(explode(',',$nextmonthresult->days));
            $nextmonthhour = $nextmonthresult->hour;
            $nextmonthdays = $nextmonthresult->days;
            $nextmonthcolor = $nextmonthresult->status;
        } else {
            $nextmonthcount = 0;
            $nextmonthhour = '06:00 AM';
            $nextmonthdays = '';
            $nextmonthcolor = 1;
        }

        $js_month_format = $m . "-01-" . $y;

        $colorthis = ($thismonthcolor) ? "red!important" : "yellow!important";
        $colornext = ($nextmonthcolor) ? "red!important" : "yellow!important";
        $fontthis = ($thismonthcolor) ? "white!important" : "black!important";
        $fontnext = ($nextmonthcolor) ? "white!important" : "black!important";

        return view('Models.reservations', compact('colorthis', 'colornext', 'fontthis', 'fontnext', 'thismonthname', 'thismonthdays', 'thismonthcount', 'thismonthhour', 'nextmonth','nextmonthname', 'nextmonthcount', 'nextmonthhour', 'nextmonthdays', 'nextmonthfirstday', 'nextmonthlastday', 'thismonthlastday', 'js_month_format'));

    }

    public function reservations2(Request $request){

        $id = Auth::guard('model')->user()->id;
        $modelname = Auth::guard('model')->user()->modelname;
        $studio_id = Auth::guard('model')->user()->studio;

        if ($request->input('sendnextmonth')) {

            $m = date("m");
            $y = date("Y");
            $nmm = $m + 1;

            if($nmm > 12) $nmm = $nmm -12;
            if ($nmm < 10) {
                $nmm = "0$nmm";
            }

            $nmy = date("Y", strtotime("+1 month"));

            $nextmonth = $nmm;
            $nextyear = $nmy;

            $datesnextmonth = $request->input('datesnextmonth');
            $datesnextmonth = str_replace(' ', '', $datesnextmonth);

            //get day format from string ex: from 10-12-2018,10-13-2018,..,.. to 12,13,..
            $arrdates = explode(",", $datesnextmonth);
            $newardates = array();

            foreach ($arrdates as $onlyday){
                if($onlyday != ''){


                    //check if selected days are in next month
                    $nextmonthh = Carbon::now()->month + 1;

                    $day = Carbon::createFromFormat('m-d-Y', $onlyday);

                    $onlyday = substr($onlyday, strpos($onlyday, "-") + 1);
                    $onlyday = explode('-', $onlyday,2)[0];
                    $newardates[] = $onlyday;
                } else {
                    ModelReservation::where('model_id',$id)->where('month',$nextmonth)->where('year',$nextyear)->delete();
                }
            }
            $datesnextmonth = implode(",",$newardates);

            $hournextmonth = $request->input('hournextmonth');

            if ((!empty($datesnextmonth)) AND (!empty($hournextmonth))) {

                $hournextmonth = explode(' ',$hournextmonth)[0];

                //update or create reservations in nextmonth
                $month =  ModelReservation::where('model_id',$id)->where('month',$nextmonth)->where('year',$nextyear)->first();
                if ($month) ModelReservation::where('model_id',$id)->where('month',$nextmonth)->where('year',$nextyear)->update(['days'=>$datesnextmonth, 'status' => 0]);
                else {
                    $month = new ModelReservation();
                    $month->model_id = $id;
                    $month->studio_id = $studio_id;
                    $month->month = $nextmonth;
                    $month->year = $nextyear;
                    $month->days = $datesnextmonth;
                    $month->hour = $hournextmonth;
                    $month->status = 0;
                    $month->save();
                }
                //$m_res = ModelReservation::insert(['model_id' => $id, 'studio_id' => $studio_id,'month' => $nextmonth,'year' => $nextyear, 'days' => $datesnextmonth, 'hour' => $hournextmonth, 'status' => 0]);

                //log and send mail and sms
                //DB::table('reservations_model_log')->insert(['model_id' => $id, 'month' => $nextmonth, 'days' => $datesnextmonth, 'hour' => $hournextmonth, 'action' => 'model_res']);
                //test
                //$this->sendReservationMail($id, $nextmonth, $hournextmonth);
                SendNotification::reservationMadeEvent($month->id, $modelname, $month->days, $month->month);
            }
        }



        return back();

    }

    public function reservations1(Request $request){

        $id = Auth::guard('model')->user()->id;
        $modelname = Auth::guard('model')->user()->modelname;
        $studio_id = Auth::guard('model')->user()->studio;

        $thismonth = date("m");
        $thisyear = date("Y");

        $datesthismonth = $request->input('datesthismonth');
        $datesthismonth = str_replace(' ', '', $datesthismonth);

        //get day format from string ex: from 10-12-2018,10-13-2018,..,.. to 12,13,..
        $arrdates = explode(",", $datesthismonth);
        $newardates = array();

        foreach ($arrdates as $onlyday) {
            if($onlyday != '') {

                //check if selected days are in curent month
                $today = Carbon::today();
                $day = Carbon::createFromFormat('m-d-Y', $onlyday)->toDateTimeString();
                if ($day < $today) continue;

                $onlyday = substr($onlyday, strpos($onlyday, "-") + 1);
                $onlyday = explode('-', $onlyday, 2)[0];
                $newardates[] = $onlyday;
            }  else {
                ModelReservation::where('model_id',$id)->where('month',$thismonth)->where('year',$thisyear)->delete();
            }
        }

        $datesthismonth = implode(",",$newardates);




        if ($request->input('sendthismonth')) {

            $hourthismonth = $request->input('hourthismonth');

            if ((!empty($datesthismonth)) AND (!empty($hourthismonth))) {

                $hourthismonth = explode(' ',$hourthismonth)[0];

                //update or create reservations in month
                $month = ModelReservation::where('model_id',$id)->where('month',$thismonth)->where('year',$thisyear)->first();
                if ($month) ModelReservation::where('model_id',$id)->where('month',$thismonth)->where('year',$thisyear)->update(['days'=>$datesthismonth, 'hour' => $hourthismonth, 'status' => 0]);
                else {
                    $month = new ModelReservation();
                    $month->model_id = $id;
                    $month->studio_id = $studio_id;
                    $month->month = $thismonth;
                    $month->year = $thisyear;
                    $month->days = $datesthismonth;
                    $month->hour = $hourthismonth;
                    $month->status = 0;
                    $month->save();
                    //$m_res = ModelReservation::insert(['model_id' => $id, 'studio_id' => $studio_id,'month' => $thismonth, 'year' =>$thisyear, 'days' => $datesthismonth, 'hour' => $hourthismonth, 'status' => 0]);
                }

                //TO-DO log and send mail and sms
                //DB::table('reservations_model_log')->insert(['model_id' => $id, 'month' => $thismonth, 'days' => $datesthismonth, 'hour' => $hourthismonth, 'action' => 'model_res']);

                //$this->sendReservationMail($id, $thismonth, $hourthismonth);
                SendNotification::reservationMadeEvent($month->id, $modelname, $month->days, $month->month);
            }
        }

        return back();

    }

    public function news(){

        return view('Models.news');

    }

    public function contract(){

        if (Auth::guard('model')->user()->status != 1) return redirect()->route('models.approval');

        $id = Auth::guard('model')->user()->id;

        $storagePath = "models/contracts/contract_".$id.".pdf";

        $exist = Storage::exists('contracts/contract_'.$id.'.pdf');
        $contract = ($exist) ? Storage::get('contracts/contract_'.$id.'.pdf') : null;

        if($contract) return view('Models.contract', compact('storagePath'));
        else return redirect('/models');



    }

    public function getHours($modelid){

        $y = now()->year;
        $p = getCurrentPeriod();

        $period = ModelPeriod::where('model_id', $modelid)->where('period', $p)->where('year', $y)->first();

        $hours = convertSecondsToHours(($period) ? $period->hours : 0);

        $sum = ($period) ? $period->total_amount : 0;

        return ["hours" => $hours, "sum" => $sum];
    }

    public function getLastPeriods($modelid){

        $periods = ModelPeriod::where('model_id', $modelid)->orderBy('period', 'DESC')->orderBy('year', 'DESC')->limit(6)->get();

        $months = [];
        $sums = [];
        $total = 0;

        $per = getPeriodNumberFromDate(now()->format('Y-m-d'))["period"];

        foreach ($periods as $period){
            $total += $period->total_amount;
            $sums[] = $period->total_amount;
            $per = $period->period;
            $months[] = getAllPeridos()[$period->period];
        }

        for($i=0;$i<=6;$i++){
            if (count($sums) == $i){
                $sums[] = 0;
                $p = (($per - $i) <= 0) ? ($per - $i + 24) : ($per - $i);
                $months[] = getAllPeridos()[$p];
            }
        }

        $total = number_format($total);

        $sums = array_reverse($sums, false);
        $months = array_reverse($months, false);

        return ["total" => $total, "months" => $months, "sums" => $sums];
    }

}
