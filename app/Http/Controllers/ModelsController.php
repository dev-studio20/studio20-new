<?php

namespace App\Http\Controllers;

use App\Country;
use App\CustomClass\SendNotification;
use App\ModelAddress;
use App\ModelPaymentDetail;
use App\ModelPaymentHistory;
use App\ModelPeriod;
use App\ModelProfile;
use App\ModelReservation;
use App\State;
use App\City;
use App\ModelStatus;
use App\Studio;
use Illuminate\Http\Request;
use App\Models;
use App\ModelFeedback;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Storage;
use App\Events\ApproveAccount;
use App\Events\DisableAccount;
use Hash;
use Auth;



class ModelsController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function profile($id)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');
        $model = Models::with('profile','address','paymentDetails','social', 'periods', 'contract', 'account', 'statusName', 'feedback','signature','studios')->findOrFail($id);

        $contractPath = "/admin/contracts/contract_".$id.".pdf";

        $btn = ['ok' => '<span class="badge badge-success">OK</span>', 'no' => '<span class="badge badge-danger">NOT OK</span>'];

        $socialCondition = $this->socialCondition($model);

        $statusinarray = [1,2,3,4,5];
        if ($model->status == 2) $statusinarray = [1,4];
        if ($model->status == 1) $statusinarray = [3,5];


        $statesSelect = ModelStatus::get()->whereIn('id', $statusinarray)->pluck('name', 'id')->toArray();

        //if model status 2 show Approve si Reject
        //if model status 1 show Suspend , Inactive

        $approvalCondition = $this->checkCondition($model);

        $approved = ($model->status == 1) ? 'disabled' : '';

        $countrys = Country::get()->pluck('name','id')->toArray();
        $states = State::where('country_id', $model->address->country_id)->get()->pluck('name','id')->toArray();
        $cities = City::where('state_id', $model->address->state_id)->get()->pluck('name','id')->toArray();
        $studios = Studio::get()->pluck('name','id')->toArray();

        $reservations = ModelReservation::where('model_id', $model->id)->get()->toArray();
        //mydd($reservations);
        $periods = ModelPeriod::with('payHistory')->where('model_id', $model->id)->get();

        return view('Admin.models.profile', compact('id', 'model', 'contractPath', 'btn','socialCondition','statesSelect','approvalCondition','countrys','states','cities','studios','approved','reservations','periods'));
    }

    public function socialCondition($model){

        return (
            ($model->social->artistic_email) &&
            ($model->social->artistic_password) &&
            ($model->social->twitter_profile) &&
            ($model->social->twitter_post) &&
            ($model->social->instagram_profile) &&
            ($model->social->twitter_account)
        );
    }

    public function checkCondition($model){

        $cond = 0;
        $sid = $model->studios->id;
        $studio = Studio::find($sid);

        if ($model->contract->pic1_status == 1) $cond++;
        if ( ($model->contract->pic2 && ($model->contract->pic2_status == 1)) || (!$model->contract->pic2) )$cond++;
        if ($model->contract->pic3_status == 1) $cond++;
        if ($model->contract->contractsigned == 1) $cond++;
        if (count(explode(',', $model->modelname)) == 1) $cond++;
        if ($model->email_verified_at) $cond++;
        if ( ($studio->phone_verification && $model->phone_verified_at) || (!$studio->phone_verification) ) $cond++;
        if ($this->socialCondition($model)) $cond++;

        return ($cond == 8);

    }

    public function set_nickname(Request $request){

        //model_id:id, modelname:modelname
        if($request->has('model_id') && $request->has('modelname')){
            $id = $request->get('model_id');
            $modelname = $request->get('modelname');

            $model = Models::where('id', $id)->update(["modelname" => $modelname]);

            return response()->json(["success" => true, "data" => $model]);

        } else return "..";

    }

    public function ignore_reservations(Request $request){

        if ($request->has('phistory_id')){

            $res_id = $request->get('phistory_id');

            $r = ModelPaymentHistory::find($res_id);

            if($r) {
                $r->ignore_res = !$r->ignore_res;
                $r->save();

                return ["success" => $r->ignore_res];

            } else return 'not found';

        }

    }

    public function edit_profile(Request $request, $id)
    {
        $data = $request->all();

        $admin = Auth::guard('admin')->user();

        $model = Models::with('profile', 'address', 'paymentDetails','contract','feedback')->where('id', $id)->firstOrFail();

        if($request->has('feedback')){
            $feed = $request->get('feedback');
            $admin =  Auth::guard('admin')->user();
            if ($admin && $admin->name){
                $feedback = new ModelFeedback();
                $feedback->model_id = $model->id;
                $feedback->message = $feed;
                $feedback->user = $admin->name;
                $feedback->save();

                SendNotification::addFeedbackEvent($admin->name, $model->modelname);

                return redirect()->back()->with('flash-message','Data updated successfully!');
            }

            //store in db

        }

        if($request->has('editsocial')){

            $model->social->fill($data['social'])->save();

            SendNotification::editSocialEvent($admin->name, $model->modelname);

            return redirect()->back()->with('flash-message','Data updated successfully!');
        }

        if ($request->has('password_text') && $request->has('password_text_confirmation')){
            $pass = $request->get('password_text');
            $pass_conf = $request->get('password_text_confirmation');

            if ($pass !== $pass_conf) {
                //show error
                return redirect()->back()->with('flash-message','Password invalid!');
            }else{
                //update pass
                $hashed = Hash::make($pass);
                $model->password = $hashed;
                $model->password_text = $pass;
                $model->save();

                SendNotification::editPasswordEvent($admin->name, $model->modelname);

                return redirect()->back()->with('flash-message','Password updated successfully!');
            }
        }

        if ($request->has('statselect')){
            $stat = $request->input('statselect');
            if ($stat != 1) {
                $reason = ($request->has('reason')) ? $request->get('reason') : null;
                $model->contract->reason = $reason;
                $model->contract->save();
            }
            $model->status = $stat;
            $model->save();

            $statusname = (ModelStatus::where('id',$stat)->first()) ? ModelStatus::where('id',$stat)->first()->name : 'error';

            SendNotification::accountStatusEvent($admin->name, $model->modelname, $statusname);

            return redirect()->back()->with('flash-message','Data updated successfully!');

        }

        if ($request->has('contract')){
            $contract = $request->get('contract');

            if (array_key_exists('reason_pic1',$contract) && $request->has('pic1')){
                $pic1 = $request->get('pic1');
                $model->contract->pic1_status = ($pic1 == 'Approve') ? 1 : 2;
                $model->contract->reason_pic1 = $contract["reason_pic1"];
                $model->contract->save();

                SendNotification::pict1StatusEvent($admin->name, $model->modelname, $pic1);

                return redirect()->back()->with('flash-message','Data updated successfully!');
            }

            if (array_key_exists('reason_pic2',$contract) && $request->has('pic2')){
                $pic2 = $request->get('pic2');
                $model->contract->pic2_status = ($pic2 == 'Approve') ? 1 : 2;
                $model->contract->reason_pic2 = $contract["reason_pic2"];
                $model->contract->save();

                SendNotification::pict2StatusEvent($admin->name, $model->modelname, $pic2);

                return redirect()->back()->with('flash-message','Data updated successfully!');
            }

            if (array_key_exists('reason_pic3',$contract) && $request->has('pic3')){
                $pic3 = $request->get('pic3');
                $model->contract->pic3_status = ($pic3 == 'Approve') ? 1 : 2;
                $model->contract->reason_pic3 = $contract["reason_pic3"];
                $model->contract->save();

                SendNotification::pict3StatusEvent($admin->name, $model->modelname, $pic3);

                return redirect()->back()->with('flash-message','Data updated successfully!');
            }

        }

        if($request->has('editprofile')){
            $model->modelname = $request->get('modelname');
            $model->email = $request->get('email');
            $model->couple = $request->get('couple');
            $model->studio = $request->get('studio');
            $model->save();


            $prof = $data["profile"];

            $profile = ModelProfile::where('model_id',$model->id)->firstOrFail();
            $profile->first_name = $prof["first_name"];
            $profile->last_name = $prof["last_name"];
            $profile->phonenumber = $prof["phonenumber"];
            $profile->birthdate = $prof["birthdate"];
            $profile->sex = $prof["sex"];
            $profile->save();

            $add = $data["address"];

            $address = ModelAddress::where('model_id',$model->id)->firstOrFail();
            $address->country_id = $add["country_id"];
            $address->state_id = $add["state_id"];
            $address->city_id = $add["city_id"];
            $address->street = $add["street"];
            $address->save();

            $pay = $data["paymentDetails"];

            $payment = ModelPaymentDetail::where('model_id',$model->id)->firstOrFail();
            $payment->idn = $pay["idn"];
            $payment->iban = $pay["iban"];
            $payment->bank = $pay["bank"];
            $payment->bank_address = $pay["bank_address"];
            $payment->currency = $pay["currency"];
            $payment->save();

        }

        SendNotification::editProfileEvent($admin->name, $model->modelname);

        return redirect()->back()->with('flash-message','Data updated successfully!');

    }

    public function approve_account(Request $request){

        $id = ($request->has('model_id')) ? $request->input('model_id') : null;

        if ($id) {
            $model = Models::findOrFail($id);
            $model->status = 1;
            $model->save();

            $message = $id;

            event(new ApproveAccount($message));

            return $message;
        }

        return 'error';
    }

    public function suspend_account(Request $request){

        $id = ($request->has('model_id')) ? $request->input('model_id') : null;

        if ($id) {
            $model = Models::findOrFail($id);
            $model->status = 2;
            $model->save();

            $message = $id;

            event(new DisableAccount($message));

            return $message;
        }

        return 'error';

    }

    public function edit_social(Request $request, $id)
    {

        //editsocial

        $data = $request->all();

        $model = Models::with('social')->where('id', $id)->firstOrFail();

        $model->social->fill($data['social'])->save();

        //return back();
        return redirect()->back()->with('flash-message','Data updated successfully!');

    }

    public function store(Request $request)
    {
        Models::updateOrCreate(['id' => $request->product_id],
            ['name' => $request->name, 'detail' => $request->detail]);

        return response()->json(['success'=>'Product saved successfully.']);
    }

    public function edit($id)
    {
        $product = Models::find($id);
        return response()->json($product);
    }

    public function destroy($id)
    {
        Models::find($id)->delete();

        return response()->json(['success'=>'Role deleted successfully.']);
    }


    public function index()
    {
        return redirect('/admin/allmodels');
    }

    public function all_models(Request $request){

        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.allmodels', compact('models'));

    }

    public function approved_models(Request $request){
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->whereStatus(1)->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.approved_models2', compact('models'));
    }

    public function pending_models(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->whereStatus(2)->orderBy('created_at','DESC')->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.pending_models', compact('models'));

    }

    public function suspended_models(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->whereStatus(3)->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.suspended_models', compact('models'));

    }

    public function rejected_models(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');

        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->whereStatus(4)->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.rejected_models', compact('models'));

    }

    public function inactive_models(Request $request)
    {
        if (in_array(getRole(),[5,6])) return redirect('/admin');
        if ($request->ajax()) {
            $s = $request->get('studio');
            $op = ($s) ? '=' : '!=';
            $models = Models::with('profile', 'studios', 'statusName', 'social')->whereStatus(5)->where('studio', $op, $s)->get();
            return Datatables::of($models)->make(true);
        }
        return view('Admin.models.inactive_models', compact('models'));

    }


}
