<?php

namespace App\Http\Controllers\Sync;

use App\Http\Controllers\Controller;
use App\Models;
use App\ModelStatus;
use App\ModelAccount;
use App\ModelContract;
use App\ModelPaymentDetail;
use App\ModelProfile;
use App\ModelReservation;
use App\ModelSale;
use App\ModelSocial;
use App\ModelAddress;
use App\State;
use App\City;
use App\Country;
use App\APIKeys;
use App\ModelKeys;
use App\Sync\Oldmodels;
use DB;
use App\Jobs\ProcessSyncTables;
use App\Jobs\SyncJobAccounts;
use App\Jobs\SyncJobIds;
use App\Sync\Olduser;
use App\Jobs\SyncJobAddress;
use App\Jobs\SyncJobContract;
use App\Jobs\SyncJobPaymentDetail;
use App\Jobs\SyncJobProfile;
use App\Jobs\SyncJobReservation;
use App\Jobs\SyncJobSale;
use App\Jobs\SyncJobSocial;
use App\Jobs\SyncJobPeriods;
use App\ModelMessages;



class SyncController extends Controller
{

    public function index(){

        $oldModels = Oldmodels::all();
        //truncate table models
        //Models::query()->truncate();

        foreach($oldModels as $omodel){
            $oldUser = Olduser::find($omodel->id);

            $model = new Models();
            $model->id = $omodel->id;
            $model->modelname = $omodel->Modelname;
            $model->studio = $omodel->studios_id;
            $model->email = $omodel->Email;
            $model->password = $oldUser->password ?? '';
            $model->password_text = $omodel->Pass;
            $model->status = ModelStatus::where('name',$omodel->Status)->first()->id;
            $model->couple = $omodel->coupleacc;
            //$model->key_id = 0;
            $model->email_verified_at = $oldUser->email_verified_at ?? now(0);
            $model->phone_verified_at = $oldUser->phone_verified_at ?? now(0);
            $model->updated_at = $oldUser->updated_at ?? now(0);
            $model->created_at = $oldUser->created_at ?? now(0);
            $model->remember_token = $oldUser->remember_token ?? '';
            $model->save();

        }


//        ProcessSyncTables::withChain([
//            new SyncJobIds,
//        ])->dispatch();

        //SyncJobAccounts - done
        //SyncJobAccounts::dispatch();

        //SyncJobAddress
        //SyncJobAddress::dispatch();

        //SyncJobContract
        //SyncJobContract::dispatch();

        //SyncJobPaymentDetail
        //SyncJobPaymentDetail::dispatch();

        //SyncJobProfile
        //SyncJobProfile::dispatch();

        //SyncJobSocial
        //SyncJobSocial::dispatch();

        //SyncJobSale
        //SyncJobSale::dispatch();

        //SyncJobReservation
        //SyncJobReservation::dispatch();

        //SyncJobStudios
        //SyncJobStudios::dispatch();

        //SyncJobPeriods
        //SyncJobPeriods::dispatch();

        //signature

        //messages

        //analytics

        //paymenthistory



//        $myimp = DB::table('xxx_to_delete_id_done')->pluck('id_done')->toArray();
//
//        $msgs = DB::connection('mysql_hetzner')->table('messages')->whereNotIn('id',$myimp)->get();
//
//        foreach($msgs as $msg){
//
//            $fromid = $msg->fromid;
//            $toid = $msg->toid;
//            $old_new = null;
//
//                if ($msg->fromid !== 'admin') {
//                    $old_new = finModelId($msg->fromid);
//
//                    if ($old_new) {
//                        $fromid = $old_new;
//                    }
//                }
//
//                if ($msg->toid !== 'admin') {
//                    $old_new = finModelId($msg->toid);
//
//                    if ($old_new) {
//                        $toid = $old_new;
//                    }
//                }
//
//                //dd(["from" => $fromid, "to" => $toid]);
//            if($old_new){
//                $messsage = new ModelMessages();
//                $messsage->fromid = $fromid;
//                $messsage->toid = $toid;
//                $messsage->subject = $msg->subject;
//                $messsage->message = $msg->message;
//                $messsage->readtime = $msg->readtime;
//                $messsage->sendtime = $msg->sendtime;
//                $messsage->sendtime1 = $msg->sendtime1;
//                $messsage->replyto = $msg->replyto;
//                $messsage->solved = $msg->solved;
//                $messsage->path = $msg->path;
//                $messsage->path2 = $msg->path2;
//                $messsage->save();
//
//                DB::table('xxx_to_delete_id_done')->insert(['id_done' => $msg->id]);
//            }
//
//
//
//
//
//        }



        return 'job done';


    }



    public function syncModels(){



    }


//    ------------------------------

    public function createArray($d, $m, $y, $id, $days, $hour, $status, $RR, $MOVE){

        $m_id = $id;
        $m_week = $this->findWeek($d, $m, $y);
        $m_month = $m;
        $m_year = $y;
        $m_days = $this->getDays($days);
        $m_hour = $hour;
        $m_status = $status;
        $m_rr = $RR;
        $m_move = $MOVE;

        return [
            "model_id" => $m_id,
            "week" => $m_week,
            "month" => $m_month,
            "year" => $m_year,
            "days" => $m_days,
            "hour" => $m_hour,
            "status" => $m_status,
            "rr" => $m_rr,
            "move" => $m_move,
        ];

    }

    public function createArray2($m, $id, $days, $hour, $status, $RR, $MOVE){

        $m_id = $id;
        $m_month = $m;
        $m_days = $days;
        $m_hour = $hour;
        $m_status = $status;
        $m_rr = $RR;
        $m_move = $MOVE;

        return [
            "model_id" => $m_id,
            "month" => $m_month,
            "days" => $m_days,
            "hour" => $m_hour,
            "status" => $m_status,
            "RR" => $m_rr,
            "MOVE" => $m_move,
        ];

    }

    public function getDays($days){

        return $days;
    }

    public function findWeek($d, $m, $y){

        return 23;
    }

    public function getDataForApp(){

        return 'here';

    }

    public function findCountryStateCityId($city, $state, $country){

        $country_res = Country::where('name', $country)->first();
        if ($country_res){
            $cid = $country_res->id;
            $state_res = State::where('name', $state)->where('country_id', $cid)->first();
            if ($state_res){
                $sid = $state_res->id;
                $city_res = City::where('name', $city)->where('state_id', $sid)->first();
                if($city_res) {
                    return ["city_id" => $city_res->id, "state_id" => $state_res->id, "country_id" => $country_res->id];
                }

            }

        }

        return null;

    }



}
