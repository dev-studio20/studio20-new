<?php

namespace App\Http\Controllers;

use App\CustomClass\SendNotification;
use App\ModelProfile;
use App\Models;
use Illuminate\Http\Request;
use App\Country;
use App\State;
use App\City;
use Auth;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
//    public function index()
//    {
//        return view('home');
//    }

    public function locationSelect(Request $request){

        if ( $request->has("country_id") && ($request->input("country_id") !== null)) {

            $phonecode = Country::where('id', $request->input("country_id"))->pluck('phonecode');

            $query = State::where('country_id', $request->input("country_id"))->get();

            $rowCount = count($query);

            $html = '';

            //Display states list
            if ($rowCount > 0) {
                $html .= '<option value="">Select state</option>';
                foreach ($query as $row) {
                    $html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
                }
            } else {
                $html .= '<option value="">State not available</option>';
            }
            $data = array($html, $phonecode);
            echo json_encode($data);

        }

        if ( $request->has("state_id") && ($request->input("state_id") !== null)) {

            $query = City::where('state_id', $request->input("state_id"))->orderBy('name', 'ASC')->get();

            //Count total number of rows
            $rowCount = count($query);

            //Display cities list
            if ($rowCount > 0) {
                echo '<option value="">Select city</option>';
                foreach ($query as $row) {
                    echo '<option value="' . $row->id . '">' . $row->name . '</option>';
                }
            } else {
                echo '<option value="">City not available</option>';
            }
        }

        return null;
    }

    public function resendPhoneLink(){

        SendNotification::resendPhoneVerification();

        return 'here';

    }

    public function resendEmailLink(){

        //re-send mail verification link
        SendNotification::resendVerificationLinkEvent(Auth::guard('model')->user()->id);

        return back();

    }

    public function changePhone(Request $request){


        if ($request->has('field_name')){
            $field = $request->get('field_name');
            if ($field == 'phone') {
                $phone = $request->get('field');
                $model = Auth::guard('model')->user();

                $profile = ModelProfile::where('model_id', $model->id)->firstOrFail();
                $profile->phonenumber = $phone;
                $profile->save();

                SendNotification::changePhoneEvent($model->modelname);
            }
        }




        return 'ok';

    }

    public function changeEmail(Request $request){


        if ($request->has('field_name')){
            $field = $request->get('field_name');
            if ($field == 'email') {
                $email = $request->get('field');
                $model = Auth::guard('model')->user();

                $checkEmail = Models::where('email', $email)->first();
                if ($checkEmail) return 'exist';

                $model = Models::where('id', $model->id)->firstOrFail();
                $model->email = $email;
                $model->save();
            }
        }


        SendNotification::changeEmailEvent();

        return 'ok';

    }
}
