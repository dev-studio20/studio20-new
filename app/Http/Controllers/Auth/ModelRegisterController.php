<?php

namespace App\Http\Controllers\Auth;

use App\Country;
use App\CustomClass\SendNotification;
use App\ModelAccount;
use App\ModelAddress;
use App\ModelAnalytics;
use App\ModelContract;
use App\ModelPaymentDetail;
use App\ModelProfile;
use App\ModelSocial;
use App\Studio;
use App\Models;
use App\ModelSignature;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\ModelRegistration;
use Auth;
use DB;
use URL;
use Mail;

class ModelRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        //$this->middleware('guest:admin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showRegistrationForm()
    {

        $countries = Country::all();

        $studios = Studio::where('active',1)->where('id', '!=', 100)->orderBy('name', 'asc')->get();

        $pic1err = '';
        $pic2err = '';
        $pic3err = '';
        $nameerr = '';
        $errcode = '';
        $lnameerr = '';
        $mailerr = '';
        $passerr = '';
        $birthdateerr = '';
        $sexerr = '';
        $countryerr = '';
        $cityerr = '';
        $phoneerr = '';
        $addresserr = '';
        $studioerr = '';
        $companynameerr = '';
        $companyadminerr = '';
        $companyaddresserr = '';
        $companyvatcodeerr = '';
        $companyregcodeerr = '';
        $ibanerr = '';
        $bankerr = '';
        $bankswifterr = '';
        $bankaddresserr = '';
        $currencyerr = '';
        $modelname1err = '';
        $modelname2err = '';
        $modelname3err = '';
        $idnerr = '';

        return view('auth.register', compact('countries', 'studios', 'pic1err', 'pic2err', 'pic3err','pic1err','pic2err','pic3err','nameerr','errcode','lnameerr','mailerr','passerr','birthdateerr','sexerr','countryerr','cityerr','phoneerr','addresserr','studioerr','companynameerr','companyadminerr','companyaddresserr','companyvatcodeerr','companyregcodeerr','ibanerr','bankerr','bankswifterr','bankaddresserr','currencyerr','modelname1err','modelname2err','modelname3err', 'idnerr'));
    }

    public function create(ModelRegistration $data)
    {

        $name = $data['name'];
        $lname = $data['lname'];
        $fname = str_replace(' ', '_', $name);
        $flname = str_replace(' ', '_', $lname);

        $email = $data['email'];
        $pass = $data['password'];
        $couple = ($data['sex'] === 'C') ? 1 : 0;
        $sex = $data['sex'];
        $currency = $data['currency'];
        $idn = $data['registeridn'];
        $registerIban = $data['registerIban'];
        $registerBank = $data['registerBank'];
        $registerBankAddress = $data['registerBankAddress'];

        $birthdate = $data['birthdate'];
        $modelname = $data['registerModelName1'] . ',' . $data['registerModelName2'] . ',' . $data['registerModelName3'];
        $date = date("Y-m-d");
        $contractname = "Contract_" . $fname . "_" . $flname . "_" . $date . ".pdf";
        $studio = (isset($data['studio'])) ? $data['studio'] : 100;
        $fullphone =  $data['realregisterphoneprefix'].$data['registerphone'];

        $street = $data["registerAddress"];
        $city_id = $data["registerCity"];
        $state_id = $data["registerstate"];
        $country_id = $data["registerCountry"];

        $pic1 = null;
        $pic2 = null;
        $pic3 = null;


        if ($data->hasFile('front1') || $data->hasFile('front2')){
            $key = ($data->hasFile('front1')) ? 'front1' : 'front2';
            $filename = $data['name'] . '_' . $data['lname'];
            $extension = $data->file($key)->getClientOriginalExtension();
            $pic1 = 'front_' . $filename.'_'.time().'.'.$extension;
            $path = $data->file($key)->storeAs('public/models/images', $pic1);

        }

        if ($data->hasFile('back1') || $data->hasFile('back2')){
            $key = ($data->hasFile('back1')) ? 'back1' : 'back2';
            $filename = $data['name'] . '_' . $data['lname'];
            $extension = $data->file($key)->getClientOriginalExtension();
            $pic2 = 'back_' . $filename.'_'.time().'.'.$extension;
            $path = $data->file($key)->storeAs('public/models/images', $pic2);

        }

        if ($data->hasFile('faceId1') || $data->hasFile('faceId2')){
            $key = ($data->hasFile('faceId1')) ? 'faceId1' : 'faceId2';
            $filename = $data['name'] . '_' . $data['lname'];
            $extension = $data->file($key)->getClientOriginalExtension();
            $pic3 = 'faceid_' . $filename.'_'.time().'.'.$extension;
            $path = $data->file($key)->storeAs('public/models/images', $pic3);

        }


        $model = new Models();
        $model->modelname  = $modelname;
        $model->studio  = $studio;
        $model->email  = $email;
        $model->password_text  = $pass;
        $model->password  = Hash::make($pass);
        $model->status  = 2;
        $model->couple  = $couple;
        $model->save();

        $fid = $model->id;

        $profile = new ModelProfile();
        $profile->model_id = $fid;
        $profile->first_name = $name;
        $profile->last_name = $lname;
        $profile->phonenumber = $fullphone;
        $profile->birthdate = $birthdate;
        $profile->sex = $sex;
        $profile->save();

        $payments = new ModelPaymentDetail();
        $payments->model_id = $fid;
        $payments->idn = $idn;
        $payments->iban = $registerIban;
        $payments->bank = $registerBank;
        $payments->bank_address = $registerBankAddress;
        $payments->currency = $currency;
        $payments->save();

        $address = new ModelAddress();
        $address->model_id = $fid;
        $address->street = $street;
        $address->city_id = $city_id;
        $address->state_id = $state_id;
        $address->country_id = $country_id;
        $address->save();

        $accounts = new ModelAccount();
        $accounts->model_id = $fid;
        $accounts->type = $data["acctype"];
        //$accounts->company_name = '';
        //$accounts->company_administrator = '';
        //$accounts->company_address = '';
        //$accounts->company_vat = '';
        //$accounts->company_regcode = '';
        $accounts->save();

        $contracts = new ModelContract();
        $contracts->model_id = $fid;
        $contracts->pic1 = $pic1;
        $contracts->pic2 = $pic2;
        $contracts->pic3 = $pic3;
        $contracts->contractsigned = 0;
        $contracts->save();

        $social = new ModelSocial();
        $social->model_id = $fid;
        $social->brand = $data["brand"];
        $social->save();

        $this->analytics($data['widthheight'], $fid);

        $model_new = Models::where('id',$fid)->first();

        Auth::guard('model')->login($model_new);

        if(Auth::guard('model')->check()) return redirect('/models/signcontract');
        else return redirect('/register');

    }

    public function signcontract(Request $request){

        if ($request->has('signature') && $request->has('id')) {
            $signature = $request->get('signature');
            $id = $request->get('id');

            $signature = ModelSignature::updateOrCreate(["model_id" => $id], ["model_signature" => $signature]);

            $findSignature = ModelSignature::find($signature->id);

            if($findSignature) return response()->json(['success'=>true, "signature" => $findSignature->model_signature ]);
            else return response()->json(['success'=>false ]);

        }

        $model = Auth::guard('model')->user();
        $mymodel = Models::with('profile','address','paymentDetails','account')->findOrFail($model->id);

        $semnaturaManager = DB::table('signature_m')->find(1)->data;
        //$semnaturaManager = DB::table('model_signature')->where('id',3)->first()->model_signature;

        $id = $mymodel->id;
        $name = $mymodel->profile->first_name;
        $lname = $mymodel->profile->last_name;
        $email = $mymodel->email;
        $country = findCountry($mymodel->address->country_id);
        $state = findState($mymodel->address->state_id);
        $city = findCity($mymodel->address->city_id);
        $address = $mymodel->address->street;
        $idn = $mymodel->paymentDetails->idn;
        $date = now()->format('d-m-Y');
        $phone = $mymodel->profile->phonenumber;
        $iban = $mymodel->paymentDetails->iban;
        $bank = $mymodel->paymentDetails->bank;
        $acctype = $mymodel->account->type;
        $companyname = $mymodel->account->company_name;
        $companyadministrator = $mymodel->account->company_administrator;
        $companyaddress = $mymodel->account->company_address;
        $companyvatcode = $mymodel->account->company_vat;
        $companyregcode = $mymodel->account->company_regcode;
        $modelname = $mymodel->modelname;

        SendNotification::registrationEvent();

        return view("Models.contract_sign", compact('id', 'name', 'lname', 'email', 'country', 'state','city', 'address', 'idn', 'date', 'phone', 'iban',
            'bank', 'acctype', 'companyname', 'companyadministrator', 'companyaddress', 'companyvatcode', 'companyregcode', 'modelname', 'semnaturaManager'));


    }

    public function finish(){

        //$model = Models::findOrFail(Auth::guard('model')->user()->id);
        //$model->sendEmailVerificationNotification();

//        $user = Auth::guard('model')->user();
//        $email = $user->email;
//        $id = $user->id;
//
//        $rowmodel = DB::table('models')->where('id', $id)->first();
//        $modlename = "Test Modelname";
//
//        $url = URL::signedRoute('autologin', ['user' => $user]);
//
//        Mail::send('auth.verifyautologin', ['modelname' => $modlename, 'url' => $url], function ($message) use ($email) {
//            $message->to($email)->subject('Email Verification');
//            $message->from('no-reply@studio20girls.com', 'Studio20');
//
//        });
//
//        $studio =  $rowmodel->Studio;
//        $emailOp = 'op@studio20.com';
//
//        Mail::send('emails.registration', ['modelname' => $modlename, 'studio' => $studio], function ($message) use ($emailOp) {
//            $message->to($emailOp)->subject('New Model Registration');
//            $message->from('no-reply@studio20girls.com', 'Studio20');
//        });
//
//        //sms verification
//        $smslink = URL::signedRoute('autologinphone', ['user' => $user]);
//
//        //send sms notification
//        $phone = DB::table('models')->where('id', $id)->first()->phonenumber;
//        $client = new Client();
//        $res = $client->get('https://api.sendsms.ro/json?action=message_send&username=frunzetti&password=sms12x&to='. $phone .'&text=Phone Verification Link:' . $smslink);
//        echo $res->getStatusCode();



        return redirect('/login');

    }

    public function uploadNewPicture(Request $request){

        if ($request->hasFile('front1') || $request->hasFile('front2') || $request->hasFile('front3')) {
            $id = Auth::guard('model')->user()->id;

            if ($request->hasFile('front1')) {
                $key1 = 'front1';
                $filename = $id . '_modelname';
                $extension = $request->file($key1)->getClientOriginalExtension();
                $fileNameToStore = 'front_' . $filename . '_' . time() . '.' . $extension;
                $path = $request->file($key1)->storeAs('public/models/images', $fileNameToStore);

                ModelContract::where('model_id', $id)->update(["pic1_status" => 0, "pic1" => $fileNameToStore]);



            };

            if ($request->hasFile('front2')) {
                $key2 = 'front2';
                $filename = $id . '_modelname';
                $extension = $request->file($key2)->getClientOriginalExtension();
                $fileNameToStore = 'back_' . $filename . '_' . time() . '.' . $extension;
                $path = $request->file($key2)->storeAs('public/models/images', $fileNameToStore);
                ModelContract::where('model_id', $id)->update(["pic2_status" => 0, "pic2" => $fileNameToStore]);


            };

            if ($request->hasFile('front3')) {
                $key3 = 'front3';
                $filename = $id . '_modelname';
                $extension = $request->file($key3)->getClientOriginalExtension();
                $fileNameToStore = 'front+ci_' . $filename . '_' . time() . '.' . $extension;
                $path = $request->file($key3)->storeAs('public/models/images', $fileNameToStore);
                ModelContract::where('model_id', $id)->update(["pic3_status" => 0, "pic3" => $fileNameToStore]);


            };

            SendNotification::reuploadPicturesEvent();

        }

        return redirect()->back();

    }

    public function resendVerificationLink(){

        //re-send mail verification link
        SendNotification::resendVerificationLinkEvent(Auth::guard('model')->user()->id);

        return back();
    }

    public function resendPhoneVerificationLink(){

        dd('here');

        //re-send phone verification link
        SendNotification::resendPhoneVerificationLinkEvent(Auth::guard('model')->user()->id);

        return back();
    }

    public function analytics($widthheight, $id){

        try {
            $ipp = get_client_ip();
            //$visitor_ip_details = json_decode(@file_get_contents("http://ipinfo.io/{$_SERVER['REMOTE_ADDR']}/json"));
            $visitor_ip_details = json_decode(@file_get_contents("http://ipinfo.io/{$ipp}/json"));
            $visitor_city = @$visitor_ip_details->city;
            if ($visitor_city == "") {
                $visitor_city = "unknown";
            }
            $visitor_region = @$visitor_ip_details->region;
            if ($visitor_region == "") {
                $visitor_region = "unknown";
            }
            $visitor_country_code = @$visitor_ip_details->country;
            if ($visitor_country_code == "") {
                $visitor_country_code = "unknown";
                $visitor_country = "unknown";
            } else {
                $v_country = DB::table('countries')->where('sortname', $visitor_country_code)->first();
                $visitor_country = $v_country->name;
            }
            $visitor_loc = explode(',', @$visitor_ip_details->loc);
            $visitor_loc_0 = @$visitor_loc[0];
            if ($visitor_loc_0 == "") {
                $visitor_loc_0 = "unknown";
            }
            $visitor_loc_1 = @$visitor_loc[1];
            if ($visitor_loc_1 == "") {
                $visitor_loc_1 = "unknown";
            }

            $visitor_org = @$visitor_ip_details->org;
            if ($visitor_org == "") {
                $visitor_org = "unknown";
            }
            $visitor_hostname = @$visitor_ip_details->hostname;
            if ($visitor_hostname == "") {
                $visitor_hostname = "No Hostname";
            }

        } catch (\Exception $e) {
            $visitor_city = "unknown";
            $visitor_region = "unknown";
            $visitor_country_code = "unknown";
            $visitor_country = "unknown";
            $visitor_loc_0 = "unknown";
            $visitor_loc_1 = "unknown";
            $visitor_org = "unknown";
            $visitor_hostname = "No Hostname";
        }

        $analytics_model = new ModelAnalytics();
        $analytics_model->model_id = $id;
        $analytics_model->ip = $_SERVER['REMOTE_ADDR'];
        $analytics_model->city = $visitor_city;
        $analytics_model->country = $visitor_country;
        $analytics_model->country_code = $visitor_country_code;
        $analytics_model->region = $visitor_region;
        $analytics_model->location_cor1 = $visitor_loc_0;
        $analytics_model->location_cor2 = $visitor_loc_1;
        $analytics_model->os = $this->getOS();
        $analytics_model->browser = $this->getBrowser();
        $analytics_model->resolution = $widthheight;
        $analytics_model->hostname = $visitor_hostname;
        $analytics_model->org = $visitor_org;
        $analytics_model->save();

    }

    public function changemodel(Request $request){

        if (get_client_ip() !== '188.214.255.240')  return redirect()->back();

        if($request->has('submitFind') || $request->has('submitNext')){
            if($request->has('modelId') && ($request->has('submitFind'))){
                $mid = $request->get('modelId');

                //$model = Models::where('id', $mid)->orWhere('modelname', $mid)->where('status', 1)->first();
                $model = Models::where('id', $mid)->orWhere('modelname', $mid)->first();

                if($model){
                    Auth::guard('model')->logout();
                    \Session::flush();

                    Auth::guard('model')->login($model);
                }
            }

            if($request->has('modelId') && ($request->has('submitNext'))){
                $mid = $request->get('modelId');

                //$model = Models::where('id','>', $mid)->orderBy('id','ASC')->where('status', 1)->first();
                $model = Models::where('id','>', $mid)->orderBy('id','ASC')->first();

                if($model){
                    Auth::guard('model')->logout();
                    \Session::flush();

                    Auth::guard('model')->login($model);
                }
            }
        }

        return redirect()->back();
    }



    function getOS()
    {

        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform = "unknown";

        $os_array = array(
            '/windows nt 10/i'      =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );

        foreach ($os_array as $regex => $value) {

            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }

        }

        return $os_platform;

    }

    function getBrowser()
    {
        // check if IE 8 - 11+
        preg_match('/Trident\/(.*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
        if ($matches) {
            $version = intval($matches[1]) + 4;     // Trident 4 for IE8, 5 for IE9, etc
            return 'Internet Explorer ' . ($version < 11 ? $version : $version);
        }

        preg_match('/MSIE (.*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
        if ($matches) {
            return 'Internet Explorer ' . intval($matches[1]);
        }

        // check if Firefox, Opera, Chrome, Safari
        foreach (array('Firefox', 'OPR', 'Chrome', 'Safari') as $browser) {
            preg_match('/' . $browser . '/', $_SERVER['HTTP_USER_AGENT'], $matches);
            if ($matches) {
                return str_replace('OPR', 'Opera',
                    $browser);   // we don't care about the version, because this is a modern browser that updates itself unlike IE
            }
        }
    }

}
