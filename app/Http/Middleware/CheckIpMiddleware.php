<?php

namespace App\Http\Middleware;

use Closure;
use Redirect;
use Request;


class CheckIpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public $whiteIps = ['188.214.255.240', '81.196.255.165', '109.110.246.50','188.214.255.235','31.153.44.232'];

    public function handle($request, Closure $next)
    {


        if (!in_array($request->ip(), $this->whiteIps)) {

            /*
                 You can redirect to any error page.
            */
            //return response()->json(['your ip address is not valid.']);
            $url = "https://girls.studio20.group/";
            return Redirect::to($url);
        }

        return $next($request);
    }
}
