<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateModel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if ($request->has('submitFind') || $request->has('submitNext')){
            return $next($request);
        }

        if (Auth::guard('model')->user() == null){
            return redirect()->guest(route( 'login' ));
        }

        if (Auth::guard('model')->user() && ( !in_array(Auth::guard('model')->user()->status, [1,3])) ){
            return redirect()->guest(route( 'models.approval' ));
        }


        return $next($request);
    }
}
