<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ModelPeriod extends Model

{

    protected $table = 'model_period';

    protected $guarded = [];


    public function model(){
        return $this->hasOne('App\Models', 'id', 'model_id');
    }

    public function payHistory(){
        return $this->hasOne('App\ModelPaymentHistory', 'period_id', 'id');
    }

    public function studio(){

        return $this->hasOne('App\Studio', 'id','studio_id');
    }

    public function periodName(){
        return $this->hasOne('App\PeriodName', 'id', 'period');
    }

    public function profile(){
        return $this->hasOne('App\ModelProfile', 'model_id', 'model_id');
    }

    public function getFullNameAttribute ()
    {
        return $this->profile->first_name . ' ' . $this->profile->last_name;
    }


}
