<?php

namespace App\Providers;

use App\CustomClass\SendNotification;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\ModelSale;
use App\ModelPeriod;
use App\Events\StatusLiked;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        ModelSale::saved(function ($sale) {

            $sum = ModelSale::where('model_id', $sale->model_id)->where('year', $sale->year)->where('period', $sale->period)->sum('sum');
            $hours = ModelSale::where('model_id', $sale->model_id)->where('year', $sale->year)->where('period', $sale->period)->sum('hours');

            //update period
            ModelPeriod::updateOrCreate([
                'model_id' => $sale->model_id,
                'studio_id' => $sale->studio_id,
                'period' => $sale->period,
                'year' => $sale->year,
            ],[
                'total_amount' => $sum,
                'total_amount_jasmin' => $sum,
                'hours' => $hours,
            ]);

            //notification
            $msg = 'P. Updated - Model: ' . $sale->model_id . ' - Period: ' .$sale->period .' - Year: ' . $sale->year . ' - Sum: ' . $sum;
            //SendNotification::updatePeriodEvent($msg);

        });

        ModelSale::deleted(function ($sale) {

            $sum = ModelSale::where('model_id', $sale->model_id)->where('year', $sale->year)->where('period', $sale->period)->sum('sum');
            $hours = ModelSale::where('model_id', $sale->model_id)->where('year', $sale->year)->where('period', $sale->period)->sum('hours');

            //update period
            ModelPeriod::updateOrCreate([
                'model_id' => $sale->model_id,
                'studio_id' => $sale->studio_id,
                'period' => $sale->period,
                'year' => $sale->year,
            ],[
                'total_amount' => $sum,
                'total_amount_jasmin' => $sum,
                'hours' => $hours,
            ]);

            //notification
            $msg = 'Period Updated(from delete sales)!';
            SendNotification::updatePeriodEvent($msg);

        });
    }
}
