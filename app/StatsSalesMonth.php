<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsSalesMonth extends Model
{
    protected $table = 'stats_sales_month';

    protected $fillable = [
        'studio_id',
        'month',
        'earnings',
        'worktime',
        'worktimeneeded'
    ];
}
