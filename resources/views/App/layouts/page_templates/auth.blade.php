<div class="wrapper ">
  @include('App.layouts.navbars.sidebar')
  <div class="main-panel">
    @include('App.layouts.navbars.navs.auth')
    @yield('content')
    @include('App.layouts.footers.auth')
  </div>
</div>