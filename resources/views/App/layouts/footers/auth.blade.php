<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>

      </ul>
    </nav>
    <div class="copyright float-right">
      &copy;
      <script>
        document.write(new Date().getFullYear())
      </script><i class="material-icons">favorite</i> S20
    </div>
  </div>
</footer>