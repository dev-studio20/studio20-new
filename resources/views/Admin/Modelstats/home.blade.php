@extends('layouts.admin_app', ['activePage' => 'modelstats', 'titlePage' => __('Models Stats')])

@push('head')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">


@endpush

@section('content')

    <style>

        .card{
            /*min-height: 273px;*/
        }

        .card:hover {
            box-shadow: 0 0 11px rgba(33,33,33,.2);
        }

        .my-card .card-body{
            background: #e91e63;
            border: 1px solid grey;
            border-radius: 3px;
            text-align: center;
            padding:0!important;
        }

        .my-card img{
            width: 100%;
        }

        .studio{
            text-align: center;
        }

        .status{
            position:absolute;
            top:1px;
            right:1px;
            border-radius: 50px;
            padding:5px;
            color:white;
            font-size: 10px;
        }

        .my-card table{
            width: 100%;
        }

        .status_free_chat {
            background: green;
        }
        .status_member_chat {
            background: blue;
          }

        .status_members_only{
            background: purple;
        }

         .status_vip_show{
             background: rebeccapurple;
          }

        .status_offline{
            background: red;
          }

        .my-card-title{
            text-align: center;
            white-space: nowrap;
            /*font-size: 1vw;*/
        }

        .col-lg-4{
            font-weight: bold;
        }

        #modalBody td{
            padding-left: 10px;
            padding-right: 10px;
        }

        #updated_label{
            position: absolute;
            top: 5px;
            right: 5px;
        }

        .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
        .toggle.ios .toggle-handle { border-radius: 20px; }

        .btn.btn-default {
            background-color: white;
        }

        .checkbox-inline{
            color:white;
        }

        #container_avg{
            position: absolute;
            top:0;
            right:0;
        }

    </style>
    <div class="content" style="background: #fff;">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-warning">
                        <div class="row" style="font-weight: bold;">
                            <div class="col-lg-1">All: <span id="count">-</span></div>
                            <div class="col-lg-1">New: <span id="count_new">-</span></div>
                            <div class="col-lg-1">Free: <span id="count_free">-</span></div>
                            <div class="col-lg-1">Private: <span id="count_private">-</span></div>
                            <div class="col-lg-1">Members: <span id="count_members">-</span></div>
                            <div class="col-lg-1">Vip: <span id="count_vip">-</span></div>
                            <div class="col-lg-1">Offline: <span id="count_offline">-</span></div>
                        </div>

                        <span id="updated_label"><span id="updated"></span><i class="material-icons">timer</i></span>
                        <hr>
                        <div style="position:relative;">
                        <input id="all_new" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="All Models" data-off="New Models" data-onstyle="success" data-style="ios" data-offstyle="primary">
                        <input id="free_chat" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Free Chat" data-off="Free Chat" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        <input id="member_chat" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Private Chat" data-off="Private Chat" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        <input id="members_only" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Members" data-off="Members" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        <input id="vip_show" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Vip Show" data-off="Vip Show" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        <input id="offline" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Offline" data-off="Offline" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        <div id="container_avg">
                            <input id="avg" type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Max AVG" data-off="Min AVG" data-onstyle="success" data-style="ios" data-offstyle="danger">
                        </div>
                        </div>
                    </div>
                    <div class="card-body table-responsive">

                        <div class="row" id="model_container">

                        </div>


                    </div>
                </div>

            </div>


        </div>
    </div>


    <div class="modal fade" id="Mymodal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modelName" style="position: absolute;"></h4>
                </div>
                <div class="modal-body">
                    <table>
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th>Privacy</th>
                            <th>Exp At</th>
                            <th>Url</th>
                            <th>Last checked</th>
                        </tr>
                        </thead>
                        <tbody id="modalBody"></tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://studio20girls.com:8443/socket.io/socket.io.js"></script>

<script>

    //    toogle filter menu
    let all_new_filter = 'on';
    let free_chat_filter = 'on';
    let member_chat_filter = 'off';
    let members_only_filter = 'off';
    let vip_show_filter = 'off';
    let offline_filter = 'off';
    let avg_filter = 'off';
    let all_filter = 'off';

    let models;
    let count_free_chat = 0;
    let count_private_chat = 0;
    let count_members_chat = 0;
    let count_vip_show = 0;
    let count_offline = 0;
    let count_new = 0;
    let count_all_studio = 0;

    let var_filter_avg = -1;

    let studio = 0;

    let socket;

    socketConnect();

    socket.on('models_status', (data) => {

        //console.log("new data");
        //console.log(data);
        //$("#count").text(data.model_status.found + ' (' + data.model_status.total + ')');
        let d = new Date();
        let minutes = (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
        let tm = d.getHours() + ':' + minutes;
        $("#updated").text(tm);
        models = data.model_status.models;
        models.sort(compare);
        showModels();

    });

    function socketConnect(){
        socket = io("https://studio20girls.com:8443", {
            reconnection: true,
            reconnectionDelay: 1000,
            reconnectionDelayMax : 5000,
            reconnectionAttempts: 99999,
            transports: ['websocket', 'xhr-polling']
        } );
    }

    let container = $("#model_container");

    $(function() {
        $('#all_new').change(function() {

            if ($(this).prop('checked')) all_new_filter = 'on';
            else all_new_filter = 'off';
            //console.log('change detected: all_new_filter..');
            applyFilters();

        })
    });

    $(function() {
        $('#free_chat').change(function() {

            if ($(this).prop('checked')) free_chat_filter = 'on';
            else free_chat_filter = 'off';
            //console.log('change detected: free_chat_filter..');
            applyFilters();

        })
    });

    $(function() {
        $('#member_chat').change(function() {

            if ($(this).prop('checked')) member_chat_filter = 'on';
            else member_chat_filter = 'off';
            //console.log('change detected: member_chat_filter..');
            applyFilters();

        })
    });

    $(function() {
        $('#members_only').change(function() {

            if ($(this).prop('checked')) members_only_filter = 'on';
            else members_only_filter = 'off';
            //console.log('change detected: members_only_filter..');
            applyFilters();

        })
    });

    $(function() {
        $('#vip_show').change(function() {

            if ($(this).prop('checked')) vip_show_filter = 'on';
            else vip_show_filter = 'off';
            //console.log('change detected: vip_show_filter..');
            applyFilters();

        })
    });

    $(function() {
        $('#offline').change(function() {

            if ($(this).prop('checked')) offline_filter = 'on';
            else offline_filter = 'off';
            //console.log('change detected: offline_filter..');
            applyFilters();

        })
    });

    $(document).ready(function() {

        studio = $("#myStudio").children("option:selected").val();
        //console.log("studio");
        //console.log(studio);

        $('#all_new').bootstrapToggle(all_new_filter);
        $('#free_chat').bootstrapToggle(free_chat_filter);
        $('#member_chat').bootstrapToggle(member_chat_filter);
        $('#members_only').bootstrapToggle(members_only_filter);
        $('#vip_show').bootstrapToggle(vip_show_filter);
        $('#offline').bootstrapToggle(offline_filter);
        $('#avg').bootstrapToggle(avg_filter);

        $('#avg').change(function() {

            filter_avg();

        })

    } );

    $("#studioSelect").change(function () {
        studio = $("#myStudio").children("option:selected").val();
        //console.log("studio");
        //console.log(studio);
        socketConnect();


    });

    function applyFilters(){

        if (all_new_filter === 'on'){
            //console.log('applyFilters.. all_new_filter.. on');
            $(".old").show();
            $(".new").show();
        } else {
            //console.log('applyFilters.. all_new_filter.. off');
            $(".old").hide();
            $(".new").show();
        }

        let cls_all_new_filter = (all_new_filter === 'on') ? ".free_chat" : ".free_chat.new";
        if (free_chat_filter === 'on'){
            //console.log('applyFilters.. free_chat_filter.. on');
            $(cls_all_new_filter).show();
        } else {
            //console.log('applyFilters.. free_chat_filter.. off');
            $(cls_all_new_filter).hide();
        }

        let cls_member_chat_filter = (all_new_filter === 'on') ? ".member_chat" : ".member_chat.new";
        if (member_chat_filter === 'on'){
            //console.log('applyFilters.. member_chat_filter.. on');
            $(cls_member_chat_filter).show();
        } else {
            //console.log('applyFilters.. member_chat_filter.. off');
            $(cls_member_chat_filter).hide();
        }

        let cls_members_only_filter = (all_new_filter === 'on') ? ".members_only" : ".members_only.new";
        if (members_only_filter === 'on'){
            //console.log('applyFilters.. members_only_filter.. on');
            $(cls_members_only_filter).show();
        } else {
            //console.log('applyFilters.. members_only_filter.. off');
            $(cls_members_only_filter).hide();
        }

        let cls_vip_show_filter = (all_new_filter === 'on') ? ".vip_show" : ".vip_show.new";
        if (vip_show_filter === 'on'){
            //console.log('applyFilters.. vip_show_filter.. on');
            $(cls_vip_show_filter).show();
        } else {
            //console.log('applyFilters.. vip_show_filter.. off');
            $(cls_vip_show_filter).hide();
        }

        let cls_offline_filter = (all_new_filter === 'on') ? ".offline" : ".offline.new";
        if (offline_filter === 'on'){
            //console.log('applyFilters.. offline_filter.. on');
            $(cls_offline_filter).show();
        } else {
            //console.log('applyFilters.. offline_filter.. off');
            $(cls_offline_filter).hide();
        }


    }

    function updateShifts(){
        //temp
        //tempavg
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/tempavg',
            //data:{studio:studio},

            success: function (data) {

                //console.log(data);
                // sleep(2000);
                updateShifts();

            }

        });
    }

    function ajaxCall() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/modelstats',

            success: function (data) {

                //console.log(data);
                //console.log("total: " + data.total);
                //console.log("found: " + data.found);
                $("#count").text(data.found + ' (' + data.total + ')');
                showModels(data.models);

            }

        });

    }

    function showModels(){
        count_free_chat = 0;
        count_private_chat = 0;
        count_members_chat = 0;
        count_vip_show = 0;
        count_offline = 0;
        count_new = 0;
        count_all_studio = 0;

        container.empty();
        models.forEach(function(model){
            insertModel(model);
        });
        applyFilters();

    }

    function filter_avg(){
        //console.log("avg filter..");
        var_filter_avg = (-1) * var_filter_avg;
        models.sort(compare);
        showModels();
    }

    function compare(a, b) {
        const avgA = a.avg.avg;
        const avgB = b.avg.avg;

        let comparison = 0;
        if (avgA > avgB) {
            comparison = 1 * var_filter_avg;
        } else if (avgA < avgB) {
            comparison = -1 * var_filter_avg;
        }
        return comparison;
    }



    function insertModel(model){

        //console.log(model);
        let studio_id = model.studios.id;

        if ((studio_id == studio) || (studio == 0)){
            let name = model.performerId;
            let status = model.status;
            let label = 'F';
            count_all_studio++;
            $("#count").text(count_all_studio);

            switch(status) {
                case "free_chat":
                    count_free_chat++;
                    label = 'F';
                    break;
                case "member_chat":
                    count_private_chat++;
                    label = 'P';
                    break;
                case "members_only":
                    count_members_chat++;
                    label = 'M';
                    break;
                case "vip_show":
                    count_vip_show++;
                    label = 'V';
                    break;
                case "offline":
                    count_offline++;
                    label = 'Off';
                    break;
                default:
                // code block
            }

            let price = model.details.chargeAmount + '$';
            let avg = model.avg.avg + '$';
            let count = model.avg.count;
            let count_cls = (count > 5) ? " old" : " new";
            if (count <= 5) count_new++;
            let src = (model.profilePictureUrl.size320x180);


            let studio_name = model.studios.name;

            let stories_privacy_premium = 0;
            let stories_privacy_free = 0;

            let st = model.stories;
            st.forEach(function(story){

                if (story.stories_privacy === "free") stories_privacy_free++;
                if (story.stories_privacy === "premium") stories_privacy_premium++;

            });
            //let story = "F: " + stories_privacy_free + '/P: ' + stories_privacy_premium;

            let story_free_class = (stories_privacy_free < 2) ? "text-danger" : "text-info";
            let story_premium_class = (stories_privacy_premium < 2) ? "text-danger" : "text-info";

            let story_Free = $("<span/>",{text:"F: " + stories_privacy_free, "class":story_free_class});
            let story_Prem = $("<span/>",{text:'/P: ' + stories_privacy_premium, "class":story_premium_class});

            let col = $("<div/>",{"class":"col-xl-2 col-lg-4 col-md-6 col-sm-12 "+ status + count_cls});

            let card = $("<div/>",{"class":"card my-card",on: {
                click: function() {
                    //console.log(model);
                    $("#modelName").text(name);
                    let bdy = $("#modalBody");
                    bdy.empty();
                    let stories = model.stories;
                    //console.log(stories);

                    stories.forEach(function(story){

                        //console.log(story.stories_privacy);
                        let tr = $("<tr/>",{});
                        let td_type = $("<td/>",{text:story.stories_type});
                        let td_privacy = $("<td/>",{text:story.stories_privacy});
                        let td_exp = $("<td/>",{text:story.stories_expire});

                        let td_lastcheck = $("<td/>",{text:story.updated_at});

                        let media = JSON.parse(story.stories_media);
                        //console.log(media);
                        let url = media[0].contentUri;
                        let td_url = $("<td/>",{});

                        let newLink = $("<a />", {
                            name : "link",
                            href : url,
                            target: "_blank",
                            text : "open"
                        });
                        td_url.append(newLink);

                        tr.append(td_type,td_privacy,td_exp,td_url,td_lastcheck);
                        bdy.append(tr);
                    });

                    $('#Mymodal').modal('show');
                }
            }
            });

            let card_header = $("<div/>",{"class":"card-header"});
            let card_title = $("<h4/>",{text: name, "class":"my-card-title text-primary"});
            let card_status = $("<div/>",{text: label, "class":"status status_"+status});
            let card_studio = $("<div/>",{text: studio_name, "class":"studio"});
            card_header.append(card_title);
            card_header.append(card_status);
            card_header.append(card_studio);
            card.append(card_header);

            let card_body = $("<div/>",{"class":"card-body"});
            //let img = $('<img />', {"id":'avatar'+name,"style": 'height: 100px!important;margin-top:10px!important', "src": src,"class": "rounded-circle"});
            let img = $('<img />', {"id":'avatar'+name, "src": src});
            card_body.append(img);
            card.append(card_body);

            let card_footer = $("<div/>",{"class":"card-footer"});
            let table = $("<table/>");
            let tr1 = $("<tr/>",{});
            let card_footer_c1 = $("<td/>",{text:"MIN","class":"text-grey"});
            let card_footer_c2 = $("<td/>",{text:"AVG","class":"text-success"});
            let card_footer_c3 = $("<td/>",{text:"STORY","class":"text-info"});
            tr1.append(card_footer_c1,card_footer_c2,card_footer_c3);
            let tr2 = $("<tr/>",{});
            let card_footer_c11 = $("<td/>",{text:price,"class":"text-grey"});
            let card_footer_c21 = $("<td/>",{text:avg,"class":"text-success"});
            let card_footer_c31 = $("<td/>",{});
            card_footer_c31.append(story_Free,story_Prem);
            tr2.append(card_footer_c11,card_footer_c21,card_footer_c31);

            table.append(tr1,tr2);
            card_footer.append(table);

            card.append(card_footer);
            col.append(card);
            container.append(col);

            $("#count_new").text(count_new);
            $("#count_free").text(count_free_chat);
            $("#count_private").text(count_private_chat);
            $("#count_members").text(count_members_chat);
            $("#count_vip").text(count_vip_show);
            $("#count_offline").text(count_offline);
        }



    }



</script>
@endpush