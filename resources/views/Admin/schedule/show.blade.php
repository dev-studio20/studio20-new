@extends('layouts.admin_app', ['activePage' => 'schedule', 'titlePage' => __('Schedule')])

@push('head')

@endpush

@section('content')
    <style>
        .ftablew{
            width:200px;
        }
        .stablew{
            width:100%;
            border: 1px solid black!important;
        }
        .fcontainer{
            margin-top: 50px;
            margin-left: 10px;
            margin-right: 10px;
            width: 100%;
        }
        .mtop{
            margin-top:35px;
        }
        .fbold{
            font-weight: bold;
        }
        .green-white{
            background-color: green;
            color: white;
            text-align: center;
        }
        .selectChange{
            width: 100%;
        }
        .table-bordered>tbody>tr>td {
            border: 1px solid black!important;
        }
        .table-bordered>thead>tr>th{
            border: 1px solid black!important;
        }
        .tcenter{
            text-align: center;
        }
        .cellm10{
            width:10%;
            color: white;
            background-color: green;
        }
        td>div>a{
            color:white!important;
        }
        .cellm20{
            overflow: hidden;
            width: 236px;
        }
        .td30{
            width:30%;
        }
        .mySelect{
            width:90%;
        }
        .ftd{
            width: 12%;
        }
        #btnSave{
            margin:10px;
            width:178px;
        }

        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url("../material/img/Preloader_2.gif") center no-repeat #fff;
        }

        .modelsNr{
            text-align: center;
            color: red;
            border-radius: 50px;
            margin: 24px;
            background: white;
        }

        .modelsNrBlack{
            text-align: center;
            color: black;
            border-radius: 50px;
            margin: 24px;
            background: white;
        }
        .chosen-single{
            background: darkgreen!important;
        }
        .chosen-container-single .chosen-single{
            width:90px!important;
        }
        .chosen-drop{
            width:200px!important;
        }

    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card"  style="background: #ecf0f5;">
                    <div class="card-header card-header-tabs card-header-warning">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper fbold">
                                <h4>Manager Schedule & Model Allocation</h4>
                                <hr>
                                <div class="row ">
                                    <div class="col-lg-3">
                                        <table class="ftablew fbold">
                                            <tbody>
                                            <tr><td>STUDIO</td><td id="studio">--</td></tr>
                                            <tr><td>MANAGER</td><td id="manager">--</td></tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-lg-3">

                                        <table class="ftablew fbold">

                                            <tbody>
                                            <tr>
                                                <td>MONTH</td>
                                                <td id="monthSelect">

                                                    {!! Form::select('months', $months, 2, [ 'class' => 'selectChange', "id" => "selectedMonth" ]) !!}

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>YEAR</td>
                                                <td>
                                                    <select class="selectChange" id="selectedYear" name="year">
                                                        <option value="0">2020</option>
                                                        <option value="1">2021</option>
                                                    </select>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="offset-lg-3 col-lg-3">
                                        @if ($role == 5)
                                            <button class="btn btn-success" id="btnSave">Save</button>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane active show">

                                <div class="row">

                                    <div class="col-lg-12">

                                        <div class="se-pre-con" style="display: none;"></div>

                                        <div class="row" id="mainContainer">

                                        </div>

                                        {{--<div class="row">--}}

                                        {{--<div class="col-lg-6">--}}

                                        {{--<table class="stablew mtop table table-bordered" style="overflow: hidden; white-space: nowrap;">--}}
                                        {{--<thead class="fbold">--}}
                                        {{--<tr><th class="tcenter">Name</th><th class="tcenter">Job Title</th><th class="tcenter" colspan="2">Models First Period</th><th class="tcenter" colspan="2">Models Second Period</th></tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody id="listTrainers">--}}

                                        {{--<tr><td>trainer1</td><td>manager</td><td class="td30"><div class="cellm20">NikkySauvage,TashaMave,SonniaRaven,AubreyNovaa,LettyLaytner,AmberlyHaile,SilviaEyrie,JessieRyah,RebeccaBlussh,TrishaAiden,ReneeRysse,RachellHell,SasshaRed,IvyBlueskyy</div></td><td class="cellm10 tcenter"><div><a href="/admin/edittrainermodels/61" target="_blank">Edit</a></div></td><td class="td30"><div class="cellm20">NikkySauvage,TashaMave,SonniaRaven,AubreyNovaa,LettyLaytner,AmberlyHaile,SilviaEyrie,JessieRyah,RebeccaBlussh,TrishaAiden,ReneeRysse,RachellHell,SasshaRed,IvyBlueskyy</div></td><td class="cellm10 tcenter"><div><a href="/admin/edittrainermodels/61" target="_blank">Edit</a></div></td></tr></tbody>--}}
                                        {{--</table>--}}

                                        {{--</div>--}}

                                        {{--</div>--}}

                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>




@endsection

@push('js')



<script>

    let studio = 0;
    let container = $("#mainContainer");
    let scheduledata = [];
    let month = $("#selectedMonth").children("option:selected").val();
    month++;
    let year = $("#selectedYear").children("option:selected").text();
    let labels = ["T1 07:00-19:00", "T1 07:00-19:00","Additional Support T1","T2 19:00-07:00","T2 19:00-07:00","Aditional Support T2", "Total models T1", "Total models T2"];


    $(window).on('load', function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });

    $(document).ready(function () {

        //on studio change get name, manager, etc
        studio = $("#myStudio").children("option:selected").val();
        ajaxCallStudioAndMonth();

        $("#studioSelect").change(function () {

            studio = $("#myStudio").children("option:selected").val();
            ajaxCallStudioAndMonth();

        });

        $("#selectedMonth").change(function () {

            month = $("#selectedMonth").children("option:selected").val();
            month++;
            ajaxCallStudioAndMonth();

        });

        $("#selectedYear").change(function () {

            year = $("#selectedYear").children("option:selected").text();
            ajaxCallStudioAndMonth();

        });


        //on month+year change ...

        $("#btnSave").click(function (){
            let data = {};
            $(".mySelect").each(function(i, obj) {
                data[i] = $(obj).find(':selected').val();
            });

            //store schedule in db
            ajaxStoreData(data);
        });


    });

    function ajaxStoreData(jData){

        month = $("#selectedMonth").val();
        month++;

        year = $("#selectedYear :selected").text();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/schedulestore',
            data:{studio:studio, schedule:jData, month:month, year: year},

            success: function (data) {

                console.log(data);
                location.reload();

            }

        });


    }

    function ajaxCallStudioAndMonth(){

        $(".se-pre-con").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/schedule',
            data:{studio:studio, month:month, year:year},

            success: function (data) {

                if (data.success) buildSchedule(data.data);
                //else showError();

                $(".se-pre-con").fadeOut("slow");

            }

        });

    }

    function buildSchedule(data){

        //console.log("data");
        //console.log(data);

        let std = data.studio.name;
        let mngr = (data.mainManager) ? data.mainManager.name : '--';

        let res = [data.reservations.T1,data.reservations.T2];
        scheduledata = data.schedule;

        //should check if data exist..

        let supports = data.supports;
        let daysMonth = data.daysMonth;

        container.empty();
        $("#studio").empty().text(std);
        $("#manager").empty().text(mngr);

        daysMonth.forEach(function (item, indexday){
            //console.log(item);
            let col = $("<div/>", {"class":"col-lg-12"});
            let table = $("<table/>", {"id":"table"+indexday+1, "class":"stablew mtop"});

            let tr = $("<tr/>", {"class":"fbold"});
            let td = $("<td/>", {text:"Period "+(indexday+1),"class":"ftd"});
            tr.append(td);
            item.forEach(function (day){
                let td_day = $("<td/>", {text:day});
                tr.append(td_day);
            });

            table.append(tr);

            labels.forEach(function (label, index){
                let tr_c = $("<tr/>", {"class":"c1"+(index+1)});
                let td_c = $("<td/>", {text:label,"class":"fbold"});
                tr_c.append(td_c);

                item.forEach(function (day, ind){
                    let td_dd = $("<td/>", {});
                    let select = $("<select/>", {"class":"chosen mySelect"});
                    let splitDay =  day.split('-')[0];
                    //console.log(splitDay);
                    //console.log("index: " + index);
                    if (index < 6) {

                        $.each(supports, function(key, value) {
                            select.append($("<option></option>").attr("value",key).text(value));
                        });
                        td_dd.append(select);

                    } else {
                        //console.log("index-dif: " + (index-6));
                        //console.log(res);
                        //console.log(res[index-6]);
                        //console.log(res[index-6][splitDay]);
                        //console.log(splitDay);
                        if (splitDay in res[index-6]) td_dd = $("<td/>", {text:res[index-6][splitDay], "id":"T1-0" + index, "class":"modelsNr"});
                        else td_dd = $("<td/>", {text:0, "id":"T1-0" + index, "class":"modelsNr"});

                    }
                    tr_c.append(td_dd);
                });
                table.append(tr_c);

            });

            col.append(table);
            container.append(col);
        });



        $(".mySelect").each(function(i, obj) {
            //check if array has data
            //scheduledata
            //console.log(scheduledata[i]);
            let myval = (scheduledata[i]) ? scheduledata[i] : 0;

            $(obj).val(myval).change();

        });



    }

</script>
@endpush