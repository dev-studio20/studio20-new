@extends('layouts.admin_app', ['activePage' => 'reservationsid', 'titlePage' => __('Edit Reservations')])

@push('head')

@endpush

@section('content')
    <style>

        table thead{
            border: 1px solid black;
        }

        ul {
            font-size:12px;
        }

        .ctop{
            background: white;
            width: 100%;
            box-shadow: 2px 6px 8px #888888;
        }

        .cmid{
            margin-top:25px;
            background: white;
            height:500px;
            width: 100%;
            box-shadow: 2px 6px 8px #888888;
            padding-top:50px;
        }

        .month_container{
            border: 1px solid dimgray;
            border-radius: 7px;
        }

        .cell{
            border:1px solid gray;
            text-align: center;
            background: whitesmoke;
        }

        .cell2{
            border:1px solid gray;
            text-align: center;
            background: lightgray;
        }

        .approve{
            background: yellow;
        }

        .rr_found{
            background: yellow;
            font-weight:bold;
        }

        .rr_day{
            background: yellow;
            font-weight:bold;
            border: 3px solid red;
        }

        .approved{
            background: red;
        }

        .selected{
            background: green;
        }

        .move_selected{
            background: greenyellow;
        }

        .mouse_over_cell{
            background: palevioletred;
        }

        .footer{
            padding: 3px;
            border: 2px solid darkgrey;
            border-radius: 6px;
        }

        .sub_res{
            word-break: break-all;
            text-align: center;
        }

        .confirmation_row{
            padding: 3px;
            border: 1px solid darkgrey;
            border-radius: 6px;
            height:44px;
            display: none;
            text-align: center;
        }

        .container_b_header{
            height: 54px;
        }
        .b_header_left{
            position: absolute;
            font-size: 23px;
            font-weight: 600;
            background: darkgrey;
            border-radius: 11px;
            padding: 2px;
            margin: 3px;
            cursor: pointer;
        }
        .b_header_center{
            position: absolute;
            font-size: 23px;
            font-weight: 600;
            background: darkgrey;
            border-radius: 11px;
            padding: 2px;
            margin: 3px;
            left: 40px;
        }
        .b_header_right{
            position: absolute;
            font-size: 23px;
            font-weight: 600;
            background: darkgrey;
            border-radius: 11px;
            padding: 2px;
            margin: 3px;
            right: 10px;
            cursor: pointer;
        }

        .footer_next{
            padding: 3px;
            border: 2px solid darkgrey;
            border-radius: 6px;
        }

        .b_footer_next{
            height:23px;
        }

        #input_field{
            width: 100%;
            height: 38px;
        }
        #error_msg{
            text-align: center;
            color: red;
            height: 25px;
        }

    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card"  style="background: #ecf0f5;">
                    <div class="card-header card-header-tabs card-header-warning">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper fbold">
                                <h4>Reservations</h4>
                                <h5>Model: {{ $modelname }}</h5>
                                <hr>

                                <div class="row">
                                    <div class="col-lg-offset-1 col-lg-3">
                                        <h3>APROVE</h3>
                                        <ul>
                                            <li>Aprove model reservations</li>
                                            <li>Edit model reservations</li>
                                            <li>Reject model reservations</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3">
                                        <h3>RR</h3>
                                        <ul>
                                            <li>RR can be made only in the first free day</li>
                                            <li>Maxim 1/week</li>
                                            <li>Maxim 6 work days/week</li>
                                        </ul>
                                    </div>

                                    <div class="col-lg-3">
                                        <h3>MOVE</h3>
                                        <ul>
                                            <li>Move can be made only in the same period</li>
                                            <li>Move can be made only if there are no RR in this period</li>
                                            <li>Maxim 6 work days/week</li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane active show">

                                <div class="row">
                                    <h3 id="error_msg"></h3>
                                </div>
                                    <div class="row">

                                        <div id="container" class="offset-lg-2 col-lg-3 month_container">

                                        </div>

                                        <div id="container2" class="offset-lg-1 col-lg-3 month_container">

                                        </div>

                                    </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>




@endsection

@push('js')



<script>

    let period = '{{ $period }}';
    let nextperiod = period;
    let modelid = '{{ $model_id }}';
    let modelname = '{{ $modelname }}';
    let modelemail = '{{ $model_email }}';
    let managerid = '{{ $manager_id }}';
    let status = '{{ $status }}';
    let hour = '{{ $hour }}';
    let days = @json($days);
    let nextdays = days;
    let action = 'model_res_approve';
    let week_days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
    let rrdayfound = null;
    let dayfound = null;
    let monthfound = null;
    let reason = null;
    let myrr = [];

    let selectedDays = [];
    let selectedMoveDays = [];
    let selectedRrDay = null;
    let selectedPeriod = 0;

    let btn_approve = $('<button/>',{text:"APPROVE", "id":"btnApprove" ,"class":"col-lg-4 btn btn-success", onclick:"approveReservation();"});
    let btn_edit = $('<button/>',{text:"EDIT", "id":"btnEdit", "class":"col-lg-4 btn btn-info", onclick:"editReservation();"});
    let btn_reject = $('<button/>',{text:"REJECT", "id":"btnReject", "class":"col-lg-4 btn btn-danger", onclick:"rejectReservation();"});

    let btnYes = $('<button/>',{text:"Confirm","class":"col-lg-offset-2 col-lg-3 btn btn-success", onclick:"ajaxApprove(action);"});
    let btnNo = $('<button/>',{text:"Cancel","class":"col-lg-offset-2 col-lg-3 btn btn-danger", onclick:"approveNo();"});

    let btnYes2 = $('<button/>',{text:"Confirm","id":"btnConfirm","class":"col-lg-offset-2 col-lg-3 btn btn-success", onclick:"ajaxRR();"});
    let btnYes3 = $('<button/>',{text:"Confirm","id":"btnConfirm","class":"col-lg-offset-2 col-lg-3 btn btn-success", onclick:"ajaxMove();"});
    let btnNo2 = $('<button/>',{text:"Cancel","class":"col-lg-offset-2 col-lg-3 btn btn-danger", onclick:"btnCancel();"});

    let btn_rr = $('<button/>',{text:"RR", "id":"btnApprove" ,"class":"col-lg-offset-2 col-lg-3 btn btn-warning", onclick:"rrReservation();"});
    let btn_move = $('<button/>',{text:"MOVE", "id":"btnEdit", "class":"col-lg-offset-2 col-lg-3 btn btn-info", onclick:"moveReservation();"});

    let footer = $('<div/>', {"class":"row footer"});

    $(document).ready(function () {

        generateSelectedMonth(period);



    });

    function getND(month_year){

        console.log("month_year");
        console.log(month_year);

        let arr = month_year.split('-');
        let month = arr[0];
        let year = arr[1];

        return new Date(year, month-1, '01').getDay();

    }

    function ajaxRR(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../sreservations",
            method: 'post',
            data: {modelid: modelid, dayfoundrr: rrdayfound, selectedday:selectedRrDay},
            success: function (html) {
                console.log(html);
                location.reload();
            }
        });

    }

    function ajaxMove(){

        console.log({ajxMoveDays: 'on', olddays: selectedDays, newdays:selectedMoveDays, modelid: modelid, period: period});

        if (selectedMoveDays.length > 0){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "../sreservations",
                method: 'post',
                data: {ajxMoveDays: 'on', olddays: selectedDays, newdays:selectedMoveDays, modelid: modelid, period: period},
                success: function (html) {
                    console.log(html);
                    location.reload();
                }
            });
        }

    }

    function findNextRr(startDay){

        let today = new Date();

        //ajax call for this week
        ajaxFetchMonth();

    }

    function ajaxFetchMonth(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../sreservations",
            method: 'post',
            data: {ajaxFindMonthRr: "ok", modelid:modelid},
            success: function (html) {
                console.log("first day found -> " + html);
                let arr = html.split('-');
                dayfound = arr[2];
                monthfound = arr[1]+"-"+arr[0];

                rrdayfound = html;

                nextperiod = monthfound;
                ajaxMonth();

            }
        });
    }

    function overCellEnter(elem){

        $(elem).addClass( "mouse_over_cell" );

    }

    function overCellLeave(elem){

        $(elem).removeClass( "mouse_over_cell" );

    }

    function addRemoveDays(elem){

        let ctd = $(elem);

        let val = ctd.text();

        console.log("action = " + action);

        if (action === 'model_rr') {

            //compare dates (today > click date)
            let cDate = val + "-" + period;

            if (compareDatesToday(cDate)) {

                if(days.includes(val)){

                    selectedDays.push(val);

                    selectedDays = selectedDays.filter( onlyUnique );
                    console.log(selectedDays);
                    ctd.addClass('selected');
                    selectedRrDay = ctd.text() + "-" + period;
                    findNextRr(ctd.text());
                }
            } else {

            }
        }

        if (action === 'model_res_edit') {
            selectedDays.push(val);
            selectedDays = selectedDays.filter( onlyUnique );
            console.log(selectedDays);
            ctd.addClass('selected');
        }

        if (action === 'model_res_move') {

            $('#btnConfirm').attr('disabled', true);

            let periodText = '';

            if(days.includes(val)) {

                let p1 = false;
                let p2 = false;

                for (let j=1;j<=31;j++){
                    let v = (j<10) ? "0" + j : j + "";
                    if (myrr.includes(v)){
                        if (j < 16) {
                            p1 = true;
                        } else {
                            p2 = true;
                        }

                    }
                }

                if ((parseInt(val) < 16) && (p1) ) {

                    alert("This period contains a RR, you cannot move in this period I!");
                    return;
                } else if ((parseInt(val) > 15) && (p2) ) {

                    alert("This period contains a RR, you cannot move in this period II!");
                    return;
                }

                if (parseInt(val) < 16){
                    periodText = 'Period I';
                    if (selectedPeriod !==1) {
                        selectedDays = [];
                        $('.selected').removeClass('selected');
                        $('.move_selected').removeClass('move_selected');
                        selectedMoveDays = [];
                    }
                    selectedPeriod = 1;
                } else {
                    periodText = 'Period II';
                    if (selectedPeriod !==2) {
                        selectedDays = [];
                        $('.selected').removeClass('selected');
                        $('.move_selected').removeClass('move_selected');
                        selectedMoveDays = [];
                    }
                    selectedPeriod = 2;
                }



                if (selectedDays.includes(val)){
                    selectedDays = removeFromArray(selectedDays, val);
                    ctd.removeClass('selected');
                }else {
                    selectedDays.push(val);
                    ctd.addClass('selected');
                }


                selectedDays = selectedDays.filter( onlyUnique );

                console.log("selected days: ");
                console.log(selectedDays);
                console.log(days);
                console.log(myrr);


                $('#error_msg').text("Select days from " + periodText);

                let footer = $("#footer_next");
                footer.empty();

                let html = 'Selected days: ';
                selectedDays.forEach(function (e){
                    html += ' ' + e + ' ';
                });

                footer.html(html);
            }


        }

    }

    function ajaxMonth(){
        //console.log("next " + nextperiod);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../sreservations",
            method: 'post',
            data: {month:nextperiod, modelid:modelid},
            success: function (html) {
                console.log(html);
                let obj = JSON.parse(html);
                //console.log(obj["month"]);
                //nextdays = obj["days"];
                generateMonthForRR(obj);
            }
        });
    }

    function generateMonthForRR(obj){

        console.log(obj);

        let days_with_rr = obj['RR'];
        myrr = obj['RR'];
        console.log(days_with_rr);

        nextperiod = obj['month'];
        let y = obj['year'];
        nextdays = obj['days'];

        let container = $("#container2");
        container.empty();

        //nextperiod = '11-2019';

        let table_b_head = $('<div/>', {"class":"container_b_header"});
        let c_left = $('<div/>', {text:'<', "class":"b_header_left", onclick:'monthLeft()'});
        let h_center = $('<div/>', {text:'Month: '+ nextperiod, "class":"b_header_center"});
        let c_right = $('<div/>', {text:'>', "class":"b_header_right", onclick:'monthRight()'});

        table_b_head.append(c_left, h_center, c_right);
        container.html(table_b_head);

        let f_day = getND(nextperiod+'-'+y);
        console.log("f_day: " + f_day);

        let table = $("<table/>",{"class":"table"});

        let thead = $('<thead/>', {});

        let htr = $('<tr/>',{});

        for(let ind = 0; ind < 7; ind++ ) htr.append($('<th/>',{text:week_days[ind]}));
        thead.append(htr);
        table.append(thead);

        let tbody = $('<tbody/>',{});
        table.append(tbody);

        let maxDays = getDaysInMonth(nextperiod+'-'+y);
        let tr = $('<tr/>', {});
        for(let buff = 1; buff <= f_day; buff++){
            let td__buff = $('<td/>',{text:'', "class":"cell"});
            tr.append(td__buff );
            //console.log("append..");
        }
        for (let i=1; i <= maxDays; i++){

            let j = (i<10) ? "0"+i : i+"";
            let cls = "cell";
            if (i > 15) cls = 'cell2';
            if(nextdays && nextdays.includes(j)){
                cls = "cell approved";
            }

            if(days_with_rr && days_with_rr.includes(j)){
                cls = "cell rr_day";
            }

            console.log("monthfound");
            console.log(monthfound);
            console.log("nextperiod");
            console.log(nextperiod);

            let m = monthfound.split("-")[0];

            //if ( (dayfound == i) && (monthfound === nextperiod) ) {
            if ( (dayfound == i) && (m == nextperiod) ) {
                cls = "cell rr_found";
                //console.log("day found");
            }

            //let td = $('<td/>', {text: j, "class":cls, onmouseenter:"overCellEnter(this)", onmouseleave:"overCellLeave(this)"});
            let td = $('<td/>', {text: j, "class":cls, onclick:"moveDays(this)"});
            tr.append(td);
            if ((i + f_day) % 7) {
                table.append(tr);
            }
            else {
                //when 0
                if (i + f_day === 7) table.append(tr);
                tr = $('<tr/>', {});
            }

        }

        table.append(tr);
        container.append(table);

        let b_footer = $("<div/>",{"id":"footer_next","class":"row b_footer_next"});
        let footer = $("<div/>",{"class":"row footer_next"});

        if (action === 'model_res_move' ) footer.append(btnYes3, btnNo2);
        else footer.append(btnYes2, btnNo2);
        container.append(b_footer);
        container.append(footer);

    }

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    function removeFromArray(arr, val){
        let newArr = [];
        for(let i = 0; i < arr.length; i++){
            if(arr[i] !== val) newArr.push(arr[i]);
        }
        return newArr;
    }

    function moveDays(elem){
        let sDay = $(elem);
        let val = parseInt(sDay.text());
        let valText = sDay.text();

        if (action === 'model_res_move') {

            let parent = sDay.parent();
            console.log(parent);




            console.log("elem: " + sDay.text());
            console.log("selected days: " + selectedDays);

            if ( ( ((selectedPeriod === 1) && (val < 16)) || ((selectedPeriod === 2) && (val > 15)) )  ){

                if (selectedMoveDays.includes(valText)){
                    //remove val and class
                    sDay.removeClass('move_selected');
                    selectedMoveDays = removeFromArray(selectedMoveDays, valText);

                } else {
                    sDay.addClass('move_selected');
                    selectedMoveDays.push(valText);
                }

            }

            let approved = parent.find('.approved').length;
            let selected = parent.find('.move_selected').length;
            let approvedselected = parent.find('.approved.move_selected').length;

            let total = approved + selected - approvedselected;

            console.log("total: " + total);
            console.log("approved: " + approved);
            console.log("selected: " + selected);
            console.log("approvedselected: " + approvedselected);

            if ( (selectedMoveDays.length === selectedDays.length) && (total < 7)) $('#btnConfirm').attr('disabled', false);
            else $('#btnConfirm').attr('disabled', true);



        }
    }


    function compareDatesToday(clickDate){
        //compare current date with nextperiod

        let thisd = new Date();
        let thisd_oo = new Date(thisd.getFullYear(), thisd.getMonth(), thisd.getDate());

        let arr = clickDate.split('-');
        let day = parseInt(arr[0]);
        let month = parseInt(arr[1]) - 1;
        let year = arr[2];
        let nextd = new Date(year, month, day);

        return (thisd_oo <= nextd);

    }

    function rrReservation(){
        console.log("RR");

        action = 'model_rr';

        $("#btnApprove").attr('disabled', true);
        $("#btnEdit").attr('disabled', true);

        $(".selected").removeClass( "selected");

        selectedRrDay = null;

        //monthRight();
    }

    function moveReservation(){
        console.log("MOVE");

        action = 'model_res_move';

        console.log("action: " + action);

        $('#btnApprove').attr('disabled', true);
        $('#btnEdit').attr('disabled', true);

        selectedDays = [];
        selectedMoveDays = [];

        ajaxMonth();
    }

    function getDaysInMonth(month_year){

        console.log("month_year");
        console.log(month_year);

        //let month_year = '09-2019';

        let arr = month_year.split('-');
        let month = arr[0];
        let year = arr[1];

        return new Date(year, month, 0).getDate();


    }


    function generateSelectedMonth(period){

        let f_day = getND(period);
        //week_days[i]

        let container = $("#container");
        container.empty();
        let table_b_head = "<span><h4><b>Month: "+period+"</b></h4></span><span><b>Hour: "+hour+"</b></span>";
        container.html(table_b_head);
        let table = $("<table/>",{"class":"table"});

        let thead = $('<thead/>', {});

        let htr = $('<tr/>',{});

        for(let ind = 0; ind < 7; ind++ ) htr.append($('<th/>',{text:week_days[ind]}));
        thead.append(htr);
        table.append(thead);

        let tbody = $('<tbody/>',{});
        table.append(tbody);

        let maxDays = getDaysInMonth(period);
        let tr = $('<tr/>', {});
        for(let buff = 1; buff <= f_day; buff++){
            let td__buff = $('<td/>',{text:'', "class":"cell"});
            tr.append(td__buff );
        }
        for (let i=1; i <= maxDays; i++){
            //console.log("i_for: ");
            //console.log(i);

            let j = (i<10) ? "0"+i : i+"";
            let cls = "cell";
            if (i > 15) cls = 'cell2';
            if(days.includes(j)){
                cls = "cell approve";
                if (status == 1) cls = "cell approved";
            }
            let td = $('<td/>', {text: j, "class":cls, onmouseenter:"overCellEnter(this)", onmouseleave:"overCellLeave(this)", onclick:"addRemoveDays(this)"});
            //let td = $('<td/>', {text: j, "class":cls, onclick:"addRemoveDays(this)"});
            tr.append(td);
            if ( ((i + f_day) % 7) || (i + f_day) === 7){
                console.log("i: ");
                console.log(i);
                console.log("f_day: ");
                console.log(f_day);
                table.append(tr);
                if (i + f_day === 7) {
                    tr = $('<tr/>', {});
                }

            }
            else {
                tr = $('<tr/>', {});

                console.log("i: ");
                console.log(i);
            }

        }

        table.append(tr);
        container.append(table);

        let row = '<div class="row"><div class="sub_res">Submited Reservations: '+days+'</div></div>';


        if (status == 0) {
            addBtnsToFooter([btn_approve, btn_edit, btn_reject]);
        } else {
            addBtnsToFooter([btn_rr, btn_move]);
        }

        container.append(row);
        container.append(footer);

    }

    function addBtnsToFooter(btns){
        footer.empty();

        btns.forEach(function (elem){
            footer.append(elem);
        });
    }


</script>
@endpush