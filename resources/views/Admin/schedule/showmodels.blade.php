@extends('layouts.admin_app', ['activePage' => 'schedulemodels', 'titlePage' => __('Schedule Models')])

@push('head')

@endpush

@section('content')
    <style>

        hr{
            margin-top:5px;
            margin-bottom:5px;
        }
        .day_number{
            background: #3c8dbc;
            margin: 0;
            color: white;
        }

        .day_name{
            text-align: center;
            background: white;
            border: 1px solid #3c8dbc;
            margin: 0;
        }

        .chosen-container{
            width: 200px!important;
        }

        .mcol{
            width:10%;
            display: inline-table;
            border: 2px solid #367fa9;
            border-radius: 10px;
            padding-bottom: 5px;
            margin-bottom: 5px;
        }
        .text_align_center{
            text-align:center;
        }
        .p_left_10{
            padding-left:10px;
        }
        .model_cell{
            color:white;
        }
        h1{
            font-size: 30px!important;
        }
        .period1{
            background: whitesmoke;
            margin-bottom: 10px;
            border-radius: 5px;
            box-shadow: 2px 6px 8px #888888;
            padding-left: 10px;
        }

        .period2{
            background: lightgray;
            border-radius: 5px;
            box-shadow: 2px 6px 8px #888888;
            padding-left: 10px;
        }

        .cell_holder{
            background: #3c8dbc;
            margin-top: 2px;
        }
        .cell_holder:hover{
            background: #367fa9;
            cursor: pointer;
        }
        .cell_holder_t{
            background: white;
            margin-top: 2px;
            color: black;
        }
        .edit_dots{
            float:right;
            color: #3c8dbc;
            font-weight: 800;
            margin-right: 5px;
        }
        .start_hour{
            float:right;
            margin-right: 5px;
            font-weight: bold;
        }
        .td-table{
            width:20px;
            height:20px;
            background:limegreen;
            border: 1px solid black;
            text-align: center;
        }
        .td-table-pass{
            width:20px;
            height:20px;
            background:yellowgreen;
            border: 1px solid black;
            text-align: center;
        }
        .td-table-rez{
            width:20px;
            height:20px;
            background:#dd4b39;
            border: 1px solid black;
            text-align: center;
        }
        .td-table-rez:hover{
        }

        .td-table:hover{
            background: white;
            cursor: pointer;
        }
        .cond{
            background: grey!important;
        }
        .cell_holder:hover > .edit_dots { color: white; }
        .cell_holder:hover > .model_cell { font-weight: bold; }

        .tableMove{
            width:100%;
        }

        .selectChange{
            width: 100%;
        }

        .rrmade{
            border-radius: 20px;
        }
        .cell_padding{
            padding-left: 8px!important;
            padding-right: 8px!important;
        }

        .status{
            background: yellow!important;
        }

        .status > .model_cell{
            color: black!important;
        }


    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card"  style="background: #ecf0f5;">
                    <div class="card-header card-header-tabs card-header-info">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper fbold">
                                <h4>Model Schedule</h4>
                                <hr>
                                <div class="row ">

                                    {{--<div class="col-lg-3">--}}
                                        {{--<div>Periods</div>--}}
                                        {{--<div>-- : --</div>--}}
                                    {{--</div>--}}

                                    <div class="col-lg-3">

                                        <table class="ftablew fbold">

                                            <tbody>
                                            <tr>
                                                <td>MONTH</td>
                                                <td id="monthSelect">
                                                    {!! Form::open() !!}
                                                    {!! Form::select('months', $months, 2, [ 'class' => 'selectChange', "id" => "selectedMonth" ]) !!}
                                                    <input type="hidden" id="studio" name="studio" value="0">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>YEAR</td>
                                                <td>
                                                    {!! Form::select('years', $years, 2, [ 'class' => 'selectChange', "id" => "selectedYear" ]) !!}
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="col-lg-3">

                                        <button class="btn btn-warning" type="submit">Show</button>

                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12" style="padding-top: 32px;">
                        <div class="row period1">

                            @foreach($period as $day => $data)
                                <div class="mcol">

                                    <h4 class="text_align_center day_number {{$data['cond']}}">{{$day}}</h4>
                                    <h5 class="day_name" style="text-align: center;">{{$data["day_name"]}}</h5>

                                    <div class="cell_holder_t">
                                        <span class="p_left_10">T1</span>
                                    </div>

                                    @foreach($data["T1"] as $tdata)

                                        <div class="cell_holder {{$data['cond']}}" type="button" onclick="location.href = '#';">
                                            <span class="p_left_10 model_cell">{{$tdata["modelname"]}}</span>
                                            <span class="start_hour">{{$tdata["hour"]}}</span>
                                        </div>

                                    @endforeach

                                    <div class="cell_holder_t">
                                        <span class="p_left_10">T2</span>
                                    </div>

                                    @foreach($data["T2"] as $tdata)

                                        <div class="cell_holder {{$data['cond']}}" type="button" onclick="location.href = '#';">
                                            <span class="p_left_10 model_cell">{{$tdata["modelname"]}}</span>
                                            <span class="start_hour">{{$tdata["hour"]}}</span>
                                        </div>

                                    @endforeach

                                </div>
                            @endforeach


                        </div>




                    </div>

                </div>


            </div>
        </div>


    </div>




@endsection

@push('js')



<script>

    let studio = $("#myStudio").children("option:selected").val();

    $(document).ready(function () {
        $("#studio").val(studio);

        $("#studioSelect").change(function () {

            studio = $("#myStudio").children("option:selected").val();
            $("#studio").val(studio);
        });

    });



    function ajaxCallStudioAndMonth(){

        $(".se-pre-con").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/schedule',
            data:{studio:studio, month:month, year:year},

            success: function (data) {



            }

        });

    }


</script>
@endpush