@extends('layouts.admin_app', ['activePage' => 'users', 'titlePage' => __('Edit Users')])

@push('head')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
@endpush

@section('content')
    <div class="content" style="background: #fff;">
        <div class="container-fluid">
            <div class="row">
                <div class="container">

                    <div class="card">
                        <div class="card-header card-header-warning">

                            <h4 class="card-title">Edit Users</h4>
                            <a class="btn btn-info" href="javascript:void(0)" id="createNewProduct"> Create New User</a>
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table table-hover data-table">
                                <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Studio</th>
                                    <th>Date</th>
                                    <th width="280px">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" class="form-horizontal">
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="form-group">
                            <label for="name" class="">Name</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="email" class="">Email</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="email" name="email" placeholder="Enter Email" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="password" class="">Password</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="password" name="password" placeholder="Enter New Password" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="role" class="">Role</label>
                            <select class="form-control col-lg-12 col-sm-12" id="role" name="role" required=""></select>
                        </div>


                        <div class="form-group">
                            <label for="studio" class="">Studio</label>
                            <select class="form-control col-lg-12 col-sm-12" id="studio" name="studio" required=""></select>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script>
    $(document).ready(function () {
        //myconsole("Test!!!");
    });

    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.users') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'role.name', name: 'role.name'},
                {data: 'studio.name', name: 'studio.name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#createNewProduct').click(function () {
            $('#saveBtn').val("create-product");
            $('#user_id').val('');
            $('#productForm').trigger("reset");
            $('#modelHeading').html("Create New User");
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editProduct', function () {
            let user_id = $(this).data('id');
            user_id = 1;
            $.get("{{ url('admin/users') }}" + '/' + user_id, function (load) {
                console.log(load);
                let data = load.user;
                let studios = load.studios;
                let roles = load.roles;

                let studioid = data.studio_id;
                let roleid = data.role_id;

                let mystudios = $('#studio');
                let myroles = $('#role');

                $('#modelHeading').html("Edit User");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#user_id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#password').val(data.password);

                mystudios.empty();
                Object.keys(studios).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(studios[key]);
                    if (studioid == key) opt.attr("selected","selected");
                    mystudios.append(opt);
                });

                myroles.empty();
                Object.keys(roles).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(roles[key]);
                    if (roleid == key) opt.attr("selected","selected");
                    myroles.append(opt);
                });

            })
        });

        $('#saveBtn').click(function (e) {

            let asad = $('#productForm').serialize();
            console.log(asad);

            e.preventDefault();

            $(this).html('Sending..');

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ url('admin/users') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    $('#saveBtn').html('Save Changes');
                    table.draw();
                    md.showNotification('Success!', 'success');

                },
                error: function (data) {
                    console.log('Error:', data);
                    md.showNotification('Error:' + data, 'danger');
                    $('#saveBtn').html('Save Changes');
                }
            });
        });

        $('body').on('click', '.deleteProduct', function () {

            let user_id = $(this).data("id");


            if (confirm("Are You sure want to delete !")) {
                // Do it!
                $.ajax({
                    data: user_id,
                    type: "DELETE",
                    dataType: 'json',
                    url: "{{ url('admin/users') }}/" + user_id,
                    success: function (data) {
                        table.draw();
                        md.showNotification('User Deleted!', 'success');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        md.showNotification('Error:' + data, 'danger');
                    }
                });
            } else {
                // Do nothing!
            }


        });

    });

</script>
@endpush