@extends('layouts.admin_app', ['activePage' => 'users', 'titlePage' => __('Edit Users')])

@push('head')

@endpush

@section('content')

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">

                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Edit Users
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>



                            </div>
                        </div>
                        <a class="btn btn-info" href="javascript:void(0)" id="createNewUser"> Create New User</a>
                    </div>

                    <div class="card-body table-responsive">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="1a">
                                <table id="allUsers" class="data-table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Studio</th>
                                        <th>Date</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="userForm" name="userForm" class="form-horizontal">
                        <input type="hidden" name="user_id" id="user_id">
                        <div class="form-group">
                            <label for="name" class="">Name</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="email" class="">Email</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="email" name="email" placeholder="Enter Email" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="password" class="">Password</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="password" name="password" placeholder="Enter Password" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="role" class="">Role</label>
                            <select class="form-control col-lg-12 col-sm-12" id="role" name="role" required=""></select>
                        </div>


                        <div class="form-group">
                            <label for="studio" class="">Studio</label>
                            <select class="form-control col-lg-12 col-sm-12" id="studio" name="studio" required=""></select>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script>

    let studio = $("#myStudio").children("option:selected").val();

    let table = $('#allUsers').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "users",
            "type": "POST",
            "data": function (d) {
                d.studio = studio;
            },
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        },
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'role.name', name: 'role.name'},
            {data: 'studio.name', name: 'studio.name'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).ready(function () {



    });





        $('#createNewUser').click(function () {
            $('#saveBtn').val("create-product");
            $('#user_id').val('');
            $('#userForm').trigger("reset");
            $('#modelHeading').html("Create New User");
            $.get("{{ url('admin/users/roles') }}", function (roles) {
                console.log(roles);
                let myroles = $('#role');
                myroles.empty();
                Object.keys(roles).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(roles[key]);

                    myroles.append(opt);
                });

            });
            $.get("{{ url('admin/users/studios') }}", function (studios) {
                console.log(studios);
                let mystudios = $('#studio');
                mystudios.empty();
                Object.keys(studios).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(studios[key]);

                    mystudios.append(opt);
                });

            });
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editUser', function () {
            let user_id = $(this).data('id');

            $.get("{{ url('admin/users') }}" + '/' + user_id, function (load) {
                console.log(load);
                let data = load.user;
                let studios = load.studios;
                let roles = load.roles;

                let studioid = data.studio_id;
                let roleid = data.role_id;

                let mystudios = $('#studio');
                let myroles = $('#role');

                $('#modelHeading').html("Edit User");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#user_id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#password').val(data.password);

                mystudios.empty();
                Object.keys(studios).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(studios[key]);
                    if (studioid == key) opt.attr("selected","selected");
                    mystudios.append(opt);
                });

                myroles.empty();
                Object.keys(roles).forEach(function (key) {
                    let opt = $("<option>").attr('value',key).text(roles[key]);
                    if (roleid == key) opt.attr("selected","selected");
                    myroles.append(opt);
                });

            })
        });

        $('#saveBtn').click(function (e) {

            e.preventDefault();

            $(this).html('Sending..');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                data: $('#userForm').serialize(),
                url: "usersid",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#userForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    $('#saveBtn').html('Save Changes');
                    table.draw();
                    md.showNotification('Success!', 'success');

                },
                error: function (data) {
                    console.log('Error:', data.responseText);
                    md.showNotification('Error:' + data.responseText, 'danger');
                    $('#saveBtn').html('Save Changes');
                }
            });
        });

        $('body').on('click', '.deleteUser', function () {

            let user_id = $(this).data("id");


            if (confirm("Are You sure want to delete !")) {
                // Do it!
                $.ajax({
                    data: user_id,
                    type: "DELETE",
                    dataType: 'json',
                    url: "{{ url('admin/users') }}/" + user_id,
                    success: function (data) {
                        table.draw();
                        md.showNotification('User Deleted!', 'success');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        md.showNotification('Error:' + data, 'danger');
                    }
                });
            } else {
                // Do nothing!
            }


        });



</script>
@endpush