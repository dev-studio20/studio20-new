@extends('layouts.admin_app', ['activePage' => 'dashboard', 'titlePage' => __('Home')])

@section('content')
    <div class="content">
        <div class="container-fluid">

            <div class="row">

                @include('Admin.dashboard.widget.models_status')

            </div>

            @if(in_array($role,[1,3,4,5,6]))
                <div class="row">

                    @include('Admin.dashboard.widget.chart_days')

                    @include('Admin.dashboard.widget.chart_months')

                    @include('Admin.dashboard.widget.chart_period')

                </div>
            @endif

            @if(in_array($role,[2,3,4,5,6]))
                <div class="row">

                    @include('Admin.dashboard.widget.models_free_chat')

                    @include('Admin.dashboard.widget.models_online')

                    @include('Admin.dashboard.widget.approved_models')

                </div>
            @endif

            @if(in_array($role,[1,3,4]))
                <div class="row">

                    @include('Admin.dashboard.widget.open_peridos')

                    @include('Admin.dashboard.widget.payment_request')

                    @include('Admin.dashboard.widget.payment_made')

                </div>
            @endif

            @if(in_array($role,[2,3,4,5,6]))
                <div class="row">

                    @include('Admin.dashboard.widget.pending_model')

                    @include('Admin.dashboard.widget.reservations_approval')

                </div>
            @endif


        </div>
    </div>
@endsection

@push('js')
<script src="https://studio20girls.com:8443/socket.io/socket.io.js"></script>


<script>

    let modelsFromthisStudio = [];

    let role = '{{$role}}';

    let socket = io("https://studio20girls.com:8443", {
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionDelayMax : 5000,
        reconnectionAttempts: 99999,
        transports: ['websocket', 'xhr-polling']
    } );

    //    socket.on('status', (data) => {
    //
    //        console.log("status update..!!");
    //        console.log(data);
    //
    //    });

    socket.on('status_online', (data) => {

        //console.log("data");
        //console.log(data);

        showFreeModels(data.body.models);
    });

    let daysSales;
    let ySales;
    let yStudioSales;
    let yearSales;
    let yearLabels;
    let maxYearSales;

    let yearStudioSales;
    let maxStudio;
    let labelsDays = ["Yesterday", "Today"];
    let studio;
    let maxSalesPeriod = 0;

    let todaySales = 0;
    let ydaySales = 0;
    let maxDaySales = 0;
    let salesDays = [0,0];

    let months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    let tableOpenPeriods;
    let tablePaymentRequest;
    let tablePaymentMade;
    let tableApprovedModels;
    let tablePendingModels;
    let tableReservationsApproval;

    $(document).ready(function () {

        studio = $("#myStudio").children("option:selected").val();
        ajaxCall(studio);

        ajaxCallApprovedModels();
        $("#approvedModels tbody").on('click', 'tr', function () {
            let data = tableApprovedModels.row( this ).data();
            if ((role != 5) && (role != 6)){
                window.open("admin/model/"+data.id);
            }
        } );

        ajaxCallOpenPeriods();
        $("#openPeriods tbody").on('click', 'tr', function () {
            let data = tableOpenPeriods.row( this ).data();
            if ((role != 5) && (role != 6)){
                window.open("admin/closeperiods/"+data.id);
            }
        } );

        ajaxCallPaymentRequest();
        $("#paymentRequest tbody").on('click', 'tr', function () {
            let data = tablePaymentRequest.row( this ).data();
            if ((role != 5) && (role != 6)) {
                window.open("admin/makepayment/");
            }
        } );

        ajaxCallPaymentMade();
        $("#paymentMade tbody").on('click', 'tr', function () {
            let data = tablePaymentMade.row( this ).data();
            if ((role != 5) && (role != 6)) {
                window.open("admin/makepayment/");
            }
        } );

        ajaxCallPendingModels();
        $("#pendingModels tbody").on('click', 'tr', function () {
            let data = tablePendingModels.row( this ).data();
            if ((role != 5) && (role != 6)) {
                window.open("admin/model/"+data.id);
            }
        } );

        ajaxCallReservationsApproval();
        $("#reservationsApproval tbody").on('click', 'tr', function () {
            let data = tableReservationsApproval.row( this ).data();
            if (role != 6)  {
                window.open("admin/reservations");
            }

        } );

    });

    function ajaxCallReservationsApproval(){

        tableReservationsApproval = $("#reservationsApproval").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "140px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'reservationsApproval';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'model.modelname', name: 'modelname' },
                { data: 'month', name: 'month' },
                { data: 'days', name: 'days' },
                { data: 'studios.name', name: 'studio' },
//                { data: 'modelname', name: 'modelname' },
//                { data: 'created_at', name: 'date' },
            ],
            "columnDefs": [
                {
                    "targets": [1],
                    render: function (data, type, row, meta)
                    {
                        if (type === 'display')
                        {
                            data = months[data-1];
                        }
                        return data;
                    }
                }
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function ajaxCallPendingModels(){

        tablePendingModels = $("#pendingModels").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "140px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'pendingModels';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'modelname', name: 'modelname' },
                { data: 'created_at', name: 'date' },
                { data: 'studios.name', name: 'studio' }
            ],
            "columnDefs": [
                {
                    "targets": [0],
                    render: function (data, type, row, meta)
                    {
                        if (type === 'display')
                        {
                            let length = 15;
                            data = data.substring(0, length);
                        }
                        return data;

                    }
                }
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function ajaxCallApprovedModels(){

        tableApprovedModels = $("#approvedModels").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "140px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'approvedModels';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'modelname', name: 'modelname' },
                { data: 'created_at', name: 'date' },
                { data: 'studios.name', name: 'studio' },
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function ajaxCallPaymentMade(){

        tablePaymentMade = $("#paymentMade").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "150px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'paymentMade';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'model.modelname', name: 'modelname' },
                { data: 'period_name.name', name: 'period' },
                { data: 'studio.name', name: 'studio' },
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function ajaxCallPaymentRequest(){

        tablePaymentRequest = $("#paymentRequest").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "150px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'paymentRequest';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'model.modelname', name: 'modelname' },
                { data: 'period_name.name', name: 'period' },
                { data: 'studio.name', name: 'studio' },
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function ajaxCallOpenPeriods(){

        tableOpenPeriods = $("#openPeriods").DataTable( {
            "processing": true,
            "serverSide": false,
            "bFilter": false,
            "paging": false,
            "scrollY": "150px",
            "scrollCollapse": true,
            "bInfo" : false,
            "ajax": {
                'url':'/admin',
                "type": "POST",
                "data": function (d) {
                    d.widget = 'openPeriods';
                    d.studio = studio;

                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            "columns": [
//                { data: 'id', name: 'id' },
                { data: 'model.modelname', name: 'modelname' },
                { data: 'period_name.name', name: 'period' },
                { data: 'studio.name', name: 'studio' },
//                { data: 'hours', name: 'hours' },
//                { data: 'closed', name: 'closed', searchable: true },
//                { data: 'created_at', name: 'created_at' },
            ],
            "iDisplayLength": -1,
            "bStateSave": true,

        } );
    }

    function showFreeModels(data){

        let myTableFree = $("#freeChatModels");
        let myTableOnline = $("#onlineModels");

        let models = data;

        //check if models in free chat and from this studio
        //if studio = 0, show all models from studios

        let td = $('<tr/>',{});

        //let size = models.length;
        let sizeFree = 0;
        let sizeOnline = 0;

        myTableFree.empty();
        myTableOnline.empty();

        models.forEach(function (element, index) {
            //console.log(item, element);

            let model_id = element.model_id;
            let modelName = element.modelname;
            let shift_start = element.shift_start;
            let status_start = element.start;
            let status_end = element.end;
            let diff = element.status_total;
            let diff_all = element.status_total;
            status_start = convertTimestampToDate(status_start);
            status_end = convertTimestampToDate(status_end);
            let last_status = element.status;
            let studio = element.studios.name;
            //let total_work = element.total_work;
            //let date = element.date;

            if (last_status !== 'offline') {

                if (modelsFromthisStudio.includes(modelName)) {
                    //console.log("models");
                    //console.log(models);

                    //show models with free_chat and more then 30 min
                    if ((diff >= (30 * 60)) && (last_status === 'free-chat')) {
                        sizeFree++;
                        diff = Math.floor(diff / 60) + " m";
                        let tr = $('<tr/>', {
                            "class": "cursor-pointer", on: {
                                click: function () {
                                    //console.log( modelName);
                                    if ((role != 5) && (role != 6)) window.open("admin/model/" + model_id);
                                }
                            },
                        });
                        //let td_id = $('<td/>',{text:model_id, "class":"d-none"});
                        let td1 = $('<td/>', {text: studio});
                        let td2 = $('<td/>', {text: modelName});
                        let td3 = $('<td/>', {text: status_start});
                        let td4 = $('<td/>', {text: status_end});
                        let td5 = $('<td/>', {text: diff});

                        tr.append(td2, td3, td4, td5, td1);
                        myTableFree.append(tr);
                    }

                    //display models in Show Online Table
                    sizeOnline++;
                    diff_all = Math.floor(diff_all / 60) + " m";
                    let trf = $('<tr/>', {
                        "class": "cursor-pointer", on: {
                            click: function () {
                                //console.log( modelName);
                                if ((role != 5) && (role != 6)) window.open("admin/model/" + model_id);
                            }
                        },
                    });
                    //let td1f = $('<td/>',{text:sizeOnline});
                    let td1f = $('<td/>', {text: studio});
                    let td2f = $('<td/>', {text: modelName});
                    let td3f = $('<td/>', {text: last_status});
                    let td4f = $('<td/>', {text: diff_all});

                    trf.append(td2f, td3f, td4f, td1f);
                    myTableOnline.append(trf);


                }


            }



        });

        $("#modelsInFreeCount").text(sizeFree);
        $("#countOnline").text(sizeOnline);


    }

    function convertTimestampToDate(unix_timestamp){

        //let unix_timestamp = 1549312452
        // Create a new JavaScript Date object based on the timestamp
        // multiplied by 1000 so that the argument is in milliseconds, not seconds.
        let date = new Date(unix_timestamp * 1000);
        // Hours part from the timestamp
        let hours = date.getHours();
        // Minutes part from the timestamp
        let minutes = "0" + date.getMinutes();
        // Seconds part from the timestamp
        let seconds = "0" + date.getSeconds();

        // Will display time in 10:30:23 format
        let formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

        return formattedTime;

    }

    function ajaxCall() {

        //console.log("Ajax CAll!! WITH STUDIO!!");
        //console.log(studio);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin',
            data:{studio:studio},

            success: function (data) {

                if (data.success) {

                    let mOnlineAll = data.modelsOnline;
                    //console.log(data);
                    modelsFromthisStudio = mOnlineAll.models_from_this_studio;
                    let modelsOnlineFromStudio = mOnlineAll.models_online_from_this_studio;

                    showFreeModels(modelsOnlineFromStudio);

                    $("#Total").text(data.mStatsCount.Total);
                    $("#Approved").text(data.mStatsCount.Approved);
                    $("#Inactive").text(data.mStatsCount.Inactive);
                    $("#Pending").text(data.mStatsCount.Pending);
                    $("#Rejected").text(data.mStatsCount.Rejected);
                    $("#Suspended").text(data.mStatsCount.Suspended);

                    $(".chartStudioName").text(data.studioName);

                    ySales = data.lastYear;

                    periodSales = data.periodSales;

                    generateChartPeriods();

                    yearSales = ySales.yearSales;
                    yearLabels = ySales.labels;
                    $("#yearUpdatedAt").text(ySales.updated);
                    maxYearSales = ySales.max;

                    daysSales = data.lastDays;
                    if (daysSales) generateChartTvsY(daysSales);
                    else {
                        salesDays = [0, 0];
                        maxDaySales = 0;
                    }


                    md.initDashboardPageCharts();

                    tableOpenPeriods.ajax.reload();
                    tablePaymentRequest.ajax.reload();
                    tablePaymentMade.ajax.reload();
                    tableApprovedModels.ajax.reload();
                    tablePendingModels.ajax.reload();
                    tableReservationsApproval.ajax.reload();

                }


            }

        });

    }

    function generateChartPeriods(){

        //console.log(periodSales);

        if (periodSales) {
            myPeriodSales = periodSales.periodSales;
            maxSalesPeriod = periodSales.max;
            periodSalesUpdated = periodSales.updated;
            $("#periodUpdated").text(periodSalesUpdated);

            periodLabels = periodSales.labels;

            let percent = calcPercent(myPeriodSales[0], myPeriodSales[1]);
            let arrow = $("#incPeriod_arrow");
            let proc = $("#incPeriod_perc");
            let text = $("#incPeriod_text");
            let color = $("#incPeriod_color");

            if (percent < 0) {
                arrow.removeClass().addClass("fa fa-long-arrow-down");
                text.text("decrease");
                color.removeClass().addClass("text-danger");
                percent *= (-1);
            }else {
                arrow.removeClass().addClass("fa fa-long-arrow-up");
                text.text("increase");
                color.removeClass().addClass("text-success");
            }

            proc.text(percent + "%");

        }
        else {
            myPeriodSales = [0,0];
            maxSalesPeriod = 0;
            periodSalesUpdated = '--';
            $("#periodUpdated").text(periodSalesUpdated);

            periodLabels = ['--', '--'];
        }



    }

    function generateChartTvsY(data_sales){

        todaySales = data_sales.today_sales;
        ydaySales = data_sales.yday_sales;

        let percent = calcPercent(ydaySales, todaySales);
        let updateAt = $("#updated_at");
        let arrow = $("#incToday_arrow");
        let proc = $("#incToday_perc");
        let text = $("#incToday_text");
        let color = $("#incToday_color");

        if (percent < 0) {
            arrow.removeClass().addClass("fa fa-long-arrow-down");
            text.text("decrease");
            color.removeClass().addClass("text-danger");
            percent *= (-1);
        }else {
            arrow.removeClass().addClass("fa fa-long-arrow-up");
            text.text("increase");
            color.removeClass().addClass("text-success");
        }

        proc.text(percent + "%");

        updateAt.text(data_sales.time_update);

        salesDays = [ydaySales, todaySales];

        maxDaySales = Math.max((ydaySales), (todaySales));

    }

    // Update the stats every 5 minutes
    let x = setInterval(function() {

        //console.log("CAll!");
        ajaxCallStats();

    }, 300000);

    function ajaxCallStats() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/stats',
            data:{studio:studio},

            success: function (data) {

                if (data.success) {
                    //console.log(data);

                    daysSales = data.lastDays;
                    if (daysSales) generateChartTvsY(daysSales);
                    else {
                        salesDays = [0, 0];
                        maxDaySales = 0;
                    }

                    md.initDashboardPageCharts();

                }


            }

        });

    }

    function calcPercent(first, second){

        let diff = 0;
        if (first > second) diff = first - second;
        else diff = second - first;

        if (!first) first = 1;

        let increase =  diff * 100 / first;

        let inc = Math.round(increase);
        if (first > second) inc = (-1) * increase;
        inc = Math.round(inc);

        return inc;

    }

    function openModels(status){

        let path = "/models";

        switch(status) {
            case "Approved":
                path = "/admin/approvedmodels";
                break;
            case "Pending":
                path = "/admin/pendingmodels";
                break;
            case "Suspended":
                path = "/admin/suspendedmodels";
                break;
            case "Rejected":
                path = "/admin/rejectedmodels";
                break;
            case "Inactive":
                path = "/admin/inactivemodels";
                break;
            default:
                path = "/admin/models";
        }

        //console.log(role);
        window.open(path);
        //if (role != '5') window.open(path);
        //window.location.href = path;
        //alert(path);
    }


</script>
@endpush