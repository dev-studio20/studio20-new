@extends('layouts.admin_app', ['activePage' => 'composemessage', 'titlePage' => __('Insert Sale')])

@push('head')

{{--<link href="{{ asset('/css/all.css') }}" rel="stylesheet" />--}}

@endpush

@section('content')

    <style>

        input[type=file]{
            display: inline;
        }
        #image_preview{
            border: 1px solid black;
            padding: 10px;
        }
        #image_preview img{
            width: 200px;
            padding: 5px;
        }
        .dropdown-menu{
            transform: scale(1)!important;
            opacity: 1!important;
        }
    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">


                <div class="card">
                    <div class="card-header card-header-warning">
                        <h4 class="card-title">Compose Message</h4>
                    </div>
                    <div class="card-body table-responsive">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">

                                <div class="box">

                                    <div class="box-body">
                                        <br>
                                        <form method="post" action="" enctype="multipart/form-data" id="subBtn">
                                            @csrf

                                            <div class="form-group row">
                                                <div class="col-sm-7">
                                                    {{ Form::select('inputGroup', ["1" => 'Model', '2' => 'Studio', '3' => 'All'], null, [ 'class' => 'form-control', 'required','name' => 'inputGroup', 'id' => 'inputGroup' ]) }}
                                                </div>
                                                {{--<label for="inputGroup" class="col-sm-2 col-form-label">Model/Studio/All</label>--}}
                                                {{--<div class="col-sm-7">--}}
                                                {{--<select name="inputGroup" id='inputGroup'>--}}
                                                {{--<option value="1">Model</option>--}}
                                                {{--<option value="2">Studio</option>--}}
                                                {{--<option value="3">All</option>--}}
                                                {{--</select>--}}
                                                {{--</div>--}}
                                            </div>

                                            <div class="form-group row">
                                                {{--<label for="inputTo" class="col-sm-2 col-form-label">To</label>--}}
                                                <div class="col-sm-7">
                                                    <input id='inputTo' type='text' name='to' autocomplete="off" class="typeahead form-control" style="margin:0px auto;" placeholder="To" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                {{--<label for="inputsubject" class="col-sm-2 col-form-label">Subject</label>--}}
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="inputsubject" name='subject' placeholder="Subject" required>
                                                </div>
                                            </div>
                                            <div id="count"></div>
                                            <div class="form-group row">

                                                {{--<label for="inputsubject" class="col-sm-2 col-form-label">Message</label>--}}
                                                <div class="col-sm-7">
                                                    <textarea class="form-control" id="message" name='message' style="overflow: hidden;" rows="5" placeholder="Your message comes here" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">

                                                <div class="offset-sm-3 col-sm-7">
                                                    <input id="submitMessage" type="submit" name="send" class="btn btn-success" value="Send Message!">
                                                    <label type="button" class="btn btn-primary ">Upload Pictures
                                                        <input type="file" style="display: none;" id="uploadFile" name="uploadFile[]" accept="image/*,application/pdf" capture="camera" multiple/>
                                                    </label>
                                                </div>
                                            </div>

                                            <div ><br/><div id="image_preview"></div></div>

                                        </form>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-12 col-lg-6">


                                <div class="box">

                                    <div class="box-body" style="border: 1px solid lightgray">

                                        <table style="height:300px; overflow-y:scroll; display:block;">
                                            <thead>
                                            <tr>
                                                <th>#</th><th>id</th><th>model</th><th>email</th><th>status</th>
                                            </tr>
                                            </thead>
                                            <tbody id="containerSent"></tbody>
                                        </table>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>






            </div>

        </div>
    </div>




@endsection

@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<script>

    let sbtn = $("#submitMessage");
    let myMmodels = [];
    let index = 0;
    let subject;
    let message;

    $(document).ready(function() {

        $("#message").keyup(function(){
            let count = (1000 - $(this).val().length);
            $("#count").text("Characters left: " + count);
            sbtn.attr("disabled", (count < 0 ));

        });

        $("#uploadFile").change(function(){
            $('#image_preview').html("");
            let total_file=document.getElementById("uploadFile").files.length;


            for(let i=0;i<total_file;i++)
            {
                let image = URL.createObjectURL(event.target.files[i]);
                console.log(event.target.files[i]);
                if (isFileImage(event.target.files[i])){
                    $('#image_preview').append("<img src='"+image+"'>");
                } else $('#image_preview').append("<img src='/img/models/doc.png'>");

            }

        });


        $( "#subBtn" ).submit(function( event ) {

            index = 0;
            myMmodels = [];

            subject = $("#inputsubject").val();
            message = $("#message").val();

            let container = $("#containerSent");
            container.empty();

            //detect all selected
            let togroup = $('#inputGroup').val();
            let to = $('#inputTo').val();
            if (togroup !== '1') {
                event.preventDefault();

                //console.log("group: " + togroup);
                //console.log("studio_id: " + to);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "/admin/onebyone",
                    method: 'post',
                    data: {sendmailall: to},
                    success: function (models) {

                        console.log("response");
                        //console.log(models);

                        models.forEach(function (model, index){
                            //console.log(model);
                            let tr = $("<tr/>",{});

                            let td_index = $("<td/>",{text:index+1});
                            let td_id = $("<td/>",{text:model.id});
                            let td_name = $("<td/>",{text:model.modelname});
                            let td_email = $("<td/>",{text:model.email});
                            let td_status = $("<td/>",{text:'--', "id":model.id});

                            tr.append(td_index, td_id, td_name, td_email,td_status);
                            container.append(tr);

                            myMmodels.push(model);

                        });

                        sendEmail();

                    }
                });

            }

        });


        function sendEmail(){

            let mymodel = myMmodels[index];

            console.log(mymodel);


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "/admin/sendtoone",
                method: 'post',
                data: {id: mymodel.id, modelname: mymodel.modelname, email:mymodel.email, subject:subject, message:message},
                success: function (data) {

                    console.log("response");
                    //console.log(data);
                    $("#"+data.id).text(data.status);

                    index++;
                    if (index < myMmodels.length){

                        sendEmail();

                    }

                }


            });


        }

        /*var groupselect = $('#inputGroup').val();
         console.log(groupselect);*/

        $('#inputGroup').change(function () {
            if ($('#inputGroup').val() === '3') {
                $('#inputTo ').val('All');
            } else {
                $('#inputTo ').val('');
            }
        });

        $('input.typeahead').typeahead( {
            items: 20,

            source:  function (query, process) {
                let groupselect = $('#inputGroup').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                console.log(groupselect);
                if (groupselect === '2') {
                    return $.post('/admin/autotype', { queryStudio: query }, function (data) {

                        console.log(data);

                        data = $.parseJSON(data);

                        return process(data);

                    });
                } else if (groupselect === '1') {
                    return $.post('/admin/autotype', { query: query }, function (data) {

                        console.log(data);

                        data = $.parseJSON(data);

                        return process(data);

                    });
                }



            }

        });


        $(".alert").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });

    });

    function isFileImage(file) {
        const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

        return file && acceptedImageTypes.includes(file['type'])
    }



</script>
@endpush