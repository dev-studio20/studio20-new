@extends('layouts.admin_app', ['activePage' => 'inbox', 'titlePage' => __('Insert Sale')])

@push('head')

@endpush

@section('content')

    <style>
        div.toggler				{ border:1px solid #ccc; background: 10px 12px #eee no-repeat; cursor:pointer; padding:10px 32px; }
        div.toggler .subject	{ font-weight:bold; }
        div.read					{ color:#666; }
        div.toggler .from, div.toggler .date { font-style:italic; font-size:11px; }
        div.body					{ padding:10px 20px; }

        input[type=file]{
            display: inline;
        }
        #image_preview{
            border: 1px solid black;
            padding: 10px;
        }
        #image_preview img{
            width: 200px;
            padding: 5px;
        }
    </style>

    <div class="content" style="background: #fff;">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-info">
                        <h3 class="box-title"><a style="margin-right:auto; margin-left:0;" type="button" class="btn btn-danger" href="{{url('/admin/inbox')}}">Back to Inbox</a></h3>
                    </div>
                    <div class="card-body table-responsive">

                        <div class="email-app mb-4">

                            <main class="message">

                                <div class="details">

                                    @foreach ($msg as $message)

                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="header">
                                                    <img class="avatar" @if ($message["from_id"] == 'admin') src="/img/models/adminavatar.png" @else src="/img/models/{{$sex}}.png" @endif style="width:50px">
                                                    <div class="from">
                                                        <div><b>@if ($message["from_id"] == 'admin'){{ 'Admin' }} @else {{ $model->modelname }} @endif</b></div><br>
                                                    </div>
                                                    <div class="date"><i class="fa fa-clock-o"></i>{{ $message["created_at"] }}</div>
                                                </div>
                                            </div>

                                            <div class="col-lg-9">
                                                <div class="content">
                                                    @if ($loop->iteration == 1) <b> {{$subj->subject}} </b>@endif
                                                    <div><i>{{  $message["message"] }}</i></div>
                                                </div>

                                            </div>

                                        </div>


                                        <hr>
                                        <div class="row">

                                            @foreach($message["attachements"] as $att)

                                                <a href="#my_modal" data-toggle="modal" data-image-id="{{$att["src"]}}" data-image-path="{{$att["path"]}}">
                                                    <div class="col">
                                                        <img src="{{$att["src"]}}" alt="" style="max-height:50px;">
                                                    </div>
                                                </a>

                                            @endforeach

                                        </div>
                                        <hr>
                                    @endforeach

                                </div>
                                <br />
                                <form method="post" action="#message"  enctype="multipart/form-data">
                                    @csrf
                                    <div id="count"></div>
                                    <div class="form-group">
                                        <input  type="hidden" name="modelid" value="{{$model->id}}">
                                        <input  type="hidden" name="subject" value="reply">
                                        <textarea class="form-control" id="message" name='message' rows="5" placeholder="Click here to reply">{{old('message')}}</textarea>


                                    </div>
                                    @error('message')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                    <div class="form-group">

                                        <label type="button" class="btn btn-primary col-lg-3 col-sm-12">Upload Files
                                            <input type="file" style="display: none;" id="uploadFile" name="uploadFile[]" accept="image/*,application/pdf" capture="camera" multiple/>
                                        </label>

                                        <input id="submitMessage" type="submit" name="reply" class="btn btn-success col-lg-3 col-sm-12" value="Send Message!">
                                        <input id="submitSolve" type="submit" name="solve" class="btn btn-success col-lg-3 col-sm-12" value="Solve!">
                                    </div>
                                </form>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="container">

                                            <br/>
                                            <div id="image_preview"></div>
                                        </div>
                                    </div>
                                </div>

                            </main>
                        </div>

                    </div>
                </div>

            </div>


        </div>
    </div>

    <div class="modal" id="my_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <img class="img-responsive" id="myImage" src=""  style="width: 100%;">
                </div>
                <div class="modal-footer">
                    <a id="myLink" href="#" download>
                        <button type="button" class="btn btn-primary">DOWNLOAD</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script>

    let sbtn = $("#submitMessage");

    $(document).ready(function() {

        $("#uploadFile").change(function(){
            $('#image_preview').html("");
            let total_file=document.getElementById("uploadFile").files.length;


            for(let i=0;i<total_file;i++)
            {
                let image = URL.createObjectURL(event.target.files[i]);
                console.log(event.target.files[i]);
                if (isFileImage(event.target.files[i])){
                    $('#image_preview').append("<img src='"+image+"'>");
                } else $('#image_preview').append("<img src='/img/models/doc.png'>");

            }

        });

        $("#message").keyup(function(){
            let count = (1000 - $(this).val().length);
            $("#count").text("Characters left: " + count);
            sbtn.attr("disabled", (count < 0 ));

        });

        $('#my_modal').on('show.bs.modal', function(e) {

            let prev_src = $(e.relatedTarget).data('image-id');
            let download_src = $(e.relatedTarget).data('image-path');
            $(e.currentTarget).find('#myImage').attr("src", prev_src);
            $(e.currentTarget).find('#myLink').attr("href", download_src);
        });


        $("#alert").fadeTo(5000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });

    } );

    function isFileImage(file) {
        const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

        return file && acceptedImageTypes.includes(file['type'])
    }


</script>
@endpush