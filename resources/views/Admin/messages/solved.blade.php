@extends('layouts.admin_app', ['activePage' => 'solved', 'titlePage' => __('Insert Sale')])

@push('head')

@endpush

@section('content')

    <div class="content" style="background: #fff;">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-info">
                        <a style="float:right;" type="button" class="btn btn-success" href="{{route('admin.composemessage')}}">Compose Message</a>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="example1" class="table table-hover data-table">
                            <thead class="text-info">

                            <tr>
                                <th>Id</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Subject</th>
                                <th style="width:60%">Message</th>
                                <th>Date <i class="fa fa-clock-o"></i></th>
                            </tr>

                            </thead>

                            <tbody style='cursor:pointer;'>
                            @foreach ($messages as $msg)

                                <tr class="info" onclick="window.location='message/{{ $msg->id }}'">

                                    <td>{{ $msg->id }}</td>
                                    <td>{{ array_key_exists($msg->from_id, $allModels) ? $allModels[$msg->from_id] : $msg->from_id }}</td>
                                    <td>{{ array_key_exists($msg->to_id, $allModels) ? $allModels[$msg->to_id] : $msg->to_id }}</td>
                                    <td>{{ $msg->subject }}</td>
                                    <td>{{ mb_substr($msg->message, 0, 100).".." }}</td>
                                    <td>{{ $msg->updated_at }}</td>

                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


        </div>
    </div>

@endsection

@push('js')
<script>

    $(document).ready(function() {

        let table = $('#example1').DataTable( {
            "bStateSave": true,
            "fnStateSave": function (oSettings, oData) {
                localStorage.setItem( 'DataTables_'+window.location.pathname, JSON.stringify(oData) );
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname) );
            }
        } );


        $("#alert").fadeTo(5000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });

    } );


</script>
@endpush