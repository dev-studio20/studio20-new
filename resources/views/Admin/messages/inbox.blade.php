@extends('layouts.admin_app', ['activePage' => 'inbox', 'titlePage' => __('Insert Sale')])

@push('head')

@endpush

@section('content')
    <div class="content" style="background: #fff;">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-info">
                        <a style="float:right;" type="button" class="btn btn-success" href="{{route('admin.composemessage')}}">Compose Message</a>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="example1" class="table table-hover data-table">
                            <thead class="text-info">

                                <tr>
                                    <th>#</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Subject</th>
                                    {{--<th>..</th>--}}
                                    <th>Date <i class="fa fa-clock-o"></i></th>
                                </tr>

                            </thead>

                            <tbody style='cursor:pointer;'>
                            @foreach ($msg as $message)


                                <tr class="info" onclick="window.location='message/{{ $message["id"] }}'" style="font-weight: {{msgReadAdmin($message['id'])}}">

                                    {{--<td>{{$message["id"]}}</td>--}}
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{ array_key_exists($message["from_id"], $allModels) ? $allModels[$message["from_id"]] : $message["from_id"] }} @if ($message["from_id"] != 'admin') {{' ('.getStudioName($message["from_id"]).')'}}@endif</td>
                                    <td>{{ array_key_exists($message["to_id"], $allModels) ? $allModels[$message["to_id"]] : $message["to_id"] }} @if ($message["to_id"] != 'admin') {{' ('.getStudioName($message["to_id"]).')'}}@endif</td>
                                    <td>{{$message["subject"]}}</td>
                                    {{--<td>{{msgReadAdmin($message["id"])}}</td>--}}
                                    <td>{{$message["updated_at"]}}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>


        </div>
    </div>


@endsection

@push('js')
<script>

    $(document).ready(function() {

        let table = $('#example1').DataTable( {
            "bStateSave": true,
            "fnStateSave": function (oSettings, oData) {
                localStorage.setItem( 'DataTables_'+window.location.pathname, JSON.stringify(oData) );
            },
            "fnStateLoad": function (oSettings) {
                return JSON.parse( localStorage.getItem('DataTables_'+window.location.pathname) );
            }
        } );

        //getSms();
        //getEmails();

        $("#alert").fadeTo(5000, 500).slideUp(500, function(){
            $("#alert").slideUp(500);
        });

        function getSms () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "ajaxAdminData",
                method: 'post',
                data: {getsms: 'dd'},
                success: function (html) {
                    let obj = JSON.parse(html);
                    //[{"id":"3389039","created":"2018-11-05 13:36:20","from":"+40725585451","to":"3764","data":"Hello","charset":"UTF-8"}]
                    //console.log(obj);
                    for (let i = 0; i < obj.length; i++) {

                        let rowNode = table.row.add(['Sms', obj[i].from, obj[i].data, obj[i].created + ' <i class="fa fa-clock-o"></i>'])
                            .draw()
                            .node();

                        $(rowNode).on('click', function () {
                            //alert('phone');
                        });

                        $(rowNode).css('color', 'red').animate({color: 'black'});

                    }
                }
            });
        }


    } );



</script>
@endpush