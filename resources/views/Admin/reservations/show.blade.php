@extends('layouts.admin_app', ['activePage' => 'reservations', 'titlePage' => __('Reservations')])

@push('head')

<link href="{{ asset('material/js/jquery-ui.multidatespicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('material/js/jquery-ui.css') }}" rel="stylesheet"/>

@endpush

@section('content')
    <style>


    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Reservations Waiting Approval
                                        </a>
                                    </li>
                                </ul>



                            </div>
                            {{$model->modelname}} - {{$model->studios->name}}
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">


                            <div class="tab-pane active show">

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <h3 style="text-align: center;">Hour: {{$hour}}</h3>
                                            </div>
                                            <div class="col-lg-6">
                                                <div id="mdp-thismonth"></div>

                                            </div>
                                            <hr>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            @if (!$status) <button class="btn btn-success col-lg-4" onclick="approve()">Approve</button> @endif
                                            {{--<button class="btn btn-warning col-lg-4" onclick="edit()">Edit</button>--}}
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>




@endsection

@push('js')



<script>


    let studio = $("#myStudio").children("option:selected").val();

    let minDate = '{{$minDate}}';
    let maxDate = '{{$maxDate}}';

    let daysString = '{{ $days }}';
    let month = '{{ $month }}';
    let year = '{{ $year }}';
    let days = daysString.split(",");

    let status = '{{$status}}';

    let id = '{{$id}}';

    let dateArray = [];

    days.forEach(function(entry) {
        let str = month + '/' + entry + '/' + year;
        console.log(str);
        dateArray.push( new Date(str));
    });


    $(document).ready(function () {




        $('#mdp-thismonth').multiDatesPicker({
            dateFormat: 'mm/dd/yy',
            minDate: minDate,
            maxDate: maxDate,
            altField: '#altField',
            //disabled: status,
            disabled: false,
            //firstDay: '03/01/2020',
            addDates: dateArray,
//            onSelect: function(dateStr) {
//                let sdate = new Date(dateStr);
//                let index = (getWeekNumber(sdate));
//                let vdate = sdate.getDay();
//                let narr = (selectedDays[index]) ? selectedDays[index] : [];
//                checkCondition($('#mdp-thismonth'), "this", dateStr, narr, vdate, index, $("#submitThis"), $("#thisError"));
//            }
        });

    });


    function approve(){
        //ajax call, notifications , etc

        let dates = $('#mdp-thismonth').multiDatesPicker('getDates');
        console.log(dates);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/reservations/'+id,
            data:{dates:dates},

            success: function (data) {

                if (data.success) {
                    console.log(data);
                    location.reload();
                }

            }

        });

    }

    function edit(){
        //check date if older and not permit editing in this case

    }



</script>
@endpush