@extends('layouts.admin_app', ['activePage' => 'reservations', 'titlePage' => __('Reservations')])

@push('head')

@endpush

@section('content')
    <style>
        .warning {
            color: #ff9800;
        }
    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Reservations Waiting Approval
                                        </a>
                                    </li>
                                </ul>



                            </div>
                        </div>
                    </div>

                    <div class="card-body" style="overflow-x: auto;">
                        <div class="tab-content">


                            <div class="tab-pane active show">

                                <div class="row">

                                    <div class="col-lg-12">

                                        <table id="reservationsTable" class="table table-hover data-table">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>#</th>
                                                <th>Modelname</th>
                                                <th>Studio</th>
                                                <th>Month</th>
                                                <th>Year</th>
                                                <th>Days</th>
                                                <th>Hour</th>
                                                {{--<th>Status</th>--}}
                                                <th>Date</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            </tbody>

                                        </table>

                                        <br>
                                        <hr>

                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>




@endsection

@push('js')
<script>

    let table;
    let tableBody = $('tbody');
    let role = '{{$role}}';
    let studio = $("#myStudio").children("option:selected").val();
    let months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    $(document).ready(function () {

        $("#reservationsTable tbody").on('click', 'tr', function () {
            let data = table.row( this ).data();
            if ((role != 5) && (role != 6)) {
                window.open("reservations/"+data.id);
            }
            //window.open("reservations/"+data.id);
        } );

    });

    $(function () {

        table = $('#reservationsTable').DataTable({
            bFilter: false,
            paging: false,
            scrollCollapse: true,
            bInfo : false,
            iDisplayLength: -1,
            bStateSave: true,
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
            serverSide: false,
            ajax: {
                'url':'/admin/reservations',
                "type": "POST",
                "data": function (d) {
                    d.studio = studio;
                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            columns: [
                {data: 'id', name: 'id', searchable: true, orderable: true},
                {data: 'model.modelname', name: 'modelname', searchable: true},
                {data: 'studios.name', name: 'studio', searchable: true},
                {data: 'month', name: 'month',searchable: true, orderable: true},
                {data: 'year', name: 'year', searchable: true, orderable: true},
                {data: 'days', name: 'days', searchable: false, orderable: false},
                {data: 'hour', name: 'hour', searchable: false, orderable: false},
//                {data: 'status', name: 'status', searchable: false, orderable: false},
                {data: 'updated_at', name: 'date', searchable: true, orderable: true},
            ],
            columnDefs: [ {
                "targets": 3,
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = months[data-1];
                    }
                    return data;


                }
            },
//                {
//                "targets": 7,
//                render: function (data, type, row, meta)
//                {
//                    if (type === 'display')
//                    {
//                        return (data) ? 'Approved' : 'Pending';
//                    }
//
//
//
//                }
//            }
            ]

        });

    });


    function decimal(dec){

        dec = parseInt(dec);

        return (dec < 10) ? '0' + dec : dec;
    }



</script>
@endpush