@extends('layouts.admin_app', ['activePage' => 'log', 'titlePage' => __('Log')])

@push('head')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-info">Log</div>
                    <div class="card-body table-responsive">
                        <table class="table table-hover data-table" id="table">
                            <thead class="text-info">

                            <tr>
                                <th class="text-center">Username</th>
                                <th class="text-center">IP</th>
                                <th class="text-center">Rank</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Data</th>
                                <th class="text-center">Hour</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($result as $item)
                                <tr class="{{$item->color}}">
                                    <td class="text-right">{{$item->username}}</td>
                                    <td class="text-right">{{$item->ip}}</td>
                                    <td class="text-right">{{$item->rank}}</td>
                                    <td class="text-right">{{$item->action}}</td>
                                    <td class="text-right">{{$item->Date}}</td>
                                    <td class="text-right">{{$item->Hour}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>




@endsection

@push('js')
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    });



</script>
@endpush