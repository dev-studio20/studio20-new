@extends('layouts.admin_app', ['activePage' => 'backgroundservice', 'titlePage' => __('Insert Sale')])

@push('head')

@endpush

@section('content')

    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-primary">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">

                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#1a" data-toggle="tab">
                                                <i class="material-icons">account_box</i> Background Jobs
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#2a" data-toggle="tab">
                                                <i class="material-icons">description</i> Background Jobs2
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#3a" data-toggle="tab">
                                                <i class="material-icons">description</i> Get Sales Period
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div>$totaltime = $restotaltime['vip_show']['value'] + $restotaltime['pre_vip_show']['value'] + $restotaltime['private']['value'] + $restotaltime['free']['value'];</div>
                                <div class="tab-pane active" id="1a">
                                    {{--@include('Admin.models.partials.model_profile')--}}
                                    <button class="btn btn-warning" onclick="getSales()">Get Sales For This Period</button>

                                    <div class="card-body table-responsive">
                                        <table class="table table-hover">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>Modelname</th>
                                                <th>day</th>
                                                <th>earnings</th>
                                                <th>worktime</th>
                                            </tr>
                                            </thead>
                                            <tbody id="jobTable">

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="tab-pane" id="2a">
                                    <div>Cron Job Time <b>{{env("CRON_JOB_TIME")}}</b></div>
                                    <div>Next: <b><span id="countDown">0</span></b></div>
                                    <button class="btn btn-warning" onclick="getSalesAllModelsDay()">Get Sales For All Models Day</button>

                                    <div class="card-body table-responsive">
                                        <table class="table table-hover">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>Modelname</th>
                                                <th>day</th>
                                                <th>earnings</th>
                                                <th>worktime</th>
                                            </tr>
                                            </thead>
                                            <tbody id="jobTable2">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="3a">
                                    <div>Selected Studio: <span id="sStudio"></span></div>
                                    <div>
                                        {!! Form::open([]) !!}
                                        {!! Form::select('periods', getAllPeridos(), $p, [ 'class' => 'form-control col-lg-2', 'id' => 'periods' ]) !!}
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-lg-2">
                                        @foreach($years as $year)

                                            <button id="{{$year}}" class="years btn @if ($year == $y) btn-success @else btn-warning @endif" onclick="changeYear({{$year}})">{{$year}}</button>

                                        @endforeach
                                    </div>
                                    <button class="btn btn-warning" onclick="getSalesPeriod()">Get Sales Period</button>

                                    <div class="card-body table-responsive">

                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')

<script>

    let cronJobTime = '{{env("CRON_JOB_TIME")}}';
    let sec = cronJobTime.split(":");
    let cronTime = ( (sec[0]*3600) + (sec[1]*60));
    let year = parseInt('{{$y}}');
    let studioSelected = $("#myStudio").children("option:selected").val();
    let textStudioSelected = $("#myStudio").children("option:selected").text();
    $("#sStudio").text(textStudioSelected);

    $("#studioSelect").change(function () {
        console.log('studio change detected..');
        studioSelected = $("#myStudio").children("option:selected").val();
        textStudioSelected = $("#myStudio").children("option:selected").text();
        $("#sStudio").text(textStudioSelected);
    });



    // Update the count down every 1 second
    let x = setInterval(function() {
        let d = new Date();
        let n = d.getHours();
        let m = d.getMinutes();
        let s = d.getSeconds();
        let cTime = ( (n*3600) + (m*60) + s);

        let distance = (24*3600) - (cTime - cronTime);

        let date1 = new Date(null);
        date1.setSeconds(distance);
        let result = date1.toISOString().substr(11, 8);

        $("#countDown").text(result);

    }, 1000);

    function getSalesPeriod(){
        console.log("Studio: ");
        console.log(studioSelected);
        console.log("Period: ");
        let per = $("#periods").children("option:selected").val();
        console.log(per);
        console.log("Year: ");
        console.log(year);

        ajaxCronPeriod(studioSelected, per, year);
    }

    function ajaxCronPeriod(s, p, y){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/getsalesallperiod',
            data:{studio:s, period:p, year:y},

            success: function (data) {
                console.log(data);
            }

        });

    }

    function changeYear(y){
        year = y;
        $('.years').removeClass('btn-success').addClass('btn-warning');
        $('#'+y).removeClass('btn-warning').addClass('btn-success');
    }

    $(document).ready(function() {


        // Bind a function to a Event (the full Laravel class)
        channel.bind('sales-model-event', function(data) {

            console.log("data from event-model: ");
            let payload = data.message.original;
            let table = $("#jobTable2");

            console.log(payload);
            if(payload.success){
                let tr = $("<tr/>");
                let td_modelname = $("<td/>",{text:payload.screenName});
                let td_day = $("<td/>",{text:payload.day});
                let td_earnings = $("<td/>",{text:payload.earnings});
                let td_workTime = $("<td/>",{text:payload.workTime});
                tr.append(td_modelname,td_day,td_earnings,td_workTime);
                table.append(tr);
            }
            //alert(data);

        });

        channel.bind('sales-event', function(data) {

            console.log("data from event: ");
            let payload = data.message.original;
            let table = $("#jobTable");

            console.log(payload);
            if(payload.success){
                let tr = $("<tr/>");
                let td_modelname = $("<td/>",{text:payload.screenName});
                let td_day = $("<td/>",{text:payload.day});
                let td_earnings = $("<td/>",{text:payload.earnings});
                let td_workTime = $("<td/>",{text:payload.workTime});
                tr.append(td_modelname,td_day,td_earnings,td_workTime);
                table.append(tr);
            }
            //alert(data);

        });

    });

    function getSalesAllModelsDay(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/getsalesallmodels',
            data:{},

            success: function (data) {
                //console.log('Done');
            }

        });

    }

    function getSales(){
        //alert("Sales");


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/getsales',
            data:{},

            success: function (data) {
                //console.log('Done');
            }

        });

    }


</script>
@endpush