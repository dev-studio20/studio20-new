<div class="col-lg-4 col-md-12">

    <div class="card">
        <div class="card-header card-header-warning">
            <h4 class="card-title">Models In Free Chat > 30 min</h4>
            <p class="card-category"><span id="modelsInFreeCount">0</span> models</p>
            <div style="position:absolute;right: 10px;bottom: 4px;"><i class="material-icons">timer</i></div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover" style="display: block;  height: 150px;  overflow-y: scroll;">
                <thead class="text-warning">
                <tr>
                    <th>Model</th>
                    <th>Start Time</th>
                    <th>Last Check</th>
                    <th>Total</th>
                    <th>Studio</th>
                </tr>
                </thead>
                <tbody id="freeChatModels">

                </tbody>
            </table>
        </div>
    </div>

</div>