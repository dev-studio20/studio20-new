<div class="col-md-4">
    <div class="card card-chart">
        <div class="card-header card-header-success">
            <div class="ct-chart" id="dailySalesChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">Daily Sales <span class="chartStudioName"></span></h4>
            <p class="card-category">
                                <span id="incToday_color" class="text-success">
                                    <i id="incToday_arrow" class="fa fa-long-arrow-up"></i>
                                    <span id="incToday_perc"> 0%</span>
                                </span>
                <span id="incToday_text">increase</span> in today sales.
            </p>
        </div>
        <div class="card-footer">
            <div class="stats">
                <i class="material-icons">access_time</i><span> updated at <span id="updated_at"></span></span>
            </div>
        </div>
    </div>
</div>