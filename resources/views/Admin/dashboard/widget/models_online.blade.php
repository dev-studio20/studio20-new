<div class="col-lg-4 col-md-12">

    <div class="card">
        <div class="card-header card-header-success">
            <h4 class="card-title">Who Is Online Now</h4>
            <p class="card-category"><span id="countOnline">0</span> models</p>
            <div style="position:absolute;right: 10px;bottom: 4px;"><i class="material-icons">timer</i></div>
        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover" style="display: block;  height: 150px;  overflow-y: scroll;">
                <thead class="text-success">
                <tr>
                    <th style="width:25%">Model</th>
                    <th style="width:25%">Status</th>
                    <th style="width:25%">Time</th>
                    <th style="width:25%">Studio</th>
                </tr>
                </thead>
                <tbody id="onlineModels">

                </tbody>
            </table>
        </div>
    </div>

</div>