<div class="col-lg-4 col-md-12">

    <div class="card" style="min-height: 273px;">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Payment Request</h4>
            <p class="card-category">First 30</p>
        </div>

        <div class="card-body table-responsive">
            <table id="paymentRequest" class="data-table table ">
                <thead class="text-primary">
                <tr>
                    {{--<th>ID</th>--}}
                    <th>Model</th>
                    <th>Period</th>
                    <th>Studio</th>
                </tr>
                </thead>

            </table>

        </div>

    </div>

</div>