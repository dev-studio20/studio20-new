<div class="col-lg-4 col-md-12">

    <div class="card" style="min-height: 273px;">
        <div class="card-header card-header-warning">
            <h4 class="card-title">Reservations Waiting Approval</h4>
            <p class="card-category"></p>
        </div>

        <div class="card-body table-responsive">
            <table id="reservationsApproval" class="data-table table ">
                <thead class="text-warning">
                <tr>
                    {{--<th>#</th>--}}
                    <th>Model</th>
                    <th>Month</th>
                    <th>Days</th>
                    <th>Studio</th>
                </tr>
                </thead>

            </table>

        </div>

    </div>

</div>