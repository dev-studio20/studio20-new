<div class="col-lg-4 col-md-12">

    <div class="card" style="min-height: 273px;">
        <div class="card-header card-header-success">
            <h4 class="card-title">Payments Made</h4>
            <p class="card-category">Last 30</p>
        </div>

        <div class="card-body table-responsive">
            <table id="paymentMade" class="data-table table ">
                <thead class="text-success">
                <tr>
                    {{--<th>ID</th>--}}
                    <th>Model</th>
                    <th>Period</th>
                    <th>Studio</th>
                </tr>
                </thead>

            </table>

        </div>

    </div>

</div>