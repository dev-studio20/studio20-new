@foreach ($mStatsCount as $k => $value)

    <div class="col-lg-2 col-md-4 col-sm-6" onclick="openModels('{{$k}}')" style="cursor:pointer">
        <div class="card card-stats">
            <div class="card-header card-header-{{getModelsStatsColor($k)}} card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">{{getModelsStatsIcon($k)}}</i>
                </div>
                <p class="card-category">Models <br>{{$k}}</p>
                <h3 class="card-title" id="{{$k}}">{{ $mStatsCount[$k] }}
                </h3>
            </div>
            {{--<div class="card-footer">--}}
                {{--<div class="stats">--}}
                    {{--<i class="material-icons">date_range</i> Last 30 days: 20--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>

@endforeach