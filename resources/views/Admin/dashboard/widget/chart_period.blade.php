<div class="col-md-4">
    <div class="card card-chart">
        <div class="card-header card-header-warning">
            <div class="ct-chart" id="periodChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">Period Sales - <span class="chartStudioName"></span></h4>
            <p class="card-category">
                                <span id="incPeriod_color" class="text-success">
                                    <i id="incPeriod_arrow" class="fa fa-long-arrow-up"></i>
                                    <span id="incPeriod_perc"> 0%</span>
                                </span>
                <span id="incPeriod_text">increase</span> in this period sales.
            </p>
        </div>
        <div class="card-footer">
            <div class="stats">
                <i class="material-icons">access_time</i><span> updated at <span id="periodUpdated"></span></span>
            </div>
        </div>
    </div>
</div>