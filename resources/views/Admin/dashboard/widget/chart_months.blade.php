<div class="col-md-4">
    <div class="card card-chart">
        <div class="card-header card-header-danger">
            <div class="ct-chart" id="monthlySalesChart"></div>
        </div>
        <div class="card-body">
            <h4 class="card-title">Monthly Sales - <span class="chartStudioName"></span></h4>
            <p class="card-category">Last 12 months</p>
        </div>
        <div class="card-footer">
            <div class="stats">
                <i class="material-icons">access_time</i><span> updated at <span id="yearUpdatedAt"></span></span>
            </div>
        </div>
    </div>
</div>