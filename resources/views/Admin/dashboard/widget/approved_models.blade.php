<div class="col-lg-4 col-md-12">

    <div class="card" style="min-height: 26px;">
        <div class="card-header card-header-success">
            <h4 class="card-title">Last Models Approved</h4>
            <p class="card-category">Last 30</p>
        </div>

        <div class="card-body table-responsive">
            <table id="approvedModels" class="data-table table">
                <thead class="text-success">
                <tr>
                    {{--<th>#</th>--}}
                    <th>Model</th>
                    <th>Date</th>
                    <th>Studio</th>
                </tr>
                </thead>

            </table>

        </div>

    </div>

</div>
