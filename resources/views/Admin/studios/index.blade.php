@extends('layouts.admin_app', ['activePage' => 'studios', 'titlePage' => __('Studios')])

@push('head')

@endpush

@section('content')

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">

                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Edit Studios
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>



                            </div>
                        </div>
                        {{--<a class="btn btn-info" href="javascript:void(0)" id="createNewStudio"> Create New Studio</a>--}}
                    </div>

                    <div class="card-body table-responsive">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="1a">
                                <table id="allPeriods" class="data-table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Phone Notif.</th>
                                        <th>Phone Verif.</th>
                                        <th>Email Notif.</th>
                                        <th>Reservation Req.</th>
                                        <th>Payment Req.</th>
                                        <th>Active</th>
                                        <th>Email</th>
                                        {{--<th width="280px">Action</th>--}}
                                    </tr>
                                    </thead>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" class="form-horizontal">
                        <input type="hidden" name="product_id" id="product_id">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Details</label>
                            <div class="col-sm-12">
                                <textarea id="detail" name="detail" required="" placeholder="Enter Details" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

<script>
    $(document).ready(function() {

    });

    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            lengthMenu: [[50, -1], [50, "All"]],
            ajax: "{{ route('studios.index') }}",
            columnDefs: [{
                "targets": 2,
                "render": function (data, type, row) {
                    let cval = (data) ? "checked=checked" : '';
                    return '<input type="checkbox" id="phone_checkbox' + data + '"  ' + cval+ '>';
                }
            },{
                "targets": 3,
                "render": function (data, type, row) {
                    let cval = (data) ? "checked=checked" : '';
                    return '<input type="checkbox" id="email_checkbox' + data + '"  ' + cval+ '>';
                }
            },{
                "targets": 4,
                "render": function (data, type, row) {
                    let cval = (data) ? "checked=checked" : '';
                    return '<input type="checkbox" id="reservation_checkbox' + data + '"  ' + cval+ '>';
                }
            },{
                "targets": 5,
                "render": function (data, type, row) {
                    let cval = (data) ? "checked=checked" : '';
                    return '<input type="checkbox" id="payment_checkbox' + data + '"  ' + cval+ '>';
                }
            },{
                "targets": 6,
                "render": function (data, type, row) {
                    let cval = (data) ? "checked=checked" : '';
                    return '<input type="checkbox" id="active' + data + '"  ' + cval+ '>';
                }
            },
                {
                    "targets": 7,
                    "render": function (data, type, row) {
                        let cval = (data) ? "checked=checked" : '';
                        return '<input type="checkbox" id="active' + data + '"  ' + cval+ '>';
                    }
                },
            ],
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'phone_notification', name: 'phone_notification', orderable: false, searchable: false},
                {data: 'phone_verification', name: 'phone_verification', orderable: false, searchable: false},
                {data: 'email_notification', name: 'email_notification'},
                {data: 'reservations_required', name: 'reservations_required'},
                {data: 'payment_request', name: 'payment_request'},
                {data: 'active', name: 'active'},
                {data: 'contact_email', name: 'contact_email'},
//                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#createNewStudio').click(function () {
            $('#saveBtn').val("create-product");
            $('#product_id').val('');
            $('#productForm').trigger("reset");
            $('#modelHeading').html("Create New Product");
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editProduct', function () {
            var product_id = $(this).data('id');
            $.get("{{ route('studios.index') }}" +'/' + product_id +'/edit', function (data) {
                $('#modelHeading').html("Edit Studio");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#product_id').val(data.id);
                $('#name').val(data.name);
            })
        });

        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Sending..');

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('studios.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    table.draw();

                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });

        $('body').on('click', '.deleteProduct', function () {

            var product_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "DELETE",
                url: "{{ route('studios.store') }}"+'/'+product_id,
                success: function (data) {
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    });

</script>
@endpush