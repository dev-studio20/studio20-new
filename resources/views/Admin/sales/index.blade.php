@extends('layouts.admin_app', ['activePage' => 'Viewsles', 'titlePage' => __('ViewSales')])

@push('head')

@endpush

@section('content')
    <style>
        .warning {
            color: #ff9800;
        }
    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-primary">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">


                                    <div style="position: relative;float: right;">Key: {{ $key }}</div>
                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#1a" data-toggle="tab">
                                                View Sales
                                                | Detailed Sales for <span class="warning">{{ $modelname }}</span>
                                                | Period <span class="warning">{{ get_period($p) }}</span>
                                                | Status: <span id="periodStatus" class="warning">{{[0 => 'OPEN', 1 => 'CLOSED'][$mperiod->closed]}}</span>
                                                <br> Total: <span
                                                        class="warning" id="totalSale">{{ $sales->sum('sum') }} {{ getCurrency() }}</span>
                                                <br> Hours: <span
                                                        class="warning" id="totalHours">{{ convertSecondsToHours($sales->sum('hours')) }}</span>
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="progress" style="height: 0.4rem;display:none">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <button id="btnStatus" onclick="closePeriod()" class="{{ ['btn btn-success','btn btn-danger'][$mperiod->closed] }}" style="margin-top: 20px">{{ ['CLOSE PERIOD','OPEN PERIOD'][$mperiod->closed] }}</button>
                                        </div>
                                        <div class="col-lg-4">
                                            <a onclick="retriveSale2()" class="btn btn-warning"
                                               style="width: 100%;margin-top: 20px">Retrive Sales This Period!</a>
                                        </div>
                                        <div class="col-lg-4">
                                            <a onclick="window.history.back();" class="btn btn-info"
                                               style="margin-top: 20px;float:right">Go Back</a>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>

                        <div class="card-body table-responsive">
                            <div class="tab-content">


                                <div class="tab-pane active show" id="1a">
                                    <table class="table table-hover data-table">
                                        <thead class="text-warning">
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Sum</th>
                                            <th>Hours</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody id="salesTable">

                                        @foreach($sales as $sale)

                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $sale->year }}-{{ decimal($sale->month) }}-{{ decimal($sale->day) }}</td>
                                                <td  id="sum{{$sale->id}}">{{ $sale->sum }} {{ getCurrency() }}</td>
                                                <td  id="hours{{$sale->id}}">{{ convertSecondsToHours($sale->hours )}}</td>
                                                <td>
                                                    <button class="btn btn-success open-modal" value="{{$sale->id}}">Edit</button>
                                                    <button onclick="deleteSale('{{$sale->id}}')" class="btn btn-danger">Delete</button>
                                                    <button onclick="retriveSale('{{$sale->id}}','{{ $sale->year }}', '{{ $sale->month }}', '{{ $sale->day }}')" class="btn btn-warning">Retrive Sale</button>
                                                </td>
                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="saleEditorModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="saleEditorModalLabel">Edit Sale for {{ $modelname }}</h4>
                    </div>
                    <div class="modal-body">
                        <p id="selectedDay"></p>
                        <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">
                            <input type="hidden" id="saleId" name="saleId" value="">

                            <div class="form-group">
                                <div class="bmd-form-group is-filled">
                                    <span>Sum</span>
                                    <input type="text" class="form-control col-sm-6" id="sum" name="sum"
                                           placeholder="" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="bmd-form-group col-sm-3">
                                        <span>Hours</span>
                                        <input type="text" class="form-control" id="hours" name="hours"
                                               placeholder="hours" value="">
                                    </div>
                                    <div class="bmd-form-group col-sm-3">
                                        <span>Minutes</span>
                                        <input type="text" class="form-control" id="minutes" name="minutes"
                                               placeholder="minutes" value="">
                                    </div>
                                    <div class="bmd-form-group col-sm-3">
                                        <span>Seconds</span>
                                        <input type="text" class="form-control" id="seconds" name="seconds"
                                               placeholder="seconds" value="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-save" value="add" onclick="updateSale()">Save changes</button>
                        {{--<input type="hidden" id="sale_id" name="sale_id" value="0">--}}
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@push('js')
<script>

    //let domain = '{{env('APP_BASE_DOMAIN')}}';


    let daysInPeriod = {!! json_encode($daysInPeriod) !!};
    let domain = '{{ url('/')  }}';
    let i = 0;
    let modelname = '{{$modelname}}';
    let periodid = '{{$mperiod->id}}';

    jQuery(document).ready(function ($) {

//        daysInPeriod.forEach(function (item, index) {
//            console.log(item, index);
//        });

        ////----- Open the modal to CREATE a sale -----////
//        jQuery('#btn-add').click(function () {
//            jQuery('#btn-save').val("add");
//            jQuery('#modalFormData').trigger("reset");
//            jQuery('#linkEditorModal').modal('show');
//        });

        ////----- Open the modal to UPDATE a sale -----////
        $('body').on('click', '.open-modal', function () {
            let sale_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'GET',
                url: domain + '/admin/viewsale/' + sale_id,
                success: function (data) {

                    console.log(data);
                    $('#saleId').val(sale_id);
                    let selectedDay = decimal(data.year) + "-" + decimal(data.month) + "-" + decimal(data.day);
                    $('#selectedDay').text(selectedDay);
                    let arr = convertToHMS(data.hours);
                    let hours = arr[0];
                    let minutes = arr[1];
                    let seconds = arr[2];
                    $('#sum').val(data.sum);
                    $('#hours').val(hours);
                    $('#minutes').val(minutes);
                    $('#seconds').val(seconds);
                    $('#btn-save').val("update");
                    $('#saleEditorModal').modal('show');

                    $("#sum").inputFilter(function(value) {
                        return /^-?\d*[.,]?\d{0,2}$/.test(value); });

                    $("#hours").inputFilter(function(value) {
                        return /^\d*$/.test(value); });

                    $("#minutes").inputFilter(function(value) {
                        return /^\d*$/.test(value) && (value === "" || parseInt(value) < 60); });

                    $("#seconds").inputFilter(function(value) {
                        return /^\d*$/.test(value) && (value === "" || parseInt(value) < 60); });

                }

            });

        });

//        $("#btn-save").click(function (e) {
//            $.ajaxSetup({
//                headers: {
//                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
//                }
//            });
//            e.preventDefault();
//
//            let sum = $('#sum').val();
//            let hours = $('#hours').val();
//            let minutes = $('#minutes').val();
//            let sec = $('#seconds').val();
//
//            let d = filterData(sum, hours, minutes, sec);
//            sum = d[0];
//            hours = d[1];
//            minutes = d[2];
//            sec = d[3];
//
//                let seconds = hours +":" + minutes + ":" + sec;
//                let formData = {
//                    sum: sum,
//                    time: seconds,
//                    day: day
//                };
//                let state = $('#btn-save').val();
//                let sale_id = $('#saleId').val();
//
//                $.ajax({
//                    type: 'POST',
//                    url: domain + '/admin/viewsale/' + sale_id,
//                    data: formData,
//                    dataType: 'json',
//                    success: function (data) {
//
//                        $('#modalFormData').trigger("reset");
//                        $('#saleEditorModal').modal('hide');
//                        location.reload();
//
//                    },
//                    error: function (data) {
//                        console.log('Error:', data);
//                    }
//                });
//
//        });

    });

    function updateSale(){

        let sum = $('#sum').val();
        let hours = $('#hours').val();
        let minutes = $('#minutes').val();
        let sec = $('#seconds').val();

        let d = filterData(sum, hours, minutes, sec);
        sum = d[0];
        hours = parseInt(d[1]);
        minutes = parseInt(d[2]);
        sec = parseInt(d[3]);
        let myday = $('#selectedDay').text();

        let seconds = (hours * 3600) + (minutes * 60) + sec;

        let formData = {
            sum: sum,
            time: seconds,
            day: myday,
            modelname: modelname
        };

        ajaxUpdateSale(formData, 'local');

    }

    function ajaxUpdateSale(formData, type){

        console.log(formData);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: domain + '/admin/viewsale/update',
            data: formData,
            success: function (data) {

                console.log(data);

                $('#modalFormData').trigger("reset");
                $('#saleEditorModal').modal('hide');

                let totalSum = data.totalSum;
                let totalHours = data.totalHours;

                let id = data.sale_id;
                let sum = data.sum;
                let hours = data.hours;
                //update UI
                updateTotal(totalSum, totalHours);

                if (type === 'local') {

                    $('#sum'+id).text(sum);
                    $('#hours'+id).text(hours);

                    $(".progress").hide();
                    //location.reload();

                } else {
                    createAndInsert(i, data.year, data.month, data.day, data.sum, data.hours, data.sale_id );
                    if (i < daysInPeriod.length) retriveSale2();
                }


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    function deleteSale(saleid){


        $(".progress").show();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "DELETE",
                url: domain + '/admin/viewsale/' + saleid,
                success: function (data) {
                    $(".progress").hide();
                    console.log(data);
                    if (data.success){
                        //update UI
                        $("#sum"+saleid).text(data.sum);
                        $("#hours"+saleid).text(data.hours);

                        let totalSum = data.totalSum;
                        let totalHours = data.totalHours;

                        updateTotal(totalSum, totalHours);

                        //location.reload();
                    } else alert('error!');


                },
                error: function (data) {
                    $(".progress").hide();
                    console.log('Error:', data);
                }
            });
    }

    function retriveSale2(){

        let date = daysInPeriod[i];
        if (!i) $("#salesTable").empty();

        //let date = year + '-' + month +'-' + day;

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: domain + '/admin/getsales/{{$modelname}}/'+date+'/',
            success: function (data) {
                console.log("data:");
                console.log( data);

                $(".progress").hide();
                i++;
                //createAndInsert(i, data.day, data.earnings, data.workTime, data.saleid, data.periodSum, data.periodHours);
                let formData = {
                    sum: data.earnings,
                    time: data.workTime,
                    day: data.day,
                    modelname: modelname
                };

                ajaxUpdateSale(formData, 'api');

            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });
    }

    function createAndInsert(index, year, month, day, sum, hours, saleid ){

        let tr = $("<tr/>",{});
        let td1 = $("<td/>",{text: index});
        let td2 = $("<td/>",{text: year + '-' + month + '-' + day});
        let td3 = $("<td/>",{text: sum, "id":"sum"+saleid});
        let td4 = $("<td/>",{text: hours, "id":"hours"+saleid});
        let td5 = $("<td/>",{});
        let btnEdit = $("<button/>",{text: 'EDIT', "class":"btn btn-success open-modal", value:saleid});
        let btnDelete = $("<button/>",{text: 'DELETE', "class":"btn btn-danger", on:{click:function() {deleteSale(saleid);}}});
        let btnRetrive = $("<button/>",{text: 'RETRIVE SALE', "class":"btn btn-warning", on:{click:function() {retriveSale(saleid, year, month, day);}}});
        td5.append(btnEdit,btnDelete,btnRetrive);
        tr.append(td1,td2,td3,td4,td5);
        $("#salesTable").append(tr);


    }

    function closePeriod(){
        //modelname
        console.log(periodid);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: domain + '/admin/closeperiodid/' + periodid,
            success: function (data) {
                console.log("data:");
                console.log( data);

                //change button text , class and status field
                if(data.success){

                    updateUiStatus(data.status);

                }




            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });
    }

    function updateUiStatus(status){

        let vals = (status === 'open') ? ['OPEN','CLOSE PERIOD','btn btn-success','danger','Period Open!'] : ['CLOSED','OPEN PERIOD','btn btn-danger','success','Period Closed!'];

        $("#periodStatus").text(vals[0]);
        $("#btnStatus").text(vals[1]);
        $("#btnStatus").removeClass().addClass(vals[2]);

        md.showNotification(vals[4], vals[3]);

    }

    function updateTotal(totalSum, totalHours){
        $("#totalSale").text(totalSum);
        $("#totalHours").text(totalHours);
    }

    function retriveSale(saleid, year, month, day){

        let date = year + '-' + month +'-' + day;

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: domain + '/admin/getsales/{{$modelname}}/'+date+'/',
            success: function (data) {
                console.log("data:");
                console.log( data);
                //console.log("earnings: " + data.earnings);
                //console.log("workTime: " + data.workTime);

                let formData = {
                    sum: data.earnings,
                    time: data.workTime,
                    day: data.day,
                    modelname: data.screenName
                };

                ajaxUpdateSale(formData, 'local');

                //$("#sum"+saleid).text(data.earnings);
                //$("#hours"+saleid).text(data.workTime);

                //location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });
    }

    function retriveAll(){

        $(".progress").show();
        let url = domain + '/admin/getsalesperiod/{{$modelname}}/{{$year}}/{{$periodnNr}}/';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                console.log("data: " + data);

                $(".progress").hide();

                location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
                alert('Error: ', data);
                $(".progress").hide();
            }
        });

    }

    // Restricts input for each element in the set of matched elements to the given inputFilter.
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    function convertToHMS(totalSeconds){

        hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        minutes = Math.floor(totalSeconds / 60);
        seconds = totalSeconds % 60;

        return [hours, minutes, seconds];

    }


    function filterData(sum, hours, minutes, sec){

        let newsum = sum;
        if (sum.includes(',')) newsum = sum.replace(/,/g, ".");
        if (!hours) hours = '00';
        if (!minutes) minutes = '00';
        if (!sec) sec = '00';

        return [newsum, hours, minutes, sec];

    }

    function decimal(dec){

        return (dec < 10) ? '0' + dec : dec;
    }


</script>
@endpush