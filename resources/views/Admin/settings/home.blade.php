@extends('layouts.admin_app', ['activePage' => 'settings', 'titlePage' => __('Settings')])

@push('head')

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-warning">
                            <h4 class="card-title">Settings</h4>
                        </div>
                        <div class="card-header card-header-tabs card-header-warning">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">

                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#1a" data-toggle="tab">
                                                <i class="material-icons">bug_report</i> Currency
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#2a" data-toggle="tab">
                                                <i class="material-icons">bug_report</i> API JASMIN
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#3a" data-toggle="tab">
                                                <i class="material-icons">bug_report</i> Cron Job
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#4a" data-toggle="tab">
                                                <i class="material-icons">bug_report</i> SMS & Email
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="1a">

                                    <div class="card-body table-responsive">

                                        <table class="table table-hover data-table">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Symbol</th>
                                                <th>Selected</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($currency as $currenc)
                                                <tr>
                                                    <td>{{ $currenc->id }}</td>
                                                    <td>{{ $currenc->name }}</td>
                                                    <td>{{ $currenc->code }}</td>
                                                    <td>{{ $currenc->symbol }}</td>
                                                    <td>{{ $currenc->selected }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                        {{--@foreach($currency as $currenc)--}}

                                        {{--<div class="form-check">--}}
                                        {{--<input class="form-check-input" type="radio" name="{{ $currenc->name  }}" id="{{ $currenc->name  }}" value="{{ $currenc->name  }}" checked>--}}
                                        {{--<label class="form-check-label" for="{{ $currenc->name  }}">--}}
                                        {{--{{ $currenc->name  }}--}}
                                        {{--</label>--}}
                                        {{--</div>--}}

                                        {{--@endforeach--}}

                                        {{--<div class="form-check">--}}
                                        {{--<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>--}}
                                        {{--<label class="form-check-label" for="exampleRadios1">--}}
                                        {{--Default radio--}}
                                        {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-check">--}}
                                        {{--<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">--}}
                                        {{--<label class="form-check-label" for="exampleRadios2">--}}
                                        {{--Second default radio--}}
                                        {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="form-check disabled">--}}
                                        {{--<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>--}}
                                        {{--<label class="form-check-label" for="exampleRadios3">--}}
                                        {{--Disabled radio--}}
                                        {{--</label>--}}
                                        {{--</div>--}}


                                    </div>
                                </div>

                                <div class="tab-pane" id="2a">

                                    <div class="card-body table-responsive">
                                        API settings (api keys)
                                        {{--@php dd($model); @endphp--}}
                                        <table class="table table-hover data-table">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Key</th>
                                            </tr>
                                            </thead>
                                            @foreach($api as $ap)

                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$ap->name}}</td>
                                                    <td>{{$ap->api_key}}</td>
                                                </tr>

                                            @endforeach
                                        </table>



                                        <table class="table table-hover data-table">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>HOUR</th>
                                            </tr>
                                            </thead>
                                            <tr><td>+02:00 GMT</td></tr>
                                        </table>

                                        {{--{{ Form::model($apikeys, ['url' => 'foo/bar']) }}--}}

                                        {{--{{ Form::label('modelname', 'Nickname') }}--}}
                                        {{--{{ Form::text('modelname', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}--}}

                                        {{--{{ Form::label('First Name') }}--}}
                                        {{--{{ Form::text('profile[first_name]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}--}}

                                        {{--{{ Form::close() }}--}}


                                        <form action="" method="post" class="form-horizontal">

                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-4">
                                                    <input name="editprofile" type="submit" value="Save Changes" class="change btn btn-primary">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="tab-pane" id="3a">

                                    <div class="card-body table-responsive">
                                        11:00 PM
                                    </div>
                                </div>

                                <div class="tab-pane" id="4a">

                                    <div class="card-body table-responsive">
                                        <div><button id="checkEmail" class="btn btn-success col-lg-3">Test Email</button>
                                            <input id="emailInput" class="col-lg-3" type="text" value='gabriel.basca@studio20.com'>
                                        </div>
                                        <div><button id="checkPhone" class="btn btn-success col-lg-3">Test Sms</button>
                                            <input id="phoneInput" class="col-lg-3" type="text" value="40751361955">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@push('js')

<script>
    $(document).ready(function() {

        $("#checkEmail").on('click', function(){
            let emailVal = $("#emailInput").val();
            testEmailOrPhone("email", emailVal);
        });

        $("#checkPhone").on('click', function(){
            let phoneVal = $("#phoneInput").val();
            testEmailOrPhone("phone", phoneVal);
        });

    });

    function testEmailOrPhone(type, val){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/testnotification',
            data:{type:type, val:val},

            success: function (data) {
                console.log(data);
            }

        });

    }

</script>
@endpush