@extends('layouts.admin_app', ['activePage' => 'backgroundservice', 'titlePage' => __('Insert Sale')])

@push('head')

@endpush

@section('content')

    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-primary">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">

                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#1a" data-toggle="tab">
                                                <i class="material-icons">account_box</i> Sync Jobs
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>




                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="1a">

                                    <button id="syncModels">1.Sync Models </button>
                                    <button id="syncAccounts">2.Sync Accounts </button>
                                    <button id="syncAddress">3.Sync Address </button>
                                    <button id="syncContracts">4.Sync Contracts </button>
                                    <button id="syncPaymentDetails">5.Sync Payment Details </button>
                                    <button id="syncProfile">6.Sync Profile </button>
                                    <button id="syncSocial">7.Sync Social </button>
                                    <button id="syncStudios">8.Sync Studios </button>
                                    <button id="syncSales">9.Sync Sales </button>
                                    <button id="syncReservations">10.Sync Reservations </button>
                                    <button id="syncPeriods">11.Sync Periods </button>
                                    <button id="syncSignatures">12.Sync Signatures </button>
                                    <button id="syncMessages">13.Sync Messagess </button>
                                    <button id="ajaxfixMessagesGroup">14.Fix MessagesGroup </button>
                                    <button id="syncAnalytics">15.Sync Analytics </button>

                                    <div class="card-body table-responsive">

                                        <table class="table table-hover">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>Event</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td><button class="event">SYSTEM</button></td>
                                            </tr>
                                            <tr>
                                                <td><button class="event">STUDIO</button></td>
                                            </tr>
                                            <tr>
                                                <td><button class="event">ACCOUNTING</button></td>
                                            </tr>
                                            <tr>
                                                <td><button class="event">ADMIN</button></td>
                                            </tr>
                                            <tr>
                                                <td><button class="event">DEV</button></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>


                                <div class="card-body table-responsive">

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')

<script>


    $(document).ready(function() {

        $(".event").on('click', function(){

            let chan = this.innerHTML;
            

            //let triggered = channel.trigger('client-test-event', {"message" : "test"});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: '/admin/pushertest',
                data:{channel: chan},

                success: function (data) {
                    console.log(data);
                }

            });
        });

        $("#syncModels").on('click', function(){
            getModels();
        });

        $("#syncAccounts").on('click', function(){
            getAccounts();
        });

        $("#syncAddress").on('click', function(){
            getAddress();
        });

        $("#syncContracts").on('click', function(){
            getContracts();
        });

        $("#syncPaymentDetails").on('click', function(){
            getPayDetails();
        });

        $("#syncProfile").on('click', function(){
            getProfile();
        });

        $("#syncSocial").on('click', function(){
            getSocial();
        });

        $("#syncStudios").on('click', function(){
            getStudios();
        });

        $("#syncSales").on('click', function(){
            getSales();
        });

        $("#syncReservations").on('click', function(){
            getReservations();
        });

        $("#syncPeriods").on('click', function(){
            getPeriods();
        });

        $("#syncSignatures").on('click', function(){
            getSignatures();
        });

        $("#syncMessages").on('click', function(){
            getMessages();
        });

        $("#ajaxfixMessagesGroup").on('click', function(){
            fixMessagesGroup();
        });

        $("#syncAnalytics").on('click', function(){
            getAnalytics();
        });


    });



    function getModels(){
        //alert("Sales");


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncmodels',
            data:{},

            success: function (data) {
                console.log(data);
            }

        });

    }

    function getAccounts(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncaccounts',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getAddress(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncaddress',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getContracts(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsynccontract',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getPayDetails(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncpaymentdetails',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getProfile(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncprofile',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getSocial(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncsocial',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getStudios(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncstudio',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getSales(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncsales',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getReservations(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncreservations',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getPeriods(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncperiods',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getSignatures(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncsignatures',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getMessages(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncmessages',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function fixMessagesGroup(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxfixMessagesGroup',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }

    function getAnalytics(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/ajaxsyncanalytics',
            data:{},
            success: function (data) {
                console.log(data);
            }
        });
    }


</script>
@endpush