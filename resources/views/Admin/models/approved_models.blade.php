@extends('layouts.admin_app', ['activePage' => 'approvedmodels', 'titlePage' => __('Dashboard')])

@push('head')

@endpush

@section('content')

    <style>

        hr{
            background: #f44336;
        }
        th {
            font-weight: bolder!important;
        }
        .card-header{
            background: #fff!important;
        }
        .card-title{
            color: #000!important;
        }
    </style>

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">Models Approved</h4>
                        <hr>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="approvedModels" class="table table-hover data-table table-striped table-bordered">
                            <thead >
                                <tr>
                                    <th>No</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Nickname</th>
                                    <th>Studio</th>
                                    <th>Status</th>
                                    <th>Promo</th>
                                    <th>Signup</th>
                                    <th>First Login</th>
                                    <th>Last Login</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@push('js')
<script>

    let table;

    $(document).ready(function() {

        $('tbody').on('click', 'tr', function () {
            let data = table.row( this ).data();
            console.log(data);
            window.open("/admin/model/" + data.id);

        } );

    });

    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        table = $('#approvedModels').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/approvedmodels') }}",
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            columns: [
                {data: 'id', searchable: false, orderable: false},
                {data: 'first_name', name:'profile.first_name', searchable: true, orderable: true},
                {data: 'last_name', name:'profile.last_name', searchable: true, orderable: true},
                {data: 'email', name:'email', searchable: true, orderable: true},
                {data: 'modelname', name:'modelname', searchable: true, orderable: true},
                {data: 'studios', name:'studios', searchable: true, orderable: true},
                {data: 'status', searchable: false, orderable: false},
                {data: 'promo', searchable: true, orderable: true},
                {data: 'created_at', searchable: false, orderable: false},
                {data: 'created_at', searchable: false, orderable: false},
                {data: 'created_at', searchable: false, orderable: false},

            ]
        });

    });

</script>
@endpush