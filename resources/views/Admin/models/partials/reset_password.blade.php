{{ Form::open() }}

{{ Form::label('password_text', 'New Password') }}
{{ Form::text('password_text', $model->password_text, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('password_text_confirmation', 'New Password Confirmation') }}
{{ Form::text('password_text_confirmation', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::submit('Reset Password', ["class" => "change btn btn-primary", "id" => "resetPAss", "name" => 'resetPass']) }}

{{ Form::close() }}