<div class="container">
    <div class="row">

        <div class="col-lg-6">
            <h4>Account Details:</h4>
            <hr>
            <div class="row">
                <div class="col-lg-6">ModelName:</div>
                <div class="col-lg-6">{{ $model->modelname }}</div>
                <div class="col-lg-6">Studio:</div>
                <div class="col-lg-6">{{ findStudio($model->studio)}}</div>
                <div class="col-lg-6">Email:</div>
                <div class="col-lg-6">{{ $model->email }}</div>
                <div class="col-lg-6">Account Type:</div>
                <div class="col-lg-6">{{[0 => 'Individual', 1 => 'Couple'][$model->couple] }}</div>
                <div class="col-lg-6">Account Password:</div>
                <div class="col-lg-6">{{ $model->password_text }}</div>
            </div>
        </div>

        <div class="col-lg-6">
            <h4>Personal Informations:</h4>
            <hr>
            <div class="row">
                <div class="col-lg-6">First Name:</div>
                <div class="col-lg-6">{{ $model->profile->first_name }}</div>
                <div class="col-lg-6">Last Name:</div>
                <div class="col-lg-6">{{ $model->profile->last_name }}</div>
                <div class="col-lg-6">Phone:</div>
                <div class="col-lg-6">{{ $model->profile->phonenumber }}</div>
                <div class="col-lg-6">Birthdate:</div>
                <div class="col-lg-6">{{ \Carbon\Carbon::parse($model->profile->birthdate)->format('d-m-Y') }}</div>
                {{--<div class="col-lg-6">{{ $model->profile->birthdate }}</div>--}}
                <div class="col-lg-6">Sex:</div>
                <div class="col-lg-6">{{ ['F' => 'Female', 'M' => 'Male', 'C' => 'Couple'][$model->profile->sex] }}</div>
            </div>
        </div>

        <div class="col-lg-6">
            <h4>Address:</h4>
            <hr>
            <div class="row">
                <div class="col-lg-6">Address:</div>
                <div class="col-lg-6">{{ $model->address->street }}</div>
                <div class="col-lg-6">Country:</div>
                <div class="col-lg-6">{{ findCountry($model->address->country_id) }}</div>
                <div class="col-lg-6">State:</div>
                <div class="col-lg-6">{{ findState($model->address->state_id) }}</div>
                <div class="col-lg-6">City:</div>
                <div class="col-lg-6">{{ findCity($model->address->city_id) }}</div>

            </div>
        </div>

        <div class="col-lg-6">
            <h4>Payment Details:</h4>
            <hr>
            <div class="row">
                <div class="col-lg-6">Bank:</div>
                <div class="col-lg-6">{{ $model->paymentDetails->bank }}</div>
                <div class="col-lg-6">Bank Address:</div>
                <div class="col-lg-6">{{ $model->paymentDetails->bank_address }}</div>
                <div class="col-lg-6">IDN:</div>
                <div class="col-lg-6">{{ $model->paymentDetails->idn }}</div>
                <div class="col-lg-6">IBAN:</div>
                <div class="col-lg-6">{{ $model->paymentDetails->iban }}</div>
                <div class="col-lg-6">Currency:</div>
                <div class="col-lg-6">EUR</div>
            </div>
        </div>

        <div class="col-lg-6">
            <h4>Contract Details:</h4>
            <hr>
            <div class="row">
                <div class="col-lg-6">Signed Contract</div>
                <div class="col-lg-6">{{ [0 => 'NO', 1 => 'YES'][($model->signature) ? 1 : 0] }}</div>
                <div class="col-lg-6">Signup Date</div>
                <div class="col-lg-6">{{$model->created_at}}</div>
                <div class="col-lg-6">Contract Type</div>
                <div class="col-lg-6">{{ ['pf' => 'Persoana Fizica', 'pj' => 'Persoana Juridica'][$model->account->type] }}</div>


            </div>
        </div>

        <div class="col-lg-6">
            <h4>Contract Actions:</h4>
            <hr>
            <div class="row">

                <div class="col-lg-6">ID Card Front</div>
                <div class="col-lg-6">
                    {{--php if ($model->contract->pic1) {$color1 = 'warning'; $modal1 = 'pic1';} else {$color1 = 'default'; $modal1 = '#';} endphp--}}
                    <button type="button" data-toggle="modal" data-target="#modal-pic1" class="btn btn-{{getPictureStatusColor($model->contract->pic1_status)}}">View</button>
                </div>

                <div class="col-lg-6">ID Card Back</div>
                <div class="col-lg-6">
                    {{--php if ($model->contract->pic2) {$color2 = 'warning'; $modal2 = 'pic2';} else {$color2 = 'default';  $modal2 = 'pic2';}endphp--}}
                    <button type="button" data-toggle="modal" data-target="#modal-pic2" class="btn btn-{{getPictureStatusColor($model->contract->pic2_status)}}">View</button>
                </div>

                <div class="col-lg-6">Face + ID Card</div>
                <div class="col-lg-6">
                    {{--php if ($model->contract->pic3) {$color3 = 'warning'; $modal3 = 'pic3';} else {$color3 = 'default'; $modal3 = '#';}endphp--}}
                    <button type="button" data-toggle="modal" data-target="#modal-pic3" class="btn btn-{{getPictureStatusColor($model->contract->pic3_status)}}">View</button>
                </div>
                <div class="col-lg-6">
                    @php
                        if ($model->contract->contractsigned) {$contractColor = 'success'; $contractDisable = ''; }
                    else  {$contractColor = 'danger'; $contractDisable = '';}
                    @endphp

                    @if ($model->status != 1)
                        <button id="generateContract" class="btn btn-{{$contractColor}}" {{" " .$contractDisable }} onclick="generateContract({{$model->id}})">Generate Contract</button>
                    @endif

                </div>
                <div class="col-lg-6">
                    @if ($model->contract->contractsigned)
                        <a class="btn btn-success"  href="{{$contractPath}}">View Contract</a>
                    @else
                        <a class="btn btn-default">View Contract</a>
                    @endif

                </div>

            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="modal-pic1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="position: absolute;">ID Front Image</h4>
            </div>
            @if ($model->contract->pic1)
                <div class="modal-body">
                    <img width="100%" height="auto" src="{{'/storage/models/images/'.$model->contract->pic1}}">
                </div>
                <div class="modal-footer">

                    {{ Form::model($model, ["class" => "w-100"]) }}
                    <div class="form-row">
                        @if ($model->status !== 1)
                            {{ Form::text('contract[reason_pic1]', null, [ 'class' => 'mt-1 form-control', 'placeholder' => 'Reason ..' ]) }}

                            {{ Form::submit('Approve',  ["class" => "btn btn-success form-control col-lg-3 col-sm-12", "id" => "pic1_approve", "name" => "pic1"]) }}

                            {{ Form::submit('Reject',  ["class" => "btn btn-danger form-control col-lg-3 col-sm-12", "id" => "pic1_reject", "name" => "pic1"]) }}
                        @endif
                        <button type="button" class="col-lg-12 btn btn-info">
                            <a href="{{ asset("/storage/models/images/".$model->contract->pic1) }}" download="front_{{ $model->profile->first_name }}_{{ $model->profile->last_name }}">Download image</a>
                        </button>

                    </div>
                    {{ Form::close() }}


                </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="modal-pic2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="position: absolute;">ID Card Back</h4>
            </div>

            <div class="modal-body">
                @if ($model->contract->pic2)
                    <img width="100%" height="auto" src="{{'/storage/models/images/'.$model->contract->pic2}}">
                @else
                    <img width="100%" height="auto" src="{{'/storage/models/images/id2.jpg'}}">
                @endif
            </div>

            <div class="modal-footer">

                {{ Form::model($model, ["class" => "w-100"]) }}
                <div class="form-row">
                    @if ($model->status !== 1)
                        {{ Form::text('contract[reason_pic2]', null, [ 'class' => 'mt-1 form-control', 'placeholder' => 'Reason ..' ]) }}

                        {{ Form::submit('Approve',  ["class" => "btn btn-success form-control col-lg-3 col-sm-12", "id" => "pic2_approve", "name" => "pic2"]) }}

                        {{ Form::submit('Reject',  ["class" => "btn btn-danger form-control col-lg-3 col-sm-12", "id" => "pic2_reject", "name" => "pic2"]) }}
                    @endif
                    <button type="button" class="col-lg-12 btn btn-info">
                        <a href="{{ asset("/storage/models/images/".$model->contract->pic2) }}" download="front_{{ $model->profile->first_name }}_{{ $model->profile->last_name }}">Download image</a>
                    </button>

                </div>
                {{ Form::close() }}

            </div>


        </div>

    </div>
</div>

<div class="modal fade" id="modal-pic3">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="position: absolute;">Face + ID Card</h4>
            </div>
            @if ($model->contract->pic3)
                <div class="modal-body">
                    <img width="100%" height="auto" src="{{'/storage/models/images/'.$model->contract->pic3}}">
                </div>
                <div class="modal-footer">

                    {{ Form::model($model, ["class" => "w-100"]) }}
                    <div class="form-row">
                        @if ($model->status !== 1)
                            {{ Form::text('contract[reason_pic3]', null, [ 'class' => 'mt-1 form-control', 'placeholder' => 'Reason ..' ]) }}

                            {{ Form::submit('Approve',  ["class" => "btn btn-success form-control col-lg-3 col-sm-12", "id" => "pic3_approve", "name" => "pic3"]) }}

                            {{ Form::submit('Reject',  ["class" => "btn btn-danger form-control col-lg-3 col-sm-12", "id" => "pic3_reject", "name" => "pic3"]) }}
                        @endif
                        <button type="button" class="col-lg-12 btn btn-info">
                            <a href="{{ asset("/storage/models/images/".$model->contract->pic3) }}" download="front_{{ $model->profile->first_name }}_{{ $model->profile->last_name }}">Download image</a>
                        </button>

                    </div>
                    {{ Form::close() }}


                </div>
            @endif
        </div>
    </div>
</div>



