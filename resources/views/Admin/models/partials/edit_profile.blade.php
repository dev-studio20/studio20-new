{{ Form::model($model) }}

{{ Form::label('modelname', 'Nickname') }}
{{ Form::text('modelname', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('First Name') }}
{{ Form::text('profile[first_name]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('Last Name') }}
{{ Form::text('profile[last_name]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('email', 'Email') }}
{{ Form::text('email', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('Phone') }}
{{ Form::text('profile[phonenumber]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{Form::label('birthdate', 'Birth Date')}}
{{ Form::date('profile[birthdate]', $model->profile->birthdate, [ 'class'=> 'form-control col-lg-6 col-sm-12', 'required' => 'required', $approved ]) }}

{{ Form::label('Gender') }}
{{ Form::select('profile[sex]', ['F' => 'Female', 'M' => 'Male'], $model->profile->sex, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('Account Type') }}
{{ Form::select('couple', [0 => 'Individual Account', 1 => 'Couple Account'], $model->couple, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('Country') }}
{{ Form::select('address[country_id]', [$model->address->country_id => findCountry($model->address->country_id)], $model->address->country_id, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('State') }}
{{ Form::select('address[state_id]', [$model->address->state_id => findState($model->address->state_id)], $model->address->state_id, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('City') }}
{{ Form::select('address[city_id]', [$model->address->city_id => findCity($model->address->city_id)], $model->address->city_id, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('Address') }}
{{ Form::text('address[street]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('studio', 'Studio') }}
{{ Form::select('studio', $studios, $model->studio, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved]) }}

{{ Form::label('IDN') }}
{{ Form::text('paymentDetails[idn]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required', $approved ]) }}

{{ Form::label('IBAN') }}
{{ Form::text('paymentDetails[iban]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('Bank') }}
{{ Form::text('paymentDetails[bank]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('Bank Address') }}
{{ Form::text('paymentDetails[bank_address]', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::label('Currency') }}
{{ Form::select('paymentDetails[currency]', ['eur' => 'EUR'], 'eur', [ 'class' => 'form-control col-lg-6 col-sm-12', 'readonly', 'required', $approved ]) }}

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
        <input name="editprofile" type="submit" value="Save Changes" class="change btn btn-primary" >
    </div>
</div>

{{ Form::close() }}