<form action="" method="post" class="form-horizontal">

    <input type="hidden" name="_token" value="KKaejKGGAgPPWQbfHMtAB3DY0JgGaHak1yaBP0LU">

    <div class="form-group">
        <label for="startDate" class="col-sm-2 control-label">Start Date:</label>
        <div class="col-sm-4">
            <div class="input-group">
                <input id="startDate" type="text" aria-describedby="start-date" name="startdate" value="2019-05-15" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="" class="form-control">
                <span id="start-date" class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="endDate" class="col-sm-2 control-label">End Date:</label>
        <div class="col-sm-4">
            <div class="input-group">
                <input id="endDate" type="text" aria-describedby="end-date" name="enddate" value="2019-05-15" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask="" class="form-control">
                <span id="end-date" class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input name="retriveapi" type="submit" value="Retrive data!" class="change">
            <input name="retriveapiadmintest" type="submit" value="Retrive data!" class="change invisible">
        </div>
    </div>
</form>