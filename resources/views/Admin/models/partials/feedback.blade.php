{{ Form::open() }}

{{ Form::label('feedback', 'Feedback about this model') }}
{{ Form::textarea('feedback', null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required' ]) }}

{{ Form::submit('Save Feedback', ["class" => "change btn btn-info"]) }}

{{ Form::close() }}


@if (count($model->feedback))

    <div class="card">
        <div class="card-header card-header-info">

        </div>
        <div class="card-body table-responsive">
            <table class="table table-hover data-table table-striped table-bordered">
                <thead class="text-info">
                <tr><th>From</th><th>Feedback text</th><th>Date</th></tr>
                </thead>
                <tbody>
                @foreach ($model->feedback as $feed)
                    <tr><td>{{$feed->user}}</td><td>{{$feed->message}}</td><td>{{$feed->created_at}}</td></tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@else
    <div class="jumbotron">
        <h3 class="display-6">No Feedback..</h3>
    </div>
@endif


