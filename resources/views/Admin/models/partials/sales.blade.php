<div class="row">
    <div class="col-lg-3">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#saleModal">Add Sale</button>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-hover data-table table-striped table-bordered">
        <thead class="text-info">
        <tr>
            <th>#</th>
            <th>Year</th>
            <th>Period</th>
            <th>Total Amount</th>
            <th>Online Hours</th>
            <th>Com T. </th>
            <th>Exchange </th>
            <th>Pay Amount </th>
            <th>Period Status</th>
            <th>Pay Status</th>
            <th>Ignore Res.</th>
            <th>Pay Date</th>
            {{--<th>Date</th>--}}
        </tr>
        </thead>
        <tbody>

        @foreach($periods as $period)
            @if (!$period->closed)<tr onclick="openNewTab({{$period->id}})"> @else<tr> @endif
                <td>{{ $loop->iteration }}</td>
                <td>{{ $period->year }}</td>
                <td>{{ get_period($period->period) }}</td>
                <td>{{ $period->total_amount  }} $</td>
                <td>{{ convertSecondsToHours($period->hours) }}</td>
                @if ($period->payHistory) <td>{{$period->payHistory->commission_total}} %</td> @else <td>--</td> @endif
                @if ($period->payHistory) <td>{{$period->payHistory->exchange}} $/€</td> @else <td>--</td> @endif
                @if ($period->payHistory) <td>{{$period->payHistory->pay_amount }} €</td> @else <td>--</td> @endif
                @if ($period->closed) <td><span class="badge badge-success">Closed</span> @else <td><span class="badge badge-secondary">Open</span></td> @endif
                @if ($period->payHistory)
                    <td>@if ($period->payHistory->pay_done) <span class="badge badge-success">Payed</span> @else <span class="badge badge-secondary">Not Payed</span> @endif</td>
                @else <td>--</td> @endif
                @if ($period->payHistory)
                    @if (($period->payHistory->pay_request == 1) || ($period->payHistory->pay_done == 1))
                        <td></td>
                    @else <td><span id="{{$period->payHistory->id}}" style="cursor:pointer;" class="badge badge-secondary" onclick="toggleRes({{$period->payHistory->id}})">{{["No", "Yes"][$period->payHistory->ignore_res]}}</span></td>

                    @endif
                @else <td></td>
                @endif
                @if ($period->payHistory) <td>{{explode(' ',$period->payHistory->pay_date)[0]}}</td> @else <td>--</td> @endif

            </tr>
            @endforeach



        </tbody>
    </table>
</div>

<!-- Modal -->
<style>
    .insertSale{
        display:none;
    }
</style>
<div class="modal fade" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="saleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="saleModalLabel">Add Sale - {{$model->modelname}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row"><div class="col-lg-3">Select day</div></div>
                <div id="mdp-salepicker"></div>
                <hr class="insertSale">
                <div class="row insertSale">
                    <span class="col-lg-1">Sum</span>
                    <input id="modalSum" class="col-lg-3" type="number" placeholder="0.00" value="0">
                    <span class="col-lg-1">H</span>
                    <input id="modalHour" class="col-lg-2" type="number" placeholder="00" value="0">
                    <span class="col-lg-1">M</span>
                    <input id="modalMinutes" class="col-lg-2" type="number" placeholder="00" value="0">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="insertSalesModal()" class="btn btn-primary insertSale">Add Sale</button>
            </div>
        </div>
    </div>
</div>