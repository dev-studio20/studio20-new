{{ Form::model($model) }}

<div class="row" style="font-size: 16px; margin-bottom: 10px;">
    <div class="col-sm-3 control-label">This model opted to be promoted by brand:</div>
    <span class="text-danger"> {{$model->social->brand}}</span>
</div>

{{ Form::label('Artistic Email') }}
{{ Form::text('social[artistic_email]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

{{ Form::label('Artistic Password') }}
{{ Form::text('social[artistic_password]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

<div class="form-group">
    <div class="col-sm-3"></div>
    <div class="col-sm-4">
        <div> The <span style="color: red;">Artistic email</span> and <span
                    style="color: red;">Artistic password</span> will be used to create:
        </div>
        <div class="col-sm-10 col-sm-offset-2" style="color: red;">
            - Jasmin account;<br>
            - Twitter account;<br>
            - Instagram account;<br>
            - Skype account;<br></div>
    </div>
</div>

{{ Form::label('Shortlink to WL for Twitter profile:') }}
{{ Form::text('social[twitter_profile]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

{{ Form::label('Shortlink to WL for Twitter posts:') }}
{{ Form::text('social[twitter_post]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

{{ Form::label('Link to WL for Instagram profile:') }}
{{ Form::text('social[instagram_profile]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

{{ Form::label('Personal site URL:') }}
{{ Form::text('social[personal_site]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

{{ Form::label('Twitter ID:') }}
{{ Form::text('social[twitter_account]', null, [ 'class' => 'form-control col-lg-6 col-sm-12' ]) }}

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-4">
        <input name="editsocial" type="submit" value="Save Changes" class="change btn btn-primary">
    </div>
</div>

{{ Form::close() }}

