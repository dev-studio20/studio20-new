<div class="dl-horizontal">
    <div class="row justify-content-center">
        <div class="col-lg-4">
            <div class="card" style="min-height:300px;">

                <div class="card-header card-header-@if($approvalCondition){{'success'}}@else{{'danger'}}@endif">
                    <h4 class="card-title" style="text-align:center">Approval Conditions</h4>
                </div>

                <div class="card-body">
                    <table style="width:100%">
                        <thead>
                        <tr>
                            <th style="width:50%">Condition</th><th style="width:50%">Status</th>
                        </tr>
                        </thead>
                        {{--show conditions--}}
                        <tr><td>Photo 1: </td><td>@if ($model->contract->pic1_status == 1) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr><td>Photo 2: </td><td>@if ($model->contract->pic2_status == 1) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr><td>Photo 3: </td><td>@if ($model->contract->pic3_status == 1) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr><td>Contract Generated:</td><td>@if ($model->contract->contractsigned) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr><td>Modelname: </td><td>@if (count(explode(',', $model->modelname)) == 1) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr>
                            @if ($model->studios->phone_verification)
                                <td>Phone Verified: </td><td>@if ($model->phone_verified_at) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td>
                                @else
                                <td>Phone Verified: </td><td>{!! $btn['ok'] !!}</td>
                            @endif
                        </tr>
                        <tr><td>Email Verified: </td><td>@if ($model->email_verified_at) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                        <tr><td>Social Media: </td><td>@if ($socialCondition) {!! $btn['ok'] !!} @else {!! $btn['no'] !!} @endif</td></tr>
                    </table>
                    <hr>
                    <div class="row">
                        <input id="inputModelNickname" class="form-control col-lg-12" type="text" value="{{$model->modelname}}" placeholder="modelname">
                        <input onclick="setModelNickname()" type="button" class="form-control col-lg-5 btn btn-success" value="SET NICKNAME" style="padding: 0;margin: 0;position: absolute; right: 20px;">
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="card" style="min-height:351px;">

                <div class="card-header card-header-{{getModelsStatusColor($model->status)}}">
                    <h4 class="card-title" style="text-align:center">{{$model->statusName->name}}</h4>
                </div>

                <div class="card-body">

                    {{ Form::open() }}

                    {{ Form::label('statselect', 'Status select') }}
                    {{ Form::select('statselect', $statesSelect, null, [ 'class' => 'form-control col-lg-6 col-sm-12', 'required','placeholder' => 'Select action'  ]) }}

                    {{ Form::label('reasonField','Reason', ["id" => "reasonLabel"]) }}
                    {{ Form::text('reason', $model->contract->reason, [ 'class' => 'form-control col-lg-6 col-sm-12', 'id' => 'reasonField','placeholder' => 'Reason for account action..' ]) }}

                    {{ Form::submit('Save', ["class" => "btn btn-success", "disabled", "id" => "saveStatus"]) }}

                    {{ Form::close() }}

                </div>

            </div>

        </div>
    </div>

</div>
