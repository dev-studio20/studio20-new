@extends('layouts.admin_app', ['activePage' => 'models', 'titlePage' => __('Models')])

@push('head')

<link href="{{ asset('material/js/jquery-ui.multidatespicker.css') }}" rel="stylesheet"/>
<link href="{{ asset('material/js/jquery-ui.css') }}" rel="stylesheet"/>

@endpush

@section('content')
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header card-header-tabs card-header-primary">
                            <div class="nav-tabs-navigation">
                                <div class="nav-tabs-wrapper">

                                    <ul class="nav nav-tabs" data-tabs="tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#1a" data-toggle="tab">
                                                <i class="material-icons">account_box</i> Model Profile
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#2a" data-toggle="tab">
                                                <i class="material-icons">description</i> Edit Profile
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#3a" data-toggle="tab">
                                                <i class="material-icons">cloud</i> Account Details & Socials
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#4a" data-toggle="tab">
                                                <i class="material-icons">power_settings_new</i> Account
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#5a" data-toggle="tab">
                                                <i class="material-icons">fingerprint</i> Reset Password
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#6a" data-toggle="tab">
                                                <i class="material-icons">list</i> Sales
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#7a" data-toggle="tab">
                                                <i class="material-icons">book</i> Reservations
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#9a" data-toggle="tab">
                                                <i class="material-icons">feedback</i> Feedback
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#10a" data-toggle="tab">
                                                <i class="material-icons">explore</i> Analytics
                                                <div class="ripple-container"></div>
                                            </a>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <h5>{{$model->modelname}} - {{$model->studios->name}}</h5>
                        </div>

                        <div class="card-body">
                            <div class="tab-content">
                                @if (session('flash-message'))
                                    <div id="msg" class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{Session::get('flash-message')}}</span>
                                    </div>
                                @endif

                                <div class="tab-pane active" id="1a">
                                    @include('Admin.models.partials.model_profile')
                                </div>

                                <div class="tab-pane" id="2a">
                                    @include('Admin.models.partials.edit_profile')
                                </div>

                                <div class="tab-pane" id="3a">
                                    @include('Admin.models.partials.account_details_socials')
                                </div>

                                <div class="tab-pane" id="4a">
                                    @include('Admin.models.partials.account')
                                </div>

                                <div class="tab-pane" id="5a">
                                    @include('Admin.models.partials.reset_password')
                                </div>

                                <div class="tab-pane" id="6a">
                                    @include('Admin.models.partials.sales')
                                </div>

                                <div class="tab-pane" id="7a">
                                @include('Admin.models.partials.reservations')
                                </div>

                                {{--<div class="tab-pane" id="8a">--}}
                                    {{--@include('Admin.models.partials.api')--}}
                                {{--</div>--}}

                                <div class="tab-pane" id="9a">
                                    @include('Admin.models.partials.feedback')
                                </div>

                                <div class="tab-pane" id="10a">
                                    @include('Admin.models.partials.analytics')
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')


<script>

    let reservations = @json($reservations);
    //console.log(reservations);

    let approvalCondition = '{{$approvalCondition}}';
    let modelStatus = '{{$model->status}}';
    let model_id = '{{$model->id}}';
    let studio_id = '{{$model->studios->id}}';

    let saveStatus = $("#saveStatus");
    let reasonField = $("#reasonField");
    let reasonLabel = $("#reasonLabel");

    let selectedModalDate = null;

    let dateArray = null;
    if (reservations.length > 0) dateArray = [];

    $(document).ready(function () {

        reservations.forEach(function(entry) {
            //console.log(entry);
            let month = entry.month;
            let year = entry.year;
            let daysStr = entry.days;

            if (daysStr && (daysStr != '') ){
                let days = daysStr.split(",");
                days.forEach(function(e) {
                    let str = month + '/' + e + '/' + year;
                    //console.log(str);
                    dateArray.push( new Date(str));
                });
            }
        });

        let dt = new Date();
        let myyear = dt.getFullYear();
        let mymonth = dt.getMonth();
        let time = dt.getHours();

        showStatusAndHour(myyear, mymonth + 1);

        setTimeout(function() {
            $('#msg').fadeOut('fast');
        }, 2000);


        $('#mdp-thismonth').multiDatesPicker({
            dateFormat: 'mm/dd/yy',
            //minDate: minDate,
            //maxDate: maxDate,
            altField: '#altField',
            //disabled: status,
            disabled: false,
            addDates: dateArray,
            onChangeMonthYear: function(dateText, inst, dateob) {
                //var navidatedMonth = new Date(dateob.selectedYear, dateob.selectedMonth,  dateob.selectedDay);
                let y = dateob.selectedYear;
                let m = dateob.selectedMonth + 1;

                showStatusAndHour(y, m);
            }
        });

        $('#mdp-salepicker').multiDatesPicker({
            dateFormat: 'mm/dd/yy',
            maxPicks: 1,
            onSelect: function(dateStr) {
                selectedModalDate = $(this).datepicker('getDate');

                if (selectedModalDate) $(".insertSale").show();
                else $(".insertSale").hide();
            }
        });


    });

    function insertSalesModal(){

        if (selectedModalDate){

            let month = selectedModalDate.getMonth() + 1;
            let year = selectedModalDate.getFullYear();
            let day = selectedModalDate.getDate();
            console.log(day + ' - ' + month + ' - ' + year);

            let sum = $("#modalSum").val();
            let hours = $("#modalHour").val();
            hours = Math.abs(hours);
            let minutes = $("#modalMinutes").val();
            minutes = Math.abs(minutes);

            let seconds = (3600*hours) + (60*minutes);

            console.log("seconds : " + seconds + ' seconds : ' + seconds);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                data: {"model_id":model_id, "studio_id":studio_id, "year":year, "month":month, "day":day, "sum":sum, "hours":seconds},
                url: "{{ url('admin/addsales') }}",
                method: "POST",
                success: function (data) {

                    console.log("Success", data);
                    location.reload();
                    //change button status

                    //if(data.success) $("#"+id).text("Yes");
                    //else $("#"+id).text("No");

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    }

    function showStatusAndHour(y, m){

        $("#hour").text("");
        $("#status").text("");

        reservations.forEach(function(entry) {
            if ( (y === entry.year) && (m === entry.month) ){
                //console.log(entry);

                $("#hour").text("Hour: " + entry.hour);
                let str_stat = (entry.status === 1) ? "Approved" : "Pending";
                $("#status").text("Status: " + str_stat);
            }
        });

    }


    $( "#statselect" ).change(function() {

        let status = $( "#statselect" ).val();

        if (status){
            console.log(status);
            if (approvalCondition && (status == 1)) {

                //enable save btn
                saveStatus.attr("disabled", false);

                //hide reason input field and label
                reasonField.hide();
                reasonLabel.hide();
                reasonField.attr("required", false);
            } else {
                //show input field with required
                reasonField.show();
                reasonField.attr("required", true);
                reasonLabel.show();

                //enable save btn
                saveStatus.attr("disabled", false);

            }
        }

         if (status == modelStatus || ( (status == 1) && (!approvalCondition))){
            //status is the same .. do nothing
             saveStatus.attr("disabled", true);
        }

    });

    function toggleRes(id){
        console.log("toggle ignore res..");
        console.log(id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            data: {"phistory_id":id},
            url: "{{ url('admin/ignoreres') }}",
            method: "POST",
            success: function (data) {

                console.log("Success", data);
                //change button status

                if(data.success) $("#"+id).text("Yes");
                else $("#"+id).text("No");

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }


    function approveAccount(id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            data: {"model_id":id},
            url: "{{ url('admin/approve_account') }}",
            method: "POST",
            success: function (data) {

                console.log("Success", data);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    }

    function suspendAccount(id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            data: {"model_id":id},
            url: "{{ url('admin/suspend_account') }}",
            method: "POST",
            success: function (data) {

                console.log("Success", data);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    }

    function setModelNickname() {
        let modelname = $("#inputModelNickname").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            data: {model_id:model_id, modelname:modelname},
            url: "{{ url('admin/modelnickname') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {

                if (data.success) {
                    location.reload();
                }

            },
            error: function (data) {

            }
        });

    }

    function generateContract(id){

        $("#generateContract").attr('disabled', true);
        md.showNotification('You will receive a notification when contract is generated!', 'success');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            data: {"model_id":id},
            url: "{{ url('admin/generate_contract') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {

                if (data.success) {
                    //location.reload();

                    // $('#productForm').trigger("reset");
                    // $('#ajaxModel').modal('hide');

                    // table.draw();
                    // md.showNotification('Success!', 'success');
                }

            },
            error: function (data) {
                console.log('Error:', data);
                // md.showNotification('Error:' + data, 'danger');
                //$('#saveBtn').html('Save Changes');
            }
        });


    }

    function openNewTab(id){

    window.open("/admin/closeperiods/"+id, '_blank');

    }


</script>
@endpush