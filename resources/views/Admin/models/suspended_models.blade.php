@extends('layouts.admin_app', ['activePage' => 'suspendedmodels', 'titlePage' => __('Dashboard')])

@push('head')

@endpush

@section('content')

    <style>

        hr{
            background: #f44336;
        }
        th {
            font-weight: bolder!important;
        }
        .card-header{
            background: #fff!important;
        }
        .card-title{
            color: #000!important;
        }
    </style>

    <div class="content" >

        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Models Suspended</h4>
                        <hr>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="suspendedModels" class="table table-hover data-table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Nickname</th>
                                <th>Studio</th>
                                <th>Status</th>
                                <th>Signup</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@push('js')
<script>

    let table;
    let tableBody = $('tbody');
    let studio = $("#myStudio").children("option:selected").val();


    $(document).ready(function() {

        tableBody.on('click', 'tr', function () {
            let data = table.row( this ).data();
            //console.log(data);
            window.open("/admin/model/" + data.id);

        } );

    });
    $(function () {

        table = $('#suspendedModels').DataTable({
            processing: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
            serverSide: true,
            ajax: {
                'url':'/admin/suspendedmodels',
                "type": "POST",
                "data": function (d) {
                    d.studio = studio;
                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            columns: [
//                {data: 'id',  name: 'id', searchable: true, orderable: true},
                {data: 'profile.first_name', name:'profile.first_name', searchable: true, orderable: true},
                {data: 'profile.last_name', name:'profile.last_name', searchable: true, orderable: true},
                {data: 'email', name:'email', searchable: true, orderable: true},
                {data: 'modelname', name:'modelname', searchable: true, orderable: true},
                {data: 'studios.name', name:'studios.name', searchable: true, orderable: true},
                {data: 'status_name.name', searchable: false, orderable: false},
//                {data: 'social.brand', searchable: true, orderable: true},
                {data: 'created_at', searchable: false, orderable: true},
            ],
            columnDefs: [ {
                "targets": 5,
                render: function (data, type, row, meta)
                {
                    if (type === 'display') data = '<b class="'+data.toLowerCase()+'">' + data + '</b>';

                    return data;
                }
            }]
        });

        $('#createNewProduct').click(function () {
            $('#saveBtn').val("create-product");
            $('#product_id').val('');
            $('#productForm').trigger("reset");
            $('#modelHeading').html("Create New Product");
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editProduct', function () {
            var product_id = $(this).data('id');
            $.get("{{ route('models.index') }}" +'/' + product_id +'/edit', function (data) {
                $('#modelHeading').html("Edit Product");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#product_id').val(data.id);
                $('#name').val(data.name);
            })
        });

        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Sending..');

            $.ajax({
                data: $('#productForm').serialize(),
                url: "{{ route('models.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    $('#productForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    table.draw();

                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });

        $('body').on('click', '.deleteProduct', function () {

            var product_id = $(this).data("id");
            confirm("Are You sure want to delete !");

            $.ajax({
                type: "DELETE",
                url: "{{ route('models.store') }}"+'/'+product_id,
                success: function (data) {
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

    });

</script>
@endpush