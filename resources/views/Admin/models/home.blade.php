@extends('layouts.admin_app', ['activePage' => 'models', 'titlePage' => __('Models')])

@push('head')

@endpush

@section('content')

    <style>

        hr{
            background: #f44336;
        }
        th {
          font-weight: bolder!important;
        }
        .card-header{
            background: #fff!important;
        }
        .card-title{
            color: #000!important;
        }
    </style>

    <div class="content">

        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-title">All Models</h4>
                        <hr>
                    </div>
                    <div class="card-body table-responsive">
                        <table id="allModels" class="table table-hover data-table table-striped table-bordered">
                            <thead>
                            <tr >
                                <th>No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Nickname</th>
                                <th>Studio</th>
                                <th>Status</th>
                                <th>Promo</th>
                                <th>Signup</th>
                                {{--<th>First Login</th>--}}
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@push('js')
<script>

    let table;
    let tableBody = $('tbody');
    let studio = $("#myStudio").children("option:selected").val();

    $(document).ready(function() {

        tableBody.on('click', 'tr', function () {
            let data = table.row( this ).data();
            console.log(data);
            window.open("/admin/model/" + data.id);

        } );

    });

    $(function () {

        table = $('#allModels').DataTable({

            processing: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
            serverSide: true,
            ajax: {
                'url':'/admin/models',
                "type": "GET",
                "data": function (d) {
//                    d.period = period;
//                    d.year = year;
                    d.studio = studio;
                },
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
            },
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            columns: [
                {data: 'id', name: 'id', searchable: true, orderable: true},
                {data: 'profile.first_name', name: 'profile.first_name', searchable: true},
                {data: 'profile.last_name', name: 'profile.last_name', searchable: true},
                {data: 'email', name: 'email', searchable: true, orderable: true},
                {data: 'modelname', name: 'modelname', searchable: true, orderable: true},
                {data: 'studios.name', name: 'studios.name', searchable: false, orderable: false},
                {data: 'status_name.name', name: 'status_name.name', searchable: false, orderable: false},
                {data: 'social.brand', name: 'social.brand', searchable: false, orderable: false},
                {data: 'created_at', name: 'created_at', searchable: false, orderable: false},
//                {data: 'profile.first_login', name: 'profile.first_login', searchable: false, orderable: false},
            ],
            columnDefs: [ {
                "targets": 6,
                render: function (data, type, row, meta)
                {
                    if (type === 'display') data = '<b class="'+data.toLowerCase()+'">' + data + '</b>';

                    return data;
                }
            }]

        });

    });

</script>
@endpush