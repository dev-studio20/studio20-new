@extends('layouts.admin_app', ['activePage' => 'paymenthistory', 'titlePage' => __('Payment History')])

@push('head')

@endpush

<link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css" rel="stylesheet" />


@section('content')
    <style>
        .warning {
            color: #ff9800;
        }
    </style>

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">

                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Payments History
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress" style="height: 0.4rem;display:none">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        @foreach($years as $year)

                                            <button id="{{$year}}" class="years btn @if ($year == $y) btn-success @else btn-warning @endif" onclick="changeYear({{$year}})">{{$year}}</button>

                                        @endforeach
                                    </div>
                                </div>
                                {!! Form::open(['url' => '/admin/closeperiod/'.date('Y'), 'id' => 'target']) !!}

                                {!! Form::select('periods', getAllPeridos(), $p, [ 'class' => 'form-control col-lg-2', 'id' => 'periods' ]) !!}

                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                    <div class="card-body table-responsive">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="1a">
                                <table id="allPeriods" class="data-table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Period</th>
                                        <th>Modelname</th>
                                        <th>Studio</th>
                                        <th>Total Jasmin</th>
                                        <th>Total USD</th>
                                        <th>Total EUR</th>
                                        <th>Com. T</th>
                                        <th>Rate $/€</th>
                                        <th>Hours</th>
                                        <th>Action</th>
                                        <th>Pay Amount</th>
                                        <th>Pay Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading">--</h4>
                </div>
                <div class="modal-body">

                    <div class="card">

                        <div class="card-body">
                            <table>
                                <tr>
                                    <td>Jasmin Revenue</td>
                                    <td id="jasminRev"></td>
                                </tr>

                                <tr>
                                    <td>Affiliate Revenue</td>
                                    <td id="affiliateRev"></td>
                                </tr>

                                <tr>
                                    <td>Personal Site Revenue</td>
                                    <td id="personalRev"></td>
                                </tr>

                                <tr>
                                    <td>Floria Revenue</td>
                                    <td id="floriaRev"></td>
                                </tr>

                                <tr>
                                    <td>Other Revenue</td>
                                    <td id="otherRev"></td>
                                </tr>

                                <tr>
                                    <td>Total Revenue</td>
                                    <td id="totalRev"></td>
                                </tr>

                                <tr>
                                    <td>Base Comision</td>
                                    <td id="baseCom"></td>
                                </tr>

                                <tr>
                                    <td>90h bonus</td>
                                    <td id="bonus90h"></td>
                                </tr>

                                <tr>
                                    <td>100h bonus</td>
                                    <td id="bonus100h"></td>
                                </tr>

                                <tr>
                                    <td>10.000$ bonus</td>
                                    <td id="bonus10k"></td>
                                </tr>

                                <tr>
                                    <td>Own studio bonus</td>
                                    <td id="bonusStudio"></td>
                                </tr>

                                <tr>
                                    <td>Total comision</td>
                                    <td id="totalCom"></td>
                                </tr>

                                <tr>
                                    <td>Deductions</td>
                                    <td id="deductions"></td>
                                </tr>

                                <tr>
                                    <td>Brute revenue</td>
                                    <td id="bruteRev"></td>
                                </tr>

                                <tr>
                                    <td>Amount paid</td>
                                    <td id="amountPaid"></td>
                                </tr>

                                <tr>
                                    <td>Exchange $/€</td>
                                    <td id="exchange"></td>
                                </tr>

                                <tr>
                                    <td>Total EUR</td>
                                    <td id="totalEur"></td>
                                </tr>

                                <tr>
                                    <td>Payment Date</td>
                                    <td id="paymentDate"></td>
                                </tr>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>


<script>

    let period = $("#periods").val();
    let year = '{{  $y }}';
    let studio = $("#myStudio").children("option:selected").val();

    $( document ).ready(function() {

        myTable();

        $('#allPeriods tbody').on( 'click', 'tr', function () {

            let data = table.row( this ).data();
            makePaymentModal(data);

        } );




//        tableBody.on('click', 'tr', function () {
//            let data = table.row( this ).data();
//            console.log(data);
//            window.open("/admin/model/" + data.id);
//
//        } );

    });



    function changeYear(y){
        year = y;
        $('.years').removeClass('btn-success').addClass('btn-warning');
        $('#'+y).removeClass('btn-warning').addClass('btn-success');
        period = 1;
        $("#periods").val(period);
        table.ajax.reload();
    }

    function makePaymentModal(pay){

        console.log(pay);

        $('#modelHeading').text('Period: ' + pay.period_name.name);

        $('#jasminRev').text(pay.jasmin_amount + ' $');
        $('#affiliateRev').text(pay.affliate_rev + ' $');
        $('#personalRev').text(pay.personal_rev + ' $');
        $('#floriaRev').text(pay.floria_rev + ' $');
        $('#otherRev').text(pay.others_rev + ' $');
        $('#totalRev').text('--');
        $('#baseCom').text(pay.commission_base + ' %');
        $('#bonus90h').text(pay.commission_90h + ' %');
        $('#bonus100h').text(pay.commission_100h + ' %');
        $('#bonus10k').text(pay.commission_10k + ' %');
        $('#bonusStudio').text(pay.commission_location + ' %');
        $('#totalCom').text(pay.commission_total + ' %');
        $('#deductions').text(pay.deductions + ' $');
        $('#bruteRev').text('--');
        $('#amountPaid').text(pay.pay_amount + ' $');
        $('#exchange').text(pay.exchange);
        $('#totalEur').text(pay.total_amount_eur + ' €');
        $('#paymentDate').text(pay.pay_date);


        $('#ajaxModel').modal('show');

    }

    $("#periods").change(function () {
        period = $("#periods").val();
        table.ajax.reload();

    });

    function myTable() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        table = $('#allPeriods').DataTable({
            processing: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            orderClasses: false,
            deferRender: true,
            serverSide: true,
            dom: 'Blfrtip',
            buttons: [
                'excel'
            ],
            ajax: {
                url: '/admin/paymenthistory',
                type: 'POST',
                data: function (d) {
                    d.period = period;
                    d.year = year;
                    d.studio = studio;
                },
//                "dataSrc": function ( json ) {
//                    console.log(json);
//                    return json;
//                }
            },
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            filters: [3],
            columns: [
                {data: 'id', name: 'id'},
                {data: 'period_name.name', name: 'period', searchable: true, orderable: true},
                {data: 'model.modelname', name: 'model.modelname', searchable: true, orderable: true},
                {data: 'studio.name', name: 'studio.name', searchable: false, orderable: false},
                {data: 'jasmin_amount', name: 'jasmin_amount', searchable: false},
                {data: 'total_amount_usd', name: 'total_amount_usd', searchable: false},
                {data: 'total_amount_eur', name: 'total_amount_eur', searchable: false},
                {data: 'commission_total', name: 'commission_total', searchable: false},
                {data: 'exchange', name: 'exchange'},
                {data: 'hours', name: 'hours', searchable: false, orderable: false},
                {data: 'pay_done', name: 'pay_done', searchable: false },
                {data: 'pay_amount', name: 'pay_amount', searchable: false },
                {data: 'pay_date', name: 'pay_date', searchable: false, orderable: false},
            ],

            columnDefs: [ {
                "targets": 10,
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        let text = 'Make Payment';
                        let cls = 'badge-danger';
                        if( parseInt(data) === 1){
                            cls = 'badge-success';
                            text = 'Payment Made';
                        }
                        data = '<span class="badge '+cls+'">'+text+'</span>';
                    }
                    return data;
                }
            },{
                "targets": [4,5],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' $';
                    }
                    return data;
                }
            },{
                "targets": [6],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' €';
                    }
                    return data;
                }
            },{
                "targets": [7],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' %';
                    }
                    return data;
                }
            },{
                "targets": [9],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = convertSeconds(data);
                    }
                    return data;
                }
            },{
                "targets": [11],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        if (data) {
                            data = (data) ? data : 0;
                            data = data + ' €';
                        }

                    }
                    return data;
                }
            },{
                "targets": [12],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data.split(" ")[0];
                    }
                    return data;
                }
            } ]
        });

    }

    function convertSeconds(totalSeconds){

        hours = (Math.floor(totalSeconds / 3600));
        totalSeconds %= 3600;
        minutes = (Math.floor(totalSeconds / 60));
        seconds = (totalSeconds % 60);

        return hours+":"+minutes+":"+seconds;

    }



</script>
@endpush