@extends('layouts.admin_app', ['activePage' => 'closeperiod', 'titlePage' => __('Close Period')])

@push('head')

@endpush

@section('content')
    <style>
        .warning {
            background: #ffe1f1!important;
        }
        .daySpan{
            /*width:10px;*/
            /*height:10px;*/
            background: white;
            vertical-align: middle;
            color:black;
            display: inline-block;
            font-weight: 400;
            font-size: 1rem;
            padding: .375rem .75rem;
            line-height: 1.5;
            border-radius: .25rem;
        }
        /*.success {*/
        /*background: #99cc00!important;*/
        /*}*/
    </style>

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">

                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            View And Correct Period
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress" style="height: 0.4rem;display:none">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        @foreach($years as $year)

                                            <button id="{{$year}}" class="years btn @if ($year == $y) btn-success @else btn-warning @endif" onclick="changeYear({{$year}})">{{$year}}</button>

                                        @endforeach
                                    </div>
                                    <div id="daysTab" class="col-lg-10" style="padding-top: 5px;">

                                    </div>
                                </div>
                                {!! Form::open(['url' => '/admin/closeperiod/'.date('Y'), 'id' => 'target']) !!}

                                {!! Form::select('periods', getAllPeridos(), $p, [ 'class' => 'form-control col-lg-2', 'id' => 'periods' ]) !!}

                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                    <div class="card-body table-responsive">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="1a">
                                <table id="allPeriods" class="data-table table  table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        {{--<th>Id</th>--}}
                                        <th>Period</th>
                                        <th>Name & Surname</th>
                                        {{--<th>Last Name</th>--}}
                                        <th>Modelname</th>
                                        <th>Hours</th>
                                        <th>Total Jasmin</th>
                                        <th>Studio</th>
                                        <th>Total</th>
                                        <th>P. Status</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" class="form-horizontal">
                        <input type="hidden" name="pH_id" id="pH_id">
                        <div class="form-group">
                            <label for="date" class="">Date</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="date" name="date"disabled  placeholder="Enter Name" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="hours" class="">Hours</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="hours" name="hours" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="exchange" class="">Exchange Rate</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="exchange" name="exchange" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="payUsd" class="">Payment USD</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="payUsd" name="payUsd" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="payEur" class="">Payment EUR</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="payEur" name="payEur" disabled required="">
                            </span>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Close Period
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>

<script>

    let period = $("#periods").val();
    let periodName = $("#periods").children("option:selected").text();
    let year = '{{  $y }}';
    let studio = $("#myStudio").children("option:selected").val();
    let studioName = $("#myStudio").children("option:selected").text();
    let index = 0;
    let dTab = $("#daysTab");

    let table = $('#allPeriods').DataTable( {
        "paging": true,
        "stateSave": true,
        "processing": true,
        dom: 'Blfrtip',
        buttons: [{
                extend: 'excel',
                exportOptions: {
                    columns: [ 1, 2, 3, 4, 5 ]
                },
                filename: function(){
                return studioName + '-' + periodName;
            },
            },
            {
                text: 'Check Period',
                action: function ( e, dt, node, config ) {
                    checkPeriodStudioJasmin();
                }
            },{
                text: 'Update Period',
                action: function ( e, dt, node, config ) {
                    updatePeriodForModels();
                }
            }
        ],
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},

        "serverSide": true,
        "aLengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "ajax": {
            'url':'/admin/closeperiod',
            "type": "POST",
            "data": function (d) {
                d.period = period;
                d.year = year;
                d.studio = studio;
            },
            'headers': {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
//            "dataSrc": function ( json ) {
//                console.log(json);
//                return json;
//            }
        },
        "columns": [
//            { data: 'id', name: 'id' },
            { data: 'period_name.name', name: 'period' },
            { data: 'full_name', name: 'full_name' },
//            { data: 'profile.last_name', name: 'profile.last_name' },
            { data: 'model.modelname', name: 'model.modelname', searchable: true },
            { data: 'hours', name: 'hours' },
            { data: 'total_amount_jasmin', name: 'total_amount_jasmin' },
            { data: 'studio.name', name: 'studio.name', searchable: true },
            { data: 'total_amount', name: 'total_amount' },
            { data: 'closed', name: 'closed', searchable: true },
            { data: 'created_at', name: 'created_at' },
        ],
        "iDisplayLength": -1,
        "bStateSave": true,
        "filters": [3],
        "columnDefs": [{
            "targets": [3],
            render: function (data, type, row, meta)
            {
                if (type === 'display')
                {
                    data = convertSeconds(data);
                }
                return data;
            }
        }, {
            "targets": 7,
            render: function (data, type, row, meta)
            {
                if (type === 'display')
                {
                    let text = 'Closed';
                    let cls = 'btn-success';
                    if( parseInt(data) === 0){
                        cls = 'btn-danger';
                        text = 'Open';
                    }
                    data = '<button onclick="closeOpenPeriod('+row.id+')" class="btn '+cls+'">'+text+'</button>';
                }
                return data;
            }
        },{
                "targets": [8],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data.split(" ")[0];
                    }
                    return data;
                }
            }
        ],
        rowCallback: function (row, data) {
            //console.log(data);
            if ( data.total_amount_jasmin !== data.total_amount) {
                $(row).addClass('warning');
            }//else $(row).addClass('success');

        }

    } );


    $( document ).ready(function() {


    });

    function updatePeriodForModels(){

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/updateSalesPeriod',
            data:{period:period, year:year, index:index},

            success: function (data) {
                console.log(data);
                $(".progress").hide();

                //get day
                //append day to tab
                //call func for next day & check if last day
                let max = data.count_days;
                let day = data.day;
                let d = $("<span/>", {text:day, "class":"daySpan"});
                dTab.append(d);

                index++;
                if (index < max) updatePeriodForModels();



            }

        });
    }


    function checkPeriodStudioJasmin(){

        //check for models with periods that are not closed

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/checkSales',
            data:{period:period, year:year},

            success: function (data) {
                //console.log(data);
                $(".progress").hide();
                table.ajax.reload();
            }

        });


    }

    function ajaxDataCall(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/closeperiod',
            data:{studio:studio, period:period, year:year},

            success: function (data) {
                //console.log(data);
                periodData = data;
                tableDraw();
            }

        });

    }

    function changeYear(y){
        year = y;
        $('.years').removeClass('btn-success').addClass('btn-warning');
        $('#'+y).removeClass('btn-warning').addClass('btn-success');
        period = 1;
        $("#periods").val(period);
        table.ajax.reload();
    }

    function closeOpenPeriod(period_id){

        window.open("/admin/closeperiods/"+period_id, '_blank');

    }

    $("#periods").change(function () {
        period = $("#periods").val();
        periodName = $("#periods").children("option:selected").text();
        table.ajax.reload();

    });

    $("#studioSelect").change(function () {

        studio = $("#myStudio").children("option:selected").val();
        studioName = $("#myStudio").children("option:selected").text();
    });

    function convertSeconds(totalSeconds){

        hours = (Math.floor(totalSeconds / 3600));
        totalSeconds %= 3600;
        minutes = (Math.floor(totalSeconds / 60));
        seconds = (totalSeconds % 60);

        return hours+":"+minutes+":"+seconds;

    }


</script>
@endpush