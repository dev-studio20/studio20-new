@extends('layouts.admin_app', ['activePage' => 'closeperiod', 'titlePage' => __('Close Period')])

@push('head')

@endpush

@section('content')
    <style>
        .warning {
            color: #ff9800;
        }
    </style>

    <div class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <div style="position: relative;float: right;">Key: {{ $model->key_id }}</div>
                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Sales for <span class="warning">{{ $model->modelname }}</span>
                                            | Period <span class="warning">{{ get_period($period->period) }}</span>
                                            | Status: <span id="periodStatus" class="warning">{{[0 => 'OPEN', 1 => 'CLOSED'][$period->closed]}}</span>
                                            <br> Total: <span
                                                    class="warning" id="totalSale">{{ $sales->sum('sum') }} $</span>
                                            <br> Hours: <span
                                                    class="warning" id="totalHours">{{ convertSecondsToHours($sales->sum('hours')) }}</span>
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>

                                <div class="progress" style="height: 0.4rem;display:none">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        {{--<button id="btnStatus" onclick="closePeriod()" class="{{ ['btn btn-success','btn btn-danger'][$period->closed] }}" style="margin-top: 20px">{{ ['CLOSE PERIOD','OPEN PERIOD'][$period->closed] }}</button>--}}
                                    </div>
                                    <div class="col-lg-4">
                                        <a class="btn btn-danger" id="showStatusBtn"
                                           style="width: 100%;margin-top: 20px;display:none">Period Closed!</a>
                                        <a onclick="retriveSale2()" class="btn btn-warning hide"
                                           style="width: 100%;margin-top: 20px">Retrive All Sales This Period!</a>
                                    </div>
                                    <div class="col-lg-4">
                                        <a onclick="window.history.back();" class="btn btn-info"
                                           style="margin-top: 20px;float:right">Back</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="tab-content">


                            <div class="tab-pane active show">

                                <div class="row">

                                    <div class="col-lg-6 col-md-12">

                                        <table class="table table-hover data-table">
                                            <thead class="text-warning">
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Sum</th>
                                                <th>Hours</th>
                                                <th class="hide">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody id="salesTable">

                                            @foreach($sales as $sale)

                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $sale->year }}-{{ decimal($sale->month) }}-{{ decimal($sale->day) }}</td>
                                                    <td  id="sum{{$sale->id}}">{{ $sale->sum }} {{ getCurrency() }}</td>
                                                    <td  id="hours{{$sale->id}}">{{ convertSecondsToHours($sale->hours )}}</td>
                                                    <td class="hide">
                                                        <button class="btn btn-success open-modal" value="{{$sale->id}}">Edit</button>
                                                        <button onclick="deleteSale('{{$sale->id}}')" class="btn btn-danger">Delete</button>
                                                        <button onclick="retriveSale('{{$sale->id}}','{{ $sale->year }}', '{{ $sale->month }}', '{{ $sale->day }}')" class="btn btn-warning">Get Sale</button>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>

                                        </table>

                                        <br>
                                        <hr>
                                        {{--<button class="btn btn-info">Add Sale</button>--}}


                                    </div>

                                    <div class="col-lg-6 col-md-12">
                                        <div class="box box-primary">

                                            {{ Form::open(['url' => url("/admin/closeperiodid/$periodid")]) }}

                                            {!! Form::label('Base Comision %') !!}
                                            {!! Form::text('base', null, [ 'class' => 'form-control rev', 'id' => 'base', 'required']) !!}
                                            <br>
                                            {{ Form::label('commission_90h' ,'Commission 90h (2%)') }}
                                            {{ Form::checkbox('commission_90h', null, null, ['class' => 'check']) }}
                                            <br>
                                            {{ Form::label('commission_100h' ,'Commission 100h (5%)') }}
                                            {{ Form::checkbox('commission_100h', null, null, ['class' => 'check']) }}
                                            <br>
                                            {{ Form::label('commission_10k' ,'Commission 10.000$ (5%)') }}
                                            {{ Form::checkbox('commission_10k', null, null, ['class' => 'check']) }}
                                            <br>
                                            {{ Form::label('commission_loc' ,'Commission location (30%)') }}
                                            {{ Form::checkbox('commission_loc', null, null, ['class' => 'check']) }}
                                            <br>
                                            {{ Form::label('comisiontotal','Total Commission %') }}
                                            {{ Form::text('comisiontotal', 0, [ 'class' => 'form-control', 'placeholder' => 0, 'disabled' => 'disabled' ]) }}

                                            {{ Form::label('affrev','Affiliate Revenue') }}
                                            {{ Form::text('affrev', 0, [ 'class' => 'form-control rev', 'placeholder' => 0 ]) }}

                                            {{ Form::label('siterev','Personal Site Revenue') }}
                                            {{ Form::text('siterev', 0, [ 'class' => 'form-control rev', 'placeholder' => 0 ]) }}

                                            {{ Form::label('floriarev','Floria Revenue') }}
                                            {{ Form::text('floriarev', 0, [ 'class' => 'form-control rev', 'placeholder' => 0 ]) }}

                                            {{ Form::label('otherrev','Other Revenues') }}
                                            {{ Form::text('otherrev', 0, [ 'class' => 'form-control rev', 'placeholder' => 0 ]) }}

                                            {{ Form::label('deductions','Deductions') }}
                                            {{ Form::text('deductions', 0, [ 'class' => 'form-control rev', 'placeholder' => 0 ]) }}

                                            {{ Form::label('paydue','Payment Due $') }}
                                            {{ Form::text('paydue', null, [ 'class' => 'form-control', 'readonly']) }}

                                            {{ Form::label('cursvalutar','Curs Eur/Usd') }}
                                            {{ Form::text('cursvalutar', 0.87, [ 'class' => 'form-control rev', 'placeholder' => 0.87 ]) }}

                                            {{ Form::label('paydueeur','Payment Due Eur') }}
                                            {{ Form::text('paydueeur', null, [ 'class' => 'form-control ', 'readonly']) }}
                                            <br>
                                            {{ Form::label('notification','Notification') }}
                                            {{ Form::checkbox('notification') }}
                                            <br>
                                            <hr>
                                            {{ Form::submit('Close Period!', [ 'class' => ' btn btn-info hide', 'id' => 'btnRegisterPay' ]) }}

                                            {{ Form::close() }}

                                            <button id="openPeriod" style="display:none;" class="btn btn-danger" onclick="openPeriod()">Open Period</button>

                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>

    <div class="modal fade" id="saleEditorModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="saleEditorModalLabel">Edit Sale for {{ $model->modelname }}</h4>
                </div>
                <div class="modal-body">
                    <p id="selectedDay"></p>
                    <form id="modalFormData" name="modalFormData" class="form-horizontal" novalidate="">
                        <input type="hidden" id="saleId" name="saleId" value="">

                        <div class="form-group">
                            <div class="bmd-form-group is-filled">
                                <span>Sum</span>
                                <input type="text" class="form-control col-sm-6" id="sum" name="sum"
                                       placeholder="" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="bmd-form-group col-sm-3">
                                    <span>Hours</span>
                                    <input type="text" class="form-control" id="hours" name="hours"
                                           placeholder="hours" value="">
                                </div>
                                <div class="bmd-form-group col-sm-3">
                                    <span>Minutes</span>
                                    <input type="text" class="form-control" id="minutes" name="minutes"
                                           placeholder="minutes" value="">
                                </div>
                                <div class="bmd-form-group col-sm-3">
                                    <span>Seconds</span>
                                    <input type="text" class="form-control" id="seconds" name="seconds"
                                           placeholder="seconds" value="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" value="add" onclick="updateSale()">Save changes</button>
                    {{--<input type="hidden" id="sale_id" name="sale_id" value="0">--}}
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
<script>

    let domain = '{{ url('/')  }}';
    let daysInPeriod = {!! json_encode($daysInPeriod) !!};
    let i = 0;

    let status = '{{$period->closed}}';

    let period_id = '{{$period->id}}';

    let total = '{{ $period->total_amount }}';

    let btnPay = $("#btnRegisterPay");

    let base = $("#base");
    let commission_90h = $("#commission_90h");
    let commission_100h = $("#commission_100h");
    let commission_10k = $("#commission_10k");
    let commission_loc = $("#commission_loc");
    let comisiontotal = $("#comisiontotal");

    let affrev = $("#affrev");
    let siterev = $("#siterev");
    let floriarev = $("#floriarev");
    let otherrev = $("#otherrev");
    let deductions = $("#deductions");

    let paydue = $("#paydue");
    let cursvalutar = $("#cursvalutar");
    let paydueeur = $("#paydueeur");

    let cls_check = $(".check");
    let cls_revenue = $(".rev");

    let modelname = '{{$model->modelname}}';

    function updateTotal(totalSum, totalHours){
        $("#totalSale").text(totalSum);
        $("#totalHours").text(totalHours);
        total = totalSum;
    }

    function retriveSale(saleid, year, month, day){

        let date = year + '-' + decimal(month) +'-' + decimal(day);

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            data:{modelname: modelname, date: date},
            url: '/admin/getsalesmodel',
            success: function (data) {
                console.log("data:");
                console.log( data);

                let formData = {
                    sum: data.earnings,
                    time: data.workTime,
                    day: data.day,
                    modelname: data.screenName
                };

                ajaxUpdateSale(formData, 'local');

                //location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });
    }

    function updateSale(){

        let sum = $('#sum').val();
        let hours = $('#hours').val();
        let minutes = $('#minutes').val();
        let sec = $('#seconds').val();

        let d = filterData(sum, hours, minutes, sec);
        sum = d[0];
        hours = parseInt(d[1]);
        minutes = parseInt(d[2]);
        sec = parseInt(d[3]);
        let myday = $('#selectedDay').text();

        let seconds = (hours * 3600) + (minutes * 60) + sec;

        let formData = {
            sum: sum,
            time: seconds,
            day: myday,
            modelname: modelname
        };

        ajaxUpdateSale(formData, 'local');

    }

    function ajaxUpdateSale(formData, type){

        console.log(formData);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: '/admin/viewsale/update',
            data: formData,
            success: function (data) {

                console.log(data);

                $('#modalFormData').trigger("reset");
                $('#saleEditorModal').modal('hide');

                let totalSum = data.totalSum;
                let totalHours = data.totalHours;

                let id = data.sale_id;
                let sum = data.sum;
                let hours = data.hours;
                //update UI
                updateTotal(totalSum, totalHours);

                if (type === 'local') {

                    $('#sum'+id).text(sum);
                    $('#hours'+id).text(hours);

                    $(".progress").hide();
                    //location.reload();

                } else {
                    createAndInsert(i, data.year, data.month, data.day, data.sum, data.hours, data.sale_id );
                    if (i < daysInPeriod.length) retriveSale2();
                }


            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    function retriveSale2(){

        let date = daysInPeriod[i];
        if (!i) $("#salesTable").empty();

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            data:{modelname: modelname, date: date},
            url: '/admin/getsalesmodel',
            success: function (data) {
                console.log("data:");
                console.log( data);

                $(".progress").hide();
                i++;
                let formData = {
                    sum: data.earnings,
                    time: data.workTime,
                    day: data.day,
                    modelname: modelname
                };

                ajaxUpdateSale(formData, 'api');

            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });
    }

    function createAndInsert(index, year, month, day, sum, hours, saleid ){

        let tr = $("<tr/>",{});
        let td1 = $("<td/>",{text: index});
        let td2 = $("<td/>",{text: year + '-' + decimal(month) + '-' + decimal(day)});
        let td3 = $("<td/>",{text: sum, "id":"sum"+saleid});
        let td4 = $("<td/>",{text: hours, "id":"hours"+saleid});
        let td5 = $("<td/>",{});
        let btnEdit = $("<button/>",{text: 'EDIT', "class":"btn btn-success open-modal", value:saleid});
        let btnDelete = $("<button/>",{text: 'DELETE', "class":"btn btn-danger", on:{click:function() {deleteSale(saleid);}}});
        let btnRetrive = $("<button/>",{text: 'GET SALE', "class":"btn btn-warning", on:{click:function() {retriveSale(saleid, year, month, day);}}});
        td5.append(btnEdit,btnDelete,btnRetrive);
        tr.append(td1,td2,td3,td4,td5);
        $("#salesTable").append(tr);

    }

    function deleteSale(saleid){


        $(".progress").show();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "DELETE",
            url: domain + '/admin/viewsale/' + saleid,
            success: function (data) {
                $(".progress").hide();
                console.log(data);
                if (data.success){
                    //update UI
                    $("#sum"+saleid).text(data.sum);
                    $("#hours"+saleid).text(data.hours);

                    let totalSum = data.totalSum;
                    let totalHours = data.totalHours;

                    updateTotal(totalSum, totalHours);

                    //location.reload();
                } else alert('error!');


            },
            error: function (data) {
                $(".progress").hide();
                console.log('Error:', data);
            }
        });
    }

    $('body').on('click', '.open-modal', function () {
        let sale_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'GET',
            url: '/admin/viewsale/' + sale_id,
            success: function (data) {

                console.log(data);
                $('#saleId').val(sale_id);
                let selectedDay = decimal(data.year) + "-" + decimal(data.month) + "-" + decimal(data.day);
                $('#selectedDay').text(selectedDay);
                let arr = convertToHMS(data.hours);
                let hours = arr[0];
                let minutes = arr[1];
                let seconds = arr[2];
                $('#sum').val(data.sum);
                $('#hours').val(hours);
                $('#minutes').val(minutes);
                $('#seconds').val(seconds);
                $('#btn-save').val("update");
                $('#saleEditorModal').modal('show');

                $("#sum").inputFilter(function(value) {
                    return /^-?\d*[.,]?\d{0,2}$/.test(value); });

                $("#hours").inputFilter(function(value) {
                    return /^\d*$/.test(value); });

                $("#minutes").inputFilter(function(value) {
                    return /^\d*$/.test(value) && (value === "" || parseInt(value) < 60); });

                $("#seconds").inputFilter(function(value) {
                    return /^\d*$/.test(value) && (value === "" || parseInt(value) < 60); });

            }

        });

    });

    function decimal(dec){

        dec = parseInt(dec);

        return (dec < 10) ? '0' + dec : dec;
    }

    // Restricts input for each element in the set of matched elements to the given inputFilter.
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    function convertToHMS(totalSeconds){

        hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        minutes = Math.floor(totalSeconds / 60);
        seconds = totalSeconds % 60;

        return [hours, minutes, seconds];

    }


    function filterData(sum, hours, minutes, sec){

        let newsum = sum;
        if (sum.includes(',')) newsum = sum.replace(/,/g, ".");
        if (!hours) hours = '00';
        if (!minutes) minutes = '00';
        if (!sec) sec = '00';

        return [newsum, hours, minutes, sec];

    }

    $(document).ready(function () {

        base.focus();

        //btnPay.attr("disabled", "disabled");
        //btnPay.removeClass().addClass("btn btn-danger");

        if (status === '1') {
            //period is closed disable all interactions
            disableAll();
        }

        cls_check.on('click', function(){
            recalculate();
        })

    });

    cls_revenue.keyup(function () {
        recalculate();
    });

    function disableAll(){

        $(".hide").hide();
        $("#showStatusBtn").show();
        $("#openPeriod").show();

    }

    function openPeriod(){

        $(".progress").show();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            data:{period_id: period_id},
            url: '/admin/openperiod',
            success: function (data) {
                console.log("data:");
                console.log( data);

                $(".progress").hide();

                $(".hide").show();
                $("#showStatusBtn").hide();
                $("#openPeriod").hide();



            },
            error: function (data) {
                console.log('Error:', data);
                $(".progress").hide();
            }
        });


    }

    function recalculate(){

        let totalcomis = parseInt(base.val());
        console.log(totalcomis);

        totalcomis += (commission_90h.is(':checked')) ? 2 : 0;
        totalcomis += (commission_100h.is(':checked')) ? 5 : 0;
        totalcomis += (commission_10k.is(':checked')) ? 5 : 0;
        totalcomis += (commission_loc.is(':checked')) ? 30 : 0;

        comisiontotal.val(totalcomis);
        console.log("total comision:" + totalcomis);
        console.log("other :" + otherrev.val());

        //add commission to fields etc
        let totalAdded = ( (parseInt(total) + parseInt(affrev.val()) + parseInt(siterev.val()) + parseInt(floriarev.val()) ) * totalcomis / 100) + parseInt(otherrev.val()) - parseInt(deductions.val());
        //totalAdded = Math.round(totalAdded);
        paydue.val(Math.round(totalAdded * 100) / 100);
        paydueeur.val(Math.round(totalAdded * cursvalutar.val() * 100) / 100);

    }


</script>
@endpush