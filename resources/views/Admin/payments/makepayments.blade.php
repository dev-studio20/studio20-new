@extends('layouts.admin_app', ['activePage' => 'makepayment', 'titlePage' => __('Make Payment')])

@push('head')

@endpush

@section('content')
    <style>
        .warning {
            color: #ff9800;
        }
    </style>

    <div class="content" style="background: #fff;">

        <div class="container-fluid">
            <div class="row">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-primary">

                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">

                                <ul class="nav nav-tabs" data-tabs="tabs">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#1a" data-toggle="tab">
                                            Make Payments
                                            <div class="ripple-container"></div>
                                        </a>
                                    </li>
                                </ul>
                                <div class="progress" style="height: 0.4rem;display:none">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2">
                                        {{--@foreach($years as $year)--}}

                                            {{--<button id="{{$year}}" class="years btn @if ($year == $y) btn-success @else btn-warning @endif" onclick="changeYear({{$year}})">{{$year}}</button>--}}

                                        {{--@endforeach--}}
                                    </div>
                                </div>
                                {{--{!! Form::open(['url' => '/admin/closeperiod/'.date('Y'), 'id' => 'target']) !!}--}}

                                {{--{!! Form::select('periods', getAllPeridos(), $p, [ 'class' => 'form-control col-lg-2', 'id' => 'periods' ]) !!}--}}

                                {{--{!! Form::close() !!}--}}
                            </div>
                        </div>

                    </div>
                    <div class="card-body table-responsive">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="1a">
                                <table id="allPeriods" class="data-table table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Period</th>
                                        <th>Modelname</th>
                                        <th>Studio</th>
                                        <th>Total Jasmin</th>
                                        <th>Total USD</th>
                                        <th>Total EUR</th>
                                        <th>Com. T</th>
                                        <th>Rate $/€</th>
                                        <th>Hours</th>
                                        <th>Action</th>
                                        <th>Pay Amount</th>
                                        <th>Pay Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="productForm" name="productForm" class="form-horizontal">
                        <input type="hidden" name="pH_id" id="pH_id">
                        <div class="form-group">
                            <label for="date" class="">Date</label>
                            <span class="bmd-form-group is-filled">
                                <input type="date" class="form-control col-lg-12 col-sm-12" id="date" name="date"   placeholder="Enter Name" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="hours" class="">Hours</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="hours" name="hours" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="exchange" class="">Exchange Rate</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="exchange" name="exchange" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="payUsd" class="">Payment USD</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="payUsd" name="payUsd" disabled placeholder="" value="" maxlength="50" required="">
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="payEur" class="">Payment EUR</label>
                            <span class="bmd-form-group is-filled">
                                <input type="text" class="form-control col-lg-12 col-sm-12" id="payEur" name="payEur" disabled required="">
                            </span>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Make Payment
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script>

    //let period = $("#periods").val();
    //let year = '{ {  $y } }';
    let studio = $("#myStudio").children("option:selected").val();

    $( document ).ready(function() {

        myTable();

        $('#allPeriods tbody').on( 'click', 'tr', function () {

            let data = table.row( this ).data();
            //console.log(data);
            makePaymentModal(data);

        } );

        $('#saveBtn').click(function (e) {

            e.preventDefault();
            let ph_id = $("#pH_id").val();

            //$(this).html('Sending..');
            console.log(ph_id);

            let d = $('#date').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                data: {"pay_hystory_id":ph_id, "date":d},
                url: "{{ url('admin/makepaymentid') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {

                    if (data.success) {
                        $('#productForm').trigger("reset");
                        $('#ajaxModel').modal('hide');

                        table.draw();
                        md.showNotification('Success!', 'success');
                    }

                },
                error: function (data) {
                    console.log('Error:', data);
                    md.showNotification('Error:' + data, 'danger');
                    //$('#saveBtn').html('Save Changes');
                }
            });
        });


//        tableBody.on('click', 'tr', function () {
//            let data = table.row( this ).data();
//            console.log(data);
//            window.open("/admin/model/" + data.id);
//
//        } );

    });



    function changeYear(y){
        year = y;
        $('.years').removeClass('btn-success').addClass('btn-warning');
        $('#'+y).removeClass('btn-warning').addClass('btn-success');
        period = 1;
        $("#periods").val(period);
        table.ajax.reload();
    }

    function makePaymentModal(pay){

        //console.log(pay);

        $('#pH_id').val(pay.id);

        let n =  new Date();
        let y = n.getFullYear();
        let m = n.getMonth() + 1;
        let d = n.getDate();
        let res1 = y + "-" + decimal(m) + "-" + decimal(d);

        //let res = pay.created_at.split(" ");
        $('#date').val(res1);
        //$('#hours').val(payHistory.hours);
        $('#hours').val(convertSeconds(pay.hours));
        $('#payUsd').val(pay.total_amount_usd);
        $('#payEur').val(pay.total_amount_eur);
        $('#exchange').val(pay.exchange);

        $('#ajaxModel').modal('show');

    }

//    $("#periods").change(function () {
//        period = $("#periods").val();
//        table.ajax.reload();
//
//    });

    function myTable() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        table = $('#allPeriods').DataTable({
            processing: true,
            "language": {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            orderClasses: false,
            deferRender: true,
            serverSide: true,
            ajax: {
                url: '/admin/makepayment',
                type: 'POST',
                data: function (d) {
                    //d.period = period;
                    //d.year = year;
                    d.studio = studio;
                },
//                "dataSrc": function ( json ) {
//                    console.log(json);
//                    return json;
//                }
            },
            lengthMenu: [[25, 50, -1], [25, 50, "All"]],
            filters: [3],
            columns: [
                {data: 'id', name: 'id'},
                {data: 'period_name.name', name: 'period_name.name', searchable: true, orderable: true},
                {data: 'model.modelname', name: 'model.modelname', searchable: true, orderable: true},
                {data: 'studio.name', name: 'studio.name', searchable: true, orderable: true},
                {data: 'jasmin_amount', name: 'jasmin_amount', searchable: false},
                {data: 'total_amount_usd', name: 'total_amount_usd', searchable: false},
                {data: 'total_amount_eur', name: 'total_amount_eur', searchable: false},
                {data: 'commission_total', name: 'commission_total', searchable: false},
                {data: 'exchange', name: 'exchange'},
                {data: 'hours', name: 'hours', searchable: false, orderable: false},
                {data: 'pay_done', name: 'pay_done', searchable: false },
                {data: 'pay_amount', name: 'pay_amount', searchable: false },
                {data: 'pay_date', name: 'pay_date', searchable: false, orderable: false},
            ],

            columnDefs: [ {
                "targets": 10,
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        let text = 'Make Payment';
                        let cls = 'btn-danger';
                        if( parseInt(data) === 1){
                            cls = 'btn-success';
                            text = 'Payment Made';
                        }
                        data = '<button class="btn '+cls+'">'+text+'</button>';
                    }
                    return data;
                }
            },{
                "targets": [4,5],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' $';
                    }
                    return data;
                }
            },{
                "targets": [6],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' €';
                    }
                    return data;
                }
            },{
                "targets": [7],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = data + ' %';
                    }
                    return data;
                }
            },{
                "targets": [9],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        data = convertSeconds(data);
                    }
                    return data;
                }
            },{
                "targets": [11],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        if (data) {
                            data = (data) ? data : 0;
                            data = data + ' €';
                        }

                    }
                    return data;
                }
            },{
                "targets": [12],
                render: function (data, type, row, meta)
                {
                    if (type === 'display')
                    {
                        //data = data.split(" ")[0];
                    }
                    return data;
                }
            } ]
        });

    }

    function convertSeconds(totalSeconds){

        hours = (Math.floor(totalSeconds / 3600));
        totalSeconds %= 3600;
        minutes = (Math.floor(totalSeconds / 60));
        seconds = (totalSeconds % 60);

        return hours+":"+minutes+":"+seconds;

    }

    function decimal(dec){

        dec = parseInt(dec);

        return (dec < 10) ? '0' + dec : dec;
    }


</script>
@endpush