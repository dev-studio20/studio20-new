<footer class="footer">
  <div class="container-fluid">
    <nav class="float-left">
      <ul>

      </ul>
    </nav>
    <div class="copyright float-right">
      <b>&copy;Studio20</b> -
      <script>
        document.write(new Date().getFullYear())
      </script>
    </div>
  </div>
</footer>