<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Studio 20') }}</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material') }}/img/apple-icon.png">
    <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>--}}

    <link href="{{ asset('material') }}/css/material-dashboard.css" rel="stylesheet" />

    <link href="{{ asset('material') }}/demo/demo.css" rel="stylesheet" />
    <link href="{{ asset('material') }}/css/chartist.css" rel="stylesheet" />
    <link href="{{ asset('material') }}/css/chartist-plugin-tooltip.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    @stack('head')

</head>
<body class="{{ $class ?? '' }}">
<div class="wrapper ">
    @include('layouts.navbars.admin_sidebar')
    <div class="main-panel">
        @include('layouts.navbars.navs.auth')
        @yield('content')
        @include('layouts.footers.auth')
    </div>
</div>


<!--   Core JS Files   -->

<script src="{{ asset('material') }}/js/core/jquery.min.js"></script>
<script src="{{ asset('material/js/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('material/js/jquery-ui.multidatespicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('material/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ asset('material') }}/js/core/popper.min.js"></script>
<script src="{{ asset('material') }}/js/core/bootstrap-material-design.min.js"></script>
<script src="{{ asset('material') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Plugin for the momentJs  -->
<script src="{{ asset('material') }}/js/plugins/moment.min.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="{{ asset('material') }}/js/plugins/sweetalert2.js"></script>
<!-- Forms Validations Plugin -->
<script src="{{ asset('material') }}/js/plugins/jquery.validate.min.js"></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="{{ asset('material') }}/js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="{{ asset('material') }}/js/plugins/bootstrap-selectpicker.js"></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="{{ asset('material') }}/js/plugins/bootstrap-datetimepicker.min.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src="{{ asset('material') }}/js/plugins/jquery.dataTables.min.js"></script>
{{--<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>--}}
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{ asset('material') }}/js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="{{ asset('material') }}/js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="{{ asset('material') }}/js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="{{ asset('material') }}/js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset('material') }}/js/plugins/nouislider.min.js"></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src="{{ asset('material') }}/js/plugins/arrive.min.js"></script>
<!-- Chartist JS -->
<script src="{{ asset('material') }}/js/plugins/chartist.min.js"></script>
<script src="{{ asset('material') }}/js/plugins/chartist-plugin-tooltip.js"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('material') }}/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{ asset('material') }}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
{{--<script src="{{ asset('material') }}/demo/demo.js"></script>--}}
<script src="{{ asset('material') }}/js/settings.js"></script>



{{--Pusher Notifications--}}
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<script>

    let notCount = $("#notificationCount");
    let notMenu = $("#navbarDropdownMenuLink");
    let notIds = [];
    // Enable pusher logging - don't include this in production
    //Pusher.logToConsole = true;

    let pusher = new Pusher('785e6444f60db91a62e0', {
        cluster: 'eu',
        disableStats: true,
        forceTLS: true
    });

    // Subscribe to the channel we specified in our Laravel Event

    let roleCh = '{{ (getChannelForRole()) }}';
     roleCh = roleCh.split(",");

    let channels = roleCh.map(channelName => pusher.subscribe(channelName));

    for (channel of channels) {

        //channel.bind('name', callback)
        //console.log("channel:");
//        if(channel.name === 'SYSTEM') {
//            // Bind a function to a Event
//            channel.bind('system-event', function(data) {
//
//                let message = data.message;
//                let color = data.color;
//                md.showNotification(message, color);
//
//            });
//        }

        channel.bind('system-event', function(data) {

            let message = data.message;
            let color = data.color;
            md.showNotification(message, color);

        });


    }


    // Bind a function to a Event
    //    channel.bind('test-event', function(data) {
    //
    //        console.log(JSON.stringify(data));
    //        let message = data.message;
    //        console.log(JSON.stringify(message));
    //        //ajaxNotification(message);
    //        md.showNotification(message, 'success');
    //
    //    });
    //    channel.bind('contract_generated-event', function(data) {
    //
    //        console.log(JSON.stringify(data));
    //        let message = data.message;
    //
    //        md.showNotification(message, 'success');
    //
    //    });

    $(document).ready(function () {

        notMenu.on('click', function(){
            //alert("Click!");
            console.log(notIds);

            //if notids is not null send ajax -> add read time to message
            //updateNotifications(JSON.stringify(notIds));
        });

    });

    function updateNotifications(elems){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/notificationsreadtime',
            data:{data:elems},

            success: function (data) {


            }

        });

    }

    function ajaxNotification(message) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/admin/notifications',
            data:{message:message},

            success: function (data) {

                if (data.success){
                    console.log(data.data);
                    showNotifications(data.data);

                } else {
                    console.log('some error..');
                }

            }

        });

    }

    function showNotifications(data){

        let size = data.length;
        if (size) {
            notCount.text(data.length);

            let notDropDown = $("#myDropdownMenuLink");
            notDropDown.empty();

            data.forEach(function myFunction(item, index) {

                let elem = $("<a/>", {"class":"dropdown-item", text:item.message});
                notDropDown.append(elem);
                notIds.push(item.id);

            });

        }


    }

</script>

@stack('js')
</body>
</html>