<div class="sidebar" data-color="danger" data-background-color="white" data-image="{{ asset('material/img/sidebar-1.jpg') }}">
 {{--mydd($activePage); --}}
    <div class="logo" style="padding: 10px;background:linear-gradient(0deg, rgba(255, 255, 255, 0.95), rgba(255, 255, 255, 0.95)), url(/material/img/studio20_logo.jpg);background-size: cover;">
        <a href="/admin" class="simple-text logo-normal">
            <h4>{{ __('Studio 20') }}</h4>
            <h6>{{Auth::guard('admin')->user()->name}}</h6>
        </a>
        @if ($activePage == 'modelstats')
            {!! Form::open(['' => '', 'id' => 'studioSelect']) !!}

            {!! Form::select('studios', getAllStudios(), adminStudio(), [ 'class' => 'form-control', 'id' => 'myStudio' ]) !!}

            {!! Form::close() !!}
        @else
            {!! Form::open(['' => '', 'id' => 'studioSelect']) !!}

            {!! Form::select('studios', getStudios(), adminStudio(), [ 'class' => 'form-control', 'id' => 'myStudio' ]) !!}

            {!! Form::close() !!}
        @endif

    </div>
    <div class="sidebar-wrapper">
        <input type="hidden" id="pageid" name="pageid" value="{{$activePage}}">
        <ul class="nav">
            {{--<li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">--}}
                {{--<a class="nav-link" href="{{ route('admin.home') }}">--}}
                    {{--<i class="material-icons">dashboard</i>--}}
                    {{--<p>{{ __('Dashboard') }}</p>--}}
                {{--</a>--}}
            {{--</li>--}}

            @if(in_array(getRole(),[1,2,3,4]))
            @php $modelsPages=["models", "pendingmodels", "suspendedmodels", "rejectedmodels", "inactivemodels", "approvedmodels"] @endphp
            <li class="nav-item {{ in_array($activePage, $modelsPages) ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelModels" aria-expanded="true">
                    {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
                    <i class="material-icons">supervisor_account</i>
                    <p>{{ __('Models') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse {{ in_array($activePage, $modelsPages) ? ' show' : '' }}" id="laravelModels">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'models' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/allmodels') }}">
                                {{--<span class="sidebar-mini"> M </span>--}}
                                <i class="material-icons">supervisor_account</i>
                                <span class="sidebar-normal">{{ __('All Models') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'approvedmodels' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/approvedmodels') }}">
                                {{--<span class="sidebar-mini"> P </span>--}}
                                <i class="material-icons">perm_identity</i>
                                <span class="sidebar-normal">{{ __('Models Approved') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'pendingmodels' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/pendingmodels') }}">
                                {{--<span class="sidebar-mini"> P </span>--}}
                                <i class="material-icons">schedule</i>
                                <span class="sidebar-normal">{{ __('Models Pending') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'suspendedmodels' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/suspendedmodels') }}">
                                {{--<span class="sidebar-mini"> S </span>--}}
                                <i class="material-icons">pause_circle_outline</i>
                                <span class="sidebar-normal">{{ __('Models Suspended') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'rejectedmodels' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/rejectedmodels') }}">
                                {{--<span class="sidebar-mini"> R </span>--}}
                                <i class="material-icons">highlight_off</i>
                                <span class="sidebar-normal">{{ __('Models Rejected') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'inactivemodels' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/inactivemodels') }}">
                                {{--<span class="sidebar-mini"> I </span>--}}
                                <i class="material-icons">hourglass_empty</i>
                                <span class="sidebar-normal">{{ __('Models Inactive') }} </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            @endif

            @if(in_array(getRole(),[1,3,4]))
            @php $paymentsPages=["insertsales", "closeperiod", "makepayments","paymenthistory"] @endphp
            <li class="nav-item {{ in_array($activePage, $paymentsPages) ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelPayments" aria-expanded="true">
                    {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
                    <i class="material-icons">credit_card</i>
                    <p>{{ __('Payments') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelPayments">
                    <ul class="nav">
                        {{--<li class="nav-item{{ $activePage == 'insertsales' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('admin.insertsales') }}">--}}
                                {{--<span class="sidebar-mini"> I </span>--}}
                                {{--<span class="sidebar-normal">{{ __('Insert Sales') }} </span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        <li class="nav-item{{ $activePage == 'closeperiod' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ url('admin/closeperiod') }}">
                                <i class="material-icons">lock_open</i>
                                <span class="sidebar-normal">{{ __('Close Periods') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'makepayment' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.makepayment') }}">
                                <i class="material-icons">payment</i>
                                <span class="sidebar-normal">{{ __('Make Payments') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'paymenthistory' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.paymenthistory') }}">
                                <i class="material-icons">payment</i>
                                <span class="sidebar-normal">{{ __('Payments History') }} </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            @endif

            @if(in_array(getRole(),[1,2,3,4]))
            @php $messsagePages=["inbox", "composemessage", "messagessent", "solved"] @endphp
            <li class="nav-item {{ in_array($activePage, $messsagePages) ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelMessages" aria-expanded="false">
                    {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
                    <i class="material-icons" style="{{msgNotReadAdmin()}}">speaker_notes</i>
                    <p>{{ __('Messages') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelMessages">
                    <ul class="nav">
                        <li class="nav-item{{ $activePage == 'inbox' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.inbox') }}">
                                {{--<span class="sidebar-mini"> I </span>--}}
                                {{--msgNotReadAdmin()--}}
                                {{--<span id="notificationCount" class="notification">0</span>--}}
                                <i class="material-icons">inbox</i>
                                <span class="sidebar-normal">{{ __('Inbox') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'composemessage' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.composemessage') }}">
                                {{--<span class="sidebar-mini"> C </span>--}}
                                <i class="material-icons">add_comment</i>
                                <span class="sidebar-normal">{{ __('Compose Message') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'messagessent' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.messagessent') }}">
                                {{--<span class="sidebar-mini"> M </span>--}}
                                <i class="material-icons">open_in_browser</i>
                                <span class="sidebar-normal">{{ __('Sent Messages') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'solved' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.solved') }}">
                                {{--<span class="sidebar-mini"> S </span>--}}
                                <i class="material-icons">thumb_up</i>
                                <span class="sidebar-normal">{{ __('Solved') }} </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>
            @endif


            @if(in_array(getRole(),[1,2,3,4,5]))
            <li class="nav-item{{ $activePage == 'reservations' ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('admin/reservations') }}">
                    <i class="material-icons">book</i>
                    <p>{{ __('Reservations') }}</p>
                </a>
            </li>
            @endif

            @if(in_array(getRole(),[2,3,4,5,6]))
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#studioSettings" aria-expanded="false">
                        <i class="material-icons">settings_applications</i>
                        <p>{{ __('Studio Management') }}
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse" id="studioSettings">
                        <ul class="nav">
                            <li class="nav-item{{ $activePage == 'schedule' ? ' active' : '' }}">
                                <a class="nav-link" href="{{ url('admin/schedule') }}">
                                    <i class="material-icons">schedule</i>
                                    <p>{{ __('Schedule Staff') }}</p>
                                </a>
                            </li>
                            <li class="nav-item{{ $activePage == 'schedulemodels' ? ' active' : '' }}">
                                <a class="nav-link" href="{{ url('admin/schedulem') }}">
                                    <i class="material-icons">schedule</i>
                                    <p>{{ __('Schedule Models') }}</p>
                                </a>
                            </li>
                            <li class="nav-item{{ $activePage == 'modelstats' ? ' active' : '' }}">
                                <a class="nav-link" href="{{ url('admin/modelstats') }}">
                                    <i class="material-icons">assessment</i>
                                    <p>{{ __('Model Stats') }}</p>
                                </a>
                            </li>
                            {{--.....--}}


                        </ul>
                    </div>
                </li>


            @endif

            @if(in_array(getRole(),[3,4]))
            <li class="nav-item{{ $activePage == 'studios' ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('admin/studios') }}">
                    <i class="material-icons">location_city</i>
                    <p>{{ __('Studios') }}</p>
                </a>
            </li>
            @endif

            @if(in_array(getRole(),[3,4,5]))
            <li class="nav-item{{ $activePage == 'users' ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('admin/users') }}">
                    <i class="material-icons">android</i>
                    <p>{{ __('Users') }}</p>
                </a>
            </li>
            @endif

            @if(in_array(getRole(),[3,4]))
            <li class="nav-item{{ $activePage == 'log' ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('admin/log') }}">
                    <i class="material-icons">list</i>
                    <p>{{ __('Log') }}</p>
                </a>
            </li>
            @endif

            @if(in_array(getRole(),[3,4]))
            <li class="nav-item{{ $activePage == 'news' ? ' active' : '' }}">
                <a class="nav-link" href="{{ url('admin/news') }}">
                    <i class="material-icons">notifications</i>
                    <p>{{ __('News') }}</p>
                </a>
            </li>
            @endif

            {{--<li class="nav-item{{ $activePage == 'backgroundservice' ? ' active' : '' }}">--}}
                {{--<a class="nav-link" href="{{ url('admin/backgroundservice') }}">--}}
                    {{--<i class="material-icons">timer</i>--}}
                    {{--<p>{{ __('Background Jobs') }}</p>--}}
                {{--</a>--}}
            {{--</li>--}}

            {{--<li class="nav-item{{ $activePage == 'settings' ? ' active' : '' }}">--}}
                {{--<a class="nav-link" href="{{ url('admin/settings') }}">--}}
                    {{--<i class="material-icons">settings_applications</i>--}}
                    {{--<p>{{ __('Settings') }}</p>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="nav-item{{ $activePage == 'logout' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('admin.logout') }}">
                    <i class="material-icons">exit_to_app</i>
                    <p>{{ __('Log out') }}</p>
                </a>
            </li>


            {{--<li class="nav-item active">--}}
                {{--<a class="nav-link" data-toggle="collapse" href="#other" aria-expanded="false">--}}
                    {{--<i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>--}}
                    {{--<p>{{ __('Other') }}--}}
                        {{--<b class="caret"></b>--}}
                    {{--</p>--}}
                {{--</a>--}}
                {{--<div class="collapse" id="other">--}}
                    {{--<ul class="nav">--}}


                        {{--<li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('notifications') }}">--}}
                                {{--<i class="material-icons">notifications</i>--}}
                                {{--<p>{{ __('Notifications') }}</p>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('profile.edit') }}">--}}
                                {{--<span class="sidebar-mini"> UP </span>--}}
                                {{--<span class="sidebar-normal">{{ __('User profile') }} </span>--}}
                            {{--</a>--}}
                        {{--</li>--}}

                        {{--<li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('table') }}">--}}
                                {{--<i class="material-icons">content_paste</i>--}}
                                {{--<p>{{ __('Table List') }}</p>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('typography') }}">--}}
                                {{--<i class="material-icons">library_books</i>--}}
                                {{--<p>{{ __('Typography') }}</p>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">--}}
                            {{--<a class="nav-link" href="{{ route('icons') }}">--}}
                                {{--<i class="material-icons">bubble_chart</i>--}}
                                {{--<p>{{ __('Icons') }}</p>--}}
                            {{--</a>--}}
                        {{--</li>--}}


                    {{--</ul>--}}
                {{--</div>--}}
            {{--</li>--}}


        </ul>
    </div>
</div>