@extends('Models.app')

@php if ($status == "Suspended") $title = "Your account is Suspended!"; else $title = "Your account is pending review!"; @endphp

@section('htmlheader_title') {{$title}} @endsection

@section('header') Approval @endsection

@section('breadcrumb') Approval @endsection

@section('content-inner')


    <section class="client">


        <div class="well text-center">
            <a href="/models/signcontract" id="gotocontract" type="button" class="btn btn-info {{$showContractSign}}">sign contract</a>

        </div>

        <div class="well text-center">
            @if (!$emailApproved) <button id="email" type="button" data-toggle="modal" data-target="#modal-change_email" class="btn btn-success">Change Email</button>@endif
            <button id="email" type="button" class="btn btn-info" disabled>{{ $email }}</button>
            <button id="email" type="button" data-toggle="modal" data-target="#modal-email"
                    @if ($emailApproved)  {{'disabled'}} @endif class="btn @php echo ($emailApproved !== null) ? 'btn-success disabled' : 'btn-danger'; @endphp">@php echo ($emailApproved !== null) ? 'Confirmed' : 'Verify your mail'; @endphp</button>

        </div>

        <div class="well text-center">
            @if (!$phoneApproved) <button id="phone" type="button" data-toggle="modal" data-target="#modal-change_phone" class="btn btn-success">Change Phone</button>@endif
            <button id="phone" type="button" class="btn btn-info" disabled>{{ $phone }}</button>
            <button id="phone" type="button" data-toggle="modal" data-target="#modal-phone"
                    @if ($phoneApproved)  {{'disabled'}} @endif class="btn @php echo ($phoneApproved !== null) ? 'btn-success' : 'btn-danger'; @endphp">@php echo ($phoneApproved !== null) ? 'Confirmed' : 'Verify your phone'; @endphp</button>

        </div>

        <div class="modal fade" id="modal-email">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <p><b> We sent a confirmation email to the address "{{ $email }}".</b></p>
                        <p>Please check your inbox and spam folder to confirm your email address. If you haven't recived
                            the email you can click here to resend it.</p>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-danger"><a style="color:white" href="{{ route('resendVerificationLink') }}">{{ __('Resend Email') }}</a></button>--}}
                        <button id="resendEmail" type="button" class="btn btn-danger">Resend Email</button>
                    </div>

                </div>

            </div>

        </div>

        <div class="modal fade" id="modal-change_email">
            <div class="modal-dialog">
                <form>
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>

                        </div>

                        <div class="modal-body">
                            <b>New Email Address</b>
                            <div class="form-group">

                                <input type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email">
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button id ="changeEmail" type="submit" class="btn btn-success">Change</button>
                        </div>

                    </div>
                </form>

            </div>

        </div>

        <div class="modal fade" id="modal-change_phone">
            <div class="modal-dialog">
                <form>
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>

                        </div>

                        <div class="modal-body">
                            <b>New Phone Number</b><br><br>
                            <p>Country code + phone number</p>
                            <p>Ex: Romania (40), <i>40751666777</i> </p>
                            <div class="form-group">


                                <input type="number" class="form-control" id="inputPhone" aria-describedby="phoneHelp" placeholder="Enter Phone Number:">
                                <small id="phoneHelp" class="form-text text-muted">Please enter new phone number.</small>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Change Phone</button>
                            {{--<button id ="changePhone" type="submit" class="btn btn-success">Change</button>--}}
                        </div>

                    </div>
                </form>

            </div>

        </div>

        <div class="modal fade" id="modal-phone">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">Phone Verification
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <p><b> If you did not receive our confirmation link, pleas click 'Resend Link'.</b></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="resendCode" class="btn btn-danger">Resend Link</button>
                    </div>

                </div>

            </div>

        </div>
        


        <div class="container">
            <style>

                .imgbtn{
                    margin-bottom: 50px;
                }
                .centerBtn{
                    position: absolute;
                    bottom: 0;
                    width: 100%;
                }
                .modal-content {
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                }

                .modal-header {
                    height: 100px;
                }


                .well .btn {
                    width: 10rem;
                    margin: 2px;
                }

                .well .btn-info {
                    width: 17rem;
                    margin: 2px;
                }

                section {
                    padding-top: 10px;
                }

                .col-lg-4 {
                    background: white;
                    padding: 10px;
                }

                .container {
                    height: 100%;
                    margin-top: 10px;
                }

                .fit-image {
                    width: 100%;
                    object-fit: cover;
                    /*height: 300px; !* only if you want fixed height *!*/
                }

                .col-lg-4 {
                    border: 1px solid lightblue;
                    background: #2f333e;
                }

                #msg0 {
                    background: #f0ad4e;
                }

                #msg1 {
                    background: #5cb85c;
                    color: white;
                }

                #msg2 {
                    background: #d9534f;
                    color: white;
                }

            </style>


            <form id="pic-form" action="{{url('models/uploadNewPicture')}}" method="post" enctype="multipart/form-data" class="row">
                @csrf

                <div class="row">

                    <div class="col-lg-4">
                        <div class="text-center" id="msg{{$photo1Status}}">{{ $stats[$photo1Status] }}<br>@if ($stats[$photo1Status]=='Rejected') {{$pic1reason}}@endif</div>
                        <div class="imgbtn"><img id="img1" class=" fit-image" src="{{ asset($pic1) }}"/></div>
                        <div class="text-center centerBtn">
                            <label type="button"
                                   class="btn btn-danger btn-file btn-lg @if (($photo1Status == 1) || (($photo1Status == 0))) {{'disabled'}} @endif">
                                Change Picture
                                <input @if (($photo1Status == 1) || (($photo1Status == 0))) {{'disabled'}} @endif type="file" style="display: none;"
                                       name="front1" class="image1" accept="image/*"/>
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="text-center" id="msg{{$photo2Status}}">{{ $stats[$photo2Status] }}<br>@if ($stats[$photo2Status]=='Rejected') {{$pic2reason}}@endif</div>
                        <div class="imgbtn"><img id="img2" class=" fit-image" src="{{ asset($pic2) }}"/></div>
                        <div class="text-center centerBtn">
                            <label type="button"
                                   class="btn btn-danger btn-file btn-lg @if (($photo2Status == 1) || (($photo2Status == 0))) {{'disabled'}} @endif">
                                Change Picture
                                <input @if (($photo2Status == 1) || (($photo2Status == 0))) {{'disabled'}} @endif type="file" style="display: none;"
                                       name="front2" class="image2" accept="image/*"/>
                            </label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="text-center" id="msg{{$photo3Status}}">{{ $stats[$photo3Status] }}<br>@if ($stats[$photo3Status]=='Rejected') {{$pic3reason}}@endif</div>
                        <div class="imgbtn"><img id="img3" class=" fit-image" src="{{ asset($pic3) }}"/></div>
                        <div class="text-center centerBtn">
                            <label type="button"
                                   class="btn btn-danger btn-file btn-lg @if (($photo3Status == 1) || (($photo3Status == 0))) {{'disabled'}} @endif">
                                Change Picture
                                <input @if (($photo3Status == 1) || (($photo3Status == 0))) {{'disabled'}} @endif type="file" style="display: none;"
                                       name="front3" class="image3" accept="image/*"/>
                            </label>
                        </div>
                    </div>


                </div>


                <button type="submit" class="btn btn-info btn-block invisible" id="btnSubmit"> {{ __('Submit') }}</button>

            </form>

        </div>

    </section>



@endsection

@push('scripts')

<script>

    let id = '{{Session::get('modelid')}}';
    //let code = '{ {$code} }';



    $('.image1').change(function () {
        //alert('sdsd');
        let input = this;
        let url = $(this).val();
        let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#img1').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            //console.log('target 1');
            if (input.files[0].length !== 0){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $('#btnSubmit').click();
            }
        }
    });

    $('.image2').change(function () {
        //alert('sdsd');
        let input = this;
        let url = $(this).val();
        let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#img2').attr('src', e.target.result);

            };
            reader.readAsDataURL(input.files[0]);
            if (input.files[0].length !== 0){
                $('#btnSubmit').click();
            }
        }
    });

    $('.image3').change(function () {
        //alert('sdsd');
        let input = this;
        let url = $(this).val();
        let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('#img3').attr('src', e.target.result);
                console.log((id));
                let file = e.target.result;

            };
            reader.readAsDataURL(input.files[0]);
            if (input.files[0].length !== 0){
                $('#btnSubmit').click();
            }
        }
    });


    $('#modal-change_email').submit(function (e) {
        let email = $('#inputEmail').val();

        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../changeEmail",
            method: 'post',
            data: {modelid: id, field_name: 'email', field: email},
            success: function (html) {
                if (html === 'exist'){
                    alert('Email is in use!');
                }
                if (html === 'ok'){
                    $('#modal-change_email').modal('toggle');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    location.reload();
                }

            }
        });

    });

    $('#modal-change_phone').submit(function (e) {
        let phone = $('#inputPhone').val();

        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../changePhone",
            method: 'post',
            data: {modelid: id, field_name: 'phone', field: phone},
            success: function (html) {

                console.log(html);
                location.reload();
                //location.reload();

//                if (html === '200'){
//                    $('#modal-change_phone').modal('toggle');
//                    $('body').removeClass('modal-open');
//                    $('.modal-backdrop').remove();
//                    location.reload();
//                }

            }
        });

    });

    $('#resendCode').click(function (e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../resendphonelink",
            method: 'post',
            success: function (html) {

            }
        });



        $('#modal-phone').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

    });

    $('#resendEmail').click(function (e) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "../resendemaillink",
            method: 'post',
            success: function (html) {

            }
        });



        $('#modal-email').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();

    });


</script>

@endpush