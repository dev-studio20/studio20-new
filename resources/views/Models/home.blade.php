@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Dashboard @endsection

@section('breadcrumb') Dashboard @endsection

@section('content-inner')


    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-4">
                    <div class="work-amount card">

                        <div class="card-body" style='margin-bottom: -7px;'>
                            <h3>Online Hours</h3>
                            <small><b>{{ $periodName }}</b> (except today)</small>
                            <div class="chart text-center">
                                <div class="text"><strong>{{$onlineHoursThisPeriod}}</strong><br><span>Hours</span></div>
                                <canvas id="pieChart" width="400" height="400"></canvas>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="work-amount card">

                        <div class="card-body" style='margin-bottom: -15px;'>
                            <h3>Earnings</h3><small> <b>{{ $periodName }}</b> (except today)</small><br>
                            <div style="font-size: 40px;padding:96px" class="number text-center count"><p><span id="target" >0</span> $</p></div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="overdue card">

                        <div class="card-body">
                            <h3>Income History</h3><small>Last 6 periods</small>
                            <div class="number text-center">{{$lsTotal}} $</div>
                            <div class="chart">
                                <canvas id="lineChart1"  width="400" height="122"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
{{--<script src="{{ asset('material/js/charts-home.js') }}"></script>--}}


<script>
    $(document).ready(function () {

        let max1 = 3222.35;
        let max = parseInt('{{$lsMax}}');
        console.log(max);
        let labels = @json($lsMonths);
        //let labels = ["October I","October II","November I","November II","December I","December II"];
        let data = @json($lsSums);
        //let data = [5140.34000000000014551915228366851806640625,7014.1099999999996725819073617458343505859375,5682.9399999999995998223312199115753173828125,7093.5299999999997453414835035800933837890625,3101.13999999999987267074175179004669189453125,4804.59000000000014551915228366851806640625];
        //data: @ json($piechartData),
        let pie = ["12.51","77.09","87.09"];
        let amount = '{{ $earningsThisPeriod }}';

        //'lsTotal', 'lsMonths', 'lsSums'


        let PIECHART = $('#pieChart');
        let myPieChart = new Chart(PIECHART, {
            type: 'doughnut',
            options: {
                cutoutPercentage: 75,
                legend: {
                    display: false
                }
            },
            data: {
                labels: [
                    "Total Hours ",
                    "Bonus 90H",
                    "Bonus 100H"
                ],
                datasets: [
                    {
                        data: pie,
                        borderWidth: [0, 0, 0, 0],
                        backgroundColor: [
                            '#44b2d7',
                            "#59c2e6",
                            "#71d1f2"
                        ],
                        hoverBackgroundColor: [
                            '#44b2d7',
                            "#59c2e6",
                            "#71d1f2"
                        ]
                    }]
            }
        });

        let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $('#target').animateNumber(
            {
                number:amount,
                numberStep: comma_separator_number_step

            }
            ,
            3000
        );

        let LINECHART1 = $('#lineChart1');
        let myLineChart = new Chart(LINECHART1, {
            type: 'line',
            options: {
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            display: false
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            max: max,
                            min: 0,
                            stepSize: 0.5
                        },
                        display: false,
                        gridLines: {
                            display: false
                        }
                    }]
                },
                legend: {
                    display: false
                }
            },
            data: {
                labels: labels,
                datasets: [
                    {
                        label: "Total Income",
                        fill: true,
                        lineTension: 0,
                        backgroundColor: "transparent",
                        borderColor: '#6ccef0',
                        pointBorderColor: '#59c2e6',
                        pointHoverBackgroundColor: '#59c2e6',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        borderWidth: 3,
                        pointBackgroundColor: "#59c2e6",
                        pointBorderWidth: 0,
                        pointHoverRadius: 4,
                        pointHoverBorderColor: "#fff",
                        pointHoverBorderWidth: 0,
                        pointRadius: 4,
                        pointHitRadius: 0,
                        data: data,
                        spanGaps: false
                    }
                ]
            }
        });

    });
</script>

@endpush