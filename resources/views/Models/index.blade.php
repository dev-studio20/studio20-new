<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title> Studio20 -  Build a career with us! </title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="robots" content="noindex, nofollow">

    <link href="{{ asset('img/models/favicon.ico') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/main.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/style.default.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/bootstrap.min.css') }}" rel="stylesheet"/>

</head>

<body>

<section class="banner" role="banner">

    <div class="container">

        <div class="col-md-10 col-md-offset-1">

            <div class="banner-text text-center">

                <h2>Build a career with us!</h2>

                <a href="{{ route('register') }}" class="btn btn-large">Join Us</a>

                <a href="{{ route('login') }}" class="btn btn-large btn-info">Log In </a>

            </div>

        </div>

    </div>

</section>

</body>

</html>

