<html lang="en">
<head>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Studio 20') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="{{ asset('material/css/jquery-ui.css') }}" rel="stylesheet"/>
    <link href="{{ asset('material/css/bootstrap.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('material/css/pygment_trac.css') }}" rel="stylesheet"/>
    <link href="{{ asset('material/css/generate-contract.css') }}" rel="stylesheet"/>

    {{--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>--}}
    <script type="text/javascript" src="{{ URL::asset('material/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/jSignature.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/jSignature.CompressorBase30.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/jquery.cookie.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('material/js/front.js') }}"></script>


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">

</head>
<body>

<div id="mcontainer">

    <div id ="scrollContainer" class="h-80 d-inline-block"
         style="background-color: white; overflow:scroll;padding-left: 20px;padding-right: 20px;">

        @php $semnatura = ''; @endphp
        @if($acctype==='pf')
            @if($country==='Romania')
                @include('Contract.ro_en')
            @else
                @include('Contract.ro_en')
            @endif
        @endif
        @if($acctype==='pj')
            @include('Contract.pj');
        @endif

    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="height: auto!important;">

                <div class="modal-body">
                    <div id="divButtBar" style="padding-top:0;">
                        <div id="signature1"></div>
                        @if($country==='Romania') <b>*Folositi mouse-ul in caseta de mai sus pentru a introduce
                            semnatura dumneavoastra;</b>
                        @else <b>*Use the mouse in the above box to enter your signature;</b>
                        @endif
                    </div>
                </div>

                <div class="modal-footer" style="height: 20%;">

                    <button type="button" class="btn btn-danger btn-block"
                            style="height: 100%;font-size: 1.5rem;white-space: normal;" data-dismiss="modal">Close
                    </button>

                    <button type="button" class="btn btn-primary btn-block"
                            style="height: 100%;font-size: 1.5rem;white-space: normal;margin-top: 0rem;"
                            onclick="$('#signature1').jSignature('clear')">Clear
                    </button>

                    <button type="button" class="btn btn-success btn-block"
                            style="height: 100%;font-size: 1.5rem;white-space: normal;margin-top: 0rem;"
                            onclick="importImg($('.displayarea'))" data-dismiss="modal">
                        @if($country==='Romania')Accepta
                        @else Accept
                        @endif
                    </button>


                </div>
            </div>
        </div>
    </div>


    <div class="container" style="    background: white;   padding: 10px 10px 10px 10px;    margin: 0;    width: 100%;">
        <div class="row">

            <div class="col-lg-6 col-sm-6 col-md-6">
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="width:100%">1. Sign Contract</button>
            </div>

            <div class="col-lg-6 col-sm-6 col-md-6">
                <button id="contract" type="button" class="btn btn-success btn-lg" style="width:100%" disabled>2. Finish Registration</button>
            </div>

        </div>

    </div>

</div>

</body>
</html>
<script type="text/javascript" src="{{ URL::asset('material/js/jSignature/jSignature.js.php') }}"></script>

<script type="text/javascript">

    let retry = 2;

    let signatureElem = $("#signature1");

    jQuery(document).ready(function ($) {

        $('#myModal').on('show.bs.modal', function (e) {
            signatureElem.jSignature();
            signatureElem.resize();
        });

        $('#contract').on('click', function (e) {

            window.location.href = "finshRegister";

        });
    });

    function importImg(sig) {

        sig.children("img.imported").remove();

        if (signatureElem.jSignature("getData")) {
            sendSignature(sig);
        }
    }

    function sendSignature(sign){

        let signatureData = signatureElem.jSignature("getData");

        retry--;

        let user_id = '{{ $id }}';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "/models/signcontract",
            method: 'post',
            data: {signature: signatureData, id: user_id},
            success: function (data) {
                console.log("response:");
                console.log(data);

                if (data.success) {

                    if (data.signature === signatureData) {
                        $('#contract').prop('disabled', false);
                        $("<img class='imported'></img").attr("src", signatureData).appendTo(sign);
                        $('#scrollContainer').animate({ scrollTop: $("#sideLeft").height() }, 'slow');
                    }
                    else {
                        if(retry < 1) {
                            alert("Error persists.. but you can now 'Finish Registration' and try to sign from your account!");
                            $('#contract').prop('disabled', false);
                        } else alert("Signature error, please try again!");
                    }

                } else {
                    if(retry < 1) {
                        alert("Error persists.. but you can now 'Finish Registration' and try to sign from your account!");
                        $('#contract').prop('disabled', false);
                    } else alert("Signature not saved, please try again!");
                }

                //fail safe



            }
        });

    }

</script>
