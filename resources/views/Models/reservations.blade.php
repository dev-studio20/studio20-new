@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Reservations @endsection

@section('breadcrumb') Reservations @endsection

@section('content-inner')


    <section class="forms">

        <style>
            #mdp-thismonth td.ui-datepicker-unselectable.ui-state-disabled.ui-state-highlight > .ui-state-default{
                background: {{$colorthis}};
                color: {{$fontthis}};
            }
            #mdp-nextmonth td.ui-datepicker-unselectable.ui-state-disabled.ui-state-highlight > .ui-state-default{
                background: {{$colornext}};
                color: {{$fontnext}};
            }
        </style>
        <div class="container-fluid">
            @if (session('msg') === '1')

                <div class="alert alert-success alert-dismissible" id="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    Reservations update successfully!
                </div>

            @endif

            <div class="row">

                <!-- Form Elements -->
                <div class="col">
                    <div class="card">

                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">{{ $thismonthname }} Reservations</h3>
                        </div>

                        <div class="card-body">
                            <div style="height:25px;">
                                <h5 id="thisError" style="color:red;display:none">You can make only (4 - 6) reservations/week!</h5>
                            </div>
                            <div id="mdp-thismonth"></div>
                            <br>

                            <form id="tmup" action="{{route('models.reservationspost')}}" method="post">
                                @csrf
                                <input type="hidden" name="datesthismonth" id="altField" value="">
                                Select the start hour for the reservations of this month! All of your reservations
                                are made for 12 hours from the start hour!<br>
                                <input class="timepicker-thismonth text-center" name="hourthismonth">
                                <br><br>
                                @if (!$thismonthcount)
                                    <input style="display:none;" id="submitThis" type="submit" class="btn btn-primary" value='Send Reservations'
                                           name="sendthismonth"><br>
                                @endif

                            </form>

                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card">

                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">{{ $nextmonthname }} Reservations</h3>
                        </div>
                        <div class="card-body">
                            <div style="height:25px;">
                                <h5 id="nextError" style="color:red;display:none">You can make only (4 - 6)  reservations/week!</h5>
                            </div>

                            <div id="mdp-nextmonth"></div>
                            <br>
                            <form id="nmup" action="{{route('models.reservationspostnext')}}" method="post">
                                @csrf
                                <input type="hidden" name="datesnextmonth" id="altzField" value="">
                                Select the start hour for the reservations of this month! All of your reservations
                                are made for 12 hours from the start hour!</br>

                                <input class="timepicker-nextmonth text-center" name="hournextmonth">

                                <br><br>
                                @if  (!$nextmonthcount)
                                    <input style="display:none;" id="submitNext" type="submit" class="btn btn-primary" value='Send Reservations'
                                           name="sendnextmonth"><br>
                                @endif

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

@push('scripts')

<script type="text/javascript" src="{{ URL::asset('material/js/jquery-ui.multidatespicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('material/js/jquery.timepicker.min.js') }}"></script>

<script>

    var selectedDays = [];

    selectedDays["this"] = 0;
    selectedDays["next"] = 0;

    var resEnabledNext = '{{ $nextmonthcount }}';
    var resEnabledThis = '{{ $thismonthcount }}';

    var condNext = (resEnabledNext > 0);
    var condThis = (resEnabledThis > 0);

    var ddd = '{{ $nextmonth }}';
    var gdda = ddd.split("-");
    var nMonth = parseInt(gdda[0]);
    var nYear = gdda[1];

    window.onload = function () {

        var data = '{{ $thismonthdays }}';
        var dates = data.split(",");

        var nextperiod = '{{ $nextmonth }}';

        var date = new Date();
        var arr = [];

        dates.forEach(function(entry) {
            arr.push(date.setDate(entry));
        });

        $('.timepicker-thismonth').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '0',
            maxTime: '23:30',
            defaultTime: '11',
            startTime: '0:0',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('#mdp-thismonth').multiDatesPicker({
            dateFormat: 'mm-dd-yy',
            minDate: 0,
            maxDate: '{{ $thismonthlastday }}',
            altField: '#altField',
            disabled: condThis,
            firstDay: 1,
            addDates: arr,
            onSelect: function(dateStr) {
                var sdate = new Date(dateStr);
                var index = (getWeekNumber(sdate));
                var vdate = sdate.getDay();
                var narr = (selectedDays[index]) ? selectedDays[index] : [];
                checkCondition($('#mdp-thismonth'), "this", dateStr, narr, vdate, index, $("#submitThis"), $("#thisError"));
            }
        });


        $('.timepicker-nextmonth').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '0',
            maxTime: '23:30',
            defaultTime: '11',
            startTime: '0:0',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        var data1 = '{{ $nextmonthdays }}';

        var dates1 = data1.split(",");
        var arr1 = [];

        dates1.forEach(function(entry) {
            if (entry){
                var parts = nextperiod.split('-');
                var dateTime = parts[1] + '-' + parts[0] + '-' + entry;
                var dddd = new Date(dateTime);
                arr1.push(dddd);
            } else arr1 = null;

        });

        $('#mdp-nextmonth').multiDatesPicker({
            dateFormat: 'mm-dd-yy',
            minDate: '{{ $nextmonthfirstday }}',
            maxDate: '{{ $nextmonthlastday }}',
            altField: '#altzField',
            disabled: condNext,
            firstDay: 1,
            addDates: arr1,
            onSelect: function(dateStr) {
                var sdate = new Date(dateStr);
                var index = (getWeekNumber(sdate));
                var vdate = sdate.getDay();
                var narr = (selectedDays[index]) ? selectedDays[index] : [];
                checkCondition($('#mdp-nextmonth'), "next", dateStr, narr, vdate, index, $("#submitNext"), $("#nextError"));
            }

        });

    };


    function checkCondition(picker, side, selectedDate, narr, vdate, index, submitBtn, errorText){

        var datezz = picker.multiDatesPicker('getDates');

        if (narr.includes(vdate)) {
            // remove element
            if (narr.length === 7) {
                if (datezz.includes(selectedDate)) {
                    console.log("yesss");
                } else {
                    console.log("noo");
                    selectedDays[side]--;
                }
            }

            narr = narr.filter(function(item) {
                return item !== vdate
            });



        } else {
            // add element
            narr.push(vdate);
            // check 5 condition (disable button, show error)
            if (narr.length > 6) {

                selectedDays[side]++;
            }

        }

        selectedDays[index] = narr;

        console.log("size: " + selectedDays[index].length);

        if (checkSelectedDays()) {
            errorText.hide();
            submitBtn.show();
        } else {
            errorText.show();
            submitBtn.hide();
        }

    }

    function checkSelectedDays(){

        var condition = true;

        for (var key in selectedDays) {

            if (typeof(selectedDays[key]) === 'object') {

                var arr = selectedDays[key];
                if ( (arr.length > 6) || ( (arr.length < 4) && (arr.length > 0)) && (getCountDaysInWeek(key, nYear))){
                    condition = false;
                }

            }

            //if (arr.length === 0) condition = false;
        }

        return condition;
    }

    function getCountDaysInWeek(w, y){

        var d = new Date(getDateOfWeek(w, y));
        var last_d = new Date(getLastDateOfWeek(w, y));
        var m_d = d.getMonth();
        var m_lastd = last_d.getMonth();

        return (m_d  === m_lastd);

    }

    function getLastDateOfWeek(w, y) {
        var simple = new Date(y, 0, (w * 7) - 1);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay());
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }

    function getDateOfWeek(w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 7 - simple.getDay());
        return ISOweekStart;
    }

    function getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
        // Return array of year and week number
        return weekNo;
    }

</script>

@endpush