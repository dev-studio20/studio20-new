@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Payment Details @endsection

@section('breadcrumb') Payment Details @endsection

@section('content-inner')


    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-12">
                    <div class="card">

                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Period: {{$periodname}}</h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="form-group row">

                                    <label class="col-sm-3 form-control-label">Jasmin Revenue</label>

                                    <div class="col-sm-9">
                                        {{$details->jasmin_amount}} $
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Affiliate Revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->affliate_rev}} $
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Personal Site Revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->personal_rev}} $

                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Floria Revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->floria_rev}} $
                                    </div>
                                </div>

                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Other Revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->others_rev}} $
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Total Revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->total_amount_usd}} $
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Base Comision</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_base}} %
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">90h bonus</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_90h}} %
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">100h bonus</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_100h}} %
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">10.000$ bonus</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_10k}} %
                                    </div>
                                </div>

                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Own studio bonus</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_location}} %
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Total comision</label>
                                    <div class="col-sm-9">
                                        {{$details->commission_total}} %
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Deductions</label>
                                    <div class="col-sm-9">
                                        {{$details->deductions}} $
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Brute revenue</label>
                                    <div class="col-sm-9">
                                        {{$details->jasmin_amount}} $
                                    </div>
                                </div>



                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Amount paid</label>
                                    <div class="col-sm-9">
                                        {{$details->amount_paid}} EUR

                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Exchange rate USD/EUR</label>
                                    <div class="col-sm-9">
                                        {{$details->exchange}}

                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Total EUR</label>
                                    <div class="col-sm-9">
                                        {{$details->total_amount_eur}} €
                                    </div>
                                </div>

                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Payment Date</label>
                                    <div class="col-sm-9">
                                        {{$details->pay_date}}
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-5 offset-sm-3">
                                        <a id="login" href="{{route('models.payment')}}" class="btn btn-primary">Go Back</a>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection

@push('scripts')


@endpush