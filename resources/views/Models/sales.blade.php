@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Sales @endsection

@section('breadcrumb') Sales @endsection

@section('content-inner')


    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Current Sales: {{$periodName}} - {{$year}}</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Day</th>
                                    <th>Sales</th>
                                    <th>Online Hours</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php $sum = 0; $hours = 0; $days = 0;@endphp

                                @foreach($sales as $sale)

                                    <tr>
                                        <td scope="row">{{$loop->index+1}}</td>
                                        <td>{{decimal($sale->day)}}-{{decimal($sale->month)}}-{{$sale->year}}</td>
                                        <td>{{$sale->sum}} $</td>
                                        <td>{{convertSecondsToHours($sale->hours)}}</td>
                                        @php $sum += $sale->sum; $hours += $sale->hours; $days = $loop->index+1 @endphp
                                    </tr>

                                @endforeach

                                </tbody>
                                <thead>
                                <tr>
                                    <th>Total</th>
                                    <th>{{$days}} Days</th>
                                    <th>{{$sum}} $</th>
                                    <th>{{convertSecondsToHours($hours)}} Hours</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>



@endsection

@push('scripts')


@endpush