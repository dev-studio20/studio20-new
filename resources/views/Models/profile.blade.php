@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Profile @endsection

@section('breadcrumb') Profile @endsection

@section('content-inner')


    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-4 col-sm-12 card">

                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Your Profile</h3>
                    </div>

                    <div class="card-body">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Model Name</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->modelname}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->profile->first_name}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->profile->last_name}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Phone Number</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->profile->phonenumber}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->email}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Birth Date</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->profile->birthdate}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">IDN</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->paymentDetails->idn}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Studio</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{findStudioName($model->studio)}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>


                            <div class="form-group row">
                                <div class="col-sm-5 offset-sm-3">


                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12 card">

                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Change Address</h3>
                    </div>

                    <div class="line"></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="">
                            <input type="hidden" name="_token" value="rJv3TPCTGe2MskaqCiVDH8yZcQvsJS7veO7Z48Fn">


                            <div class="form-group row">
                                <label for="registerCountry" class="col-sm-3 form-control-label">Country</label>
                                <div class="col-sm-9">
                                    <select name="registerCountry" id="country" class="form-control" disabled="" required="">
                                        <option value="" selected="">{{findCountry($model->address->country_id)}}</option>

                                    </select>
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="form-group row">
                                <label for="registerstate" class="col-sm-3 form-control-label">State</label>

                                <div class="col-sm-9">
                                    <select name="registerstate" id="state" class="form-control" disabled="" required="">
                                        <option value="">{{findState($model->address->state_id)}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="line"></div>

                            <div class="form-group row">
                                <label for="registerCity" class="col-sm-3  form-control-label">City</label>

                                <div class="col-sm-9">
                                    <select name="registerCity" id="city" class="form-control" disabled="" required="">
                                        <option value="" selected="">{{findCity($model->address->city_id)}}</option>
                                    </select>
                                </div>

                            </div>


                            <div class="line"></div>


                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Address</label>
                                <div class="col-sm-9">
                                    <input name="registerAddress" type="text" value="{{$model->address->street}}" class="form-control" disabled="">
                                </div>
                            </div>
                            <div class="line"></div>

                            <div class="form-group row">
                                <div class="col-sm-5 offset-sm-3">
                                    <input type="submit" name="changeaddress" class="btn btn-success" value="Change Address!" disabled="">

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-12 card">

                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Payment Details</h3>
                    </div>

                    <div class="line"></div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="">
                            <input type="hidden" name="_token" value="rJv3TPCTGe2MskaqCiVDH8yZcQvsJS7veO7Z48Fn">                                <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Bank Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="bankName" value="{{$model->paymentDetails->bank}}" required="" disabled class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Bank Address</label>
                                <div class="col-sm-9">
                                    <input type="text" name="bankAddress" value="{{$model->paymentDetails->bank_address}}" disabled class="form-control">
                                </div>
                            </div>

                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">IBAN</label>
                                <div class="col-sm-9">
                                    <input type="text" name="bankIban" required="" value="{{$model->paymentDetails->iban}}" disabled class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Currency</label>
                                <div class="col-sm-9">
                                    <input type="text" disabled value="{{$model->paymentDetails->currency}}" class="form-control">
                                </div>
                            </div>
                            <div class="line"></div>


                            <div class="form-group row">
                                <div class="col-sm-5 offset-sm-3">
                                    <input type="submit" name="changedetails" class="btn btn-success" value="Change Payment Details!" disabled="">

                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection

@push('scripts')

@endpush