@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Messages @endsection

@section('breadcrumb') Messages @endsection

@section('content-inner')

    <style type="text/css">
        .email-app {
            display: flex;
            flex-direction: row;
            background: #fff;

        }

        .email-app nav {
            flex: 0 0 200px;
            padding: 1rem;

        }

        .email-app nav .btn-block {
            margin-bottom: 15px;
        }

        .email-app nav .nav {
            flex-direction: column;
        }

        .email-app nav .nav .nav-item {
            position: relative;
        }

        .email-app nav .nav .nav-item .nav-link,
        .email-app nav .nav .nav-item .navbar .dropdown-toggle,
        .navbar .email-app nav .nav .nav-item .dropdown-toggle {
            color: #151b1e;
            border-bottom: 1px solid #e1e6ef;
        }

        .email-app nav .nav .nav-item .nav-link i,
        .email-app nav .nav .nav-item .navbar .dropdown-toggle i,
        .navbar .email-app nav .nav .nav-item .dropdown-toggle i {
            width: 20px;
            margin: 0 10px 0 0;
            font-size: 14px;
            text-align: center;
        }

        .email-app nav .nav .nav-item .nav-link .badge,
        .email-app nav .nav .nav-item .navbar .dropdown-toggle .badge,
        .navbar .email-app nav .nav .nav-item .dropdown-toggle .badge {
            float: right;
            margin-top: 4px;
            margin-left: 10px;
        }

        .email-app main {
            min-width: 0;
            flex: 1;
            padding: 1rem;
        }

        .email-app .inbox .toolbar {
            padding-bottom: 1rem;
            border-bottom: 1px solid #e1e6ef;
        }

        .email-app .inbox .messages {
            padding: 0;
            list-style: none;
        }

        .email-app .inbox .message {
            position: relative;
            padding: 1rem 1rem 1rem 2rem;
            cursor: pointer;
            border-bottom: 1px solid #e1e6ef;
        }

        .email-app .inbox .message:hover {
            background: #f9f9fa;
        }

        .email-app .inbox .message .actions {
            position: absolute;
            left: 0;
            display: flex;
            flex-direction: column;
        }

        .email-app .inbox .message .actions .action {
            width: 2rem;
            margin-bottom: 0.5rem;
            color: #c0cadd;
            text-align: center;
        }

        .email-app .inbox .message a {
            color: #000;
        }

        .email-app .inbox .message a:hover {
            text-decoration: none;
        }

        .email-app .inbox .message.unread .header,
        .email-app .inbox .message.unread .title {
            font-weight: bold;
        }

        .email-app .inbox .message .header {
            display: flex;
            flex-direction: row;
            margin-bottom: 0.5rem;
        }

        .email-app .inbox .message .header .date {
            margin-left: auto;
        }

        .email-app .inbox .message .title {
            margin-bottom: 0.5rem;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .email-app .inbox .message .description {
            font-size: 12px;
        }

        .email-app .message .toolbar {
            padding-bottom: 1rem;
            border-bottom: 1px solid #e1e6ef;
        }

        .email-app .message .details .title {
            padding: 1rem 0;
            font-weight: bold;
        }

        /*.email-app .message .details .header {*/
        /*display: flex;*/
        /*padding: 1rem 0;*/
        /*margin: 1rem 0;*/

        /*border-bottom: 1px solid #e1e6ef;*/
        /*}*/

        /*.email-app .message .details .header .avatar {*/
        /*width: 40px;*/
        /*height: 40px;*/
        /*margin-right: 1rem;*/
        /*}*/

        /*.email-app .message .details .header .from {*/
        /*font-size: 12px;*/
        /*color: #9faecb;*/
        /*align-self: center;*/
        /*}*/

        .email-app .message .details .header .from span {
            display: block;
            font-weight: bold;
        }

        /*.email-app .message .details .header .date {*/
        /*margin-left: auto;*/
        /*}*/

        .email-app .message .details .attachments {
            padding: 1rem 0;
            margin-bottom: 1rem;
            border-top: 3px solid #f9f9fa;
            border-bottom: 3px solid #f9f9fa;
        }

        .email-app .message .details .attachments .attachment {
            display: flex;
            margin: 0.5rem 0;
            font-size: 12px;
            align-self: center;
        }

        .email-app .message .details .attachments .attachment .badge {
            margin: 0 0.5rem;
            line-height: inherit;
        }

        .email-app .message .details .attachments .attachment .menu {
            margin-left: auto;
        }

        .email-app .message .details .attachments .attachment .menu a {
            padding: 0 0.5rem;
            font-size: 14px;
            color: #e1e6ef;
        }

        @media (max-width: 767px) {
            .email-app {
                flex-direction: column;
            }
            .email-app nav {
                flex: 0 0 100%;
            }
        }

        @media (max-width: 575px) {
            .email-app .message .header {
                flex-flow: row wrap;
            }
            .email-app .message .header .date {
                flex: 0 0 100%;
            }
        }

        input[type=file]{
            display: inline;
        }
        #image_preview{
            border: 1px solid black;
            padding: 10px;
        }
        #image_preview img{
            width: 200px;
            padding: 5px;
        }

    </style>

    <!-- Client Section-->
    <section class="client no-padding-bottom">

        <div class="tables" style="width:100%">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">

                            <div class="card-header d-flex align-items-center">
                                <a style="margin-right:auto; margin-left:0;" type="button" class="btn btn-danger" href="{{route('models.messages')}}">Back to Inbox</a>
                            </div>
                            <div class="card-body">
                                <div class="email-app mb-4">

                                    <main class="message">
                                        <div class="details">

                                            <hr>
                                            @foreach ($msg as $message)

                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="header">
                                                            <img class="avatar" @if ($message["from_id"] == 'admin') src="/img/models/adminavatar.png" @else src="/img/models/{{$sex}}.png" @endif style="width:50px">
                                                            <div class="from">
                                                                <div><b>@if ($message["from_id"] == 'admin'){{ 'Admin' }} @else {{ 'You' }} @endif</b></div><br>
                                                            </div>
                                                            <div class="date"><i class="fa fa-clock-o"></i>{{ $message["created_at"] }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-9">
                                                        <div class="content">
                                                            @if ($loop->iteration == 1) <b> {{$subj->subject}} </b>@endif
                                                            <div><i>{{  $message["message"] }}</i></div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <hr>
                                                <div class="row">

                                                    @foreach($message["attachements"] as $att)

                                                        <a href="#my_modal" data-toggle="modal" data-image-id="{{$att["src"]}}" data-image-path="{{$att["path"]}}">
                                                            <div class="col">
                                                                <img src="{{$att["src"]}}" alt="" style="max-height:50px;">
                                                            </div>
                                                        </a>

                                                    @endforeach

                                                </div>
                                                <hr>
                                            @endforeach

                                        </div>
                                        <br />
                                        <form method="post" action="#message"  enctype="multipart/form-data">
                                            @csrf
                                            <div id="count"></div>
                                            <div class="form-group">
                                                <input  type="hidden" name="subject" value="tt">
                                                <textarea class="form-control" id="message" name='message' rows="5" placeholder="Click here to reply">{{old('message')}}</textarea>


                                            </div>
                                            @error('message')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <div class="form-group">

                                                <label style="margin-bottom: 0;" type="button" class="btn btn-primary col-lg-3 col-sm-12">Upload Files
                                                    <input type="file" style="display: none;" id="uploadFile" name="uploadFile[]" accept="image/*,application/pdf" capture="camera" multiple/>
                                                </label>

                                                <input id="submitMessage" type="submit" name="reply" class="btn btn-success col-lg-3 col-sm-12" value="Send Message!">

                                            </div>
                                        </form>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="container">

                                                    <br/>
                                                    <div id="image_preview"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </main>
                                </div>
                            </div>



                        </div>
                    </div>

                </div>



            </div>
        </div>

        <div class="modal" id="my_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">X</span><span class="sr-only">Close</span></button>
                    </div>
                    <div class="modal-body">
                        <img class="img-responsive" id="myImage" src=""  style="width: 100%;">
                    </div>
                    <div class="modal-footer">
                        <a id="myLink" href="#" download>
                            <button type="button" class="btn btn-primary">DOWNLOAD</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

@push('scripts')

<script>

    $('#my_modal').on('show.bs.modal', function(e) {

        let prev_src = $(e.relatedTarget).data('image-id');
        let download_src = $(e.relatedTarget).data('image-path');
        $(e.currentTarget).find('#myImage').attr("src", prev_src);
        $(e.currentTarget).find('#myLink').attr("href", download_src);
    });

    let sbtn = $("#submitMessage");

    $("#message").keyup(function(){
        let count = (1000 - $(this).val().length);
        $("#count").text("Characters left: " + count);
        sbtn.attr("disabled", (count < 0 ));

    });

    $("body").on("submit", "form", function() {
        $(this).submit(function() {
            return false;
        });
        return true;
    });

    $("#uploadFile").change(function(){
        $('#image_preview').html("");
        let total_file=document.getElementById("uploadFile").files.length;


        for(let i=0;i<total_file;i++)
        {
            let image = URL.createObjectURL(event.target.files[i]);
            console.log(event.target.files[i]);
            if (isFileImage(event.target.files[i])){
                $('#image_preview').append("<img src='"+image+"'>");
            } else $('#image_preview').append("<img src='/img/models/doc.png'>");

        }

    });

    function isFileImage(file) {
        const acceptedImageTypes = ['image/gif', 'image/jpeg', 'image/png'];

        return file && acceptedImageTypes.includes(file['type'])
    }

</script>

@endpush