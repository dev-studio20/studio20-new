@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Messages @endsection

@section('breadcrumb') Messages @endsection

@section('content-inner')

    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="messages-list" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#inbox" role="tab" aria-controls="description" aria-selected="true">Inbox</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  href="#outbox" role="tab" aria-controls="history" aria-selected="false">Oubox</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#system" role="tab" aria-controls="deals" aria-selected="false">Notifications</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <a style="margin-right:auto; margin-left:0;" type="button" class="btn btn-danger" href="/models/messagescreate">Send message to Admin</a>

                            <div class="tab-content mt-3">
                                <div class="tab-pane active" id="inbox" role="tabpanel">

                                    <div class="table-responsive">
                                        <table class="table" cellspacing="0" width="100%">
                                            <thead >

                                            <tr>
                                                <th>#</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mail Subject</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>

                                            <tbody style='cursor:pointer;'>

                                            @foreach($messages as $message)

                                                <tr class="{{ $message->body->first()->readtime ? '' : 'font-weight-bold'}}" onclick="window.location='/models/messages/{{$message['id']}}'">
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>@if ($message['from_id'] != 'admin'){{'You'}} @else {{'admin'}}@endif</td>
                                                    <td>@if ($message['to_id'] != 'admin'){{'You'}} @else {{'admin'}}@endif</td>
                                                    <td>{{$message['subject']}}</td>
                                                    <td>{{$message['created_at']}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="tab-pane" id="outbox" role="tabpanel" aria-labelledby="history-tab">

                                    <div class="table-responsive">
                                        <table class="table" cellspacing="0" width="100%">
                                            <thead >

                                            <tr>
                                                <th>#</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mail Subject</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>

                                            <tbody style='cursor:pointer;'>

                                            @foreach($msgout as $message)
                                                <tr onclick="window.location='/models/messages/{{$message['id']}}'">
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>@if ($message['from_id'] != 'admin'){{'You'}} @else {{'admin'}}@endif</td>
                                                    <td>@if ($message['to_id'] != 'admin'){{'You'}} @else {{'admin'}}@endif</td>
                                                    <td>{{$message['subject']}}</td>
                                                    <td>{{$message['created_at']}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="tab-pane" id="system" role="tabpanel" aria-labelledby="deals-tab">

                                    <div class="table-responsive">
                                        <table class="table" cellspacing="0" width="100%">
                                            <thead >

                                            <tr>
                                                <th>#</th>
                                                <th>Subject</th>
                                                <th>Message</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>

                                            <tbody style='cursor:pointer;'>
                                            @foreach($msgSystem as $message)
                                                <tr onclick="window.location='/models/messagesmass/{{$message->id}}'">
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$message->subject}}</td>
                                                    <td>{{(strlen($message->message) > 23) ? substr($message->message,0,20).'...' : $message->message }}</td>
                                                    <td>{{$message->created_at}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </section>
@endsection

@push('scripts')

<script>

    $('#messages-list a').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show')
    })

</script>

@endpush