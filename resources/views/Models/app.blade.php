<!DOCTYPE html>

<html lang="en" style="height: 100%;">

@section('htmlheader')

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title> GB Producer - Model Center - @yield('htmlheader_title', 'Your title here') </title>

        @stack('css-top')

        <link href="{{ asset('css/models/icons.css') }}" rel="stylesheet"/>

        <link href="{{ asset('css/models/bootstrap.min.css') }}" rel="stylesheet"/>

        <link href="{{ asset('css/models/style.default.css') }}" rel="stylesheet"/>

        <link href="{{ asset('css/models/custom.css') }}" rel="stylesheet"/>
        <link href="{{ asset('material/js/jquery-ui.multidatespicker.css') }}" rel="stylesheet"/>
        <link href="{{ asset('material/js/jquery-ui.css') }}" rel="stylesheet"/>
        <link href="{{ asset('css/models/font-awesome.min.css') }}" rel="stylesheet"/>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">

        <link rel="shortcut icon" href="{{ url('/img/models/favicon.ico') }}">

        {{--<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>--}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="{{ asset('material/js/tether.min.js') }}"></script>
        <script src="{{ asset('material/js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('material/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('material/js/front.js') }}"></script>
        <script src="{{ asset('material/js/jquery.cookie.js') }}"></script>
        <script src="{{ asset('material/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('material/js/jquery.animateNumber.min.js') }}"></script>

    </head>

@show

@section('scripts-top')

    @stack('scripts-top')

@show
<body style="height:100%">
<div class="page home-page" style="height:100%">
    <!-- Main Navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <!-- Navbar Header-->
                    <div class="navbar-header">
                        <!-- Navbar Brand -->
                        {{--<a href="{{route ('modelcenter')}}" class="navbar-brand">--}}
                        <a href="#" class="navbar-brand">
                            <div class="brand-text brand-big hidden-lg-down">
                                <span>Models </span><strong>Dashboard</strong></div>
                            <div class="brand-text brand-small"><strong>MD</strong></div>
                        </a>
                        <!-- Toggle Button--><a id="toggle-btn" href="#"
                                                class="menu-btn active"><span></span><span></span><span></span></a>
                    </div>

                    <div>
                        @if (get_client_ip() == '188.214.255.240')
                            <form class="form-inline" action="/models/changemodel">
                                <div class="form-group mb-2">
                                    <label for="modelId" class="sr-only">id</label>
                                    <input name="modelId" type="text"  class="form-control-plaintext" id="modelId" value="{{getCurrentModel()}}">
                                </div>

                                <input type="submit" name="submitFind" class="btn btn-primary mb-2" value="Go">
                                <input type="submit" name="submitNext" class="btn btn-primary mb-2" value="Next">
                            </form>
                        @endif
                    </div>
                    <!-- Navbar Menu -->
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">

                        <!-- Logout    -->
                        <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>
                        {{--<li class="nav-item"><a href="#" class="nav-link logout">Logout<i class="fa fa-sign-out"></i></a></li>--}}
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div class="page-content d-flex align-items-stretch" style="min-height: 100%">

        <nav class="side-navbar">
            <!-- Sidebar Header-->
            <div class="sidebar-header d-flex align-items-center">

                @php $sex = getModelSex(Auth::guard('model')->user()->id); @endphp

                <div class="avatar"><img src="{{URL::asset('/img/models/'.$sex.'.png')}}" alt="..."
                                         class="img-fluid rounded-circle"></div>
                <div class="title" style="overflow: hidden; word-break: break-word;">
                    <h1 class="h4">Model</h1>
                    <p>{{ Auth::guard('model')->user()->modelname  }}</p>
                </div>
            </div>
            <!-- Sidebar Navidation Menus-->
            <span class="heading">Main</span>
            <ul class="list-unstyled">
                {{--<li class=""><a href="{{ route('modelcenter') }}"><i class="icon-home"></i>Home</a></li>--}}
                <li class=""><a href="/models"><i class="icon-home"></i>Home</a></li>

                {{--<li><a href={{ route('profile') }}> <i class="icon-grid"></i>Personal Data </a></li>--}}
                <li><a href="/models/profile"> <i class="icon-grid"></i>Personal Data </a></li>
                @php $id = Auth::guard('model')->user()->id @endphp
                @php  $read = \App\ModelMessagesBody::where('to_id', $id)->whereNull('readtime')->get() @endphp
                @php $count = count($read) @endphp
                {{--@php dd($count)@endphp--}}
                <li><a href="/models/messages"> <i class="fa fa-envelope-o"></i>Messages @if ($count)<span class="badge badge-danger">New</span>@endif</a></li>

                <li><a href="/models/sales"> <i class="fa fa-bar-chart"></i>Sales </a></li>
                {{--<li><a href="{{ route('paymenthistory') }}"> <i class="icon-padnote"></i>Payment History </a></li>--}}
                <li><a href="/models/payment"> <i class="icon-padnote"></i>Payment History </a></li>

                @php $studio = Auth::guard('model')->user()->studio @endphp
                @php $req_res = \App\Studio::where('id', $studio)->first() @endphp
                @php $req = ($req_res) ? $req_res->reservations_required : 0; @endphp
                @if ($req) <li><a href="/models/reservations"><i class="fa fa-calendar"></i>Reservations </a></li> @endif


                {{--<li><a href="{{ route('news') }}"><i class="fa fa-newspaper-o"></i>News @include('MainTemplate.countnews')</a></li>--}}
                {{--<li><a href="/models/news"><i class="fa fa-newspaper-o"></i>News</a></li>--}}

                <li><a href="/models/contract"> <i class="icon-padnote"></i>Contract </a></li>
            </ul>
        </nav>

        <div class="content-inner">

            <!-- Page Header-->
            <header class="page-header">
                <div class="container-fluid">
                    <h2 class="no-margin-bottom">@yield('header', '')</h2>
                </div>
            </header>

            <!-- Breadcrumb-->
            <ul class="breadcrumb">
                <div class="container-fluid">
                    <li class="breadcrumb-item"><a href="{{ url('models') }}">Home</a></li>
                    <li class="breadcrumb-item active">@yield('breadcrumb', '')</li>
                </div>
            </ul>

            @yield('content-inner')

        </div>


    </div>

    <!-- Page Footer-->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>Copyright &copy; Studio 20</p>
                </div>
            </div>
        </div>
    </footer>

</div>

{{--
<script src="{{ URL::asset('/js/app.js') }}"></script>
--}}


@section('scripts')

    @stack('scripts')

@show

<script src="https://js.pusher.com/5.0/pusher.min.js"></script>

<script>

    //let modelid = '{ {Auth::guard('model')->user()->id} }'
    // Enable pusher logging - don't include this in production
    //Pusher.logToConsole = true;

    let pusher = new Pusher('785e6444f60db91a62e0', {
        cluster: 'eu',
        forceTLS: true
    });

    // Subscribe to the channel we specified in our Laravel Event
    let channel = pusher.subscribe('test-channel');

    // Bind a function to a Event
    channel.bind('account_approved', function(data) {

        console.log(JSON.stringify(data));
        let message = data.message;
        console.log(JSON.stringify(message));
        //alert("Approved");

        window.location.href = '/models';

    });

    channel.bind('account_approved_no', function(data) {

        console.log(JSON.stringify(data));
        let message = data.message;
        console.log(JSON.stringify(message));
        window.location.href = '/models';

    });


</script>

</body>

</html>