@extends('Models.app')

@section('htmlheader_title') @endsection

@section('header') Payment History @endsection

@section('breadcrumb') Payment History @endsection

@section('content-inner')


    <!-- Client Section-->
    <section class="client no-padding-bottom">
        <div class="container-fluid">
            <div class="row ">

                <div class="col-lg-12">

                    <div class="card" style="overflow-x: auto;">

                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Payment History</h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Period</th>
                                    <th>Total Revenue</th>
                                    <th>Total Comision</th>
                                    <th>Status</th>
                                    <th>Brute Revenue $</th>
                                    <th>Exchange Rate</th>
                                    <th>Payment Date</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $index = 1; @endphp
                                @foreach($periods as $p)

                                    <tr>

                                        <td>{{$index}}</td>
                                        <td>{{getAllPeridos()[$p->period]}} - {{$p->year}}</td>
                                        <td>{{$p->payhistory->jasmin_amount + $p->payhistory->affliate_rev + $p->payhistory->personal_rev + $p->payhistory->floria_rev + $p->payhistory->others_rev}} $</td>
                                        <td>{{$p->payhistory->commission_total}} %</td>
                                        <td>

                                            @if($p->payhistory->pay_done == 1)
                                                <button class="btn btn-success">Payment Made</button>
                                            @else

                                                @if ($p->payhistory->pay_request)
                                                    <button class="btn btn-primary">Waiting For Payment</button>
                                                @else
                                                    @if ($studio->payment_request)
                                                        {{--if studio has conditio -> check --}}
                                                        @if ($condition || ($p->payhistory->ignore_res))
                                                            {{--if condition ok show request button--}}
                                                            <button class="btn btn-danger" id="btn{{$p->payhistory->id}}" onclick="requestPayment({{$p->payhistory->id}})">Request Payment!</button>
                                                        @else
                                                            {{--else show condition not met button--}}
                                                            <button class="btn btn-warning">Reservations Missing!</button>
                                                        @endif
                                                    @else
                                                        {{--else show button--}}
                                                        <button class="btn btn-danger" id="btn{{$p->payhistory->id}}" onclick="requestPayment({{$p->payhistory->id}})">Request Payment!</button>
                                                    @endif

                                                @endif

                                            @endif

                                        </td>
                                        <td>{{$p->payhistory->total_amount_usd}} $</td>
                                        <td>{{$p->payhistory->exchange}}</td>
                                        <td>{{explode(' ',$p->payhistory->pay_date)[0]}}</td>
                                        <td><a href="paymentdetails/{{$p->payhistory->id}}">View Details</a></td>
                                    </tr>

                                    @php $index++; @endphp



                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



@endsection

@push('scripts')

<script>

    function requestPayment(id){

        $('#btn'+id).attr('disabled', true).text('Payment Requested!');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            data: {periodhistoryid:id},
            url: '/models/requestpayment',
            success: function (data) {

                if(data.success) {
                    location.reload();
                } else console.log(data.data);

            }

        });
    }

</script>


@endpush