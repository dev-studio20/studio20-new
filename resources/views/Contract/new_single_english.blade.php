<div><b>
        <center>MANAGEMENT AND ARTISTIC IMPRESARIO CONTRACT</center>
    </b></div>
<center><span><span><b>No. {{ $id }} signed on {{ $date }}</b></span></span></center>
<span>
            <br><span><br/> <br/>The parties:<br/>(collectively referred to as "<strong>the Parties</strong>" and the individual  "<strong>Party</strong>"): <br/><br><br><br>
                GlobalMaca Holdings SRL, a cypriot legal person, having its registered office in Chrysanthou Milona 1, Panagides Court, Ofice 602, 3030 Limassol, Cyprus, with a fiscal code HE381214, IBAN GB79REVO00996914166749, the bank RevolutBank, legally represented by the Director Mugurel Cosmin Frunzetti, as manager and agent of the Artist, hereinafter referred to as the "<strong>Manager</strong>",
                <br/>
                and<br/>
        {{ $name }} {{ $lname }},permanent resident in {{ $country }}, town {{ $city }}, address{{ $address }}, CNP (or SSN) {{ $idn }}
        , IBAN bank account {{ $iban }} open at {{ $bank }} bank, email {{ $email }}, phone number +{{ $phone }}
        , referred to in this contract as "<strong>Artist</strong>"<br/></span></span><br><br>

<span>
    <span>Considering that:</span>
</span><br><br>

<ul>
    <li>a)    The artist intends to pursue an artistic career in the audio-video field, recognizing the extent of the efforts expected from a professional artist in this field, as well as taking diligence in their accomplishment;</li>
    <li>b)    The manager owns the know-how, the expertise and the staff required to provide professional management and artistic services;
    </li>
    <li>c)    The manager enjoys a professional reputation in the field of art production and streaming, collaborates with professional third-parties who bring to their audience / clients their own creations in the field through various web sites and also with several studios for recording and broadcasting;
    </li>
    <li>d)    Making artists' artworks known to the public involves the assignment of patrimonial rights of authors to these creations to professional third parties;
    </li>
    <li>e)   The Artist intends to collaborate with the Manager for better management of his efforts, for professional guidance and for identifying opportunities that can lead to the artist's career development;
    </li>
    <li>f)    The manager intends to accept the task of managing the Artist's career as efficiently as possible in view of an exclusive and long-lasting collaboration;
    </li>
    <li>g)    Both parties consider themselves to be professionals and have carefully studied this contract, understanding and agreeing to all of its terms;</li>
</ul>
<br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>
    <span>They have decided to conclude this contract (the ”</span><strong> Contract</strong><span>&rdquo;) in the following terms:</span>
</span><br><br>

<span>Article 1 – Subject of the Contract</span><br><br>

<span>1.1   By this Agreement, the Parties shall establish the terms and conditions applicable to an exclusive professional collaboration between the Manager and the Artist, throughout the duration of the Contract, which shall include the following:</span>
<br><br>
<ul>
    <li>  a)    Entrepreneur's activity and management of the Artist and of its creations, namely: Manager's submission of all efforts to identify and involve the Artist in projects of a nature to increase both the artist's popularity, his professional level and the fees paid by the Artist (both individually and / or together with other artists).
    </li>
    <li>  b)    Valuation of the works created by the artist, as follows: The artist promises to transfer by assignment the patrimonial rights of the author on the works he will create during the period of this contract to the Manager, who will then assign them to third parties in the recording and broadcasting activity, the purpose of the assignment being that the Manager obtains the most advantageous price on these works;
    </li>
    <li>  c)  Subsequent to the assignment of the patrimonial rights of the author to the Artist's creations, by the Manager to the professional third-parties, the Manager will refund the Artist a percentage, as set forth in Art. 4.1., the percentage applied to the price obtained, the difference being retained by the Manager, as the price of the services mentioned under point a), rendered services to the Artist's benefit.</li>
</ul>

<span>1.2.  The Artist understands that the Manager's activity involves discussions and negotiations to identify opportunities to take advantage of the Artist's career, and the Manager will make certain statements, promises and commitments to third parties in the name and on behalf of the Artist. In order for the Manager to efficiently accomplish this common goal, the Artist grants the Manager, by signing the Contract, a representation mandate valid for the entire duration of the Contract and for all operations reasonably circumscribed to the image of the person and / or artist's career, both for the preservation and management of the Artist's performances.
</span><br>

<span>For the avoidance of any doubt, the Parties states that (i) in principle, any commitments, agreements, partnerships, conventions or contracts concluded with third parties in relation to the Artist's activity, career, image or person will be signed by both, the Manager and Artist (who will assume exclusively and completely all the performances related to his person), and (ii) as exception, the Manager, respectively the Artist, will conclude separately legal acts previously mentioned in the name and on behalf of the Artist only after the confirmation by the other Party of the essential conditions that are the subject of the respective acts, the Artist's liability for its performances remains exclusive and integral.
</span><br><br>

<span>Article 2 – Duration of the Contract</span><br><br>

<span>2.1 This Agreement shall enter into force at the time of signing and shall take effect until the expiration of a period of 5 (five) years from the date of signature.
</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>2.2 Except when a cause of premature termination of the Contract occurs, it shall automatically be extended, under the same conditions, for successive periods of 1 (one) year.
</span><br><br>


<span>Article 3 – Exclusivity</span><br><br>

<span>3.1 The Artist declares that he / she understands and accepts that the Manager makes a financial and logistic effort to develop under the best conditions of the Artist's career and to increase its popularity and notoriety, and this goal cannot be achieved unless the Artist's promotion takes place in a professional and unitary way. Thus, the Artist understands that only the Manager will be able to engage in the relationships and activities of the nature of the subject matter of this Agreement, the exclusivity conferred upon the Manager being an essential condition both for the efficiency of the Manager's performance and for the results that may result from this Agreement.
    </span><br>
<span>3.2 Therefore, all professional relationships, commitments, agreements, conventions or contracts relating to or referring to the Artist, to the Artist's image, to his career, to the Artist's performances, individually or together with other artists, such as but not limited to, third-party artisitic, recordings, streaming or marketing professionals who are about to be publicly disclosed or who are eligible to be publicly disclosed, will be concluded only in accordance with the provisions of this Contract with the agreement and participation of the Manager.    </span><br>
<span>3.3 In the absence of a prior written consent of the Manager, no other person, including the Artist, will be able to exercise during the term of this Agreement any of the rights conferred on the Manager under this Agreement, he / she will not be able to hold the Artist's manager and will not be able to acquire from the Artist fees and / or commissions related to such activity.    </span><br>
<span>3.4 The manager may represent other artists without limitation.</span><br><br>

<span>Article 4 – Contract price</span><br><br>

<span>4.1. During this Agreement, the Artist will pay to the Manager a 50% applied to the gross remuneration or any other
    Valuable Benefits earned by the Artist as a result of any activity that may reasonably be circumscribed to the scope
    of this Agreement (whether such activity relates to the Artist's person, image, or career) such as but not limited
    to:</span><br>
<ul>
    <li>a) Audio, video, text or photo recordings, distributed by any channel and methods;</li>
    <li>b) live streaming and other public appearances through streaming media technology;</li>
    <li>c) exploitation of patrimonial rights of the author and related rights;</li>
    <li>d) image exploitation;</li>
    <li>e) sponsorship or patronage contracts;</li>
    <li>f) promotion and publicity activities;</li>
    <li>g) the exploitation of any Internet site or site entirely or partially dedicated to the Artist or its activity</li>
</ul>

<span>Paying the Artist a 50% gross of the total, all payments shall be made in EUR..</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>4.2. In addition to the 50% base percentage, the Manager will pay the Artist the following bonuses:</span><br><br>
<ul>
    <li> - 2% of the amounts collected, if the Artist offers more than 90 hours of live artistic performances in a half-month period (between 1 and 15 or between 16 and 30/31 of the month);    </li>
    <li> - 5% of the amounts collected, if the Artist offers more than 100 hours of live artistic performances in a half-month period (between 1 and 15 or between 16 and 30/31 of the month);    </li>
    <li> - 5% of the amounts collected, if the Artist generates more than 10,000 (ten thousand) US dollars in a half-month (between 1 and 15 or between 16 and 30/31 of the month);    </li>
    <li> - 5% of the amounts collected, if the Artist has more than 12 months of running this Contract with the Manager and had artistic creations each contractual month;   </li>
    <li> - 30% of the amounts collected, if the Artist ensures its entire space and facilities, as well as the studio assistance required for live broadcasts and audio, photo and video recordings that are the subject of this Contract;    </li>
    <li> - any other bonus that the Manager decides to offer as a special reward to the Artist.</li>
</ul>

<span>These bonuses will be paid monthly, quarterly, semesterly or yearly, in advance or retroactive, according to the    Manager’s decision.</span><br><br>

<span>4.3. For all the contracted activities covered by this Agreement, the Manager will collect the negotiated amounts from third parties and pay the Artist the resulting amounts after deducting the due percentage to the Manager.</span><br><br>
<span>Payments will be made to the Artist monthly on 25th of each month for revenue earned by the Manager in the previous
    calendar month from their third-party professional by the Manager as a result of the Artist's activities and
    creations. The Manager can pay in advance on the 10th of each month, a part of the previous months’s revenue to
    enable the Artist to finance his creative and interpreting activities.
</span><br><br>

<span>4.4. The Artist will be responsible for accounting for his / her own earnings and expenses, without involving or empowering the Manager in any way. The Artist's income statements and any other tax obligations of the Artist will be the sole responsibility of the Artist.
</span><br>
<span>4.5. The Manager will not be obliged to pay the negotiated amounts due to the Artist in accordance with the agreements concluded under this Contract in the event that she / he does not collect the remuneration due from the third parties obliged to pay the mentioned amounts. In all cases, the Parties will agree on the legal remedies that will be imposed as a result of payment incidents.</span><br>
<span>4.6. If the Artist asks the Manager to rent a studio for live broadcasts or recordings and does not present to use the reserved space, regardless of the cause, the Manager will deduct from Artist 100 EUR (one hundred euro) for each unused reservation (a reservation has between 4 and 12 hours). </span><br>
<span>4.7. In case the manager notices or suspenct the Artist is breaking any obligation from this contract, the Manager has the right to suspend any payments towards the Artist, unltil the partied have agreed on the value of damages or, if they cannot reach an agreement, the case was solved by the court.
</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Article 5 – The Services and Rights of the Manager</span><br><br>

<span>5.1. In general, the Manager will use his resources and expertise to identify opportunities for increasing the reputation, notoriety of the Artist and for the development of his career. Particularlly, the Manager undertakes to make every reasonable effort to carry out the following actions:</span><br><br>
<span>5.1.1 to carry out activities necessary for the development of the Artist's career;</span><br>
<span>5.1.2 to negotiate all contracts regarding all Artist's artistic activities in full agreement with the Artist;</span><br>
<span>5.1.3 to identify the artist's opportunities to collaborate with third parties specialized in the artistic production and streaming activity throughout the world, acting as his / her agent;
</span><br>
<span>5.1.4 counseling the Artist in any matter related to his / her careers;</span><br>
<span>5.1.5 identification of specialized providers of support services for the artistic activity in the field of artistic production and broadcasting at a professional level, as well as other services related to the carrying out of a professional activity such as, but not limited to: accounting, marketing and public relations services, etc.;
</span><br>
<span>5.1.6 supervising and managing the Artist's income revenue;</span><br>
<span>5.1.7  creating and finalizing the artist's image and style (clothing, footwear, hairstyle, makeup), including through collaboration with specialized service providers;</span><br>
<span>5.1.8 recommending and implementing methods of promoting the Artist (videos, photographers, journalistic articles, participation in events specific to artistic production and broacasting, etc.), including through collaboration with specialized service providers;
</span><br>
<span>5.1.9 recommending and making a web page or website dedicated to the Artist, including through collaboration with specialized suppliers;</span><br>
<span>5.1.10 developing a list of objectives to be accomplished in relation to the Artist's career development and work plans to meet those objectives.</span><br><br>

<span>5.2.The Manager is entitled:</span><br><br>

<span>5.2.1 to be permanently mentioned by the Artist as its Manager, in the Artist's third party's professional relationship. The name and / or the Manager's insignia will appear on all Artist's works and on the means of promoting the Artist, such as, but not limited to, videos, live streaming media, photos, etc.;
</span><br>
<span> 5.2.2  to negotiate and sign all contracts in connection with any Artist's artistic activities in accordance with Art. 1.2;</span><br>
<span>5.2.3  to decide on any matter related to the Artist's image;</span><br>
<span> 5.2.4 to collect the proceeds of the Artist's artistic activities and to pay the Artist the amounts due under this Contract;</span><br>
<span> 5.2.5 to decide when, where, and under what conditions, any public appearances of the Artist (including but not limited to: streaming multimedia technology, events dedicated to artistic production, etc.);
</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 5.2.6 to financially sanction the Artist's manifestations of the nature to jeopardize the proper performance of this Agreement, including but not limited to:</span>
<ul>
    <li> a) repeated and excessive drinking of alcohol;</li>
    <li> b) any method of drug usage;  </li>
    <li> c) any behavior incompatible with her artistic activity (1. language that incites to hatred, violence, racism, discrimination, etc. 2. violent manifestations of any nature etc.);</li>
    <li> d) non-observance of the image decided by the Manager; </li>
    <li> e) non-compliance with exclusivity; </li>
    <li> f) not presenting to the artistic activities of any type established by the Manager etc.; </li>
    <li> g) non-observance of the rules and regulations of the sites and platforms that the Artist broadcasts his works;</li>
    <li> h)  the public disclosure of confidential details such as the Artist's personal data;</li>
    <li> i) asking for money ot other benefits considering the artistic activity or the brands used, <b>{{$modelname}}</b>, which are owned by the Manager, any king of personal relations and communication with their fans.</li>
</ul>
<span> 5.2.7 to hold the exclusive right of exploitation of the Artist's image during the term of this Agreement, having the exclusive right to negotiate and conclude contracts for the use of the image, logo, trademark or any other element identifying the Artist;
</span><br>
<span> 5.2.8 to use freely, directly or indirectly, for the entire spectrum of advertising, the first and last name, the pseudonym, photos and other images representing the Artist, provided that it does not prejudice its public image. If the Artist provides photos or other images to the Manager, the Artist will be fully responsible for obtaining the rights of use from their authors and will ensure the Manager against any claim that may be made by the authors of the photographs and / or  the images or from any other part that contributed to their realization (make-up artist, hair stylist, fashion styling, fashion models, directors, screenwriters, etc.);
</span><br>
<span> 5.2.9 to provide similar services to other persons and to engage in other companies or associations having an object of activity similar to or identical to this Agreement and may perform its activity as a Manager even for artists whose artistic talent may be similar or which can find itself in competition with the Artist, without having to dedicate all the activity or all the resources and time in favor of the Artist.
</span><br><br>

<span> Article 6 – Artist's Obligations and Rights</span><br><br>

<span> 6.1. In addition to those set out in this Agreement, the Artist undertakes:</span>
<br>
<span> 6.1.1  to permanently prepare for the artistic activity that he / she carries out through physical exercises (rehearsals, trainings, etc.), physical maintenance, a balanced life regime, so that his / her artistic performance will always rise to the highest quality level;
</span><br>
<span> 6.1.2 in the framework of artistic performances, to have an appropriate behavior for the professional artistic activity;
</span><br>
<span> 6.1.3 in the framework of artistic performances, to have an appropriate behavior for the professional artistic activity;
</span><br>
<span> 6.1.4 to comply with the commitments assumed by third parties through the legal acts concluded by the Manager under this Contract and to have in all cases appropriate professional conduct;</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 6.1.5 to transmit to the Manager all requests received directly from third parties for artistic performances, the Manager will negotiate and eventually sign the contracts;</span><br>
<span> 6.1.6 not to perform any performance of the nature described by the Agreement for any natural or legal person without the prior written consent of the Manager;</span><br>
<span> 6.1.7 to abide by the decisions of the Manager in relation to his style and image (clothing, footwear, hairstyle, make-up, etc.);</span><br>
<span> 6.1.8 not to make audio and / or video recordings, as well as live performances by streaming multimedia technology, without the prior written approval of the Manager;</span><br>
<span> 6.1.9 not to negotiate and conclude with third Parties intellectual property assignment contracts. These contracts will be negotiated by the Manager in full agreement with the Artist and will then be signed by the Artist and countersigned by the Manager;</span><br>
<span> 6.1.10 to mention the Manager in any written or verbal reference in the professional artistic activity;</span><br>
<span> 6.1.11  not to make public statements about the data and information relating to the performance of the Contract and not to take any actions or statements that could prejudice the public image of the Manager, colleagues from the artistic group (s) to be subject to the provisions of this Agreement, and not to issue any kind of statements / actions that could adversely affect their activity for the entire duration of this Agreement and for a period of 2 (two) years after the cessation thereof;
</span><br>
<span> 6.1.12 The Artist shall not have the right, without the prior written authorization of the Manager, to create and / or manage or designate other persons, to create and / or administer any site, website, account for any Internet site in related to the project (s) covered by this Agreement.</span><br><br>

<span> 6.2. The Artist is entitled:</span><br><br>

<span> 6.2.1 to be advised by the Manager about his / her artistic orientation and artistic style approached and be consulted on collaborators proposed by the Manager (if applicable);</span><br>
<span> 6.2.2  to be advised on decisions related to his / her image, including image associations with private entities. If the Artist does not want to associate his or her own name and image with certain private entities, they will be communicated in writing to the Manager;</span><br>
<span> 6.2.3 establish the artistic program in agreement with the Manager;</span><br><br>

<span> Article 7 – Contractual liability</span><br><br>

<span> 7.1. In the event that the Parties have not previously assessed the amount of damages due to a breach of a contractual obligation by the Penalty, the Party directly or indirectly guilty of failure to perform, inadequate or late fulfillment of the obligations of this Agreement shall assume the obligation to indemnify the other Party direct financial damages provided for in the Agreement or foreseeable at the time of the conclusion of the Contract, whether they are effective (damnum emmergens) at the time of their occurrence, or are derived from (lucrum cessans), such as, but not limited to, loss of data or information, loss of revenue or profits, customer loss, replacement value, fees paid to third parties, expenses, fees incurred by the injured Party and / or third parties.
</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 7.2. The obligation to compensate for the damages provided for in the previous paragraph shall be applicable regardless of whether the Party in fault was or was not warned or could foresee the possibility of causing the damage, and whether the party who suffered the damage foresaw or could foresee it.
</span><br>
<span> 7.3. If the Manager repeatedly receives written complaints from third parties about the Artist's poor artistic performance or its conduct in relation to the recipients, the public or other persons involved in the realization of the action for which the Artist was contracted, or in the cases presented in Article 5.1.6. the manager will be entitled to penalize the Artist a 100% of the amounts owed to the Artist for the defective performance or for which the complaints have been filed.</span><br>
<span> 7.4. Given that the exclusivity conferred to the Manager is a essential condition for which he has concluded this
Agreement, if the Artist violates the exclusivity of the Manager, as regulated in Art. 3 of this Contract, in any
manner, or closes/deletes or asks any third party for the closure/deletion of any artistic accounts for broadcasting, recording or marketing, she / he will owe and pay the amount of 50.000 EUR, as compensation, and the Manager will not be held to proof any injury.
</span><br><br>
<span> Article 8 – Notifications</span><br><br>
<span> 8.1. Notifications submitted by the Parties in accordance with the provisions of this Agreement shall be in writing and will be valid only if the form of transmission allows confirmation of receipt. Both Parties are obliged to transmit the new contact details as soon as possible in case they change. Otherwise, notifications sent to previously communicated contact details will be considered valid.</span><br>
<span> 8.2. The work addresses for receipt of correspondence are the following, at the date of signing the Contract:</span><br>
<ul>
    <li>a) For Manager:<br> Chrysanthou Milona 1, Panagides Court, Ofice 602, 3030 Limassol, Cyprus and contact@globalmaca.net;</li>
    <li>b) For Artist:<br> {{$address}}, state {{$state}}, town {{$city}}, {{$country}}, email: {{$email}}.</li>
</ul><br><br>

<span>Article 9 – Termination and Suspension of the Contract</span><br><br>

<span>9.1 This Agreement terminates by reaching the deadline, and the tacit renewal according to Art. 2.1. is not applicable if at least one Party notifies its intention to terminate the contract, at least 60 days prior to the expiration date of the duration for which the contract was concluded / extended.</span><br>
<span>9.2 The contract may be terminated by the common will of the Parties expressed in writing.</span><br>
<span>9.3 Given the professional and logistical effort undertaken by the Manager to promote the Artist's image and interests, effort made in consideration of a long-standing commercial collaboration, the Parties expressly agree that the unilateral denunciation of this Agreement is not permitted.</span><br>
<span>9.4 The Contract will terminate, by dissolution - the termination pact, in case of non-fulfillment or inadequate fulfillment of the obligations assumed by the Parties, as follows:</span><br>
<ul>

    @if($semnatura !== '')
        <div class="parent blue">
            <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
            <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
        </div>
    @endif

    <div class="page-break"></div>

    <li>a) By Manager:<br>
        - The payment obligation stipulated in Art. 4.1, in the case where there is a delay with no reason and justification in its execution of at least 30 days.</li>
    <li>b) By Artist:<br>
        - Obligation consisting in the Artist's promise to transfer the author’s patrimonial rights stipulated in art. 1.1. lett. b);<br>
        - Obligation to grant exclusivity to the Manager, provided in art. 3.1; 3.2. and 3.3;</li>
</ul>
<br>
<span>9.5 For the application of the termination pact stipulated in Art. 9.4, the interested Party will communicate to the other Party a notification requesting the fulfillment or proper fulfillment of the obligation be made within maximum 30 days. If after the expiration of this term, the obligation is not fulfilled or is inadequately fulfilled, the contract will be terminated by law, through the effect of the termination pact.</span><br>
<span>9.6 As regards the other obligations, non-fulfillment or inadequate fulfillement of other contractual obligations, other than those listed in art. 9.4, the contract may be terminated by either party, by unilateral dissolution.</span><br>
<span>9.7 For the application of the unilateral dissolution stipulated in art. 9.6, the interested party will communicate to the other party a notification requesting the fulfilment or proper fulfillment of the obligation within a reasonable time, according to the specific nature of the obligation. If after the expiration of this term, the obligation is not fulfilled or is inadequately fulfilled, the interested party will apply the unilateral dissolution of the contract, notifying the other party of this circumstance.</span><br>
<span>9.8 The Contract will be suspended automatically, and its duration will be extended accordingly, if the Artist, for personal reasons, finds himself for periods higher than 30 days in total or partial incapacity to devote himself to developing his / her career (for example because of a medical condition or as a result of family problems).</span><br><br>

<span>As exception from the provisions of Art. 9.3 above, the Manager can unilaterally denounce this Agreement if the period of suspension of the Contract exceeds 2 (two) months. For the avoidance of doubt, it is stated that the suspension of the Contract under this Article will not affect the rights of the Parties to the fees deriving from previous performances.</span><br><br>

<span>Article 10 – Effects of termination of the contract</span><br><br>

<span>10.1 Termination of the Agreement does not affect the execution of obligations already existing between the Parties.</span><br>
<span>10.2 In the event of termination of the Contract, any legal act concluded in connection with this Agreement shall remain in effect and shall take effect until the expiration date, thus not affecting the execution of obligations already due between the Parties.</span><br>
<span>10.3 Obligations that, expressly or implicitly, are intended to survive or begin to produce effects upon the termination of this Agreement or which may take effect upon termination of the Contract shall have such effects.</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Article 11 – Processing of personal data</span><br><br>

<span>11.1 The Artist understands that the Manager will process his / her personal data, under the provision of EU legislation regarding personal data and privacy, the purpose of the processing being exclusively the execution of this Contract.</span><br><br>

<span>Article 12 – Confidentiality</span><br><br>

<span>12.1 The Parties undertake to take all necessary measures to ensure the confidentiality of all data and information to which they will have access under this Agreement, non-public data and information which, by their nature, cannot be disclosed to third parties.</span><br>
<span>12.2 The confidentiality of the data and information will be retained for the duration of this Agreement and for an indefinite time period after its termination.</span><br><br>

<span>Article 13 – Non-renunciation of rights</span><br><br>

<span>13.1 Not exercising or delaying the exercise by one Party of any of its rights from this Agreement shall not constitute and / or will not be interpretated as a renunciation of such rights and shall not prevent the future exercise of such rights.</span><br><br>

<span>Article 14 – Force Majeure and Fortuitous event</span><br><br>

<span>14.1 The Force Majeure and Fortuitous event are applicable to this Agreement.</span><br><br>

<span>Article 15 – Transfer of Contract</span><br><br>

<span>15.1 The Artist understands that the transfer, assignment, partial or total novation of his rights and obligations under this Agreement to third parties is not permitted.</span><br>
<span>15.2 The Manager has the right to subcontract any of its performances without the Artist's consent. The Manager will remain, in all cases, fully liable for the subcontracted performances.</span><br><br>

<span>Article 16 – Jurisdiction</span><br><br>

<span>16.1 The Parties have agreed that any disagreement on the validity of this Agreement or resulting from its interpretation, execution or termination shall be settled amiably by their representatives.</span><br>
<span>16.2 Any dispute arising out of or in connection with this Agreement, unresolved amiably by the Parties, shall be settled by the Cyprus courts.</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Article 17 – Applicable law</span><br><br>

<span>17.1 This agreement is subject to law, interpretation and jurisdiction on the Manager's main office country and city.</span><br><br>

<span>Article 18 – Complete Will</span><br><br>

<span>18.1 This Agreement fully reflects the will of the Parties, contains the entire agreement of the Parties' will in respect of the subject matter of this Agreement and supersedes any agreements or documents concluded prior to this Agreement or negotiations that have taken place in writing or orally between the Parties regarding aspects addressed in this Agreement. There are no secondary elements relating to the Agreement and the agreement of the Parties which have not been reflected in this Agreement.</span><br><br>

<span>Article 19 – Effectiveness of clauses</span><br><br>

<span>19.1 If one or more clauses of this Agreement are to be declared void, the Parties undertake to replace them by other clauses as close as possible to the meaning and purpose of this Agreement, the other provisions remaining valid and must be performed by the Parties.</span><br><br>

<span>Article 20 – Understanding of the contract</span><br><br>

<span>20.1 The Parties expressly declare that:</span>

<span>(i) each Party has provided all necessary information for the other Party to express unwarranted consent to the conclusion of the Contract;</span><br>
<span>(ii) have read and understood completely the content and effects of all contractual terms, including, but not limited to, the essential elements mentioned in the Agreement. In particular, the Parties declare irrevocably that they have been fully informed and understood all provisions of the Agreement that have been negotiated in good faith and agreed by both Parties and, by signing the Contract, the Parties freely express their agreement to contract in order to acquire all rights and obligations under this Agreement, as a whole, so that any subsequent misunderstanding by one Party of any provision of the Agreement will in no way affect the contractual relationship that the agreement is based on;</span><br>
<span>(iii) have been assisted during negotiations by lawyers / legal advisers who have correctly and fully explained the contractual and legal effects of all the clauses of this Agreement, including, but not limited to, essential elements mentioned in the Contract;</span><br>
<span>(iv) is not in error of fact (de facto) or error of law (de jure) in respect of any of the clauses of the Agreement;</span><br>
<span>(v) any obligation they have assumed under the Agreement, including, but not limited to, damages and / or penalties, is a fair and reasonable equivalent of the other party's damage caused by the inability to execute and none of them prejudices;</span><br>
<span>(vi) is not in any of the situations that may be described as a state of necessity.</span><br>

<br>
<span>20.2 Without prejudice to any statements and warranties under this Agreement, the Artist expressly assumes the risk of error in respect of his obligations.</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Article 21 – Essential and unusual clauses</span><br><br>

<span>The Parties expressly declare that the essential elements of the Contract in the absence of which this Agreement was not concluded are the clauses relating to: the identity of the Parties, the subject of the Contract, the duration of the Contract and the impossibility of unilateral denunciation, exclusivity, payment, late payment penalties and / or damages that a Party is entitled to receive in the event of breach of obligations by the other Party.</span><br>

<span>21.2. The Parties declare that they have acknowledged and expressly accepted the unusual clauses contained in this Agreement, in the following articles:</span>
<ul>
    <li>Art. 2.2: Tacit renewal of the Contract;</li>
    <li>Art. 3: Exclusivity;</li>
    <li>Art. 16: Jurisdiction;</li>
    <li>Art. 17: Applicable law.</li>
</ul>

Signed  today, <b>{{ $date }}</b>, in two original copies, one for each Party.<br><br>
GlobalMaca Holdings Ltd SRL,<br>
By Mugurel Cosmin Frunzetti<br>
As a Manager,<br>
____________________________
<div class="col"><img src="{{$semnaturaManager}}" width="200" height="200"/></div>

{{ $name }} {{ $lname }}<br>
As an Artist,<br>
____________________________
<div id="semnaturaArtist" class="col"><img src="{{$semnatura}}" width="200" height="200"/></div>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="displayarea" style="width:200px;height:200px;display: inline;"></div>
        </div>
    </div>
</div>