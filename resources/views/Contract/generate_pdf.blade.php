<link href="{{ asset('css/generate_contract.css') }}" rel="stylesheet"/>
<style>
    .page-break {
        page-break-after: always;
    }
    .parent {
        position: relative;
        width: 80%;
        height: 10px;
        margin: 0 4px;
    }
    .previous, .next {
        position: absolute;
        width: 30px;
        height: 20px;
        top: 0;
        bottom: 0;
        margin: auto;
    }
    .previous {
        left: 0;
        margin-left: 15px;
    }
    .next {
        right: 0;
        margin-right: 15px;
    }

</style>

@if($acctype==='pf')
    @if($country==='Romania')
        @include('Contract.ro_en')
    @else
        @include('Contract.en_en')
    @endif
@endif

@if($acctype==='pj')
    @include('Contract.pj')

@endif
