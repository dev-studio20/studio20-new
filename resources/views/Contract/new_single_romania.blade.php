<div><b>
        <center>CONTRACT DE MANAGEMENT SI IMPRESARIAT ARTISTIC</center>
    </b></div>
<center><span><span><b>Nr. {{ $id }} din data de {{ $date }}</b></span></span><br><br></center>
<span>
            <span><br/> <br/>Partile:<br/>(denumite impreuna "<strong>Partile</strong>" si individual "<strong>Partea</strong>"): <br/><br><br><br>
                GlobalMaca Holdings Ltd, persoana juridica cipriota, cu sediul social in Chrysanthou Milona 1, Panagides Court, Ofice 602, 3030 Limassol, Cyprus, Numar de inregistrare HE381214, IBAN GB79REVO00996914166749, banca RevolutBank, reprezentata legal prin Director Mugurel Cosmin Frunzetti, in calitate de manager si impresar al Artistului, denumita in continuare "<strong>Manager</strong>",
                <br/>
                si<br/>
                {{ $name }} {{ $lname }}, rezident permanent in {{ $country }}, oras {{ $city }}, adresa {{ $address }}, CNP (or SSN) {{ $idn }}
                , cont bancar IBAN {{ $iban }} deschis la banca {{ $bank }}
                , email {{ $email }}, numar de telefon mobil  +{{ $phone }}
                , denumit in prezentul contract "<strong>Artist</strong>"<br/></span></span><br><br>

<span>
    <span>Avand in vedere faptul ca:</span>
</span><br><br>

<ul>
    <li>a) Artistul intentioneaza sa urmeze o cariera artistica in domeniul audio-video, avand la cunostinta dimensiunea
        eforturilor asteptate de la un artist profesionist in acest domeniu, precum si asumarea diligentelor in
        indeplinirea acestora;
    </li>
    <li>b) Managerul detine know-how-ul, expertiza si personalul necesare prestarii unor servicii profesionale de
        management si impresariat artistic;
    </li>
    <li>c) Managerul se bucura de o reputatie profesionala in domeniul broadcastingului, respectiv colaboreaza cu terti
        profesionisti in domeniul productie si distributiei<br> artistice, care aduc la cunostinta publicului/
        clientilor proprii, creatiile artistilor in domeniu, prin intermediul diferitelor web-siteuri, precum si cu mai
        multe studiouri de inregistrari si transmisii live;
    </li>
    <li>d) Aducerea la cunostinta publicului a creatiilor artistilor presupune cesionarea drepturilor patrimoniale de
        autor asupra acestor creatii, catre tertii profesionisti;
    </li>
    <li>e) Artistul intentioneaza sa colaboreze cu Managerul pentru o mai buna gestionare a eforturilor sale, pentru
        indrumarea profesionala si pentru identificarea unor oportunitati care sa poata conduce la dezvoltarea carierei
        Artistului;
    </li>
    <li>f) Managerul intentioneaza sa accepte sarcina de a gestiona cat mai eficient cariera Artistului in considerarea
        unei colaborari exclusive si de lunga durata;
    </li>
    <li>g) Ambele parti se considera profesionisti si au studiat cu atentie contractul de fata, intelegand si fiind de
        acord cu toate clauzele acestuia;
    </li>
</ul>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>
    <span>Au hotarat incheierea prezentului contract (denumit in continuare”</span><strong> Contractul</strong><span>&rdquo;) cu respectarea urmatoarelor conditii:</span>
</span><br><br>

<span>Articolul 1 - Obiectul Contractului</span><br><br>

<span>1.1   Prin acest Contract, Partile stabilesc termenii si conditiile aplicabile unei colaborari profesionale exclusive intre Manager si Artist, pe toata durata Contractului, colaborare ce va presupune:</span>
<br><br>
<ul>
    <li> a) Activitate de impresariat si manageriere a Artistului si a creatiilor acestuia, respectiv: depunerea de
        catre Manager a tuturor eforturilor in vederea identificarii si implicarii Artistului in proiecte de natura a
        creste atat popularitatea Artistului, nivelul sau profesional, cat si onorariile percepute de Artist (atat
        individual cat si/sau impreuna cu alti artisti);
    </li>
    <li> b) Punerea in valoare a operelor create de artist, astfel: Artistul promite ca va transmite prin cesiune,
        drepturile patrimoniale de autor asupra operelor pe care le va crea pe durata acestui contract catre Manager,
        acesta urmand a le cesiona la randul sau catre terti profesionisti in activitatea de broadcasting, scopul
        cesiunii fiind acela ca Managerul sa obtina cel mai avantajos pret asupra acestor opere;
    </li>
    <li> c) Ulterior cesionarii drepturilor patrimoniale de autor asupra creatiilor Artistului, de catre Manager catre
        tertii
        profesionisti, Managerul va transmite Artistului un procent, astfel cum este stabilit in art. 4.1. , procent
        aplicat
        asupra pretului obtinut, diferenta fiind retinuta de catre Manager, cu titlu de pret al serviciilor mentionate
        la
        litera a), servicii prestate in beneficiul Artistului.
    </li>
</ul>

<span>1.2.  Artistul intelege ca activitatea Managerului implica discutii si negocieri in vederea identificarii unor
    oportunitati care sa profite carierei Artistului, Managerul urmand a face anumite declaratii, promisiuni si
    angajamente tertilor in numele si pe seama Artistului. Pentru ca Managerul sa poata aduce la indeplinire in mod
    eficient acest deziderat comun, Artistul confera Managerului, prin semnarea Contractului, un mandat cu reprezentare,
    valabil pe toata durata Contractului si pentru toate operatiunile care, in mod rezonabil, sunt circumscrise
    imaginii, persoanei si/sau carierei Artistului, atat pentru conservarea si administrarea prestatiilor Artistului.
</span><br>

<span>Pentru evitarea oricarui dubiu, Partile precizeaza ca (i) in principiu, orice angajamente, acorduri, parteneriate,
    conventii sau contracte incheiate cu tertii in legatura cu activitatea, cariera, imaginea sau persoana Artistului
    vor fi semnate atat de catre Manager, cat si de catre Artist (care isi va asuma exclusiv si integral toate
    prestatiile ce tin de persoana sa), si (ii) cu titlu de exceptie, Managerul, respectiv Artistul, vor incheia separat
    actele juridice precizate anterior in numele si pe seama Artistului numai in urma confirmarii de catre cealalta
    Parte a conditiilor esentiale ce fac obiectul actelor respective, raspunderea Artistului in legatura cu prestatiile
    sale ramanand exclusiva si integrala.</span><br><br>

<span>Articolul 2 &ndash; Durata Contractului</span><br><br>


<span>2.1 Acest Contract intra in vigoare la momentul semnarii si isi va produce efectele pana la expirarea unei perioade
        de 5 (cinci) ani calculata de la data semnarii.</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>2.2 Cu exceptia cazului in care intervine o cauza de incetare prematura a Contractului, acesta se va prelungi
        automat, in aceleasi conditi, pe durate succesive de [1] (un) an.</span><br><br>


<span>Articolul 3 &ndash; Exclusivitate</span><br><br>


<span>3.1 Artistul declara ca intelege si accepta faptul ca Managerul realizeaza un efort financiar si logistic pentru
        dezvoltarea in cele mai bune conditii a carierei Artistului si pentru cresterea popularitatii si notorietatii
        acestuia, iar acest obiectiv nu poate fi atins decat daca promovarea Artistului are loc intr-o modalitate
        profesionala si unitara. Astfel, Artistul intelege ca doar Managerul se va putea implica in relatiile si
        activitatile de natura celor ce fac obiectul acestui Contract, exclusivitatea conferita Managerului fiind o
        conditie esentiala atat pentru eficienta prestatiilor Managerului, cat si pentru rezultatele ce pot izvori din
        acest Contract.<br>
    </span>
<span>3.2 Prin urmare, toate relatiile profesionale, angajamentele, acordurile, conventiile sau contractele ce au ca
        obiect sau care se refera la Artist, la imaginea Artistului, la cariera sa, la prestatiile Artistului,
        individual sau impreuna cu alti artisti, cum ar fi dar fara a se limita la, intelegerile cu terti profesionisti
        in domeniul broadcasting-ului ce urmeaza a fi comunicate public sau care sunt apte a fi comunicate public, vor
        fi incheiate numai in conformitate cu prevederile acestui Contract, cu acordul si participarea Managerului.
    </span><br>
<span>3.3 In lipsa acordului scris si prealabil al Managerului, nicio alta persoana, inclusiv Artistul, nu va putea
        exercita, pe durata acestui Contract, vreunul din drepturile conferite Managerului potrivit acestui Contract, nu
        va putea detine calitatea de manager al Artistului si nu va putea dobandi de la Artist onorarii si/sau
        comisioane in legatura cu o atare activitate.
    </span><br>
<span>3.4 Managerul va putea reprezenta alti artisti, fara limitare.</span><br><br>

<span>Articolul 4 &ndash; Pretul Contractului</span><br><br>

<span>4.1. Pe durata acestui Contract, Managerul va retine un procent de 50% aplicat la remuneratia bruta sau la orice alte
    beneficii evaluabile in bani realizate de Artist, ca urmare a realizarii oricarei activitati care, in mod rezonabil,
    poate fi circumscrisa ariei de aplicare a acestui Contract (indiferent daca o asemenea activitate se refera la
    persoana, imaginea, sau cariera Artistului), cum ar fi, dar fara a se limita la:</span>

<ul>
    <li>a) productii audio, video, text sau foto, inregistrate si difuzate prin orice mijloace;</li>
    <li>b) prestatii live si alte aparitii destinate publicului prin intermediul tehnologiei multimedia de tip
        streaming;
    </li>
    <li>c) exploatarea drepturilor patrimoniale de autor si a drepturilor conexe;</li>
    <li>d) exploatarea imaginii;</li>
    <li>e) sponsorizari sau contracte de mecenat;</li>
    <li>f) activitati de promovare si publicitate;</li>
    <li>g) exploatarea oricarei pagini de Internet sau site dedicat total sau partial Artistului sau activitatii sale
    </li>
</ul>

<span>Remunerand Artistul cu un procent de 50% brut din totalul realizat, toate platile vor fi facute in EUR.</span><br>
<br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>


<span>4.2. Suplimentar fata de procentul de baza de 50%, Artistul este indreptatit la urmatoarele bonusuri:</span><br>
<br>

<ul>
    <li> - 2% din sumele incasate, daca Artistul ofera mai mult de 90 de ore de prestatii artistice live intr-o perioada
        de jumatate de luna (intre 1 si 15 sau intre 16 si 30/31 ale lunii);
    </li>
    <li> - 5% din sumele incasate, daca Artistul ofera mai mult de 100 de ore de prestatii artistice live intr-o perioada
        de jumatate de luna (intre 1 si 15 sau intre 16 si 30/31 ale lunii);
    </li>
    <li> - 5% din sumele incasate, daca Artistul genereaza incasari mai mari de 10.000 (zece mii) USD intr-o perioada de
        jumatate de luna (intre 1 si 15 sau intre 16 si 30/31 ale lunii);
    </li>
    <li> - 5% din sumele incasate, daca Artistul are mai mult de 12 luni de derulare a prezentului contract cu Managerul
        si a avut creatii artistice in fiecare luna contractuala;
    </li>
    <li> - 30% din sumele incasate, daca Artistul isi asigura singur, integral, spatiile si echipamentele, precum si
        asistenta de studio necesare transmisiilor live si inregistratilor audio, foto si video care fac obiectul
        prezentului contract;
    </li>
    <li> - orice alt bonus pe care Managerul decide sa il ofere artistului pentru mertitele sale speciale.</li>
</ul>

<span>Aceste bonusuri pot fi platite lunar, trimestial, semestrial sau anual, in avans sau retroactiv, in functie de
    decizia Managerului.</span><br><br>

<span>4.3. Pentru toate activitatile contractate ce fac obiectul prezentului Contract, Managerul va incasa sumele negociate
    de la terti si va achita Artistului sumele rezultate dupa scaderea procentului cuvenit Managerului.</span><br><br>

<span>Platile vor fi efectuate catre Artist lunar, in data de 25 a fiecarei luni, pentru veniturile incasate de Manager de
    la tertii profesionisti, in luna calendaristica anterioara, ca urmare a activiatii si creatiilor
    Artistului.Managerul poate plati in avans, pe 10 a fiecarei luni, o parte din veniturile lunii anterioare, pentru a
    permite Artistului finantarea activitatilor sale de creatie si interpretare.</span><br><br>

<span>4.4. Artistul se va ocupa de contabilizarea propriilor venituri si cheltuieli, fara a implica sau responsabiliza
    Managerul in niciun fel. Declaratiile de venit ale Artistului si orice alte obligatii fiscale ale acestuia
    constituie responsabilitatea exclusiva a Artistului.</span><br>
<span>4.5. Managerul nu va fi obligat la plata sumelor negociate cuvenite Artistului conform acordurilor incheiate in baza
    acestui Contract, in cazul in care nici acesta, la randul sau, nu incaseaza remuneratia cuvenita de la tertii
    obligati a achita sumele mentionate. In toate cazurile, Partile vor conveni impreuna asupra remediilor legale ce se
    vor impune ca urmare a incidentelor de plata.</span><br>
<span>4.6. Daca Artistul solicita Managerului inchirierea de spatii de studio pentru transmisii live sau inregistrari si nu
    se prezinta pentru a utiliza spatiile rezervate, indiferent de cauza, Managerul va deduce din sumele cuvenite
    Artistului 100 EUR (osuta euro) pentru fiecare rezervare neutilizata (o rezervare avand intre 4 si 12 ore).</span><br>
<span>4.7. In cazul in care Managerul observa sau suspecteaza orice incalcari ale contractului de catre Artist, acesta are
    dreptul de a suspenda orice plati pana la lamurirea completa a situatiei si stabilirea pe care amiabila ori in
    instanta a daunelor produse.</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Articolul 5 – Prestatiile si drepturile Managerului</span><br><br>

<span>5.1. In general, Managerul va folosi resursele si expertiza sa in vederea identificarii unor oportunitati pentru
    cresterea reputatiei, notorietatii Artistului si pentru dezvoltarea carierei sale. In particular, Managerul se
    obliga sa depuna toate eforturile rezonabile in vederea indeplinirii urmatoarele actiuni:</span><br><br>

<span>5.1.1 realizarea activitatilor necesare dezvoltarii carierei Artistului;</span><br>

<span>5.1.2 negocierea tuturor contractelor privind toate activitatile artistice ale Artistului in deplina intelegere cu
    acesta;</span><br>
<span>5.1.3 identificarea oportunitatilor de colaborare a Artistului cu terti specializati in activitatea de productie
    artistica broadcasting pe intreg mapamondul, actionand in calitate de impresar al acestuia;</span><br>
<span>5.1.4 consilierea Artistului in orice privinta aferenta cariere sale;</span><br>
<span>5.1.5 identificarea unor furnizori specializati de servicii de suport pentru desfasurarea activitatii artistice in
    domeniul broadcasting-ului la un nivel profesionist, precum si alte tipuri de servicii conexe desfasurarii
    activitatii profesionale, precum, dar fara a se limita la: servicii de contabilitate, servicii de marketing si
    public relations, etc.;</span><br>
<span>5.1.6 supravegherea si gestionarea incasarii veniturilor Artistului;</span><br>
<span>5.1.7 crearea si definitivarea imaginii si stilului artistului (imbracaminte, incaltaminte, coafura, machiaj),
    inclusiv prin colaborarea cu furnizori de servicii specializati;</span><br>
<span>5.1.8 recomandarea si punerea in practica a metodelor de promovare a Artistului (videoclipuri; fotografii; articole
    publicistice; participarea la evenimente specifice domeniului productiei artistice si broadcasting-ului, etc.),
    inclusiv prin colaborarea cu furnizori de servicii specializati;</span><br>
<span>5.1.9 recomandarea si realizarea a unei pagini de web sau a unui website dedicat Artistului, inclusiv prin
    colaborarea cu furnizori specializati;</span><br>
<span>5.1.10 elaborarea unei liste de obiective de indeplinit in legatura cu dezvoltarea carierei Artistului si planuri de
    lucru pentru indeplinirea obiectivelor respective.</span><br><br>

<span>5.2. Managerul are dreptul:</span><br><br>

<span>5.2.1 sa fie mentionat in permanenta de catre Artist in calitate de Manager al acestuia, in raporturile profesionale
    ale Artistului cu tertii. Numele si/sau insemnele Managerului vor aparea pe toate operele Artistului si pe
    mijloacele de promovare a acestuia, precum, dar fara a se limita la: videoclipuri, reprezentantii live prin
    intermediul tehnologiei multimedia de tip streaming, fotografii, etc.;</span><br>
<span> 5.2.2 sa negocieze si sa semneze toate contractele in legatura cu orice activitati artistice ale Artistului
    respectandu-se prevederile Art 1.2;</span><br>
<span>5.2.3 sa decida in orice chestiune aferenta imaginii Artistului;</span><br>
<span> 5.2.4 sa incaseze sumele provenite din contractarea activitatilor artistice ale Artistului si sa plateasca
    Artistului sumele ce ii revin conform prezentului Contract;</span><br>
<span> 5.2.5 sa decida cand, unde si in ce conditii vor avea loc aparitiile publice de orice fel ale Artistului (incluzand,
    dar fara a se limita la: aparitii prin tehnologia multimedia de tip streaming, evenimente dedicate activitatii de
    broadcasting, etc.);</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 5.2.6 sa sanctioneze financiar manifestarile Artistului de natura a pune in pericol derularea in bune conditii a
    prezentului Contract incluzand, dar fara a se limita la:</span><br>
<span> a) consumul repetat si exagerat de alcool;</span><br>
<span> b) orice modalitate de consum de droguri;</span><br>
<span> c) comportamentul incompatibil cu activitatea de artist si de broadcaster (1. limbaj care incita la ura, violenta,
    rasism, discriminare etc.; 2. manifestari violente de orice natura etc.);</span><br>
<span> d) nerespectarea imaginii decise de Manager;</span><br>
<span> e) nerespectarea exclusivitatii;</span><br>
<span> f) neprezentarea la activitatile artistice de orice tip stabilite de Manager etc.;</span><br>
<span> g) nerespectarea regulilor si regulamentelor site-urilor si platformelor pe care Artistul isi difuzeaza operele;</span><br>
<span> h) dezvaluirea publica sau private a unor detalii confidentiale precum datele personale ale Artistului;</span><br>
<span> i) solicitarea oricaror de sume de bani avand in vedere activitatea sa de artist, brandurile de artist folosit,
    respectiv <b>{{$modelname}}</b> si care sunt detinute de Manager, orice relatii sau comunicari personale purtate cu fanii
    sai.</span><br>
<span> 5.2.7 sa detina dreptul de exploatare in mod exclusiv asupra imaginii Artistului pe perioada prezentului Contract
    avand dreptul exclusiv de a negocia si incheia contracte privind utilizarea imaginii, a logo-ului, a marcii sau a
    oricarui alt element de identificare a Artistului;</span><br>
<span> 5.2.8 sa utilizeze liber, direct sau indirect, pentru intreg spectrul publicitatii, numele si prenumele,
    pseudonimul, fotografiile si alte imagini care il reprezinta pe Artist, cu conditia de a nu aduce prejudicii
    imaginii publice a acestuia. In cazul in care Artistul furnizeaza fotografii sau alte imagini Managerului, Artistul
    va fi pe deplin raspunzator de obtinerea drepturilor de utilizare de la autorii acestora si asigura Managerul
    impotriva oricarei pretentii ce ar putea fi emisa de autorii fotografiilor si/ sau autorii imaginilor respective sau
    oricare alta parte care a contribuit la realizarea lor (make-up artist, hair stylist, styling vestimentar,
    fotomodele, regizori, scenaristi etc.);</span><br>
<span> 5.2.9 sa ofere servicii similare altor persoane si sa se angajeze in alte companii sau asociatii care au obiect de
    activitate similar sau identic prezentului Contract si poate presta activitatea sa de Manager chiar pentru artisti
    al caror talent artistic ar putea fi similar sau care s-ar putea afla in competitie cu Artistul, fara a fi obligat
    sa-si dedice intreaga activitate ori totalitatea resurselor si a timpului in favoarea Artistului.</span><br><br>


<span> Articolul 6 – Obligatiile si drepturile Artistului</span><br><br>

<span> 6.1. Suplimentar fata de cele expuse in acest Contract, Artistul se obliga:</span><br>
<span> 6.1.1 sa se pregateasca permanent pentru activitatea artistica pe care o desfasoara, prin exercitiu sustinut
    (repetitii, antrenamente etc), intretinere fizica, un regim de viata echilibrat, astfel incat prestatia sa artistica
    sa se ridice de fiecare data la cel mai inalt nivel calitativ;</span><br>
<span> 6.1.2 in cadrul prestatiilor artistice, sa aiba un comportament adecvat activitatii profesionale de
    broadcasting;</span><br>
<span> 6.1.3 sa nu acorde interviuri si sa nu participe la emisiuni radio sau TV, in legatura cu activitatea de
    broadcasting, decat cu aprobarea prealabila a Managerului si in conditiile stabilite de catre acesta;</span><br>
<span> 6.1.4 sa respecte intocmai angajamentele asumate fata de terti prin intermediul actelor juridice incheiate de
    Manager potrivit acestui Contract si sa aiba, in toate cazurile, o conduita profesionala corespunzatoare;</span><br>
<span> 6.1.5 sa transmita catre Manager toate cererile primite direct de la terti pentru prestatii artistice, Managerul
    urmand a negocia si eventual semna contractele;</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 6.1.6 sa nu efectueze nicio prestatie de natura celor descrie prin Contract pentru nicio persoana fizica sau
    juridica fara stiinta si aprobarea prealabila in scris a Managerului;</span><br>
<span> 6.1.7 sa respecte intocmai deciziile Managerului in legatura cu stilul si imaginea sa (imbracaminte, incaltaminte,
    coafura, machiaj etc);</span><br>
<span> 6.1.8 sa nu efectueze inregistrari audio si/sau video, precum si reprezentatii live prin tehnologia multimedia de
    tip streaming, fara aprobarea prealabila, in scris, a Managerului;</span><br>
<span> 6.1.9 sa nu negocieze si sa nu incheie cu terti contracte de cesiune a drepturilor de proprietate intelectuala.
    Aceste contracte vor fi negociate de catre Manager in deplina intelegere cu Artistul si vor fi apoi semnate de catre
    Artist si contrasemnate de catre Manager;</span><br>
<span> 6.1.10 sa mentioneze Managerul in orice referire scrisa sau verbala din activitatea profesionala de
    broadcasting;</span><br>
<span> 6.1.11 sa nu faca declaratii publice cu privire la datele si informatiile legate de derularea Contractului si sa nu
    intreprinda niciun fel de actiuni sau declaratii ce ar putea prejudicia imaginea publica a Managerului, a colegilor
    din grupul/grupurile artistice ce se vor supune prevederilor prezentului Contract, a partenerilor sai contractuali,
    si sa nu emita nici un fel de declaratii/ actiuni care ar putea afecta in mod negativ activitatea acestora, pe
    intreaga durata a prezentului Contract, precum si pentru un termen de 2 (doi) ani de la incetarea acestuia;</span><br>
<span> 6.1.12 Artistul nu va avea dreptul, fara autorizarea prealabila, in scris, a Managerului, sa realizeze si/sau sa
    administreze sau sa desemneze alte persoane sa realizeze si/sau sa administreze orice site, pagina de internet, cont
    pentru orice pagina de Internet in legatura cu proiectul/ proiectele care fac obiectul prezentului Contract.</span><br><br>

<span> 6.2. Artistul are urmatoarele drepturi:</span><br><br>

<span> 6.2.1 sa fie consiliat de catre Manager cu privire la orientarea sa artistica si stilul artistic abordat si sa fie
    consultat cu privire la colaboratori propusi de Manager (daca este cazul);</span><br>
<span> 6.2.2 sa fie consiliat in ceea ce priveste deciziile legate de imaginea sa, inclusiv in cazul asocierilor de imagine
    cu enitati private. In cazul in care Artistul nu doreste asocierea numelui propriu si a imaginii sale cu anumite
    enitati private, acestea vor fi comunicate in scris Managerului;</span><br>
<span> 6.2.3 sa stabileasca programul artistic de comun acord cu Managerul.</span><br><br>

<span> Articolul 7 – Raspunderea Contractuala</span><br><br>

<span> 7.1. In situatia in care Partile nu au evaluat anticipat, prin clauza penala, cuantumul despagubirilor rezultate din
    incalcarea unei obligatii contractuale, Partea aflata, direct sau indirect, in culpa pentru neindeplinirea,
    indeplinirea necorespunzatoare sau cu intarziere a obligatiilor din prezentul Contract isi asuma obligatia de a
    despagubi cealalta Parte pentru orice prejudicii pecuniare directe, prevazute in Contract sau previzibile la
    momentul incheierii Contractului, fie ca sunt efective (damnum emmergens) la momentul producerii acestora sau
    reprezinta foloase nerealizate (lucrum cessans). In acest sens mentionam, cu titlu exemplificativ: prejudicii de
    imagine, pierderi de date sau informatii, pierderi de incasari sau profituri, pierderi de clientela, valoare de
    inlocuire, onorarii platite unor terti, cheltuieli, taxe suportate de Partea prejudiciata si/sau de terti.</span><br>
<span> 7.2. Obligatia de despagubire prevazuta la alineatul anterior, este aplicabila indiferent daca Partea in culpa a
    fost sau nu avertizata sau putea sa prevada posibilitatea producerii prejudiciului, si indiferent daca Partea care a
    suferit prejudiciul l-a prevazut sau putea sa il prevada.</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span> 7.3. In cazul in care Managerul va primi in mod repetat reclamatii scrise de la terti in legatura cu slaba prestatie
    artistica a Artistului sau in legatura cu comportamentul acestuia in relatia cu beneficiarii, publicul sau cu alte
    persoane implicate in realizarea activitatii pentru care a fost contractat Artistul, sau in cazurile prevazute de
    Art. 5.1.6., Managerul va fi indreptatit sa penalizeze Artistul cu 100% din sumele cuvenite acestuia pentru
    prestatia efectuata defectuos sau in legatura cu care s-au inregistrat reclamatiile.</span><br>
<span> 7.4. Avand in vedere ca exclusivitatea conferita Managerului reprezinta pentru acesta o conditie esentiala
pentru care a incheiat prezentul Contract, in cazul in care Artistul aduce atingere, in orice mod,  exclusivitatii
Managerului, astfel cum este reglementata in art. 3 din prezentul Contract, sau inchide/sterge ori solicita oricarei terte parti sa inchida/stearga orice cont artistic folosit pentru transmisii live, inregistrari sau marketing, va datora si achita Managerului, suma de 50.000 EUR, cu titlu de despagubiri.</span><br><br>


<span> Articolul 8 – Notificari</span><br><br>

<span> 8.1. Notificarile transmise de catre Parti conform prevederilor prezentului Contract se vor face in scris si vor fi
    valabile doar in cazul in care forma de transmitere permite confirmarea primirii. Ambele Parti sunt obligate ca, in
    cel mai scurt timp, sa transmita noile date de contact in caz ca acestea se schimba. In caz contrar se vor considera
    valabile notificarile transmise la datele de contact comunicate anterior.</span><br>
<span> 8.2. Adresele de lucru pentru primirea corespondentei, la data semnarii prezentului Contract, sunt urmatoarele:</span><br><br>

<ul>
    <li>a) Pentru Manager:<br> Chrysanthou Milona 1, Panagides Court, Ofice 602, 3030 Limassol, Cyprus, contact@globalmaca.net;
    </li>
    <li>b) Pentru Artist:<br> {{$address}} judet {{$state}}, oras {{$city}}, {{$country}}, email: {{$email}}.</li>
</ul>
<br><br>

<span>Articolul 9 – Incetarea si Suspendarea Contractului</span><br><br>

<span>9.1. Prezentul Contract inceteaza prin ajungerea la termen,
iar renoirea tacita conform art. 2.1. nu va opera, daca cel
putin una dintre Parti notifica intentia sa de a inceta
contractul, cu minim 60 de zile anterior expirarii duratei
    pentru care acesta a fost perfectat/ prelungit.</span><br>
<span>9.2. Contractul poate inceta prin vointa comuna a Partilor
exprimata in scris.</span><br>
<span>9.3. Avand in vedere efortul profesional si logistic asumat
de Manager in vederea promovarii imaginii si intereselor
Artistului, efort depus inclusiv in considerarea unei
colaborari profesionale de lunga durata, Partile convin in
mod expres ca denuntarea unilaterala a acestui Contract nu
este permisa.</span><br>
<span>9.4. Contractul va inceta, prin reziliere - pact comisoriu, in
cazul neindeplinirii sau indeplinirii necorespunzatoare a
obligatiilor asumate de catre parti, astfel:</span><br>
<ul>
    <li>a) De catre Manager:<br>
        - Obligatia de plata prevazuta in art. 4.1, in ipoteza in care
        exista o intarziere nemotivata si nejustificata in executarea
        acesteia de minim 30 de zile.</li><br>

    @if($semnatura !== '')
        <div class="parent blue">
            <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
            <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
        </div>
    @endif

    <div class="page-break"></div>

    <li>b) De catre Artist:<br>
        - Obligatia constand in promisiunea Artistului de a cesiona
        drepturile patrimoniale de autor prevazuta in art. 1.1. lit. b);<br>
        - Obligatia de a acorda exclusivitate Managerului,
        prevazuta in art. 3.1; 3.2. si 3.3;</li>
</ul>
<span>9.5. In aplicarea pactului comisoriu prevazut in art. 9.4,
partea interesata va comunica celeilalte parti o notificare,
prin care ii va solicita indeplinirea sau indeplinirea
corespunzatoare a obligatiei in termen de maxim 30 de zile.
In ipoteza in care si dupa expirarea acestui termen, obligatia
nu este indeplinita sau este indeplinita in mod
necorespunzator, contractul va fi reziliat de drept, prin
    efectul pactului comisoriu.</span><br>

<span>9.6. In ceea ce priveste celelalte obligatii, neindeplinirea sau
indeplinirea necorespunzatoare a celorlalte obligatii contractuale, altele decat cele enumerate in art. 9.4,
contractul poate fi reziliat de catre oricare parte, prin
    reziliere unilaterala.</span><br>

<span>9.7. In aplicarea rezilierii unilaterale prevazute in art. 9.6,
partea interesata va comunica celeilalte parti o notificare,
prin care ii va solicita indeplinirea sau indeplinirea
corespunzatoare a obligatiei intr-un termen rezonabil,
raportat la specificul obligatiei. In ipoteza in care si dupa
expirarea acestui termen, obligatia nu este indeplinita sau
este indeplinita in mod necorespunzator, partea interesata
va aplica rezolutiunea unilaterala a contractului, notificand
cealalta parte despre aceasta imprejurare.</span><br>

<span>9.8. Contractul va fi suspendat automat, durata acestuia
urmand a fi prelungita in mod corespunzator, in cazul in
care Artistul, din motive personale, se regaseste pentru
perioade mai mari de 30 zile in incapacitate totala sau
partiala de a se dedica dezvoltarii carierei sale (de exemplu
ca urmare a unei afectiuni medicale sau ca urmare a unor
    probleme familiale).</span><br>
<span>Cu titlul de exceptie de la prevederile Art. 9.3 de mai sus,
Managerul va putea denunta unilateral acest Contract in
situatia in care durata suspendarii Contractului va depasi 2
(doua) luni. Pentru evitarea oricarui dubiu, se precizeaza ca
suspendarea Contractului potrivit acestui articol nu va
afecta drepturile Partilor la onorarii izvorand din prestatii
    anterioare.</span><br><br>


<span>Articolul 10 – Efectele incetarii contractului</span><br><br>

<span>10.1. Incetarea Contractului nu afecteaza executarea
obligatiilor deja scadente intre Parti.</span><br>
<span>10.2. In situatia incetarii Contractului, orice act juridic
incheiat in legatura cu acest Contract va ramane in vigoare
si isi va produce efectele pana la momentul incetarii,
nefiind astfel afectata executarea obligatiilor deja scadente
intre Parti.</span><br>
<span>10.3. Obligatiile care, in mod expres sau implicit, sunt
intentionate a supravietui sau a incepe sa-si produca
efectele la incetarea prezentului Contract sau care isi mai
pot produce efecte la incetarea contractului, isi vor produce
aceste efecte.</span><br><br>

<span>Articolul 11 – Prelucrarea datelor cu caracter personal</span><br><br>

<span>11.1. Artistul intelege ca Managerul va prelucra datele cu
caracter personal ale acestuia, fiind aplicabile dispozitiile
legislatiei EU privind colestarea si utilizarea datelor cu
character personal, scopul prelucrarii fiind exclusiv
executarea prezentului contract.</span><br><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>Articolul 12 – Confidentialitate</span><br><br>

<span>12.1. Partile se obliga sa ia toate masurile necesare pentru a
asigura confidentialitatea tuturor datelor si informatiilor la
care vor avea acces in temeiul prezentului Contract, date si
informatii care nu au caracter public si care, prin natura lor,
nu pot fi dezvaluite tertelor parti.</span><br>
<span>12.2. Confidentialitatea datelor si informatiilor va fi pastrata
pe toata durata prezentului Contract, cat si pe o perioada
nedeterminata de timp dupa incetarea acestuia.</span><br><br>

<span>Articolul 13 – Nerenuntarea la drepturi</span><br><br>

<span>13.1. Neexercitarea sau intarzierea exercitarii de catre una
dintre Parti a oricaruia dintre drepturile sale prevazute de
prezentul Contract nu va constitui si / sau nu va fi
interpretata ca o renuntare la aceste drepturi si nu va
impiedica exercitarea viitoare a acestor drepturi.</span><br><br>

<span>Articolul 14 – Forta Majora si Cazul Fortuit</span><br><br>

<span>14.1. Forta Majora si Cazul Fortuit sunt aplicabile
prezentului Contract.</span><br><br>

<span>Articolul 15 – Transferul Contractului</span><br><br>

<span>15.1. Artistul intelege faptul ca transferul, cesiunea, novatia
partiala sau totala a drepturilor si obligatiilor sale potrivit
    acestui Contract unor terti nu este permisa.</span><br>
<span>15.2. Managerul are dreptul de a subcontracta oricare dintre
prestatiile sale, fara acordul Artistului. Managerul va
ramane, in toate cazurile, integral raspunzator pentru
prestatiile subcontractate.</span><br><br>

<span>Articolul 16 – Jurisdictie</span><br><br>

<span>16.1. Partile au convenit ca toate neintelegerile privind
validitatea prezentului Contract sau rezultate din
interpretarea, executarea sau incetarea acestuia sa fie
rezolvate pe cale amiabila de catre reprezentantii lor.</span><br>
<span>16.2. Orice litigiu decurgand din sau in legatura cu
prezentul Contract, nesolutionat de catre Parti in mod
amiabil, va fi supus spre solutionare instantelor
<span>Articolul 17 – Lege aplicabila</span><br><br>
judecatoresti din Cipru.</span><br><br>


<span>17.1. Prezentul Contract este supus legislatiei din Cipru si interpretat in conformitate cu prevederile acesteia.</span><br><br>

<span>Articolul 18 – Vointa completa</span><br><br>

<span>18.1. Prezentul Contract reflecta in intregime vointa
Partilor, contine intregul acord de vointa al acestora in ceea
ce priveste obiectul prezentului Contract si inlocuieste orice
acorduri sau documente incheiate inainte de acest Contract
sau negocierile care au avut loc in scris sau oral intre Parti
in ceea ce priveste aspectele abordate in prezentul Contract.

    @if($semnatura !== '')
        <div class="parent blue">
    <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
    <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
</div>
    @endif

    <div class="page-break"></div>

Nu exista elemente secundare referitoare la Contract si
acordul Partilor, care nu au fost reflectate in acest Contract.</span><br>

<span>Articolul 19 – Eficacitatea clauzelor</span><br><br>

<span>19.1. In situatia in care una sau mai multe clauze din acest
Contract vor fi declarate nule, Partile se obliga sa le
inlocuiasca prin alte clauze, cat mai apropiate de sensul si
scopul acestui Contract, celelalte prevederi ramanand valide
si urmand a fi executate de Parti.</span><br><br>

<span>Articolul 20 – Intelegerea contractului</span><br><br>

<span>20.1. Partile declara in mod expres ca:</span><br>
<span>(i) fiecare Parte a pus la dispozitie toate informatiile
necesare pentru ca cealalta Parte sa-si poata exprima
consimtamantul neviciat cu privire la incheierea
Contractului;</span><br>
<span>(ii) au citit si au inteles complet continutul si efectele
tuturor clauzelor contractuale, inclusiv, dar fara a se limita
la cele referitoare la elementele esentiale mentionate in
Contract. In mod special, Partile declara irevocabil ca au
fost informate si au inteles complet toate prevederile din
Contract, care au fost negociate cu buna-credinta si
convenite de ambele Parti si, prin semnarea Contractului,
Partile isi exprima liber acordul de a contracta cu scopul de
a dobandi toate drepturile si obligatiile in temeiul
prezentului Contract, in ansamblul lor, astfel incat orice
neintelegere ulterioara de catre o Parte, a oricarei prevederi
a Contractului, nu va influenta in niciun fel relatiile
    contractuale pe care acordul se bazeaza;</span><br>
<span>(iii) au beneficiat de asistenta in timpul negocierilor din
partea avocatilor/consilierilor juridici care le-au explicat in
mod corect si in intregime efectele contractuale si legale ale
tuturor clauzelor prezentului Contract, inclusiv, dar fara a se
limita la cele referitoare la elementele esentiale mentionate
    in Contract;</span><br>
<span>(iv) nu se afla in eroare de fapt (de facto) sau de drept (de
jure) in ceea ce priveste oricare dintre clauzele
    Contractului;</span><br>
<span>(v) fiecare obligatie pe care si-au asumat-o in conformitate
cu Contractul, inclusiv, dar fara a se limita la plata
despagubirilor si/sau penalitator, reprezinta un echivalent
corect si rezonabil al prejudiciului celeilalte parti cauzat de
incapacitatea de a executa si nimic dintre acestea nu le
    prejudiciaza/lezeaza;</span><br>
<span>(vi) nu se afla in oricare dintre situatiile care ar putea fi
   descrise ca stare de necesitate.</span><br>
<span>20.2. Fara a aduce atingere oricarei declaratii si garantii din
prezentul Contract, Artistul isi asuma in mod expres riscul
    de eroare in ceea ce priveste obligatiile sale.</span><br><br>

<span>Articolul 21 – Clauze esentiale si neuzuale</span><br><br>

<span>21.1. Partile declara in mod expres ca elementele esentiale
ale Contractului, in absenta carora prezentul Contract nu ar
fi fost incheiat, sunt reprezentate de clauzele referitoare la:
identitatea Partilor, obiectul Contractului, durata
Contractului si imposibilitatea denuntarii unilaterale,
exclusivitatea, pretul Contractului si modalitatea de plata,
penalitatile de intarziere si/sau despagubirile pe care o Parte
este indreptatita sa le primeasca in cazul incalcarii
    obligatiilor de catre cealalta Parte.</span><br>

@if($semnatura !== '')
    <div class="parent blue">
        <div class="previous green"><div><img  src="{{$semnatura}}" width="150" height="150"/></div></div>
        <div class="next red"><div><img  src="{{$semnaturaManager}}" width="150" height="150"/></div></div>
    </div>
@endif

<div class="page-break"></div>

<span>21.2. Partile declara ca au luat la cunostinta si accepta in
mod expres clauzele neuzuale din Contract, cuprinse in
    urmatoarele articole:</span><br>
<ul>
    <li>Art. 2.2: Reinnoirea tacita a contractului;</li>
    <li>Art. 3: Exclusivitate;</li>
    <li>Art. 16: Jurisdictie;</li>
    <li>Art. 17: Legea aplicabila.</li>
</ul>

Semnat astazi, <b>{{ $date }}</b>, in doua exemplare originale, cate unul pentru fiecare Parte.<br><br>
GlobalMaca Holdings Ltd SRL,<br>
Prin Mugurel Cosmin Frunzetti<br>
In calitate de Manager,<br>
____________________________
<div class="col" id="semnaturaManager">
    <img src="{{$semnaturaManager}}" width="200" height="200"/>
</div>

{{ $name }} {{ $lname }}<br>
In calitate de Artist,<br>
____________________________
<div id="semnaturaArtist" class="col"><img src="{{$semnatura}}" width="200" height="200"/></div>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="displayarea" style="width:200px;height:200px;display: inline;"></div>
        </div>
    </div>
</div>
<div class="page-break"></div>