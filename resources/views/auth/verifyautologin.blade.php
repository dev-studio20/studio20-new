<table style="background: #ececec url('https://studio20girls.com/bg-tile-grey.jpg') repeat-x left top;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td align="center" valign="top">
            <table border="0" width="750" cellspacing="0" cellpadding="0" align="center">
                <tbody>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" width="690" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                            <tr>
                                <td align="right" valign="top"><img src="https://studio20girls.com/shim.gif" width="1" height="30" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table style="background: #ffffff; border: 1px solid #dddddd;" border="0" width="668" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="padding-top: 30px;" align="center">
                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td align="left" valign="top" width="100%">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="padding: 10px 0px 20px 20px; color: #505050; font-family: Helvetica, Arial; font-size: 28px; font-weight: bold; line-height: 100%; text-align: center;" align="left" valign="top">
                                                <h5><a href="https://studio20.group"><strong><span style="color: #ff0000;">Studio20 - We succeed together!</span></strong></a></h5>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td align="right" valign="top"></td>
                            </tr>
                            </tbody>
                        </table>

                        <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td style="padding: 0px 65px;" align="left" valign="top">
                                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td style="color: #505050; font-family: Helvetica,Arial; font-size: 20px; font-weight: bold; line-height: 100%; padding-bottom: 20px;">Email confirmation for '{{ $modelname }}'</td>
                                        </tr>
                                        <tr>
                                            <td style="color: #525252; font-family: Helvetica,Arial; font-size: 14px; line-height: 21px; font-weight: 300; padding-bottom: 40px;">Hello ,

                                                Thank you for registering on our website www.studio20.group.

                                                Please verify your email address by click-ing on the activation link bellow
                                                <table style="height: 23px;" width="523">
                                                    <tbody>
                                                    <tr>
                                                        <td><a href="{{ $url }}">{{ __('Click activation link') }}</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                &nbsp;

                                                If you have any questions please contact us at: <a href="mailto:contact@studio20.group">contact@studio20.group</a>.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table border="0" width="668" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="border-bottom-width: 6px; border-bottom-color: #b3b3b3; border-bottom-style: solid;" colspan="2" align="left" valign="middle"><img src="https://studio20girls.com/shim.gif" width="1" height="20" /></td>
                </tr>
                <tr>
                    <td align="left" valign="middle"></td>
                    <td>
                        <table style="color: #878787; font-family: Helvetica,Arial; font-size: 11px; line-height: 13px; font-weight: 300; text-align: right;" border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td style="padding-top: 16px;" align="right" valign="top"></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 6px;" align="right" valign="top"><a style="color: #8dae55;" href="https://studio20girls.com" target="_blank" rel="noopener">https://studio20girls.com</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px 0 30px;" colspan="2"><img src="https://studio20girls.com/divider-grey.jpg" width="689" height="1" /></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
