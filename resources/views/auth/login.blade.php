<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <title> Studio20 -  Login</title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="robots" content="noindex, nofollow">

    <link href="{{ asset('img/models/favicon.ico') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/main.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/style.default.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/models/bootstrap.min.css') }}" rel="stylesheet"/>

</head>

<body>

<style>
    .login-page .form-holder .info, .login-page .form-holder .form {
        min-height: 0!important;
    }

</style>

<div class="page login-page">

    <div class="container d-flex align-items-center">

        <div class="form-holder has-shadow">

            <div class="row">

                <div class="col-lg-6">

                    <div class="info d-flex align-items-center">

                        <div class="content">

                            <div class="logo">

                                <h1>Model Login.</h1>

                            </div>

                            <p style="color:white;">Log into your model account</p>

                        </div>

                    </div>

                </div>

                <!-- Form Panel    -->

                <div class="col-lg-6 bg-white">

                    <div class="form d-flex align-items-center">

                        <div class="content">

                            <form method="POST" action="{{ route('login') }}">

                                @csrf

                                <div class="form-group">

                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group">

                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <input id="login" name="submit" type="submit" value="Login" class="btn btn-primary">

                            </form>

                            <a class="forgot-pass"  href="{{ route('model.password.request') }}">{{ __('Forgot Your Password?') }}</a><br>
                            <small>Do not have an account? </small><a href="{{route('register')}}" class="signup">Signup</a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

</body>

</html>

