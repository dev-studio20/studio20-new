<html>
<head>
    <title>Model Center</title>
    {{--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>--}}
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <link href="{{ asset('css/models/register.css') }}" rel="stylesheet"/>

    <script src="{{ asset('material/js/tether.min.js') }}"></script>
    <script src="{{ asset('material/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('material/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('material/js/front.js') }}"></script>
    <script src="{{ asset('material/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('material/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('material/js/jquery.animateNumber.min.js') }}"></script>

    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>

<div class="page login-page">
    <div class="container-fluid">

        {{ Form::open(['url' => 'register', 'files' => true, 'class'=>'col-lg-8 offset-lg-2 col-sm-12', 'style' => 'background:white;']) }}

        <div id="header-form">
            <div class="logo">
                <h1>Model Registration</h1>
            </div>
        </div>

        <div class="form-group row">
            {{Form::label('model_first_name', 'First Name', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{Form::text('model_first_name', null, [ 'class'=> 'form-control', 'required' => 'required' ])}}
                <span class="error text-danger">{{ $errors->first('model_first_name') }}</span>
            </div>

            {{Form::label('model_last_name', 'Last Name', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('model_last_name', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('model_last_name') }}</span>
            </div>

            {{Form::label('email', 'E-Mail Address', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::email('email', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('email') }}</span>
            </div>

            <label for="password" class="col-md-4  text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}"
                       name="password" required @php echo $errors->has('password') ? 'autofocus' : '' @endphp>


                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>

            </div>

            <label for="password-confirm"
                   class="col-md-4  text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" value="{{(old('password_confirmation'))}}" name="password_confirmation" required>
            </div>

            <div  class="col-md-4  text-md-right"><span style="color:red">*</span></div>
            <div class="col-md-6"><i>
                    Password must be at least 8 characters long and contain at least one Uppercase (A-Z), Lowercase (a-z), Digits (0-9), Special (~!@#$%^&*()_+-=)
                </i></div>


            {{Form::label('birthdate', 'Birth Date', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::date('birthdate', \Carbon\Carbon::now()->subYears(25), [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('birthdate') }}</span>
            </div>

            {{Form::label('sex', 'Sex', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('sex', ['F' => 'Female', 'M' => 'Male', 'C' => 'Couple'], 'F', [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('sex') }}</span>
            </div>

            {{Form::label('country', 'Country', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('country', $countries, 1, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('country') }}</span>
            </div>

            {{Form::label('state', 'State', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('state', [1 => 'Bucharest'], 1, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('state') }}</span>
            </div>

            {{Form::label('city', 'City', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('city', [1 => 'Bucharest'], 1, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('city') }}</span>
            </div>

            {{Form::label('address', 'Address', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('address', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('address') }}</span>
            </div>

            {{Form::label('phone', 'Mobile Phone', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('phone', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('phone') }}</span>
            </div>

            {{Form::label('idn', 'Personal Identification Number', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('idn', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('idn') }}</span>
            </div>

            <div class="col-md-4  text-md-right">
                <label for="broadcastingspace radio-inline">I want the Manager to provide me with broadcasting
                    and recording studios</label>
            </div>
            <div class="col-md-6">
                <input type="radio" name="broadcastingspace" onchange="enable();" value="yes" checked> Yes<br>
                <input type="radio" name="broadcastingspace" onchange="disable();" value="no"> No, I'll broadcast
                from home<br>
            </div>

            {{Form::label('studio', 'Studio', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('studio', [1 => 'Studio20 Victoriei'], 1, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('studio') }}</span>
            </div>

            <div class="col-md-4  text-md-right">
                <label for="acctype">Account type</label>
            </div>
            <div class="col-md-6 ">
                <input type="radio" name="acctype" onchange="disablecompany();"
                       value="pf" checked> I want to work as individual<br>
                {{--<input type="radio" name="acctype" onchange="enablecompany();"
                       value="pj"> I want to work as a company<br>--}}

            </div>

            {{Form::label('iban', '(IBAN EURO) "All payments will be made in EURO"', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('iban', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('iban') }}</span>
            </div>

            {{Form::label('bank', 'Bank Name', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('bank', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('bank') }}</span>
            </div>

            {{Form::label('bank_address', 'Bank Address', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('bank_address', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('bank_address') }}</span>
            </div>

            {{Form::label('currency', 'Currency', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('currency', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('currency') }}</span>
            </div>

            {{Form::label('promo', 'I want to have my brand promoted', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::select('promo', [1 => 'Openly promoted', 2 => 'Restricted in my country'], 1, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('promo') }}</span>
            </div>

            {{Form::label('modelname1', 'Model nickname', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('modelname1', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('modelname1') }}</span>
            </div>

            {{Form::label('modelname2', 'Model nickname 2', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('modelname2', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('modelname2') }}</span>
            </div>

            {{Form::label('modelname3', 'Model nickname 3', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::text('modelname3', null, [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('modelname3') }}</span>
            </div>

            <label class="col-lg-12 text-center">Front ID Card Picture</label>

            <div class="offset-md-4 col-md-6">
                <img id="img1" src="{{ asset('img/models/id1.jpg') }}"/>

                @if ($errors->has('front1') || $errors->has('front2'))
                    <label id="register-name-error" class="error" for="front1" autofocus>This photo is required.</label>
                @endif

                <div class="container-btns">
                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Browse <input type="file" style="display: none;" name="front1" class="image1" accept="image/*"/>
                    </label>

                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Take Picture <input type="file" style="display: none;" name="front2" class="image1" accept="image/*" capture="camera"/>
                    </label>
                </div>
            </div>

            <label class="col-lg-12 text-center">Back ID Card Picture(optional)</label>

            <div class="offset-md-4 col-md-6">
                <img id="img2" src="{{ asset('img/models/id2.jpg') }}"/>

                <div class="container-btns">
                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Browse <input type="file" style="display: none;" name="back1" class="image2"
                                      accept="image/*"/>
                    </label>

                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Take Picture <input type="file" style="display: none;" name="back2" class="image2"
                                            accept="image/*" capture="camera"/>
                    </label>
                </div>

            </div>


            <label class="col-lg-12 text-center">Face+ID Card Picture</label>

            <div class="offset-md-4 col-md-6">
                <img id="img3" src="{{ asset('img/models/id3.jpg') }}"/>

                {{--@if ($pic3err === 1)
                    <label id="register-name-error" class="error" for="register-name">This photo
                        is required.</label>
                @endif--}}

                @if ($errors->has('faceId1') || $errors->has('faceId2'))
                    <label id="register-name-error" class="error" for="faceId1" autofocus>This photo is required.</label>
                @endif

                <div class="container-btns">
                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Browse <input type="file" style="display: none;" name="faceId1" class="image3"
                                      accept="image/*"/>
                    </label>


                    <label type="button" class="btn btn-primary btn-file btn-lg">
                        Take Picture <input type="file" style="display: none;" name="faceId2" class="image3"
                                            accept="image/*" capture="camera"/>
                    </label>
                </div>

            </div>


        </div>

        <div class="form-group row mb-0">
            <div class="col-md-4 offset-md-4">
                <button type="submit" class="btn btn-primary btn-block" id="btnRegister">
                    {{ __('Register') }}
                </button>
            </div>
        </div>


        {{ Form::close() }}




        <div id="btnLogin" class="text-center">Already have an account?
            <a href="login" class="signup">Login</a>
        </div>
    </div>


</div>

</body>
</html>