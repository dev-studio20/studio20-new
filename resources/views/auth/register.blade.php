<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ __('Studio 20') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png">

    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <link href="{{ asset('css/models/register.css') }}" rel="stylesheet"/>

    <script src="{{ asset('material/js/tether.min.js') }}"></script>
    <script src="{{ asset('material/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('material/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('material/js/front.js') }}"></script>
    <script src="{{ asset('material/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('material/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('material/js/jquery.animateNumber.min.js') }}"></script>


</head>
<body>

<div class="page login-page">
    <div class="container-fluid">

@php $err = ($errors->has('faceId2')) ? '#faceid' : ''; @endphp
@php $err = ($errors->has('front2')) ? '#front' : ''; @endphp

        <form id="register-form" action="{{route('register', "$err")}}" method="post" class="col-lg-8 offset-lg-2 col-sm-12"
              enctype="multipart/form-data">
            <div id="header-form">
                <div class="logo">
                    <h1>Model Registration</h1>
                </div>
            </div>
            @csrf

        <div class="form-group row">
            <label for="name"
                   class="col-md-4 text-md-right">{{ __('First Name') }}</label>

            <div class="col-md-6">
                <input id="name" type="text"
                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                       name="name" value="{{ old('name') }}"
                       required @php echo $errors->has('name') ? 'autofocus' : '' @endphp>

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="lname"
                   class="col-md-4  text-md-right">{{ __('Last Name') }}</label>

            <div class="col-md-6">
                <input id="lname" type="text"
                       class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}"
                       name="lname" value="{{ old('lname') }}"
                       required @php echo $errors->has('lname') ? 'autofocus' : '' @endphp>

                @if ($errors->has('lname'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="email"
                   class="col-md-4  text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email"
                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
        </div>


        <div class="form-group row">
            <label for="password" class="col-md-4  text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6"> <input id="password" type="password"
                                          class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}"
                                          name="password" required @php echo $errors->has('password') ? 'autofocus' : '' @endphp>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm"
                   class="col-md-4  text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" value="{{(old('password_confirmation'))}}" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group row">
            <div  class="col-md-4  text-md-right"><span style="color:red">*</span></div>
            <div class="col-md-6"><i>
                    Password must be at least 8 characters long and contain at least one Uppercase (A-Z), Lowercase (a-z), Digits (0-9), Special (~!@#$%^&*()_+-=)
                </i></div>
        </div>

        <div class="form-group row">

            {{Form::label('birthdate', 'Birth Date', ["class" => "col-md-4 text-md-right"])}}
            <div class="col-md-6">
                {{ Form::date('birthdate', \Carbon\Carbon::now()->subYears(25), [ 'class'=> 'form-control', 'required' => 'required' ]) }}
                <span class="error text-danger">{{ $errors->first('birthdate') }}</span>
            </div>

        </div>

        <div class="form-group row">

            <label for="sex" class="col-md-4  text-md-right">{{ __('Sex') }}</label>

            <div class="col-md-6">

                <select name="sex" id="inlineCheckbox4" class="col" required>
                    <option value="">Select</option>
                    <option value="M" {{ old('sex') == 'M' ? 'selected' : '' }}>
                        Male
                    </option>
                    <option value="F" {{ old('sex') == 'F' ? 'selected' : '' }}>
                        Female
                    </option>
                    <option value="C" {{ old('sex') == 'C' ? 'selected' : '' }}>
                        Couple
                    </option>
                </select>

            </div>
        </div>


        <div id="extrafield" style="display:none;">

            <div class="form-group row">
                <label for="name2" class="col-md-4 text-md-right">{{ __('First Name#2') }}</label>

                <div class="col-md-6">
                    <input id="name2" type="text"
                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name2" value="{{ old('name2') }}"
                           required @php echo $errors->has('name2') ? 'autofocus' : '' @endphp>

                    @if ($errors->has('name2'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name2') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="lname2"
                       class="col-md-4  text-md-right">{{ __('Last Name#2') }}</label>

                <div class="col-md-6">
                    <input id="lname2" type="text"
                           class="form-control{{ $errors->has('lname2') ? ' is-invalid' : '' }}"
                           name="lname2" value="{{ old('lname2') }}"
                           required @php echo $errors->has('lname2') ? 'autofocus' : '' @endphp>

                    @if ($errors->has('lname2'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname2') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="birthM2"
                       class="col-md-4  text-md-right">{{ __('Birth Date#2') }}</label>

                <div class="col-md-6">
                    <label class="checkbox-inline">
                        <select name="birthM2" id="inlineCheckbox12" class="col" required>
                            <option value="">--</option>
                            @php
                                for ($i = 1; $i <= 12; $i++) {
                                if ($i < 10) {
                                $x = "0$i";
                                } else {
                                $x = $i;
                                }
                                echo "<option>$x</option>";
                                }
                            @endphp
                        </select>

                    </label>

                    <label class="checkbox-inline">
                        <select name="birthD2" id="inlineCheckbox22" class="col" required>
                            <option value="">--</option>
                            @php
                                for ($x = 1; $x <= 31; $x++) {
                                if ($x < 10) {
                                $i = "0$x";
                                } else {
                                $i = $x;
                                }
                                echo "<option>$i</option>";
                                }
                            @endphp
                        </select>
                    </label>

                    <label class="checkbox-inline">
                        <select name="birthY2" id="inlineCheckbox32" class="col" required>
                            <option value="">----</option>
                            @php
                                for ($z = 2000; $z >= 1960; $z--) {
                                echo "<option>$z</option>";
                                }
                            @endphp
                        </select>
                    </label>
                    @if ($birthdateerr === 1)
                        <label style="color: #d9534f;
                                                 font-size: 0.75em;
                                                 position: absolute;
                                                 top: auto;
                                                 bottom: -30px;
                                                 left: 0;" for="birthM2">This field is required.</label>
                        You must be over 18+ years to register
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="sex1" class="col-md-4  text-md-right">{{ __('Sex#1') }}</label>
                <div class="col-md-6">
                    <select name="sex1" id="inlineCheckbox41" class="col" required>
                        <option value="{{ old('sex1') }}">@php echo (old('sex1')) ? old('sex1') : 'Select' @endphp</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="register-idn1" class="col-md-4  text-md-right">Personal Identification Number#1</label>

                <div class="col-md-6">
                    <input id="register-idn1" type="text" name="registeridn1" class="form-control"
                           placeholder="Personal Identification Number, SSN or CNP" required>
                </div>


                @if ($idnerr === 1)
                    <label id="register-name-error1" class="error" for="register-idn1">This field is
                        required.</label>
                @endif
            </div>

            <div class="form-group row">
                <label for="sex2" class="col-md-4  text-md-right">{{ __('Sex#2') }}</label>
                <div class="col-md-6">
                    <select name="sex2" id="inlineCheckbox42" class="col" required>
                        <option value="{{ old('sex2') }}">@php echo (old('sex2')) ? old('sex2') : 'Select' @endphp</option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="register-idn2" class="col-md-4  text-md-right">Personal Identification Number#2</label>

                <div class="col-md-6">
                    <input id="register-idn2" type="text" name="registeridn2" class="form-control"
                           placeholder="Personal Identification Number, SSN or CNP" required value="{{ old('registeridn2') }}">
                </div>


                @if ($idnerr === 1)
                    <label id="register-name-error2" class="error" for="register-idn2">This field is
                        required.</label>
                @endif
            </div>


        </div>


        <div class="form-group row">
            <label for="registerCountry"
                   class="col-md-4  text-md-right">{{ __('Country') }}</label>

            <div class="col-md-6">

                <select name="registerCountry" id="country" class="col" required>
                    <option value="{{ old('registerCountry') }}">@if (old('registerCountry') === null) {{'Select country'}} @endif</option>

                    @forelse ($countries as $country)
                        <li>
                            <option value="{{ $country->id }}" <?php if(old('registerCountry') == $country->id) echo 'selected';?> required>{{ $country->name }}</option>
                        </li>
                    @empty
                        <p>
                            <option value="">Country not available</option>
                        </p>
                    @endforelse

                </select>


            </div>

        </div>

        <div class="form-group row">
            <label for="registerstate"
                   class="col-md-4  text-md-right">{{ __('State') }}</label>

            <div class="col-md-6">
                <select name="registerstate" id="state" class="col" required>
                    @php echo (old('hiddenState') !== null) ? old('hiddenState') : 'Select country first.' @endphp
                </select>
                <input type="hidden" id="hiddenState" name="hiddenState" value="{{ old('hiddenState') }}" {{ old('hiddenState') }}>


            </div>

        </div>


        <div class="form-group row">
            <label for="registerCity"
                   class="col-md-4  text-md-right">{{ __('City') }}</label>

            <div class="col-md-6">
                <select name="registerCity" id="city" class="col" required>
                    @php echo (old('hiddenCity') !== null) ? old('hiddenCity') : 'Select state first.' @endphp

                </select>
            </div>

            <input type="hidden" id="hiddenCity" name="hiddenCity" value="{{ old('hiddenCity') }}" {{ old('hiddenCity') }}>

        </div>


        <div class="form-group row">
            <label for="register-address"
                   class="col-md-4  text-md-right">Address </label>


            <div class="col-md-6">
                <input id="register-address" type="text" name="registerAddress" class="form-control" value="{{ old('registerAddress') }}" required>
            </div>

            @if ($addresserr === 1)
                <label id="register-address-error" class="error" for="register-address">This field
                    is
                    required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-phone" class="col-md-4  text-md-right">Mobile
                Phone </label>

            <div class="col-md-2">
                <input id="register-phone-prefix" type="text" name="registerphoneprefix" class="form-control text-center"
                       value="{{ old('registerphoneprefix') }}" readonly>

                <input id="reg-ph-pref" type="hidden" name="realregisterphoneprefix" class="form-control text-center"
                       value="{{ old('realregisterphoneprefix') }}" readonly>
            </div>

            <div class="col-md-4">
                <input id="register-phone" type="number" name="registerphone" class="form-control"
                       value="{{ old('registerphone') }}" required>
            </div>


            @if ($phoneerr === 1)
                <label id="register-phone-error" class="error" for="register-phone">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group row" id="pin">
            <label for="register-idn" class="col-md-4  text-md-right">Personal Identification Number</label>

            <div class="col-md-6">
                <input id="register-idn" type="text" name="registeridn" class="form-control"
                       placeholder="Personal Identification Number, SSN or CNP" value="{{ old('registeridn') }}" required>
            </div>


            @if ($idnerr === 1)
                <label id="register-name-error" class="error" for="register-idn">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group checkbox row">
            <div class="col-md-4  text-md-right">
                <label for="broadcastingspace radio-inline">I want the Manager to provide me with broadcasting
                    and recording studios</label>
            </div>
            <div class="col-md-6">
                <input type="radio" name="broadcastingspace" onchange="enable();" value="yes" checked> Yes<br>
                <input type="radio" name="broadcastingspace" onchange="disable();" value="no"> No, I'll broadcast
                from home<br>
            </div>
        </div>

        <div class="form-group row">
            <label for="studio"
                   class="col-md-4  text-md-right">{{ __('Studio') }}</label>

            <div class="col-md-6">

                <select name="studio" id="studio" class="col" required>
                    <option value="">Select</option>

                    @foreach ($studios as $studio)
                        <option value="{{ $studio->id }}" {{ old('studio') == $studio->id ? 'selected' : '' }}>
                            {{ $studio->name }}
                        </option>
                    @endforeach

                </select>

            </div>
        </div>

        <div class="form-group checkbox row">
            <div class="col-md-4  text-md-right">
                <label for="acctype">Account type</label>
            </div>
            <div class="col-md-6 ">
                <input type="radio" name="acctype" onchange="disablecompany();"
                       value="pf" checked> I want to work as individual<br>

            </div>
        </div>

        <div class="form-group row" id="x" style="display: none;">
            <label for="register-companyname" class="col-md-4  text-md-right">Company Name</label>

            <div class="col-md-6">
                <input id="register-companyname" type="text" name="registercompanyname" class="form-control">
            </div>

            @if ($companynameerr === 1)
                <label id="register-companyname-error" class="error" for="register-name">This
                    field is required.</label>
            @endif
        </div>

        <div class="form-group row" id="xy" style="display: none;">
            <label for="register-companyadmin" class="col-md-4  text-md-right">Company Director</label>

            <div class="col-md-6">
                <input id="register-companyadmin" type="text" name="registercompanyadmin" class="form-control">
            </div>
            @if ($companyadminerr === 1)
                <label id="register-companyadmin-error" class="error" for="register-name">This
                    field is required.</label>
            @endif
        </div>

        <div class="form-group row" id="xz" style="display: none;">
            <label for="register-companyaddress" class="col-md-4  text-md-right">Company Address</label>

            <div class="col-md-6">
                <input id="register-companyaddress" type="text" name="registercompanyaddress" class="form-control">
            </div>
            @if ($companyaddresserr === 1)
                <label id="register-companyaddress-error" class="error" for="register-name">This
                    field is required.</label>
            @endif
        </div>

        <div class="form-group row" id="xx" style="display: none;">
            <label for="register-companyvatcode" class="col-md-4  text-md-right">VAT Code</label>

            <div class="col-md-6">
                <input id="register-companyvatcode" type="text" name="registercompanyvatcode" class="form-control"
                       placeholder="The Value Added Tax code.For Romania is ROxxxxx">
            </div>

            @if ($companyvatcodeerr === 1)
                <label id="register-companyvatcode-error" class="error" for="register-name">This
                    field is required.</label>
            @endif
        </div>

        <div class="form-group row" id="xq" style="display: none;">
            <label for="register-companyregcode" class="col-md-4  text-md-right">Registration Code</label>

            <div class="col-md-6">
                <input id="register-companyregcode" type="text" name="registercompanyregcode" class="form-control"
                       placeholder="The Registration Code.For Romania is Jxx-xxx">
            </div>

            @if ($companyregcodeerr === 1)
                <label id="register-companyvatcode-error" class="error" for="register-name">This
                    field is required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-Iban" class="col-md-4  text-md-right"> (IBAN EURO) <br>"All payments will be made in EURO"</label>

            <div class="col-md-6">
                <input id="register-Iban" type="text" name="registerIban" class="form-control" value="{{ old('registerIban') }}" required>
            </div>

            @if ($ibanerr === 1)
                <label id="register-name-error" class="error" for="register-name">This field is required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-Bank" class="col-md-4  text-md-right">Bank
                Name</label>

            <div class="col-md-6">
                <input id="register-Bank" type="text" name="registerBank" class="form-control" value="{{ old('registerBank') }}" >
            </div>

            @if ($bankerr === 1)
                <label id="register-Bank-error" class="error" for="register-Bank">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-BankAddress" class="col-md-4  text-md-right">Bank
                Address</label>

            <div class="col-md-6">
                <input id="register-BankAddress" type="text" name="registerBankAddress" class="form-control" value="{{ old('registerBankAddress') }}">
            </div>

            @if ($bankaddresserr === 1)
                <label id="register-name-error" class="error" for="register-name">This field is
                    required.</label>
            @endif
        </div>


        <div class="form-group row">
            <label for="inlineCheckbox5"
                   class="col-md-4  text-md-right">{{ __('Currency') }}</label>

            <div class="col-md-6">

                <input name="currency" type="text" class="form-control" readonly id="inlineCheckbox5" required value="EUR">

            </div>
        </div>

        <div class="form-group row">

            <label for="brand" class="col-md-4  text-md-right">I want to have my brand promoted</label>

            <div class="col-md-6">

                <select name="brand" id="6" class="col" required>
                    <option value="Yes">Openly promoted</option>
                    <option value="No">Restricted in my country</option>
                </select>

            </div>
        </div>

        <div class="form-group row">
            <label for="register-modelname1" class="col-md-4  text-md-right">Model nickname </label>

            <div class="col-md-6">
                <input id="register-modelname1" type="text" name="registerModelName1" class="form-control" value="{{ old('registerModelName1') }}"
                       placeholder="Model nickname first choice.">
            </div>

            @if ($modelname1err === 1)
                <label id="register-name-error" class="error" for="register-name">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-modelname2" class="col-md-4  text-md-right">Model nickname 2 </label>

            <div class="col-md-6">
                <input id="register-modelname2" type="text" name="registerModelName2" class="form-control"  value="{{ old('registerModelName2') }}"
                       placeholder="Second choice if first one is not available">
            </div>

            @if ($modelname2err === 1)
                <label id="register-name-error" class="error" for="register-name">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group row">
            <label for="register-modelname3" class="col-md-4  text-md-right">Model nickname 3 </label>

            <div class="col-md-6">
                <input id="register-modelname3" type="text" name="registerModelName3" class="form-control" value="{{ old('registerModelName3') }}"
                       placeholder="Third choice, if first and second are not available">
            </div>
            @if ($modelname3err === 1)
                <label id="register-name-error" class="error" for="register-name">This field is
                    required.</label>
            @endif
        </div>

        <div class="form-group text-center">
            <label class="text-center">Front ID Card Picture</label>
            <label id="front"></label>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-centered">
                    <img id="img1" src="{{ asset('img/models/id1.jpg') }}"/>

                    @if ($errors->has('front1') || $errors->has('front2'))
                        <label id="register-name-error" class="error" for="front1">This photo is required.</label>
                    @endif


                    <div class="container-btns">
                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Browse <input type="file" style="display: none;" name="front1" class="image1" accept="image/*"/>
                        </label>

                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Take Picture <input type="file" style="display: none;" name="front2" class="image1" accept="image/*" capture="camera"/>
                        </label>

                    </div>


                </div>
            </div>
        </div>




        <div class="form-group text-center">
            <label class="text-center">Back ID Card Picture(optional)</label>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-centered">
                    <img id="img2" src="{{ asset('img/models/id2.jpg') }}"/>

                    <div class="container-btns">
                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Browse <input type="file" style="display: none;" name="back1" class="image2"
                                          accept="image/*"/>
                        </label>

                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Take Picture <input type="file" style="display: none;" name="back2" class="image2"
                                                accept="image/*" capture="camera"/>
                        </label>
                    </div>

                </div>
            </div>


        </div>

        <div class="form-group text-center">
            <label class="text-center">Face+ID Card Picture</label>
            <label id="faceid"></label>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-centered">
                    <img id="img3" src="{{ asset('img/models/id3.jpg') }}"/>

                    @if ($errors->has('faceId1') || $errors->has('faceId2'))
                        <label id="register-name-error" class="error" for="faceId1">This photo is required.</label>
                    @endif

                    <div class="container-btns">
                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Browse <input type="file" style="display: none;" name="faceId1" class="image3"
                                          accept="image/*"/>
                        </label>


                        <label type="button" class="btn btn-primary btn-file btn-lg">
                            Take Picture <input type="file" style="display: none;" name="faceId2" class="image3"
                                                accept="image/*" capture="camera"/>
                        </label>
                    </div>

                </div>
            </div>


        </div>
        <input type="hidden" id="widthheight" name="widthheight" value="">

        <div class="form-group row mb-0">
            <div class="col-md-4 offset-md-4">
                <button type="submit" class="btn btn-primary btn-block" id="btnRegister">
                    {{ __('Register') }}
                </button>
            </div>
        </div>


        </form>


        <div id="btnLogin" class="text-center">Already have an account?
            <a href="login" class="signup">Login</a>
        </div>
    </div>


</div>

</body>
</html>

<script type="text/javascript">
    function disable() {
        document.getElementById("studio").setAttribute('disabled', true);
    }

    function enable() {
        document.getElementById("studio").removeAttribute('disabled');
    }

    function disablecompany() {
        $("#x").css({"display": "none"});
        $("#xq").css({"display": "none"});
        $("#xy").css({"display": "none"});
        $("#xz").css({"display": "none"});
        $("#xx").css({"display": "none"});
    }

    function enablecompany() {
        $("#x").css({"display": "yes"});
        $("#xq").css({"display": "yes"});
        $("#xy").css({"display": "yes"});
        $("#xz").css({"display": "yes"});
        $("#xx").css({"display": "yes"});
    }

    jQuery(document).ready(function ($) {

        let width = window.screen.availWidth;
        let height = window.screen.availHeight;
        $('#widthheight').val(width+'x'+height);

        let stateHtml = '';
        let cityHtml = '';

        $('.image1').change(function(){
            let input = this;
            let url = $(this).val();
            let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
            {
                let reader = new FileReader();

                reader.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }

        });

        $('.image2').change(function(){
            let input = this;
            let url = $(this).val();
            let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
            {
                let reader = new FileReader();

                reader.onload = function (e) {
                    $('#img2').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }

        });

        $('.image3').change(function(){
            let input = this;
            let url = $(this).val();
            let ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg"))
            {
                let reader = new FileReader();

                reader.onload = function (e) {
                    $('#img3').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }

        });


        let phone = '';
        let countrycode = '';

        $('form select[name=sex]').change(function () {
            if ($('#inlineCheckbox4 option:selected').val() == 'C') {
                $('#extrafield').show();
                $('#pin').hide();
            } else {
                $('#extrafield').hide();
                $('#pin').show();
            }
        });

        $('#country').on('change', function (e) {
            let countryID = $(this).val();
            if (countryID) {

                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "ajaxData",
                    method: 'post',
                    data: 'country_id=' + countryID,
                    success: function (html) {
                        html = JSON.parse(html);
                        countrycode = '(+' + html[1] + ')';
                        $('#state').html(html[0]);
                        $('#city').html('<option value="">Select state first</option>');
                        $("#register-phone-prefix").val(countrycode);
                        $("#reg-ph-pref").val(html[1]);
                    }
                });


            } else {
                $('#state').html('<option value="">Select country first</option>');
                $('#city').html('<option value="">Select state first</option>');
            }

        });

        $('#state').on('change', function (e) {
            let stateID = $(this).val();
            let optionText = $("#state option:selected").text();

            stateHtml = '<option value="'+stateID+'">'+ optionText +'</option>';

            $('#hiddenState').val(stateHtml);

            if (stateID) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                jQuery.ajax({
                    url: "ajaxData",
                    method: 'post',
                    data: 'state_id=' + stateID,
                    success: function (html) {
                        $('#city').html(html);
                    }
                });

            } else {
                $('#city').html('<option value="">Select state first</option>');
            }
        });

        $('#city').on('change', function (){
            let cityID = $(this).val();
            let optionText = $("#city option:selected").text();

            stateHtml = '<option value="'+cityID+'">'+ optionText +'</option>';

            $('#hiddenCity').val(stateHtml);

        });



    });
</script>

