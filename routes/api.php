<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('sales', 'Api\TestController@salesget');

Route::post('login', 'Api\UserController@login');
//Route::post('register', 'Api\UserController@register');
Route::group(['middleware' => 'auth:api'], function(){

    Route::any('status', 'Api\StatusController@index');
    Route::any('modelstatus', 'Api\StatusController@modelstatus');



//    extern API routes
    //all models
    Route::get('allmodels', 'Api\ApiController@allModels');

    //models approved
    Route::get('modelsapproved', 'Api\ApiController@modelsApproved');

    //stories
    Route::get('stories', 'Api\ApiController@stories');


});