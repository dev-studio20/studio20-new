<?php


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "admin" middleware group. Enjoy building your Admin!
|
*/
//Route::view('/', 'Admin.index');
//
//Auth::routes();
//
//Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
//Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
//
//Route::post('/login/admin', 'Auth\LoginController@adminLogin');
//Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
//
//Route::view('/admin', 'admin');
