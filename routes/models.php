<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Models Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Models routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Models" middleware group. Enjoy building your Models!
|
*/

Route::get('/', function () {
    return ['Models'];
});
