<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models;
use Carbon\Carbon;

URL::forceScheme('https');

//route for aoutologin link
Route::get('/autologin/{model}', function (Models $model) {
    Auth::guard('model')->login($model);
    $model->email_verified_at = Carbon::now();
    $model->save();
    return redirect()->route('models.approval');
})->name('autologin');

//route for phoneaoutologin link
Route::get('/autologinphone/{model}', function (Models $model) {
    Auth::guard('model')->login($model);

    $model->phone_verified_at = Carbon::now();
    $model->save();

    return redirect()->route('models.approval');
})->name('autologinphone');

    Route::view('/', 'Models.index');

// Authentication Routes...
//models
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::any('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
    Route::get('/register', 'Auth\ModelRegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Auth\ModelRegisterController@create')->name('register_post');

// Password Reset Routes...
    Route::post('/password/email','Auth\ModelForgotPasswordController@sendResetLinkEmail')->name('model.password.email');
    Route::post('/password/reset', 'Auth\ModelResetPasswordController@reset');
    Route::get('/password/reset', 'Auth\ModelForgotPasswordController@showLinkRequestForm')->name('model.password.request');
    Route::get('/password/reset/{token}', 'Auth\ModelResetPasswordController@showResetForm')->name('model.password.reset');


    Route::any('/ajaxData', 'AjaxController@locationSelect');
    Route::any('/resendphonelink', 'AjaxController@resendPhoneLink');
    Route::any('/resendemaillink', 'AjaxController@resendEmailLink');
    Route::any('/changePhone', 'AjaxController@changePhone');
    Route::any('/changeEmail', 'AjaxController@changeEmail');

    Route::prefix('admin')->group(function () {

        //admin login
        Route::get('/login', 'Auth\AdminLoginController@showAdminLoginForm')->name('admin.login');
        Route::post('/login', 'Auth\AdminLoginController@adminLogin');

        //admin register
        Route::get('/register', 'Auth\RegisterController@showAdminRegisterForm');
        Route::post('/register', 'Auth\RegisterController@createAdmin');

        //admin password reset routes
        Route::post('/password/email','Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::post('/password/reset','Auth\AdminResetPasswordController@reset');
        Route::get('/password/reset','Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::get('/password/{token}','Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');


        Route::group(['middleware' => ['admin']], function() {

            //admin home page
            Route::any('/', 'Admin\HomeController@index')->name('admin.home');
            Route::post('/stats', 'Admin\HomeController@stats');
            Route::any('/updatestudio', 'Admin\HomeController@updatestudio')->name('admin.updatestudio');

            //models page
            Route::resource('/models','ModelsController');

            Route::any('/allmodels','ModelsController@all_models');
            Route::any('/approvedmodels','ModelsController@approved_models');
            Route::any('/pendingmodels','ModelsController@pending_models');
            Route::any('/suspendedmodels','ModelsController@suspended_models');
            Route::any('/rejectedmodels','ModelsController@rejected_models');
            Route::any('/inactivemodels','ModelsController@inactive_models');

            //model profile -> TO DO should check if user has access to model
            Route::get('/model/{id}','ModelsController@profile');
            Route::post('/model/{id}','ModelsController@edit_profile');
            Route::post('/modelnickname','ModelsController@set_nickname');
            Route::post('/ignoreres','ModelsController@ignore_reservations');

            //Payments
            Route::get('/insertsales', 'Admin\PaymentsController@index')->name('admin.insertsales');
            Route::post('/addsales', 'Admin\SalesController@addsales')->name('admin.addsales');
            Route::any('/makepayment', 'Admin\PaymentsController@makepayment')->name('admin.makepayment');
            Route::any('/paymenthistory', 'Admin\PaymentsController@paymenthistory')->name('admin.paymenthistory');
            Route::any('/makepaymentid', 'Admin\PaymentsController@makepaymentid')->name('admin.makepaymentid');
            Route::get('/viewsales/{modelid}/{year}/{period}', 'Admin\SalesController@viewsales')->name('admin.viewsales');
            Route::get('/viewsale/{id}', 'Admin\SalesController@viewsale');
            Route::post('/viewsale/update', 'Admin\SalesController@updatesale');
            Route::delete('/viewsale/{id}', 'Admin\SalesController@deletesale');

            Route::any('/closeperiod', 'Admin\PaymentsController@closeperiods')->name('admin.closeperiods');
            Route::get('/closeperiodid/{periodid}', 'Admin\SalesController@closeperiod');
            Route::get('/closeperiods/{periodid}', 'Admin\PaymentsController@closeperiod');
            Route::post('/closeperiodid/{periodid}', 'Admin\PaymentsController@closeperiodid');
            Route::post('/openperiod', 'Admin\PaymentsController@openperiod');
            Route::post('/checkSales', 'Admin\PaymentsController@checkPeriodSales');
            Route::post('/updateSalesPeriod', 'Admin\PaymentsController@updateSalesPeriod');

            //Sales from Api
            Route::post('/getsalesmodel', 'JasminApiController@getSalesDay');
            Route::get('/getsalesperiod/{modelname}/{year}/{period}', 'JasminApiController@getSalesPeriod');

            //Messages
            Route::get('/inbox', 'Admin\MessageController@index')->name('admin.inbox');
            Route::get('/message/{id}', 'Admin\MessageController@show');
            Route::post('/message/{id}', 'Admin\MessageController@insert');
            Route::any('/composemessage', 'Admin\MessageController@composemessage')->name('admin.composemessage');
            Route::post('/onebyone', 'Admin\MessageController@onebyone')->name('admin.onebyone');
            Route::post('/sendtoone', 'Admin\MessageController@sendtoone')->name('admin.sendtoone');
            Route::get('/messagessent', 'Admin\MessageController@messagessent')->name('admin.messagessent');
            Route::get('/solved', 'Admin\MessageController@solved')->name('admin.solved');
            Route::post('/autotype', 'Admin\MessageController@autotype');

            //Reservations
            Route::any('/reservations', 'Admin\ReservationsController@index')->name('admin.reservations');
            Route::any('/reservations/{id}', 'Admin\ReservationsController@show');

            //studio edit page (admins only, etc)
            Route::resource('/studios','Admin\StudioController');

            //roles page
            //Route::resource('/roles','Admin\RolesController');

            //twitter page
            Route::get('/twitter', 'Admin\TwitterController@index')->name('admin.twitter');

            //users
            Route::any('/users', 'Admin\EditAdminsController@index')->name('admin.users');
            Route::get('/users/roles', 'Admin\EditAdminsController@roles');
            Route::get('/users/studios', 'Admin\EditAdminsController@studios');

            Route::get('/users/{id}', 'Admin\EditAdminsController@viewUser');
            Route::post('/usersid', 'Admin\EditAdminsController@editUser');
            Route::delete('/users/{id}', 'Admin\EditAdminsController@deleteUser');

            //notifications
            Route::post('/notifications', 'Admin\NotificationController@index')->name('admin.notifications');
            Route::post('/notificationsreadtime', 'Admin\NotificationController@addReadTime');

            //log
            Route::get('/log', 'Admin\LogController@index')->name('admin.log');

            //news
            Route::get('/news', 'Admin\NewsController@index')->name('admin.news');

            //background jobs
            Route::get('/backgroundservice', 'Admin\BackgroundController@index')->name('admin.backgroundservice');
            Route::get('/getsales', 'Admin\BackgroundController@getsales')->name('admin.getsales');
            Route::get('/getsalesallmodels', 'Admin\BackgroundController@getsalesallmodels')->name('admin.getsalesallmodels');
            Route::post('/getsalesallperiod', 'Admin\BackgroundController@getsalesallperiod')->name('admin.getsalesallperiod');

            //Generate Contract
            Route::post('/generate_contract', 'Admin\GenerateContractController@store')->name('admin.generate_contract');
            Route::get('/contracts/contract_{slug}.pdf', [
                'as' => 'contracts.show',
                'uses' => 'Admin\GenerateContractController@showContract',
            ]);


            //settings
            Route::get('/settings', 'Admin\SettingsController@index')->name('admin.settings');

            //notifications
            Route::post('/testnotification', 'NotificationsController@test')->name('admin.notifications');

            //admin logout
            Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

            Route::resource('user', 'UserController', ['except' => ['show']]);

            Route::get('profile', ['as' => 'profile.edit', 'uses' => 'Admin\ProfileController@edit']);
            Route::put('profile', ['as' => 'profile.update', 'uses' => 'Admin\ProfileController@update']);
            Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'Admin\ProfileController@password']);

            //models statistics
            Route::any('/modelstats', 'Admin\ModelStatsController@index');
            Route::any('/tempavg', 'Admin\ModelStatsController@updateshiftsales');

            //schedule staff
            Route::any('/schedule', 'Admin\ScheduleController@show');
            Route::post('/schedulestore', 'Admin\ScheduleController@insert');

            //schedule models
            Route::any('/schedulem', 'Admin\ScheduleModelsController@show');
            Route::post('/sreservations', 'Admin\ScheduleModelsController@ajaxreservations');
            Route::any('/sreservations/{rid}', 'Admin\ScheduleModelsController@reservations');

            Route::get('/testhelper', 'Admin\TestmailController@testhelper');
            Route::get('/testmail', 'Admin\TestmailController@index');
            Route::get('/fixsales', 'Admin\SalesController@fixsales');
            Route::get('/apitest', 'JasminApiController@index');
            Route::get('/fixsale', 'Admin\SalesController@fixSalePeriod');
            Route::get('/testdays', 'Admin\HomeController@getLastYearSales');


        });



    });



//models
    Route::prefix('models')->group(function () {

        //model approved, suspended, etc..
        Route::get('/approval', 'Models\HomeController@approval')->name('models.approval');

        Route::post('/uploadNewPicture', 'Auth\ModelRegisterController@uploadNewPicture')->name('uploadNewPicture');

        //sign contract
        Route::any('/signcontract', 'Auth\ModelRegisterController@signcontract')->name('signcontract');

        Route::group(['middleware' => ['model']], function() {

            //models navigation (admin only should be)
            Route::any('/changemodel', 'Auth\ModelRegisterController@changemodel');

            Route::any('/finshRegister', 'Auth\ModelRegisterController@finish')->name('finish');

            Route::get('/resendVerificationLink', 'Auth\ModelRegisterController@resendVerificationLink')->name('resendVerificationLink');
            Route::get('/resendPhoneVerificationLink', 'Auth\ModelRegisterController@resendPhoneVerificationLink')->name('resendPhoneVerificationLink');

            Route::get('/', 'Models\HomeController@index')->name('models.home');

            Route::get('/profile', 'Models\HomeController@profile')->name('models.profile');

            Route::get('/messages', 'Models\MessageController@messages')->name('models.messages');
            //Route::get('/messages_test', 'Models\MessageController@messages_test')->name('models.messages_test');
            Route::get('/messagescreate', 'Models\MessageController@createmsg');
            Route::post('/messagescreate', 'Models\MessageController@createmsgpost');
            Route::get('/messages/{id}', 'Models\MessageController@show');
            Route::get('/messagesmass/{id}', 'Models\MessageController@showmass');
            Route::post('/messages/{id}', 'Models\MessageController@insert');

            Route::get('/sales', 'Models\HomeController@sales')->name('models.sales');

            //Route::get('/payment_old', 'Models\HomeController@payment')->name('models.payment_old');
            Route::get('/payment', 'Models\HomeController@payment2')->name('models.payment');
            Route::get('/paymentdetails/{id}', 'Models\HomeController@paymentdetails');
            Route::post('/requestpayment', 'Models\HomeController@requestpayment');

            Route::get('/reservations', 'Models\HomeController@reservations')->name('models.reservations');
            Route::post('/reservations1', 'Models\HomeController@reservations1')->name('models.reservationspost');
            Route::post('/reservations2', 'Models\HomeController@reservations2')->name('models.reservationspostnext');

            Route::get('/news', 'Models\HomeController@news')->name('models.news');

            Route::get('/contract', 'Models\HomeController@contract')->name('models.contract');

            Route::get('/contracts/contract_{slug}.pdf', [
                'as' => 'contracts.show',
                'uses' => 'Admin\GenerateContractController@showContract',
            ]);

        });

    });


//Route::get('/showmodels', 'Api\TestController@showmodels');
//Route::get('/testapi', 'Api\TestController@testapidetails');