<?php


/*
|--------------------------------------------------------------------------
| App Routes
|--------------------------------------------------------------------------
|
| Here is where you can register App routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "app" middleware group. Enjoy building your App!
|
*/
Route::get('/', function () {
    return view('App.login');
});

// Authentication Routes...
//models
Route::get('/login', 'Auth\AppLoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\AppLoginController@login');
Route::any('/logout', 'Auth\AppLoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register', 'Auth\AppRegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\AppRegisterController@register');

// Password Reset Routes...
Route::get('/password/reset', 'Auth\AppForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\AppForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset/{token}', 'Auth\AppResetPasswordController@showResetForm');
Route::post('/password/reset', 'Auth\AppResetPasswordController@reset');

